from rest_framework import serializers
from Asset.models import Asset
from attachdetached.serializers import AttachDetachSerializer
class AssetSerializer(serializers.ModelSerializer):
    aid = serializers.IntegerField(required = False)
    class Meta:
        model  = Asset
        exclude = ('created_DT','updated_DT','is_active')

class ListAssetSerializer(serializers.ModelSerializer):
    att_detta = AttachDetachSerializer(many = True)
    class Meta:
        model  = Asset
        fields = ('id','asset_id','asset_type','registeration_no','is_attach','att_detta')


class CreateAssetSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Asset
        exclude = ('created_DT','updated_DT','is_active')

    def create(self, validated_data):
        carr_com_id = self.context.get("carr_com_id")
        asset_obj = Asset.objects.create(CompanyID_id = carr_com_id,**validated_data)
        return asset_obj


class OrderDetailAssetSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Asset
        fields = ('id','asset_id','asset_type','registeration_no')


class ListAssetManagerSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Asset
        exclude = ('created_DT','updated_DT','is_active')


class UpdateAssetManagerSerialz(serializers.ModelSerializer):
    class Meta:
        model  = Asset
        exclude = ('created_DT','updated_DT','is_active')

    def update(self, instance, validated_data):
        print('$$$$$$$$$$$$$$',validated_data.get("asset", instance.asset))
        instance.asset_id      = validated_data.get('asset_id', instance.asset_id)
        instance.is_attach     = validated_data.get('is_attach', instance.is_attach)
        instance.asset_type    = validated_data.get('asset_type', instance.asset_type)
        instance.make          = validated_data.get('make', instance.make)
        instance.asset         = validated_data.get('asset', instance.asset)
        instance.model         = validated_data.get('model', instance.model)
        instance.configuration = validated_data.get('configuration', instance.configuration)
        instance.vin_chassis   = validated_data.get('vin_chassis', instance.vin_chassis)
        instance.engine_no     = validated_data.get('engine_no', instance.engine_no)
        instance.build_date    = validated_data.get('build_date', instance.build_date)
        instance.e_tag_id      = validated_data.get('e_tag_id', instance.e_tag_id)
        instance.gps_id        = validated_data.get('gps_id', instance.gps_id)
        instance.dangerous_good_compliance   = validated_data.get('dangerous_good_compliance', instance.dangerous_good_compliance)
        instance.dangerous_good_license_no   = validated_data.get('dangerous_good_license_no', instance.dangerous_good_license_no)
        instance.registeration_no        = validated_data.get('registeration_no', instance.registeration_no)
        instance.registeration_due_date  = validated_data.get('registeration_due_date', instance.registeration_due_date)
        instance.state                   = validated_data.get('state', instance.state)
        instance.asset_status            = validated_data.get('asset_status', instance.asset_status)
        instance.odometer                = validated_data.get('odometer', instance.odometer)
        instance.maintenance_intervals   = validated_data.get('maintenance_intervals', instance.maintenance_intervals)
        instance.maintenance_type   = validated_data.get('maintenance_type', instance.maintenance_type)
        instance.asset_capacity     = validated_data.get('asset_capacity', instance.asset_capacity)
        instance.gross_weight       = validated_data.get('gross_weight', instance.gross_weight)
        instance.tare_weight        = validated_data.get('tare_weight', instance.tare_weight)
        instance.purchase_cost      = validated_data.get('purchase_cost', instance.purchase_cost)
        instance.ownership_type     = validated_data.get('ownership_type', instance.ownership_type)
        instance.financed_term      = validated_data.get('financed_term', instance.financed_term)
        instance.hourly_cost        = validated_data.get('hourly_cost', instance.hourly_cost)
        instance.save()
        return instance


class AssetDownloadSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Asset
        exclude = ('created_DT','updated_DT','is_active')