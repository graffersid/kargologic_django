from django.conf.urls import url

from Asset import views

urlpatterns = [
   url('listasset/',views.ListAsset.as_view()),
   url('assetmanagerlist/',views.ListAssetManager.as_view()),
   url('createasset/',views.AssetCreate.as_view()),
   url('uploadasset/',views.AssetUpload.as_view()),
   url('managerassetupdate/',views.UpdateAssetManager.as_view()),
   url('sampleasset/',views.AssetSample.as_view()),
   url('trucklist/',views.listAssetTruck.as_view()),
   url('download_asset/',views.AssetDownload.as_view()),
   url('delete_asset/',views.DeleteAsset.as_view()),
]
