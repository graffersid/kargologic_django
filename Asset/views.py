from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from Asset.models import Asset
from AssetRelation.models import AssetRelation
from AssetRelation.serializers import ListAssetRelation
from attachdetached.models import AttachDetached
from Asset.serializers import ListAssetSerializer, CreateAssetSerializer, ListAssetManagerSerializer,UpdateAssetManagerSerialz, AssetDownloadSerializer
import pandas as pd
from rest_framework.parsers import MultiPartParser,FileUploadParser
from django.db import transaction
from django.http import HttpResponse
import xlsxwriter
import io
from datetime import datetime as dt
import math
def time_in_range(start, end, x):
    """Return true if x is in the range [start, end]"""
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end



class ListAsset(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)    
        try:
            data = request.data
            print('data',data)
            ass_obj = Asset.objects.filter(CompanyID_id = int(content[0][0]), is_active = True, is_delete = False)
            serializ = ListAssetSerializer(ass_obj ,many = True)
            
            from_time = (data['time_from']).split('.')
            from_time = dt.strptime(from_time[0], "%Y-%m-%dT%H:%M:%S")       
            to_time   = (data['time_to']).split('.')
            to_time = dt.strptime(to_time[0], "%Y-%m-%dT%H:%M:%S")
            
            ser_data = serializ.data
            final_list = []

            for resp_data in ser_data:
                for ke,va in resp_data.items():
                    if ke == 'att_detta':
                        if va != []:
                            co = 0
                            for indata in va:
                                co = co+1
            
                                s_from = indata['schedule_from']
            
                        
                                s_from = dt.strptime(s_from, "%Y-%m-%dT%H:%M:%SZ")
            
                                s_to   = (indata['schedule_to'])
                                s_to   = dt.strptime(s_to, "%Y-%m-%dT%H:%M:%SZ")
            
                                if ((time_in_range(s_from, s_to, from_time) or (time_in_range(s_from, s_to, to_time) ) ) or ((time_in_range(from_time, to_time, s_from)) or (time_in_range(from_time, to_time, s_to)))):
                                    
                                    resp_data.update(attach ='busy')
                                    
                                    final_list.append(resp_data)
                                    break
                                elif len(va) == co:
                                    resp_data.update(attach ='free')
                                    
                                    final_list.append(resp_data)
            
                                else:
            
                                    continue
        
                        else:
                            final_list.append(resp_data) 

            context = {
                "data":final_list,
                "message":"success"
            }
            return Response(context, status = status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":str(e)
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)
class AssetCreate(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)    
        try:
            data = request.data
            if data['id'] == 0:
                seializer = CreateAssetSerializer(data = data, context = {"carr_com_id":int(content[0][0])})
                if seializer.is_valid():
                    seializer.save()
                context = {
                    "data":seializer.data['id'],
                    "message":"Asset created"
                }
                return Response(context,status=status.HTTP_200_OK)    
            else:
                obj_upd = Asset.objects.filter(id = int(data['id']), is_active = True, is_delete = False).update(**data)
                context = {
                    "data":"",
                    "message":"Asset updated"
                }
                return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('error',e)
            context = {
                            "data": '',
                            "message":'Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

def removenan(data):
    try:
        if math.isnan(float(data)):
            return ''
        else:
            return data
    except Exception as e:
        print('in excep',e)
        return data
class AssetUpload(APIView):
    parser_classes = [MultiPartParser,FileUploadParser]
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        # try: 
        request_file = request.FILES['data_file']

        df = pd.read_excel(request_file, sheetname='Asset template',dtype = dict)
        file_dic = df.to_dict('records')
        print('file_dic',file_dic)
        
        for data_in in file_dic:
            check_ass_id = Asset.objects.filter(CompanyID_id = int(content[0][0]), asset_id = data_in['Asset id(*)']).exclude(is_delete = True).first()
            print('ghhhgrhrtgj',type(data_in['Maintenance due (km)']))
            if check_ass_id:
                asset_obj = Asset.objects.filter(asset_id = removenan(data_in['Asset id(*)'])).update(asset_type = removenan(data_in['Asset type(*)']), make = removenan(data_in['make']), model= removenan(data_in['model']), configuration= removenan(data_in['Configuration(*)']), vin_chassis= removenan(data_in['VIN/Chassis']),
                    engine_no = removenan(data_in['Engine no']), build_date = removenan(data_in['Build date']), e_tag_id = removenan(data_in['E-TAG ID']), gps_id = removenan(data_in['GPS ID']), dangerous_good_compliance = removenan(data_in['Dangerous good compliance']),
                    dangerous_good_license_no = removenan(data_in['Dangerous good License no']), registeration_no = removenan(data_in['Registeration No(*)']), registeration_due_date = removenan(data_in['Registeration due date']), state = removenan(data_in['State']), asset_status = removenan(data_in['Asset status']), odometer = removenan(data_in['Odometer']), 
                    maintenance_intervals = removenan(data_in['Maintenance due (km)']), maintenance_type = removenan(data_in['Maintenance Type']), asset_capacity = removenan(data_in['Asset Capacity']), gross_weight = removenan(data_in['Gross Weight']), tare_weight = removenan(data_in['Tare weight']),
                    purchase_cost = removenan(data_in['Purchase cost']), ownership_type = removenan(data_in['Ownership type']), financed_term = removenan(data_in['Financed term']), hourly_cost = removenan(data_in['hourly cost p/km']))
            else:
                asset_obj = Asset.objects.create(CompanyID_id = int(content[0][0]) ,asset_id = data_in['Asset id(*)'], asset_type= removenan(data_in['Asset type(*)']), make = removenan(data_in['make']), model= removenan(data_in['model']), configuration= removenan(data_in['Configuration(*)']), vin_chassis= removenan(data_in['VIN/Chassis']),
                    engine_no = removenan(data_in['Engine no']), build_date = removenan(data_in['Build date']), e_tag_id = removenan(data_in['E-TAG ID']), gps_id = removenan(data_in['GPS ID']), dangerous_good_compliance = removenan(data_in['Dangerous good compliance']),
                    dangerous_good_license_no = removenan(data_in['Dangerous good License no']), registeration_no = removenan(data_in['Registeration No(*)']), registeration_due_date = removenan(data_in['Registeration due date']), state = removenan(data_in['State']), asset_status = removenan(data_in['Asset status']), odometer = removenan(data_in['Odometer']), 
                    maintenance_intervals = removenan(data_in['Maintenance due (km)']), maintenance_type = removenan(data_in['Maintenance Type']), asset_capacity = removenan(data_in['Asset Capacity']), gross_weight = removenan(data_in['Gross Weight']), tare_weight = removenan(data_in['Tare weight']),
                    purchase_cost = removenan(data_in['Purchase cost']), ownership_type = removenan(data_in['Ownership type']), financed_term = removenan(data_in['Financed term']), hourly_cost = removenan(data_in['hourly cost p/km']))
        
        context = {
            "data":"",
            "message":"success"
        }
        return Response(context,status=status.HTTP_200_OK)    
    
        # except Exception as e:
        #     transaction.savepoint_rollback(sid)
        #     print('error',e)
        #     context = {
        #                     "data": '',
        #                     "message":'Error'
        #             }
        #     return Response(context,status=status.HTTP_400_BAD_REQUEST)


class ListAssetManager(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)    
        try:
            ass_obj = Asset.objects.filter(CompanyID_id = int(content[0][0]), is_active = True, is_delete = False)
            serializ = ListAssetManagerSerializer(ass_obj ,many = True)
            context = {
                "data":serializ.data,
                "message":"success"
            }
            return Response(context, status = status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":str(e)
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)

class UpdateAssetManager(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def put(self, request):
        sid = transaction.savepoint()
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)    
        try:
            data = request.data
            qu = Asset.objects.get(id = data['id'])
            serializ = UpdateAssetManagerSerialz(qu, data = data)
            if serializ.is_valid():
                serializ.update(instance = qu, validated_data = data)
                serializ.save()
                context = {
                    "data":"",
                    "message":"success"
                }
                return Response(context, status = status.HTTP_200_OK)
            else:
                print('error',serializ.errors())
                context = {
                                "data": '',
                                "message":"fail"
                        }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":str(e)
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)       

class AssetSample(APIView):
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:
            output = io.BytesIO()
            workbook = xlsxwriter.Workbook(output, {'in_memory': True})
            worksheet = workbook.add_worksheet("Asset template")
            header_format = workbook.add_format({
                                        'border': 1,
                                        'bg_color': '#C6EFCE',
                                        'bold': True,
                                        'text_wrap': True,
                                        'valign': 'vcenter',
                                        'indent': 1,
                                        })
            worksheet.write('A1', 'Asset id(*)', header_format) 
            worksheet.write('B1', 'Asset type(*)', header_format)
            worksheet.write('C1', 'make', header_format)
            worksheet.write('D1', 'model', header_format)
            worksheet.write('E1', 'Configuration(*)', header_format)
            worksheet.write('F1', 'VIN/Chassis', header_format)
            worksheet.write('G1', 'Engine no', header_format)
            worksheet.write('H1', 'Build date', header_format)
            worksheet.write('I1', 'E-TAG ID', header_format)
            worksheet.write('J1', 'GPS ID', header_format)
            worksheet.write('K1', 'Dangerous good compliance', header_format)
            worksheet.write('L1', 'Dangerous good License no', header_format)
            worksheet.write('M1', 'Registeration No(*)', header_format)
            worksheet.write('N1', 'Registeration due date', header_format)
            worksheet.write('O1', 'State', header_format)
            worksheet.write('P1', 'Asset status', header_format)
            worksheet.write('Q1', 'Odometer', header_format)
            worksheet.write('R1', 'Maintenance due (km)', header_format)
            worksheet.write('S1', 'Maintenance Type', header_format)
            worksheet.write('T1', 'Asset Capacity', header_format)
            worksheet.write('U1', 'Gross Weight', header_format)
            worksheet.write('V1', 'Tare weight', header_format)
            worksheet.write('W1', 'Purchase cost', header_format)
            worksheet.write('X1', 'Ownership type', header_format)
            worksheet.write('Y1', 'Financed term', header_format)
            worksheet.write('Z1', 'hourly cost p/km', header_format)
            worksheet.data_validation('N2:N1000', {'validate': 'any',
                                'input_title': 'Date Field',
                                'input_message':'format: DD/MM/YYYY'
                                })
            worksheet.data_validation('H2:H1000', {'validate': 'any',
                                'input_title': 'Date Field',
                                'input_message':'format: DD/MM/YYYY'
                                })
            workbook.close()
            output.seek(0)

            response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = "attachment; filename=AssetSample.xlsx"

            output.close()

            return response
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":str(e)
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)


class listAssetTruck(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)  
        try:
            ass_truck = Asset.objects.filter(CompanyID_id = int(content[0][0]), asset_type__iexact = 'truck', is_active = True, is_delete = False).values_list("id","asset_id","asset_type","registeration_no")
            final_list = []
            for data in ass_truck:
                dic = {}
                dic["id"] = data[0]
                dic["asset_id"] = data[1]
                dic["vehicle"] = data[2]
                dic["registeration_no"] = data[3]
                final_list.append(dic)
            context = {
                    "data":final_list,
                    "message":"success"
                }
            return Response(context, status = status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":str(e)
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)

class AssetDownload(APIView):
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:
            carr_com_id = request.GET["carr_com_id"]
            if request.GET['asset_list']:
                asset_id = request.GET['asset_list']
                res = asset_id.strip('][').split(',')
                if res != ['']:
                    res = [int(i) for i in res]
                else:
                    res = []
                asset_obj = ''
                if res != []:
                    asset_obj = Asset.objects.filter(id__in = res, CompanyID_id = carr_com_id)
                else:
                    asset_obj = Asset.objects.filter(CompanyID_id = carr_com_id, is_active = True, is_delete = False)
                serializ = AssetDownloadSerializer(asset_obj, many = True)
                data = serializ.data
                output = io.BytesIO()
                workbook = xlsxwriter.Workbook(output, {'in_memory': True})
                worksheet = workbook.add_worksheet("Asset template")
                header_format = workbook.add_format({
                                            'border': 1,
                                            'bg_color': '#C6EFCE',
                                            'bold': True,
                                            'text_wrap': True,
                                            'valign': 'vcenter',
                                            'indent': 1,
                                            })
                worksheet.write('A1', 'Asset id', header_format) 
                worksheet.write('B1', 'Asset type', header_format)
                worksheet.write('C1', 'make', header_format)
                worksheet.write('D1', 'model', header_format)
                worksheet.write('E1', 'Configuration', header_format)
                worksheet.write('F1', 'VIN/Chassis', header_format)
                worksheet.write('G1', 'Engine no', header_format)
                worksheet.write('H1', 'Build date', header_format)
                worksheet.write('I1', 'E-TAG ID', header_format)
                worksheet.write('J1', 'GPS ID', header_format)
                worksheet.write('K1', 'Dangerous good compliance', header_format)
                worksheet.write('L1', 'Dangerous good License no', header_format)
                worksheet.write('M1', 'Registeration No', header_format)
                worksheet.write('N1', 'Registeration due date', header_format)
                worksheet.write('O1', 'State', header_format)
                worksheet.write('P1', 'Asset status', header_format)
                worksheet.write('Q1', 'Odometer', header_format)
                worksheet.write('R1', 'Maintenance due (km)', header_format)
                worksheet.write('S1', 'Maintenance Type', header_format)
                worksheet.write('T1', 'Asset Capacity', header_format)
                worksheet.write('U1', 'Gross Weight', header_format)
                worksheet.write('V1', 'Tare weight', header_format)
                worksheet.write('W1', 'Purchase cost', header_format)
                worksheet.write('X1', 'Ownership type', header_format)
                worksheet.write('Y1', 'Financed term', header_format)
                worksheet.write('Z1', 'hourly cost p/km', header_format)
                row = 1
                for data_in in data:
                    worksheet.write(row,0,data_in['asset_id'])
                
                    worksheet.write(row,1,data_in['asset_type'])
                    
                    worksheet.write(row,2,data_in['make'])

                    worksheet.write(row,3,data_in['model'])
                    
                    worksheet.write(row,4,data_in['configuration'])
                    
                    worksheet.write(row,5,data_in['vin_chassis'])
                    
                    worksheet.write(row,6,data_in['engine_no'])
                    
                    worksheet.write(row,7,data_in['build_date'])
                    
                    worksheet.write(row,8,data_in['e_tag_id'])
                    
                    worksheet.write(row,9,data_in['gps_id'])
                    
                    worksheet.write(row,10,data_in['dangerous_good_compliance'])
                    
                    worksheet.write(row,11,data_in['dangerous_good_license_no'])
                    
                    worksheet.write(row,12,data_in['registeration_no'])

                    worksheet.write(row,13,data_in['registeration_due_date'])

                    worksheet.write(row,14,data_in['state'])

                    worksheet.write(row,15,data_in['asset_status'])

                    worksheet.write(row,16,data_in['odometer'])

                    worksheet.write(row,17,data_in['maintenance_intervals'])

                    worksheet.write(row,18,data_in['maintenance_type'])

                    worksheet.write(row,19,data_in['asset_capacity'])

                    worksheet.write(row,20,data_in['gross_weight'])

                    worksheet.write(row,21,data_in['tare_weight'])

                    worksheet.write(row,22,data_in['purchase_cost'])

                    worksheet.write(row,23,data_in['ownership_type'])

                    worksheet.write(row,24,data_in['financed_term'])

                    worksheet.write(row,25,data_in['hourly_cost'])
                    row = row + 1
                    
                workbook.close()
                output.seek(0)

                response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                response['Content-Disposition'] = "attachment; filename=Assets.xlsx"

                output.close()

                return response
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":str(e)
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)




class DeleteAsset(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        asset_list = request.data['asset_list']
        if asset_list != []:
            asset_obj = Asset.objects.filter(id__in = asset_list).update(is_active = False, is_delete = True)
            if asset_obj:
                context = {
                    "data":"",
                    "message":"Deleted"
                }
                return Response(context, status = status.HTTP_200_OK)
        
            else:
                context = {
                    "data":"",
                    "message":"Failed to delete"
                }
                return Response(context, status = status.HTTP_400_BAD_REQUEST)     
        else:
            context = {
                    "data":"",
                    "message":"No selection to delete"
            }
            return Response(context, status = status.HTTP_200_OK)