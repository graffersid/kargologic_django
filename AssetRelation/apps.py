from django.apps import AppConfig


class AssetrelationConfig(AppConfig):
    name = 'AssetRelation'
