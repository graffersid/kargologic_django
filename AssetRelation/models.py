from django.db import models
from Pickup.models import Pick
from Delivery.models import Drop
from Asset.models import Asset
from company.models import Company
class AssetRelation(models.Model):
    CompanyID   = models.ForeignKey(Company,related_name = 'assetcom',on_delete = models.CASCADE, null =True, blank =True)
    AssetID     = models.ForeignKey(Asset,on_delete = models.CASCADE, null =True, blank =True)
    PickID      = models.ForeignKey(Pick,related_name = 'assetpick',on_delete = models.CASCADE, null =True, blank =True)
    DropID      = models.ForeignKey(Drop,related_name = 'assetdrop',on_delete = models.CASCADE, null =True, blank =True)
    is_attach   = models.BooleanField(default = False)
    created_DT  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT  = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active   = models.BooleanField(default=True)
    is_delete   = models.BooleanField(default=False)

    def __str__(self):
        return '%s-%s-(%s)'%(self.AssetID,self.is_attach,self.id)