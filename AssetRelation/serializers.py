from rest_framework import serializers
from Asset.serializers import AssetSerializer, ListAssetSerializer, OrderDetailAssetSerializer
from AssetRelation.models import AssetRelation
class AssetRelationSerializer(serializers.ModelSerializer):
    AssetID = AssetSerializer(required = False)
    class Meta:
        model = AssetRelation
        exclude = ('created_DT','updated_DT','is_active')


class ListAssetRelation(serializers.ModelSerializer):
    AssetID = ListAssetSerializer(required = False)
    class Meta:
        model = AssetRelation
        exclude = ('created_DT','updated_DT','is_active')

class OrderDetailAssetRelationSerializer(serializers.ModelSerializer):
    AssetID = OrderDetailAssetSerializer(required = False)
    class Meta:
        model = AssetRelation
        fields = ('AssetID','PickID','DropID','is_attach')

