from django.db import models
from company.models import Company
from drivers.models import DriverExtraInfo
from status.models import Status
from orders.models import Order
from carriers.models import CarrierExtraInfo
from customers.models import CustomerExtraInfo
class CarrierDriver(models.Model):
    CarrierCompanyID      = models.ForeignKey(Company, on_delete = models.CASCADE,null=True,blank=True)
    DriverID              = models.ForeignKey(DriverExtraInfo, on_delete = models.CASCADE,null=True,blank=True)
    status                = models.ForeignKey(Status, on_delete = models.CASCADE, null=True,blank=True)
    rating                = models.FloatField(null=True,blank=True)

    created_DT            = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT            = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active             = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)
    def __str__(self):
        return '%s---%s (%s)' % (self.CarrierCompanyID, self.DriverID,self.id)