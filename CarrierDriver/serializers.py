from rest_framework import serializers
from CarrierDriver.models import CarrierDriver

from company.serializer import CompanySerializer
from drivers.serializers import DriverExtraInfoSerializer, CarriertoDriverListSerializer, AddMoreDriverListSerializer
from TruckType.serializers import CarrDrivtoListSerializer
from status.serializers import StatusSerializer
from status.serializers import CarrDrivStatusSerializer

class CarrierDriverSerializer(serializers.ModelSerializer):
    CarrierCompanyID    = CompanySerializer(required=False)
    driverID = DriverExtraInfoSerializer(required=False)
    status              = StatusSerializer(required=False)

    class Meta:
        model = CarrierDriver
        exclude = ('created_DT','updated_DT','is_active')


class AddMoreDriverSerializer(serializers.ModelSerializer):
    DriverID           = AddMoreDriverListSerializer(required=False)
    status             = CarrDrivStatusSerializer(required=False)
    class Meta:
        model = CarrierDriver
        exclude = ('id','created_DT','updated_DT','is_active','CarrierCompanyID','rating')        



class CarrierDriverListSerializer(serializers.ModelSerializer):
    DriverID           = CarriertoDriverListSerializer(required=False)
    status             = CarrDrivStatusSerializer(required=False)
    class Meta:
        model = CarrierDriver
        exclude = ('id','created_DT','updated_DT','is_active','CarrierCompanyID','rating','is_delete')      


class ArrOrderDriver(serializers.ModelSerializer):
    # CarrierCompanyID    = CompanySerializer(required=False)
    # driverID            = DriverExtraInfoSerializer(required=False)
    # status              = StatusSerializer(required=False)

    class Meta:
        model = CarrierDriver
        exclude = ('created_DT','updated_DT')


class CarrDriverListDownload(serializers.ModelSerializer):
    DriverID           = CarriertoDriverListSerializer(required=False)
    status             = CarrDrivStatusSerializer(required=False)
    class Meta:
        model = CarrierDriver
        exclude = ('id','created_DT','updated_DT','is_active','CarrierCompanyID','rating','is_delete')       

    def to_representation(self, instance):
        data = super(CarrDriverListDownload, self).to_representation(instance)
        print('data',data['DriverID']['id'])
        sort_dic = {}
        if data['DriverID']['id']:
            sort_dic['driver_id'] = data['DriverID']['id']
        if data['DriverID']['name']:
            sort_dic['name'] = data['DriverID']['name']
        if data['DriverID']['address']:
            sort_dic['address'] = data['DriverID']['address']
        if data['DriverID']['mobile_num']:
            sort_dic['mobile_number'] = data['DriverID']['mobile_num']
        if data['DriverID']['license_num']:
            sort_dic['license_number'] = data['DriverID']['license_num']
        if data['DriverID']['TruckType']['capacity']:
            sort_dic['capacity'] = data['DriverID']['TruckType']['capacity']
        if data['status']:
            sort_dic['status'] = data['status']['status_name']
        return sort_dic





#         [
#     {
#         "DriverID": {
#             "id": 5,
#             "name": "parlod",
#             "address": null,
#             "mobile_num": "51254875125",
#             "license_num": "GLDXD54G4HNG",
#             "image": null,
#             "TruckType": {
#                 "truck_type": "large",
#                 "truck_reg_num": "MP09GS2154",
#                 "capacity": null
#             }
#         },
#         "status": {
#             "status_name": "driver pending"
#         }
#     }
# ]