from django.db import models
from company.models import Company

class CompanySenderReceiver(models.Model):
    CompanyID            = models.ForeignKey(Company, on_delete = models.CASCADE, null=True, blank=True)
    name                 = models.CharField(max_length = 500,null=True,blank=True)
    primary_contact      = models.CharField(max_length = 500,null=True,blank=True)
    pri_email_id         = models.EmailField(max_length=100,null=True,blank=True)    
    pri_phone            =  models.CharField(max_length = 500,null=True,blank=True)
    secondary_contact    = models.CharField(max_length = 500,null=True,blank=True)
    sec_email_id         = models.EmailField(max_length=100,null=True,blank=True)
    sec_phone            = models.CharField(max_length = 500,null=True,blank=True)
    business_address     = models.CharField(max_length = 500,null=True,blank=True)
    contract_start_date  = models.CharField(max_length = 500,null=True,blank=True)
    contract_end_date    = models.CharField(max_length = 500,null=True,blank=True)
    contract_documents   = models.URLField(null=True,blank=True)
    Customer_code        = models.CharField(max_length = 500,null=True,blank=True)
    is_sender            = models.BooleanField(default=False)
    created_DT           = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT           = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active            = models.BooleanField(default=True)
    is_delete            = models.BooleanField(default=False)
    def __str__(self):
        return '%s %s %s (%s)'%(self.CompanyID, self.name, self.is_sender, self.id)