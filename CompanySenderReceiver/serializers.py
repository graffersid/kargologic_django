from rest_framework import serializers
from CompanySenderReceiver.models import CompanySenderReceiver
from SenderReceiverManyRelation.models import SendReceiveManyRelation
from SenderReceiverManyRelation.serializers import CreatSenderCreateSerialz, Manager_docserializer
from drf_extra_fields.fields import Base64FileField
from KargoLogics.settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME, EMAIL_HOST_USER,AWS_S3_CUSTOM_DOMAIN,BUCKET_URL
import boto3
from boto3.session import Session
class PDFBase64File(Base64FileField):
    ALLOWED_TYPES = ['pdf','img']

    def get_file_extension(self, filename, decoded_file):
            return 'img'

class OrderCreateComSenderReceiver(serializers.ModelSerializer):
    kid = serializers.IntegerField(required = False)
    class Meta:
        model = CompanySenderReceiver
        exclude = ('CompanyID','is_sender','created_DT','updated_DT')


class listComSenderReceiver(serializers.ModelSerializer):
    
    class Meta:
        model = CompanySenderReceiver
        fields = ('id','name','is_sender')


class UploadSenderReceiver(serializers.ModelSerializer):
    class Meta:
        model = CompanySenderReceiver
        exclude = ('created_DT','updated_DT')


class listComSenderReceiverManager(serializers.ModelSerializer):
    com_send_rec = Manager_docserializer(many = True)
    class Meta: 
        model = CompanySenderReceiver
        fields = ('id','CompanyID','name','primary_contact','pri_email_id','pri_phone','secondary_contact','sec_email_id','sec_phone','business_address','contract_start_date','contract_end_date','contract_documents','Customer_code','is_sender','com_send_rec')


class CreateSenderReceSerializer(serializers.ModelSerializer):
    # image_read = PDFBase64File()
    sendrecemany = CreatSenderCreateSerialz(required = False ,many = True)
    class Meta:
        model = CompanySenderReceiver
        exclude = ('created_DT','updated_DT')

    def create(self ,validated_data):
        
        com_id = self.context['com_id']
        rel_data_pop = validated_data.pop('sendrecemany')
        obj = CompanySenderReceiver.objects.create(CompanyID_id = com_id, **validated_data)
        if rel_data_pop != []:
            
            for data_in in  rel_data_pop:
                
                session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                    aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
            
                image_str = data_in['image_read']
                print('########################',image_str)


                s3 = session.resource('s3')
                
                cloudFilename = "CompanySenderReceiver/"+(str(validated_data['name'])).replace(' ', '_')+(str(validated_data['primary_contact'])).replace(' ', '_')+(str(data_in["attachment_name"])).replace(' ', '_')
                s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=image_str, ACL='public-read')
                SRM_obj = SendReceiveManyRelation.objects.create(ComSendReceiverID_id = obj.id , attachment_name = (data_in['attachment_name']).replace(' ', '_'), doc_upload = str(BUCKET_URL+cloudFilename))
                
        
        return "success"


class SenderReceiverDownloadSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanySenderReceiver
        exclude = ('created_DT','updated_DT')