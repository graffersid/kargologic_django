from django.conf.urls import url

from CompanySenderReceiver import views

urlpatterns = [
   url('upload_sender_receiver/',views.UploadSenderReciever.as_view()),
   url('managersenderrecei/',views.ManagerComSenderReceiverList.as_view()),
   url('createsendrec/',views.CreateSenderReceiver.as_view()),
   url('sample_sender_reciever/',views.SampleSendReceiver.as_view()),
   url('sr_download/',views.SendRecieverDownload.as_view()),
   url('delete_sedreci/',views.DeleteSenderReciever.as_view()),
]
