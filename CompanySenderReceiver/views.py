from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from Pickup.models import Pick
from OrderHistory.models import OrderHistory
from datetime import datetime
from django.db import transaction
import pandas as pd
from rest_framework.parsers import MultiPartParser,FileUploadParser
from CompanySenderReceiver.models import CompanySenderReceiver
from CompanySenderReceiver.serializers import listComSenderReceiverManager, CreateSenderReceSerializer, SenderReceiverDownloadSerializer
import io
import xlsxwriter
from django.http import HttpResponse
from SenderReceiverManyRelation.serializers import CreatSenderCreateSerialz
import math
def removenan(data):
    try:
        if math.isnan(float(data)):
            return ''
        else:
            return data
    except ValueError:
        return data
class UploadSenderReciever(APIView):
    parser_classes = [MultiPartParser,FileUploadParser]
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        try:
            request_file = request.FILES['data_file']

            df = pd.read_excel(request_file, sheetname='Customer template',dtype = dict)
            file_dic = df.to_dict('records')
            print('file_dic',file_dic)
            if request.GET['type'] == 'sender':
                for data_in in file_dic:
                    CompanySenderReceiver.objects.create(CompanyID_id = int(content[0][0]), name = removenan(data_in['customer business name(*)']), primary_contact = removenan(data_in['primary contact(*)']) , pri_email_id = removenan(data_in['primary email id']), pri_phone = removenan(data_in['primary phone(*)']),
                    secondary_contact = removenan(data_in['secondary contact']), sec_email_id = removenan(data_in['secondary email']), sec_phone = removenan(data_in['secondary phone']) , business_address = removenan(data_in['business address(*)']), contract_start_date = removenan(data_in['contract start date']), contract_end_date = removenan(data_in['contract end date']), Customer_code = removenan(data_in['Customer code']), is_sender = "True")
                
                context = {
                "data":"",
                "message":"success"
                }
                return Response(context,status=status.HTTP_200_OK)

            elif request.GET['type'] == 'receiver':
                for data_in in file_dic:
                    CompanySenderReceiver.objects.create(CompanyID_id = int(content[0][0]), name = removenan(data_in['customer business name(*)']), primary_contact = removenan(data_in['primary contact(*)']) , pri_email_id = removenan(data_in['primary email id']), pri_phone = removenan(data_in['primary phone(*)']),
                    secondary_contact = removenan(data_in['secondary contact']), sec_email_id = removenan(data_in['secondary email']), sec_phone = removenan(data_in['secondary phone']) , business_address = removenan(data_in['business address(*)']), contract_start_date = removenan(data_in['contract start date']), contract_end_date = removenan(data_in['contract end date']), 
                    Customer_code = removenan(data_in['Customer code']), is_sender = "False")
                
                context = {
                    "data":"",
                    "message":"success"
                    }
                return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('error',e)
            context = {
                            "data": '',
                            "message":'Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)



class ManagerComSenderReceiverList(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'Token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            comsendrc_obj = CompanySenderReceiver.objects.filter(CompanyID_id = int(content[0][0]), is_active = True, is_delete = False)
            comsenreceiver_serializer = listComSenderReceiverManager(comsendrc_obj , many = True)
            
            context = {
                "data":comsenreceiver_serializer.data,
                "message":"success"
            }
            return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)

class CreateSenderReceiver(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            data = request.data
            
            if data['id'] == 0:
                serializ = CreateSenderReceSerializer(data = data, context = {"com_id":int(content[0][0])})
                if serializ.is_valid():
                    serializ.save()
                    context = {
                    "data":"",
                    "message":"success"
                    }
                    return Response(context,status=status.HTTP_200_OK)

                else:
                    print('serializer error',serializ.errors())
                    context = {
                        "data":"",
                        "message":"fail"
                        }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)
            else:
                pop_id = data.pop("id")
                pop_sr_rel_data = data.pop('sendrecemany')
                com_sr_obj = CompanySenderReceiver.objects.filter(id = int(pop_id)).update(**data)
                print('inside else')
                if pop_sr_rel_data != []:
                    
                    serialz = CreatSenderCreateSerialz(data = pop_sr_rel_data, context = {"csr_id":int(pop_id),"name":data['name'], "primary_contact":data['primary_contact']}, many = True)
                    if serialz.is_valid():
                        serialz.save()
                        print('pop serializ')
                        context = {
                        "data":"",
                        "message":"success"
                        }
                        return Response(context,status=status.HTTP_200_OK)
                    else:
                        print('error', serialz.errors())
                        print('ser else')
                        context = {
                        "data":"",
                        "message":"serializer error"
                        }
                        return Response(context,status=status.HTTP_400_BAD_REQUEST)
                else:
                    print('pop else')
                    context = {
                    "data":"",
                    "message":"success"
                    }
                    return Response(context,status=status.HTTP_200_OK)
                 
        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('error',e)
            context = {
                            "data": '',
                            "message":'Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class SampleSendReceiver(APIView):
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:
            output = io.BytesIO()
            workbook = xlsxwriter.Workbook(output, {'in_memory': True})
            worksheet = workbook.add_worksheet("Customer template")
            header_format = workbook.add_format({
                                        'border': 1,
                                        'bg_color': '#C6EFCE',
                                        'bold': True,
                                        'text_wrap': True,
                                        'valign': 'vcenter',
                                        'indent': 1,
                                        })
            worksheet.write('A1', 'customer business name(*)', header_format) 
            worksheet.write('B1', 'primary contact(*)', header_format)
            worksheet.write('C1', 'primary email id', header_format)
            worksheet.write('D1', 'primary phone(*)', header_format)
            worksheet.write('E1', 'secondary contact', header_format)
            worksheet.write('F1', 'secondary email', header_format)
            worksheet.write('G1', 'secondary phone', header_format)
            worksheet.write('H1', 'business address(*)', header_format)
            worksheet.write('I1', 'contract start date', header_format)
            worksheet.write('J1', 'contract end date', header_format)
            worksheet.write('K1', 'Customer code', header_format)
            

            workbook.close()
            output.seek(0)

            response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = "attachment; filename=SenderRecieverSample.xlsx"

            output.close()

            return response
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)


class SendRecieverDownload(APIView):
    @transaction.atomic
    def get(self , request):
        sid = transaction.savepoint()
        try:
            s_or_r = request.GET['s_or_r']
            print('s_or_r',s_or_r)
            carr_com_id = int(request.GET['carr_com_id'])
            if request.GET['sr_list']:
                sr_id = request.GET['sr_list']
                res = sr_id.strip('][').split(',')
                if res != ['']:
                    res = [int(i) for i in res]
                else:
                    res = []
                sr_object = ''
                if res != []:
                    sr_object = CompanySenderReceiver.objects.filter(id__in = res, CompanyID_id = carr_com_id, is_sender = s_or_r, is_active = True, is_delete = False)
                else:
                    sr_object = CompanySenderReceiver.objects.filter(CompanyID_id = carr_com_id, is_sender = s_or_r, is_active = True, is_delete = False)
                seializ = SenderReceiverDownloadSerializer(sr_object , many = True)
                data = seializ.data
                output = io.BytesIO()
                workbook = xlsxwriter.Workbook(output, {'in_memory': True})
                worksheet = workbook.add_worksheet("Customer template")
                header_format = workbook.add_format({
                                            'border': 1,
                                            'bg_color': '#C6EFCE',
                                            'bold': True,
                                            'text_wrap': True,
                                            'valign': 'vcenter',
                                            'indent': 1,
                                            })
                worksheet.write('A1', 'customer business name', header_format) 
                worksheet.write('B1', 'primary contact', header_format)
                worksheet.write('C1', 'primary email id', header_format)
                worksheet.write('D1', 'primary phone', header_format)
                worksheet.write('E1', 'secondary contact', header_format)
                worksheet.write('F1', 'secondary email', header_format)
                worksheet.write('G1', 'secondary phone', header_format)
                worksheet.write('H1', 'business address', header_format)
                worksheet.write('I1', 'contract start date', header_format)
                worksheet.write('J1', 'contract end date', header_format)
                worksheet.write('K1', 'Customer code', header_format)
                
                row = 1
                for data_in in data:
                    worksheet.write(row,0,data_in['name'])
                    worksheet.write(row,1,data_in['primary_contact'])
                    worksheet.write(row,2,data_in['pri_email_id'])
                    worksheet.write(row,3,data_in['pri_phone'])
                    worksheet.write(row,4,data_in['secondary_contact'])
                    worksheet.write(row,5,data_in['sec_email_id'])
                    worksheet.write(row,6,data_in['sec_phone'])
                    worksheet.write(row,7,data_in['business_address'])
                    worksheet.write(row,8,data_in['contract_start_date'])
                    worksheet.write(row,9,data_in['contract_end_date'])
                    worksheet.write(row,10,data_in['Customer_code'])
                    row = row + 1

                workbook.close()
                output.seek(0)

                response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                response['Content-Disposition'] = "attachment; filename=SRDownload.xlsx"

                output.close()

                return response
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)


class DeleteSenderReciever(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        sendrec_id_list = request.data['sendrec_id']
        
        if sendrec_id_list != []:
            sr_obj = CompanySenderReceiver.objects.filter(id__in = sendrec_id_list).update(is_active = False, is_delete = True)
            if sr_obj:
                context = {
                    "data":"",
                    "message":"Deleted"
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                context = {
                    "data":"",
                    "message":"Delete failed"
                }
                return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                "data":"",
                "message":"No selection to delete"
            }
            return Response(context, status=status.HTTP_200_OK)