from django.db import models
from company.models import Company
from customers.models import CustomerExtraInfo
from carriers.models import CarrierExtraInfo
from CompanyUserRole.models import CompanyUserRole
class CompanyUser(models.Model):
    CompanyID             = models.ForeignKey(Company, related_name = 'comp_user' ,on_delete = models.CASCADE,null=True,blank=True)
    Created_by_CustomerID = models.ForeignKey(CustomerExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    Created_by_CarrierID  = models.ForeignKey(CarrierExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    CompanyUserRoleID     = models.ForeignKey(CompanyUserRole, on_delete = models.CASCADE, null=True,blank=True)
    created_DT            = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT            = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active             = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)
    def __str__(self):
        return 'company %s (%s)'%(self.CompanyID, self.id)