from rest_framework import serializers
from CompanyUser.models import CompanyUser
from company.models import Company
from carriers.models import CarrierExtraInfo
from custom_users.models import User

from company.serializer import CompanySerializer, CarrierSettingCompanySerializer
from customers.serializers import CustomerExtraInfoSerializer
from carriers.serializers import CarrierExtraInfoSerializer, CarrierSettingCarrierExtraInfoSerializer
from CompanyUserRole.serializers import CompanyUserRoleSerializer

class CompanyUserSerializer(serializers.ModelSerializer):
    CompanyID          = CompanySerializer(required=False)
    Created_by_CustomerID             = CustomerExtraInfoSerializer(required=False)
    Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    CompanyUserRoleID  = CompanyUserRoleSerializer(required=False)
    class Meta:
        model  = CompanyUser
        exclude = ('created_DT','updated_DT','is_active')


class CarrierSettingCompanyUserSerializer(serializers.ModelSerializer): 
    CompanyID          = CarrierSettingCompanySerializer(required=False)
    Created_by_CarrierID = CarrierSettingCarrierExtraInfoSerializer(required=False)
    
    class Meta:
        model  = CompanyUser
        fields = ('CompanyID','Created_by_CarrierID')

    def update(self, instance, validated_data):
        print('validated', validated_data)
        com_data = validated_data["CompanyID"]
        carr_data = validated_data["Created_by_CarrierID"]
        com_id = self.context.get("com_id")
        carr_id = self.context.get("carr_id")
        user_id = self.context.get("user_id")
        if validated_data['Created_by_CarrierID']['email']:
            user_obj = User.objects.filter(id = user_id).update(user_name = carr_data["name"], email = validated_data['Created_by_CarrierID']['email'])
        else:
            user_obj = User.objects.filter(id = user_id).update(user_name = carr_data["name"])    
        com_obj = Company.objects.filter(id = com_id).update(**com_data)
        print('aqqqq',com_obj)
        carr_obj = CarrierExtraInfo.objects.filter(id = carr_id).update(**carr_data)
        print('carrrr',carr_obj)
        return ("sucess")    


class CompanyListUserSerializer(serializers.ModelSerializer):
    CompanyID               = CompanySerializer(required=False)
    Created_by_CustomerID   = CustomerExtraInfoSerializer(required=False)

    class Meta:
        model = CompanyUser
        fields = ('CompanyID','Created_by_CustomerID')


