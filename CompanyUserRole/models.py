from django.db import models
from company.models import Company
class CompanyUserRole(models.Model):
    CompanyID   = models.ForeignKey(Company,on_delete = models.CASCADE,null=True,blank=True)
    role_name   = models.CharField(max_length = 100,null=True,blank=True)
    description = models.CharField(max_length = 500,null=True,blank=True)
    created_DT  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT  = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active   = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)