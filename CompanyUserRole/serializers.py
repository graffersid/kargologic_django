from rest_framework import serializers
from CompanyUserRole.models import CompanyUserRole
from company.serializer import CompanySerializer
class CompanyUserRoleSerializer(serializers.ModelSerializer):
    CompanyID = CompanySerializer(required=False)
    class Meta:
        model  = CompanyUserRole 
        exclude = ('created_DT','updated_DT','is_active')