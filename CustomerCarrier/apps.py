from django.apps import AppConfig


class CustomercarrierConfig(AppConfig):
    name = 'CustomerCarrier'
