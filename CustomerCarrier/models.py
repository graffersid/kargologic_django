from django.db import models
from company.models import Company
from status.models import Status
class CustomerCarrier(models.Model):
    CustomercompanyID   = models.ForeignKey(Company,related_name='customer_company',on_delete = models.CASCADE,null=True,blank=True)
    CarriercompanyID    = models.ForeignKey(Company, on_delete = models.CASCADE,null=True,blank=True)
    r_status            = models.ForeignKey(Status, on_delete = models.CASCADE, null=True,blank=True)
    rating              = models.FloatField(null=True,blank=True)
    
    business_name        = models.CharField(max_length = 500,null=True,blank=True)
    name                 = models.CharField(max_length=100,null=True,blank=True)
    
    mobile_num           = models.CharField(max_length=100,null=True,blank=True)
    email                = models.EmailField(max_length=100,null=True,blank=True)
    secondary_contact    = models.CharField(max_length = 500,null=True,blank=True)
    sec_email_id         = models.EmailField(max_length=100,null=True,blank=True)
    sec_phone            = models.CharField(max_length = 500,null=True,blank=True)
    business_address     = models.CharField(max_length = 500,null=True,blank=True)
    contract_start_date  = models.CharField(max_length = 500,null=True,blank=True)
    contract_end_date    = models.CharField(max_length = 500,null=True,blank=True)
    Customer_code        = models.CharField(max_length = 500,null=True,blank=True)
    
    
    
    
    created_DT          = models.DateTimeField(auto_now_add=True)
    updated_DT          = models.DateTimeField(auto_now=True)
    is_active           = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)

    def __str__(self):
        return '%s %s (%s)' % (self.CustomercompanyID, self.CarriercompanyID,self.id)