from rest_framework import serializers
from CustomerCarrier.models import CustomerCarrier
from status.serializers import StatusSerializer
from company.serializer import CompanySerializer
from CompanyUser.models import CompanyUser
from customer_attach.models import CustomerAttach
class CustomerCarrierSerializer(serializers.ModelSerializer):
    CustomercompanyID = CompanySerializer(required=False)
    CarriercompanyID  = CompanySerializer(required=False)
    class Meta:
        model  = CustomerCarrier
        exclude = ('created_DT','updated_DT','is_active')
        

class CustomerCarrierListSerializer(serializers.ModelSerializer):
    # CustomercompanyID = CompanySerializer(required=False)
    CarriercompanyID  = CompanySerializer(required=False)
    r_status = StatusSerializer(required=False)
    class Meta:
        model  = CustomerCarrier
        exclude = ('created_DT','updated_DT','is_active','is_delete','CustomercompanyID')
        
class ListCustomerCarrierSerializer(serializers.ModelSerializer):
    CustomercompanyID = CompanySerializer(required=False)
    # CarriercompanyID  = CompanySerializer(required=False)
    r_status = StatusSerializer(required=False)
    custt_att = serializers.SerializerMethodField()
    def get_custt_att(self, CustomerCarrier):
        CU  = CompanyUser.objects.filter(CompanyID_id = CustomerCarrier.CustomercompanyID.id).first()
        cust_id = CU.Created_by_CustomerID.id
        url = CustomerAttach.objects.filter( CustomerID_id = cust_id).values_list("attach_url")
        return url 
    class Meta:
        model  = CustomerCarrier
        exclude = ('created_DT','updated_DT','is_active')


