from django.db import models
from orders.models import Order
from OrderDriver.models import OrderDriver
from CompanySenderReceiver.models import CompanySenderReceiver
from Asset.models import Asset
class Drop(models.Model):
    AssetID                     = models.ForeignKey(Asset, related_name='drop_asset', on_delete = models.CASCADE, null=True,blank=True)
    OrderID                     = models.ForeignKey(Order, related_name='order_drop_related', on_delete = models.CASCADE, null=True,blank=True)
    OrderDriverID               = models.ForeignKey(OrderDriver,related_name='order_driver_drop_related', on_delete = models.CASCADE, null=True,blank=True)
    receiver                    = models.ForeignKey(CompanySenderReceiver, on_delete = models.CASCADE, null=True,blank=True) 
    delivery_date_time          = models.DateTimeField(null=True,blank=True)
    delivery_date_time_to       = models.DateTimeField(null=True,blank=True)
    actual_delivery_date_time   = models.DateTimeField(null=True,blank=True)
    address                     = models.CharField(max_length = 500, null=True,blank=True)
    longitude                   = models.CharField(max_length = 200, null=True,blank=True)
    latitude                    = models.CharField(max_length = 200, null=True,blank=True)
    assigned_quantity           = models.CharField(max_length = 200, null=True, blank=True)
    pod_image                   = models.URLField(null=True,blank=True)
    
    created_DT                  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT                  = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active                   = models.BooleanField(default=True)
    is_delete                   = models.BooleanField(default=False)
    def __str__(self):
        return '%s-%s-%s'%(self.OrderID,self.OrderDriverID,self.id)