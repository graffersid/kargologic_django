from rest_framework import serializers
from Delivery.models import Drop
from OrderDriver.models import OrderDriver
from OrderDriver.serializers import AddMoreOrderDriverSerializer
from OrderDriver.serializers import CarrGetOrderDetailsOrderDriver
from status.models import Status
from orders.models import Order
from CompanySenderReceiver.serializers import OrderCreateComSenderReceiver
from Asset.serializers import AssetSerializer
class ArrOrderDropSerializer(serializers.ModelSerializer):
    receiver = OrderCreateComSenderReceiver(required = False)
    class Meta:
        model = Drop
        exclude = ('created_DT','updated_DT','is_active')


class AssignDropDriverSerializer(serializers.ModelSerializer):
    DriverID = serializers.IntegerField()
    receiver = OrderCreateComSenderReceiver(required = False)
    asset    = AssetSerializer(many = True, required = False)
    ass_truck_id = serializers.IntegerField()
    asset_start = serializers.DateTimeField(required=False)
    class Meta:
        model = Drop
        exclude = ('created_DT','updated_DT','is_active')


class AssignDropDriverOrderDetailSerializer(serializers.ModelSerializer):
    OrderDriverID = CarrGetOrderDetailsOrderDriver(required=False)
    class Meta:
        model = Drop
        fields = ('id','OrderDriverID','receiver', 'delivery_date_time', 'delivery_date_time_to','actual_delivery_date_time','pod_image', 'address', 'longitude', 'latitude', 'assigned_quantity', 'created_DT', 'updated_DT')
        depth = 1
class DropDriverOrderDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Drop
        exclude = ('OrderID','OrderDriverID','is_active')        
        depth = 1

class AddMoreDropSerializer(serializers.ModelSerializer):
    class Meta:
        model = Drop
        exclude = ('id','OrderID','OrderDriverID','created_DT','updated_DT','is_active')        
    
    def create(self, validated_data):
        order_driver_id = self.context.get('order_driver_id')
        drop_obj = Drop.objects.create(OrderDriverID_id = order_driver_id, **validated_data)
        return 'success'


class AddMoreDropSerializer2(serializers.ModelSerializer):
    OrderDriverID = AddMoreOrderDriverSerializer(required=False)
    tem_id = serializers.IntegerField()
    class Meta:
        model = Drop
        fields = ('tem_id','OrderDriverID','longitude','latitude','delivery_date_time','delivery_date_time_to','address','assigned_quantity')

    def create(self, validated_data):
        print('validated_drop ', validated_data)
        order_driver = validated_data.pop('OrderDriverID')
        st = Status.objects.filter(status_name = "pending").first()
        print('st form drop',st)
        order_driver_obj = OrderDriver.objects.get_or_create(OrderID = order_driver['OrderID'],DriverID  = order_driver['DriverID'], order_status = st)
        if validated_data['tem_id'] != 0:
            tem_id_pop = validated_data.pop('tem_id')
            drop_obj = Drop.objects.filter(id = tem_id_pop, OrderDriverID = order_driver_obj[0]).update(**validated_data)    
        else:
            tem_id_pop = validated_data.pop('tem_id') 
            drop_obj = Drop.objects.create(OrderDriverID = order_driver_obj[0], **validated_data)

        status_obj = Status.objects.filter(status_name = "pending").values_list("id")
        order_id = order_driver['OrderID'].id
        ord_obj = Order.objects.filter(id = order_id).update(StatusID_id = status_obj[0][0])
        return 'success'