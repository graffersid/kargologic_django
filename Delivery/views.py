from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from Delivery.models import Drop
from OrderHistory.models import OrderHistory
from datetime import datetime
# Create your views here.
from KargoLogics.settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME, EMAIL_HOST_USER,AWS_S3_CUSTOM_DOMAIN,BUCKET_URL
import boto3
from boto3.session import Session
import base64
from django.core.files.base import ContentFile
from Notifications.models import Notification
from drivers.models import DriverExtraInfo
class DriverUpdateDrop(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        print('data#########################',request.data)
        order_id = request.data['order_id']
        carr_id  = request.data['carr_id']
        driver_name = request.data['driver_name']
        order_job   = request.data['order_job']  
        pod_img     = request.data['pod_img']  
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)    
            
        data = request.data['update_data']
        driv_obj = DriverExtraInfo.objects.filter(id = int(content[0][0])).first()
        try:
            img_url = ''
            if pod_img:
                # print('in image',pod_img)

                file_input = base64.b64decode(pod_img)

                
                

                session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                            aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

                s3 = session.resource('s3')
                cloudFilename = "media/pod/"+str(order_id)+'-'+str(order_job)+'-'+str(driver_name)+'-'+str(data['drop_id'])+'.jpg'
                s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=file_input, ACL='public-read')
                img_url = str(BUCKET_URL)+str(cloudFilename)
                print('img_url',img_url)

            drop_obj = Drop.objects.filter(id = data['drop_id']).update(actual_delivery_date_time = data['actual_delivery_date_time'],pod_image = img_url)
            time_drop_obj = Drop.objects.filter(id = data['drop_id']).values_list('actual_delivery_date_time')
            # psrint('QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ',str(time_drop_obj[0][0]))
            date = str(time_drop_obj[0][0])
            date = date.split('+')
            datetime_object = datetime.strptime(date[0], '%Y-%m-%d %H:%M:%S')

            old_format = '%Y-%m-%d %H:%M:%S'
            new_format = '%d/%m/%Y %I:%M %p'

            new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)
            # print(new_datetime_str)
            
            
            
            comment = '%s has been Delivered by %s on %s '%(order_job,driver_name, str(new_datetime_str))
            ord_his_obj = OrderHistory.objects.create(OrderID_id = order_id, Created_by_CarrierID_id = carr_id, comments = comment)
            # ord_his_obj = OrderHistory.objects.create(OrderID_id = order_id, Created_by_CustomerID_id = , comments = comment)
            noti_obj = Notification.objects.create(OrderID_id = order_id, created_by_driver_id = driv_obj.id , created_for_carrier_id = carr_id, comments = comment)
            context = {
                "data":"",
                "message":"success" 
            }   
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            print('kkkkkkkkkkkkkkkkkk',e)
            context = {
                            "data": '',
                            "message":'try again later'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)    