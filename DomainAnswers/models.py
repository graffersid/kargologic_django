from django.db import models
from DomainRelatedOrderExtraField.models import DomainRelatedOrderExtraField
from orders.models import Order
from custom_users.models import User
from customers.models import CustomerExtraInfo
from carriers.models import CarrierExtraInfo


class DomainAnswers(models.Model):
    OrderID                        = models.ForeignKey(Order, on_delete = models.CASCADE,null=True,blank=True)
    Created_by_CustomerID          = models.ForeignKey(CustomerExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    Created_by_CarrierID           = models.ForeignKey(CarrierExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    DomainRelatedOrderExtraFieldID = models.ForeignKey(DomainRelatedOrderExtraField, on_delete = models.CASCADE,null=True,blank=True)
    field_value                    = models.CharField(max_length = 200, null=True,blank=True)   
    created_DT                     = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT                     = models.DateTimeField(auto_now=True,null=True,blank=True) 
    is_active                      = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)
    
    def __str__(self):
        return '%s'%(self.OrderID)