from rest_framework import serializers
from DomainAnswers.models import DomainAnswers

from orders.serializers import OrderSerializer
from custom_users.serializers import UserSerializer
from customers.serializers import CustomerExtraInfoSerializer
from carriers.serializers import CarrierExtraInfoSerializer
from DomainRelatedOrderExtraField.models import DomainRelatedOrderExtraFieldSerializer
class DomainAnswersSerializer(serializers.ModelSerializer):
    OrderID = OrderSerializer(required=False)
    
    Created_by_CustomerID  = CustomerExtraInfoSerializer(required=False)
    Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)

    DomainRelatedOrderExtraFieldID = DomainRelatedOrderExtraFieldSerializer(required=False)
    class Meta:
        model  = DomainAnswers
        exclude = ('created_DT','updated_DT','is_active')