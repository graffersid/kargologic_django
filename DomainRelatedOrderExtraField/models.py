from django.db import models
from domain.models import Domain
class DomainRelatedOrderExtraField(models.Model):
    DomainID    = models.ForeignKey(Domain, on_delete = models.CASCADE,null=True,blank=True)
    
    field_name  = models.CharField(max_length = 200,null=True,blank=True)
    field_type  = models.CharField(max_length =200,null=True,blank=True)
    group_label = models.CharField(max_length = 100,null=True,blank=True)  
    
    created_DT  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT  = models.DateTimeField(auto_now=True,null=True,blank=True)  
    
    is_active   = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)
    def __str__(self):
        return str(self.DomainID)