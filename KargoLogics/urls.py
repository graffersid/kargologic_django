"""KargoLogics URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from custom_users.views import LogIn
from django.conf.urls import url, include
from render_index.views import index
urlpatterns = [
    url('backend/admin/', admin.site.urls),
    url('api/login', LogIn.as_view(), name='login'),
    
    url('carr/', include('carriers.urls')),
    
    url('cust/', include('customers.urls')),

    url('orderdetails/', include('OrderExtraField.urls')),

    url('driv/', include('drivers.urls')),

    url('truck/', include('TruckType.urls')),

    url('truck_categ/', include('TruckCategory.urls')),

    url('order_hisory/', include('OrderHistory.urls')),

    url('pickup/', include('Pickup.urls')),

    url('delivery/', include('Delivery.urls')),

    url('orders/', include('orders.urls')),

    url('order_attach/', include('OrderAttachment.urls')),

    url('user_role/', include('user_role.urls')),

    url('asset/', include('Asset.urls')),

    url('companysendrecei/', include('CompanySenderReceiver.urls')),

    url('companyconfig/', include('companyconfigure.urls')),

    url('comments/', include('OrderComment.urls')),

    url(r'^', index, name = 'index'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
