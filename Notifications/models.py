from django.db import models
from orders.models import Order
from customers.models import CustomerExtraInfo
from carriers.models import CarrierExtraInfo
from drivers.models import DriverExtraInfo
# Create your models here.
class Notification(models.Model):
    OrderID               = models.ForeignKey(Order, related_name='notify', on_delete = models.CASCADE,null=True,blank=True)
    created_by_carrier    = models.ForeignKey(CarrierExtraInfo, related_name = 'noti_carr',on_delete = models.CASCADE,null=True,blank=True)
    created_by_customer   = models.ForeignKey(CustomerExtraInfo, related_name = 'noti_cust',on_delete = models.CASCADE,null=True,blank=True)
    created_by_driver     = models.ForeignKey(DriverExtraInfo, related_name = 'noti_driv',on_delete = models.CASCADE,null=True,blank=True)
    created_for_carrier   = models.ForeignKey(CarrierExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    created_for_customer  = models.ForeignKey(CustomerExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    created_for_driver    = models.ForeignKey(DriverExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    comments              = models.CharField(max_length =1000,null=True,blank=True)
    is_read               = models.BooleanField(default = False)
    date_time_of_create   = models.DateTimeField(null=True,blank=True)
    is_active             = models.BooleanField(default=True)
    created_DT            = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT            = models.DateTimeField(auto_now=True,null=True,blank=True) 

    def __str__(self):
        return "%s %s"%(self.OrderID,self.id)