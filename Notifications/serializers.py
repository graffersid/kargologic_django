
from rest_framework import serializers
from .models import Notification


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        exclude = ('created_DT','updated_DT')



class DriverNotificationSerializer(serializers.ModelSerializer):
    order_status = serializers.SerializerMethodField()
    
    def get_order_status(self, obj):
        return obj.OrderID.StatusID.status_name
    class Meta:
        model = Notification
        exclude = ('created_DT','updated_DT')