from django.apps import AppConfig


class OrderattachmentConfig(AppConfig):
    name = 'OrderAttachment'
