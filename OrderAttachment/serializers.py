from rest_framework import serializers
from OrderAttachment.models import OrderAttachment

from orders.serializers import OrderSerializer
from custom_users.serializers import UserSerializer
from customers.serializers import CustomerExtraInfoSerializer
from carriers.serializers import CarrierExtraInfoSerializer
from drivers.serializers import DriverExtraInfoSerializer

from drf_extra_fields.fields import Base64FileField
class OrderAttachmentSerializer(serializers.ModelSerializer):
    # OrderID = OrderSerializer(required=False)
    # Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    Created_by_DriverID  = DriverExtraInfoSerializer(required=False)
    class Meta:
        model  = OrderAttachment
        fields = ('id','attachment_name','physical_path','Created_by_CarrierID','Created_by_CustomerID','Created_by_DriverID','created_DT','updated_DT')
        depth = 1

class DriverJobDetailAttachSerializer(serializers.ModelSerializer):
    # OrderID = OrderSerializer(required=False)
    # Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    #Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    #Created_by_DriverID  = DriverExtraInfoSerializer(required=False)
    class Meta:
        model  = OrderAttachment
        fields = ('attachment_name','physical_path')





class PDFBase64File(Base64FileField):
    ALLOWED_TYPES = ['pdf','img']

    def get_file_extension(self, filename, decoded_file):
            return 'img'

class OrderCreateAttachSerializer(serializers.ModelSerializer):
    # OrderID = OrderSerializer(required=False)
    # Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    #Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    #Created_by_DriverID  = DriverExtraInfoSerializer(required=False)
    
    image_read = PDFBase64File()
    class Meta:
        model  = OrderAttachment
        fields = ('attachment_name','physical_path','image_read')