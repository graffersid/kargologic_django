from django.conf.urls import url

from OrderAttachment import views

urlpatterns = [
   url('attach_pod/',views.DriverPODupload.as_view()),
   url('load_attachment/',views.OrderAttachment.as_view()),
   url('delete_attach/',views.DeleteOrderAtt.as_view()),
]
