from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from OrderAttachment.models import OrderAttachment as attachment
from KargoLogics.settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME, EMAIL_HOST_USER,AWS_S3_CUSTOM_DOMAIN,BUCKET_URL
from rest_framework.parsers import FileUploadParser
from rest_framework import authentication, permissions

import boto3
from boto3.session import Session


class DriverPODupload(APIView):
    def post(self, request):
        ord_id = request.GET['order_id']
        pod_file = request.FILES['pod_file']
        uploaded_file = pod_file.read()
        ord_obj = attachment.objects.create( OrderID_id = ord_id, physical_path = pod_file) 
        context = {
            "data":"",
            "message":"success"
        }
        return Response(context, status=status.HTTP_200_OK)

#######################################################

class OrderAttachment(APIView):
    permission_classes = (IsAuthenticated,)
    parser_class = (FileUploadParser,)
    def post(self, request):

        try:
            key = request.auth
            param = Token.objects.filter(key = key).values_list("param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        f = request.data['file']
        order_id = request.data['order_id']
        file_name = request.data['file_name']
        user_type = request.GET['user_type']
        pysical_path = BUCKET_URL+"orderattachment/"+str(f.name)
        if user_type == "carrier":
            att_obj = attachment.objects.create(OrderID_id = order_id, Created_by_CarrierID_id = param[0][0], physical_path = pysical_path, attachment_name = file_name)
        if user_type == "customer":
            att_obj = attachment.objects.create(OrderID_id = order_id, Created_by_CustomerID_id = param[0][0], physical_path = pysical_path, attachment_name = file_name)
        obj_url = attachment.objects.filter(OrderID_id = order_id).values_list("physical_path")
        
        
        fileToUpload = f
        cloudFilename ="orderattachment/"+fileToUpload.name

        session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                    aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
        s3 = session.resource('s3')
        s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=fileToUpload, ACL='public-read')

        context = {
            "data":{
                "physical_path":BUCKET_URL+str(cloudFilename),
                "attachment_name":str(fileToUpload.name)
                },
            "message":"success" 
        }

        return Response(context,status=status.HTTP_200_OK)
##########################################################################



class DeleteOrderAtt(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            param = Token.objects.filter(key = key).values_list("param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        data = request.data
        
        att_obj = attachment.objects.filter(id__in = data['att_id_list'])
        
        if att_obj != []:
            a = att_obj.delete()
        
        context = {
            "data":"",
            "message":"deleted"                   
        }
        return Response(context,status=status.HTTP_200_OK)