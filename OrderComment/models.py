from django.db import models
from orders.models import Order
from custom_users.models import User
from customers.models import CustomerExtraInfo
from carriers.models import CarrierExtraInfo
from drivers.models import DriverExtraInfo

class OrderComment(models.Model):
    OrderID              = models.ForeignKey(Order, related_name='comments',on_delete = models.CASCADE,null=True,blank=True)
    Created_by_CustomerID = models.ForeignKey(CustomerExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    Created_by_CarrierID  = models.ForeignKey(CarrierExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    Created_by_DriverID   = models.ForeignKey(DriverExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    date_time_of_create   = models.DateTimeField(null=True,blank=True)
    comment               = models.CharField(max_length = 500,null=True,blank=True)
    created_DT            = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT            = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active             = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)
    
    def __str__(self):
        return '%s (%s)'% (self.OrderID, self.id)
                    
    
    