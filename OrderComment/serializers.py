from rest_framework import serializers
from OrderComment.models import OrderComment

from orders.serializers import OrderSerializer
from custom_users.serializers import UserSerializer
from customers.serializers import CustomerExtraInfoSerializer
from carriers.serializers import CarrierExtraInfoSerializer
from rest_framework.reverse import reverse
from drivers.serializers import DriverSerializer

class OrderCommentSerializer(serializers.ModelSerializer):
    Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    Created_by_DriverID  = DriverSerializer(required=False) 
    
    # CustomUserID = UserSerializer(required=False)
    class Meta:
        model  = OrderComment
        fields = ('id','comment','Created_by_CustomerID','Created_by_CarrierID','Created_by_DriverID','created_DT','updated_DT','date_time_of_create')
 