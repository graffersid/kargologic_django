from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from KargoLogics.settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME, EMAIL_HOST_USER,AWS_S3_CUSTOM_DOMAIN,BUCKET_URL
from rest_framework.parsers import FileUploadParser
from rest_framework import authentication, permissions
from OrderComment.models import OrderComment
from OrderComment.serializers import OrderCommentSerializer
class OrderDetailsCommets(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            param = Token.objects.filter(key = key).values_list("param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        or_id = request.data['order_id']
        ordcom_obj = OrderComment.objects.filter(OrderID_id = or_id)
        serialz = OrderCommentSerializer(ordcom_obj, many = True)
        context ={
                "data":serialz.data,
                "message":"success"     
            }
        return Response(context,status=status.HTTP_200_OK)
