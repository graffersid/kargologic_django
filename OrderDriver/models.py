from django.db import models
from orders.models import Order
from custom_users.models import User
from drivers.models import DriverExtraInfo
from carriers.models import CarrierExtraInfo
from customers.models import CustomerExtraInfo
from status.models import Status
from Asset.models import Asset
class OrderDriver(models.Model):
    DriverID              = models.ForeignKey(DriverExtraInfo, related_name='driver_od',on_delete = models.CASCADE,null=True,blank=True)
    OrderID               = models.ForeignKey(Order,related_name='ord_driv_order', on_delete = models.CASCADE, null=True,blank=True)
    Created_by_CustomerID = models.ForeignKey(CustomerExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    Created_by_CarrierID  = models.ForeignKey(CarrierExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    order_status          = models.ForeignKey(Status,on_delete = models.CASCADE,null=True,blank=True)
    AssetID               = models.ForeignKey(Asset, on_delete = models.CASCADE,null=True,blank=True)
    order_enroute         = models.DateTimeField(null=True,blank=True)
    order_final_deliver   = models.DateTimeField(null=True,blank=True)
    quantity_taken        = models.CharField(max_length = 200, null=True, blank=True)
    capacity              = models.CharField(max_length =200,null=True,blank=True)
    address               = models.CharField(max_length = 500, null=True,blank=True)
    is_pickup             = models.BooleanField(default = True)
    longitude             = models.CharField(max_length = 500, null=True,blank=True)
    latitude              = models.CharField(max_length = 500, null=True,blank=True)
    expected_DT           = models.DateTimeField(null=True,blank=True)
    actual_TD             = models.DateTimeField(null=True,blank=True)
    
    created_DT            = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT            = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active             = models.BooleanField(default=True)
    is_delete             = models.BooleanField(default=False)
  
    def __str__(self):
        return '%s %s(%s)'%(self.OrderID,self.DriverID,self.id)
