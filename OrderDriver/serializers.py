from rest_framework import serializers
from OrderDriver.models import OrderDriver
from Asset.models import Asset
from AssetRelation.models import AssetRelation

from orders.serializers import OrderSerializer, CarrDrivOrderDetailsOrderSer
from custom_users.serializers import UserSerializer
from drivers.serializers import DriverExtraInfoSerializer, CarrDrivOrderDetailsDriverExtraSer, CarrGetOrderDetailsDriverInfoSerializer, TrackingDriverSerializer
from Asset.serializers import OrderDetailAssetSerializer
class OrderDriverSerializer(serializers.ModelSerializer):
    OrderID = OrderSerializer(required=False)
    Created_by_DriverID  = DriverExtraInfoSerializer(required=False)

    class Meta:
        model  = OrderDriver
        exclude = ('created_DT','updated_DT','is_active')


class AssignDriverSerializer(serializers.ModelSerializer):
    
    class Meta:
        model  = OrderDriver
        exclude = ('created_DT','updated_DT','is_active')

    def create(self, validated_data):
        order_id  = self.context.get("order_id")
        driv_id = validated_data["DriverID"]
        #capacity = validated_data["capacity"]
        order_driv_obj = OrderDriver.objects.create(OrderID_id = order_id, **validated_data) 
        return order_driv_obj


class CarrierDriverOrderDetailsSer(serializers.ModelSerializer):
    pickup_date_time = serializers.SlugRelatedField(
                                            many=True,
                                            read_only=True,
                                            source='order_driver_pick_related',
                                            slug_field='pickup_date_time')

    pickup_date_time_to = serializers.SlugRelatedField(
                                            many=True,
                                            read_only=True,
                                            source='order_driver_pick_related',
                                            slug_field='pickup_date_time_to')                                            

    delivery_date_time = serializers.SlugRelatedField(
                                            many=True,
                                            read_only=True,
                                            source='order_driver_drop_related',
                                            slug_field='delivery_date_time')

    delivery_date_time_to = serializers.SlugRelatedField(
                                            many=True,
                                            read_only=True,
                                            source='order_driver_drop_related',
                                            slug_field='delivery_date_time_to')
                        
    # assigned_quantity = serializers.SlugRelatedField(
    #                                         many=True,
    #                                         read_only=True,
    #                                         source='order_driver_pick_related',
    #                                         slug_field='assigned_quantity')

    OrderID = CarrDrivOrderDetailsOrderSer(required=False)
    #DriverID  = CarrDrivOrderDetailsDriverExtraSer(required=False)

    class Meta:
        model = OrderDriver
        fields = ("pickup_date_time",'pickup_date_time_to',"delivery_date_time",'delivery_date_time_to','OrderID')

class CarrGetOrderDetailsOrderDriver(serializers.ModelSerializer): 
    DriverID  = CarrGetOrderDetailsDriverInfoSerializer(required=False)
    AssetID   = OrderDetailAssetSerializer(required=False)
    class Meta:
        model  = OrderDriver
        fields = ('DriverID','AssetID')

class DriverDashboardSerializer(serializers.ModelSerializer):
    
    class Meta:
        model  = OrderDriver
        fields = ('OrderID','order_enroute','order_final_deliver','order_driver_pick_related','order_driver_drop_related')
        depth = 2

    def to_representation(self, instance):
        data = super(DriverDashboardSerializer, self).to_representation(instance)
        #print(data)
        sort_dic = {
            'extra_detail':{},
            'pickup':[],
            'delivery':[]
        }
        for key,val in data.items():
            if key == 'OrderID':
                for key1,val1 in val.items():
                    if key1 == 'id':
                        sort_dic['extra_detail']['order_id'] = val1

                    if key1 == 'job_id':
                        sort_dic['extra_detail']['job_no'] = val1

                    if key1 == 'StatusID':
                        for key2,val2 in val1.items():
                            if key2 == 'status_name':
                                sort_dic['extra_detail']['status'] = val2
                            
                    if key1 == 'CustomerCompanyID':
                        for key2,val2 in val1.items():
                            if key2 == 'id':
                                sort_dic['extra_detail']['customer_company_id'] = val2
                            if key2 == 'name':
                                sort_dic['extra_detail']['customer_company_name'] = val2
                            if key2 == 'mobile_num':
                                sort_dic['extra_detail']['customer_company_number'] = val2

                    if key1 == 'CarrierCompanyID':
                        for key2,val2 in val1.items():
                            if key2 == 'id':
                                sort_dic['extra_detail']['carrier_company_id'] = val2
                            if key2 == 'name':
                                sort_dic['extra_detail']['carrier_company_name'] = val2
                            if key2 == 'mobile_num':
                                sort_dic['extra_detail']['carrier_company_number'] = val2

        
            if key == 'order_driver_pick_related':
                for val in val:
                    sub_dic = {}
                    for key1,val1 in val.items():
                        if key1 == 'id':
                            dic = {"id":val1}
                            sub_dic.update(dic)            
                        if key1 == 'pickup_date_time':
                            dic = {"pickup_date_time":val1}
                            sub_dic.update(dic)
                        if key1 == 'pickup_date_time_to':
                            dic = {"pickup_date_time_to":val1}
                            sub_dic.update(dic)
                        if key1 == 'actual_pickup_date_time':
                            dic = {"actual_pickup_date_time":val1}
                            sub_dic.update(dic)        
                        if key1 == 'address':
                            dic = {"address":val1}
                            sub_dic.update(dic)

                        if key1 == 'assigned_quantity':
                            dic = {"assigned_quantity":val1}
                            sub_dic.update(dic)

                        if key1 == 'longitude':
                            dic = {"longitude":val1}
                            sub_dic.update(dic)

                        if key1 == 'latitude':
                            dic = {"latitude":val1}
                            sub_dic.update(dic)

                    sort_dic['pickup'].append(sub_dic)

            if key == 'order_driver_drop_related':
                for val in val:
                    sub_dic = {}
                    for key1,val1 in val.items():
                        if key1 == 'id':
                            dic = {"id":val1}
                            sub_dic.update(dic)            
                        if key1 == 'delivery_date_time':
                            dic = {"delivery_date_time":val1}
                            sub_dic.update(dic)
                        if key1 == 'delivery_date_time_to':
                            dic = {"delivery_date_time_to":val1}
                            sub_dic.update(dic)    
                        if key1 == 'actual_delivery_date_time':
                            dic = {"actual_delivery_date_time":val1}
                            sub_dic.update(dic)            
                        
                        if key1 == 'pod_image':
                            dic = {"pod_image":val1}
                            sub_dic.update(dic) 
                        
                        if key1 == 'address':
                            dic = {"address":val1}
                            sub_dic.update(dic)

                        if key1 == 'assigned_quantity':
                            dic = {"assigned_quantity":val1}
                            sub_dic.update(dic)    

                        if key1 == 'longitude':
                            dic = {"longitude":val1}
                            sub_dic.update(dic)

                        if key1 == 'latitude':
                            dic = {"latitude":val1}
                            sub_dic.update(dic)
                    sort_dic['delivery'].append(sub_dic)

        return sort_dic

class DriverJobDetailSerializer(serializers.ModelSerializer):
    
    class Meta:
        model  = OrderDriver
        fields = ('OrderID','AssetID','order_enroute','order_enroute','order_final_deliver','order_driver_pick_related','order_driver_drop_related')
        depth = 2

    def to_representation(self, instance):
        data = super(DriverJobDetailSerializer, self).to_representation(instance)
        #print(data)
        sort_dic = {
            'extra_detail':{},
            'pickup':[],
            'delivery':[],
            'order_enroute': '',
            'order_final_deliver': ''
        }
        for key,val in data.items():
            if key == 'order_enroute':
                sort_dic['order_enroute'] = val
            
            if key == 'order_final_deliver':
                sort_dic['order_final_deliver'] = val
                
            if key == 'OrderID':
                for key1,val1 in val.items():
                    if key1 == 'id':
                        sort_dic['extra_detail']['order_id'] = val1
                    if key1 == 'job_id':
                        sort_dic['extra_detail']['job_no'] = val1

                    if key1 == 'load_carried':
                        sort_dic['extra_detail']['load_carried'] = val1

                    if key1 == 'load_type':
                        sort_dic['extra_detail']['load_type'] = val1

                    if key1 == 'load_weight':
                        sort_dic['extra_detail']['load_weight'] = val1 
   
                    if key1 == 'length_load_dimen':
                        sort_dic['extra_detail']['length_load_dimen'] = val1

                    if key1 == 'width_load_dimen':
                        sort_dic['extra_detail']['width_load_dimen'] = val1            

                    if key1 == 'height_load_dimen':
                        sort_dic['extra_detail']['height_load_dimen'] = val1

                    if key1 == 'measure_unit':
                        sort_dic['extra_detail']['measure_unit'] = val1                            

                    if key1 == 'units':
                        sort_dic['extra_detail']['units'] = val1 

                    if key1 == 'hazardous':
                        sort_dic['extra_detail']['hazardous'] = val1 

                    if key1 == 'additional_note':
                        sort_dic['extra_detail']['additional_note'] = val1                                  

                    if key1 == 'StatusID':
                        for key2,val2 in val1.items():
                            if key2 == 'status_name':
                                sort_dic['extra_detail']['status'] = val2
                            
                    if key1 == 'CustomerCompanyID':
                        for key2,val2 in val1.items():
                            if key2 == 'id':
                                sort_dic['extra_detail']['customer_company_id'] = val2
                            if key2 == 'name':
                                sort_dic['extra_detail']['customer_company_name'] = val2
                            if key2 == 'mobile_num':
                                sort_dic['extra_detail']['customer_company_number'] = val2

                    if key1 == 'CarrierCompanyID':
                        for key2,val2 in val1.items():
                            if key2 == 'id':
                                sort_dic['extra_detail']['carrier_company_id'] = val2
                            if key2 == 'name':
                                sort_dic['extra_detail']['carrier_company_name'] = val2
                            if key2 == 'mobile_num':
                                sort_dic['extra_detail']['carrier_company_number'] = val2
        
            if key == "AssetID":
                if val:
                    for key1,val1 in val.items():
                        if key1 == 'asset_id':
                            sort_dic['extra_detail']['vehicle_type'] = val1
                        if key1 == 'registeration_no':
                            sort_dic['extra_detail']['vehicle_number'] = val1
                        
            if key == 'order_driver_pick_related':
                for val in val:
                    sub_dic = {'data_of':"pickup"}
                    for key1,val1 in val.items():
                        pick_id = ''
                        if key1 == 'id':
                            dic = {"id":val1}
                            pick_id = val1
                            sub_dic.update(dic)            
                        if pick_id:
                            assre_obj = AssetRelation.objects.filter(PickID_id = int(pick_id)).values_list('AssetID')
                            print('ass_obj',assre_obj)
                            ass_obj = Asset.objects.filter(id__in = assre_obj).values_list("asset_id","asset_type","registeration_no")
                            ass_list = []
                            for data_in in ass_obj:
                                print('data_in',data_in)
                                di = {"asset_id":data_in[0],"asset_type":data_in[1],"registeration_no":data_in[2]}
                                ass_list.append(di)
                            dic = {"asset_list":ass_list}
                            sub_dic.update(dic)                            
                        if key1 == 'pickup_date_time':
                            dic = {"pickup_date_time":val1}
                            sub_dic.update(dic)
                        if key1 == 'pickup_date_time_to':
                            dic = {"pickup_date_time_to":val1}
                            sub_dic.update(dic)
                        if key1 == 'actual_pickup_date_time':
                            dic = {"actual_pickup_date_time":val1}
                            sub_dic.update(dic)        
                        if key1 == 'address':
                            dic = {"address":val1}
                            sub_dic.update(dic)

                        if key1 == 'assigned_quantity':
                            dic = {"assigned_quantity":val1}
                            sub_dic.update(dic)

                        if key1 == 'longitude':
                            dic = {"longitude":val1}
                            sub_dic.update(dic)

                        if key1 == 'latitude':
                            dic = {"latitude":val1}
                            sub_dic.update(dic)

                        if key1 == 'sender':
                            dic = {"sender":val1}
                            sub_dic.update(dic)

                    sort_dic['pickup'].append(sub_dic)

            if key == 'order_driver_drop_related':
                for val in val:
                    sub_dic = {'data_of':"delivery"}
                    for key1,val1 in val.items():
                        drop = ''
                        if key1 == 'id':
                            dic = {"id":val1}
                            drop = val1
                            sub_dic.update(dic) 
                        if drop:
                            assre_obj = AssetRelation.objects.filter(DropID_id = int(drop)).values_list('AssetID')
                            print('ass_obj',assre_obj)
                            ass_obj = Asset.objects.filter(id__in = assre_obj).values_list("asset_id","asset_type","registeration_no")
                            ass_list = [] 
                            for data_in in ass_obj:
                                print('data_in',data_in)
                                di = {"asset_id":data_in[0],"asset_type":data_in[1],"registeration_no":data_in[2]}
                                ass_list.append(di)
                            dic = {"asset_list":ass_list}
                            sub_dic.update(dic)               
                        if key1 == 'delivery_date_time':
                            dic = {"delivery_date_time":val1}
                            sub_dic.update(dic)
                        if key1 == 'delivery_date_time_to':
                            dic = {"delivery_date_time_to":val1}
                            sub_dic.update(dic)    
                        if key1 == 'actual_delivery_date_time':
                            dic = {"actual_delivery_date_time":val1}
                            sub_dic.update(dic)            
                        
                        if key1 == 'pod_image':
                            dic = {"pod_image":val1}
                            sub_dic.update(dic) 
                        
                        if key1 == 'address':
                            dic = {"address":val1}
                            sub_dic.update(dic)

                        if key1 == 'assigned_quantity':
                            dic = {"assigned_quantity":val1}
                            sub_dic.update(dic)    

                        if key1 == 'longitude':
                            dic = {"longitude":val1}
                            sub_dic.update(dic)

                        if key1 == 'latitude':
                            dic = {"latitude":val1}
                            sub_dic.update(dic)
                                
                        if key1 == 'receiver':
                            dic = {"receiver":val1}
                            sub_dic.update(dic)

                    sort_dic['delivery'].append(sub_dic)

        return sort_dic


class AddMoreOrderDriverSerializer(serializers.ModelSerializer):
    # OrderID = OrderSerializer(required=False)
    # Created_by_DriverID  = DriverExtraInfoSerializer(required=False)

    class Meta:
        model  = OrderDriver
        exclude = ('created_DT','updated_DT','is_active')



class ProgressbarOrderDriverSerializer(serializers.ModelSerializer):
    # OrderID = OrderSerializer(required=False)
    # Created_by_DriverID  = DriverExtraInfoSerializer(required=False)
    AssetID = OrderDetailAssetSerializer(required=False)
    class Meta:
        model  = OrderDriver
        fields = ('id','order_enroute','order_final_deliver','AssetID')



class TrackingOrderDriverSerializer(serializers.ModelSerializer):
    # OrderID = OrderSerializer(required=False)
    DriverID  = TrackingDriverSerializer(required=False)

    class Meta:
        model  = OrderDriver
        fields = ('DriverID',)