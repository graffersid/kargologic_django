from django.shortcuts import render
from django.db import transaction

from OrderDriver.serializers import AssignDriverSerializer
# Create your views here.
from rest_framework.response import Response
from rest_framework import status


def assign_driver(driv_data, order_id):
    serialz = AssignDriverSerializer(data = driv_data['driver'], context = {"order_id":order_id}, many = True)
    if serialz.is_valid():
        serialz.save()
        context = {
            "message":"success",
            "exception":"",
            "data":""
            }
        return context
    else:
        #print(serialz.errors)
        context = {
            "message":"'driver' provided details are invalid",
            "exception":"",
            "data":""
            }
        return context