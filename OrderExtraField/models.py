from django.db import models
from OrderTemplate.models import OrderTemplate
from orders.models import Order
class OrderExtraField(models.Model):
    OrderID         = models.ForeignKey(Order,related_name='extra_fields', on_delete = models.CASCADE,null=True,blank=True)
    OrderTemplateID = models.ForeignKey(OrderTemplate, on_delete = models.CASCADE,null=True,blank=True)
    field_name      = models.CharField(max_length =200, null=True,blank=True)
    field_value     = models.CharField(max_length =200, null=True,blank=True)
    field_type      = models.CharField(max_length = 200, null=True,blank=True)
    created_DT      = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT      = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active       = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)
    def __str__(self):
        return '%s---%s(%s)' % (self.OrderID, self.field_name,self.id)