from rest_framework import serializers
from OrderExtraField.models import OrderExtraField

from orders.serializers import OrderDetailSerializer
from OrderTemplate.serializers import OrderTemplateSerializer, CarrGetOrderDetailTemplate

class OrderExtraFieldSerializer(serializers.ModelSerializer):
    OrderID = OrderDetailSerializer(required=False)
    OrderTemplateID = OrderTemplateSerializer(required=False)
    class Meta:
        model  = OrderExtraField 
        exclude = ('created_DT','updated_DT','is_active')


class CarrtoOrderCreateSerializer(serializers.ModelSerializer):
    OrderID = OrderDetailSerializer(required=False)
    OrderTemplateID = OrderTemplateSerializer(required=False)
    class Meta:
        model = OrderExtraField
        exclude = ('created_DT','updated_DT','is_active')


class CarrCreateOrderExtraFieldSerializer(serializers.ModelSerializer):
    # OrderID = OrderDetailSerializer(required=False)
    # OrderTemplateID = OrderTemplateSerializer(required=False)
    class Meta:
        model  = OrderExtraField 
        exclude = ('created_DT','updated_DT','is_active')  
    
    def create(self, validated_data):
        order_id = self.context.get("order_id")
        order_ex_obj = ''
        if self.context.get("template_id"):
            template_id = self.context.get("template_id")
            order_ex_obj = OrderExtraField.objects.create(OrderID_id =order_id, OrderTemplateID_id=template_id, **validated_data)
        else:
            order_ex_obj = OrderExtraField.objects.create(OrderID_id =order_id, **validated_data)
        return order_ex_obj

class ArrOrderExtraFieldSerializer(serializers.ModelSerializer):
    # OrderID = OrderDetailSerializer(required=False)
    # OrderTemplateID = OrderTemplateSerializer(required=False)
    class Meta:
        model  = OrderExtraField 
        exclude = ('created_DT','updated_DT')


class CarrGetOrderDetailExtraFields(serializers.ModelSerializer):
    # OrderID = OrderDetailSerializer(required=False)
    OrderTemplateID = CarrGetOrderDetailTemplate(required=False)
    class Meta:
        model  = OrderExtraField 
        fields = ('id','field_name','field_value','OrderTemplateID','created_DT','updated_DT')