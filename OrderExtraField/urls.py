from django.conf.urls import url

from OrderExtraField import views


urlpatterns = [
    url('details/', views.CarrOrderDetails.as_view(), name='order_details'),
]