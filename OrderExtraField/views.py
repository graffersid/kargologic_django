from django.shortcuts import render

from rest_framework import status
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework import status
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework import status

from OrderExtraField.serializers import OrderExtraFieldSerializer

from orders.models import Order
from status.models import Status
from custom_users.models import User
from OrderExtraField.models import OrderExtraField


# Create your views here.
class CarrOrderDetails(APIView):
    def get(self, request ):
        obj_order =  OrderExtraField.objects.filter(OrderID_id = int(request.GET["order_id"]))
        serialz   =  OrderExtraFieldSerializer(obj_order, many = True)
        return Response(serialz.data)