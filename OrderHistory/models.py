from django.db import models

from orders.models import Order
from status.models import Status
from custom_users.models import User
from customers.models import CustomerExtraInfo
from carriers.models import CarrierExtraInfo
from drivers.models import DriverExtraInfo

# Create your models here.

class OrderHistory(models.Model):
    OrderID               = models.ForeignKey(Order, related_name='order_history', on_delete = models.CASCADE,null=True,blank=True)
    StatusID              = models.ForeignKey(Status, on_delete = models.CASCADE,null=True,blank=True)
    Created_by_CustomerID = models.ForeignKey(CustomerExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    Created_by_CarrierID  = models.ForeignKey(CarrierExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    Created_by_DriverID   = models.ForeignKey(DriverExtraInfo,on_delete = models.CASCADE,null=True,blank=True)

    comments              = models.CharField(max_length=500,null=True, blank=False)
    is_read_carr          = models.BooleanField(default=False)
    is_read_cust          = models.BooleanField(default=False)
    is_read_driv          = models.BooleanField(default=False)
    is_active             = models.BooleanField(default=True)
    created_DT            = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT            = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_en_route           = models.BooleanField(default=False)
    is_delete             = models.BooleanField(default=False)

    def __str__(self):
        return "%s %s"%(self.OrderID,self.id)