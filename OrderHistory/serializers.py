from rest_framework import serializers
from OrderHistory.models import OrderHistory

from orders.serializers import OrderSerializer
from status.serializers import StatusSerializer
from custom_users.serializers import UserSerializer
from customers.serializers import CustomerExtraInfoSerializer
from carriers.serializers import CarrierExtraInfoSerializer
from drivers.serializers import DriverExtraInfoSerializer

class OrderHistorySerializer(serializers.ModelSerializer):
    OrderID = OrderSerializer(required=False)
    StatusID = StatusSerializer(required=False)
    Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    Created_by_DriverID  = DriverExtraInfoSerializer(required=False)

    class Meta:
        model  = OrderHistory
        exclude = ('created_DT','updated_DT','is_active')


class CarrNotifOrderHistorySerializer(serializers.ModelSerializer):
    # OrderID = OrderSerializer(required=False)
    # StatusID = StatusSerializer(required=False)
    # Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    # Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    # Created_by_DriverID  = DriverExtraInfoSerializer(required=False)

    class Meta:
        model  = OrderHistory
        fields = ('id','OrderID','comments','is_read_carr','created_DT','updated_DT')


class GetOrderCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model  = OrderHistory
        fields = ('id','comments','Created_by_CustomerID','Created_by_CarrierID','Created_by_DriverID','created_DT','updated_DT')

class EnrouteSerializer(serializers.ModelSerializer):
    class Meta:
        model  = OrderHistory
        fields = ('id','comments','created_DT','updated_DT')


class DriverNotificationSerializer(serializers.ModelSerializer): 
    class Meta:
        model  = OrderHistory
        fields = ('id','OrderID','comments','is_read_driv','created_DT','updated_DT')