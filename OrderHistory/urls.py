from django.conf.urls import url
from OrderHistory.views import *


urlpatterns = [
    url('carr_dash_hist/', CarrierNotification.as_view(), name='notification'),
    url('cust_dash_hist/', CustomerNotification.as_view(), name='cust-notification'),
    url('carr_notification_read/', CarrNotificationRead.as_view(), name='notification-read'),
    url('driver_notification/', DriverNotification.as_view(), name='notification-driver'),
    
]
