from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework import status
from OrderHistory.serializers import CarrNotifOrderHistorySerializer ,DriverNotificationSerializer
from OrderHistory.models import OrderHistory
# Create your views here.
class CarrierNotification(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)   

        order_his_obj = OrderHistory.objects.filter(Created_by_CarrierID_id = content[0][1], is_en_route = False, is_delete = False).order_by('-id')
        serializ = CarrNotifOrderHistorySerializer(order_his_obj, many = True)
        context = {
            "data":serializ.data,
            "message":"success"
        }
        return Response(context, status=status.HTTP_200_OK)

class CustomerNotification(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)   

        order_his_obj = OrderHistory.objects.filter(Created_by_CustomerID_id = content[0][1], is_en_route = False, is_delete = False).order_by('-id')
        print('order_his_obj',order_his_obj)
        serializ = CarrNotifOrderHistorySerializer(order_his_obj, many = True)
        context = {
            "data":serializ.data,
            "message":"success"
        }
        return Response(context, status=status.HTTP_200_OK)

class CarrNotificationRead(APIView):
    permission_classes = (IsAuthenticated,)
    def put(self, request):
        noti_id = request.data['notification_id']
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        ord_his = OrderHistory.objects.filter(id__in = noti_id).update(is_read_carr = True).order_by('-id')
        context = {
            "data":"",
            "success":"success"
        }
        return Response(context,status=status.HTTP_200_OK)

class DriverNotification(APIView): 
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


        order_his_obj = OrderHistory.objects.filter(Created_by_DriverID_id = int(content[0][0]), is_delete = False, is_active = True).order_by('-id')
        seria_obj = DriverNotificationSerializer(order_his_obj , many = True)
        context = {
            "data":seria_obj.data,
            "success":"success"
        }
        return Response(context,status=status.HTTP_200_OK)