from rest_framework import serializers
from OrderTemplate.models import OrderTemplate
from company.serializer import CompanySerializer
from custom_users.serializers import UserSerializer
from customers.serializers import CustomerExtraInfoSerializer
from carriers.serializers import CarrierExtraInfoSerializer


class OrderTemplateSerializer(serializers.ModelSerializer):
    # Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    # Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    # CustomerCompanyID = CompanySerializer(required=False)
    class Meta:
        model  = OrderTemplate
        exclude = ('created_DT','updated_DT','is_active')


class ArrOrderTemplate(serializers.ModelSerializer):
    Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    CustomerCompanyID = CompanySerializer(required=False)
    class Meta:
        model  = OrderTemplate
        exclude = ('id','created_DT','updated_DT')

class CarrGetOrderDetailTemplate(serializers.ModelSerializer):
    # Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    # Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    # CustomerCompanyID = CompanySerializer(required=False)
    class Meta:
        model  = OrderTemplate
        fields = ('template_name','created_DT','updated_DT')




