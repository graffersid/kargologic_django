from django.db import models
from orders.models import Order
from OrderDriver.models import OrderDriver
from CompanySenderReceiver.models import CompanySenderReceiver
from Asset.models import Asset
class Pick(models.Model):
    #orderDriver id is used to identify if the entry is for assigned driver or for create order
    #if order create pickup then orderdriverid is null and if pickup is for driver then orderID 
    Asset_id                = models.ForeignKey(Asset,related_name='aaset_pick',on_delete = models.CASCADE, null=True,blank=True)
    OrderID                 = models.ForeignKey(Order,related_name='order_pick_related',on_delete = models.CASCADE, null=True,blank=True)
    OrderDriverID           = models.ForeignKey(OrderDriver,related_name='order_driver_pick_related', on_delete = models.CASCADE, null=True,blank=True)
    sender                  = models.ForeignKey(CompanySenderReceiver, on_delete = models.CASCADE, null=True,blank=True)
    pickup_date_time        = models.DateTimeField(null=True,blank=True)
    pickup_date_time_to     = models.DateTimeField(null=True,blank=True)
    actual_pickup_date_time = models.DateTimeField(null=True,blank=True)
    address                 = models.CharField(max_length = 500, null=True,blank=True)
    longitude               = models.CharField(max_length = 200, null=True,blank=True)
    latitude                = models.CharField(max_length = 200, null=True,blank=True)
    assigned_quantity       = models.CharField(max_length = 200, null=True, blank=True)
    created_DT              = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT              = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active               = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)
    def __str__(self):
        return '%s-%s-%s'%(self.OrderID,self.OrderDriverID,self.id)