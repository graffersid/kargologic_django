from rest_framework import serializers
from Pickup.models import Pick
from drivers.models import DriverExtraInfo
from OrderDriver.models import OrderDriver
from status.models import Status
from orders.models import Order
from OrderHistory.models import OrderHistory
from OrderDriver.serializers import CarrGetOrderDetailsOrderDriver, AddMoreOrderDriverSerializer
from CompanySenderReceiver.serializers import OrderCreateComSenderReceiver
from Asset.serializers import AssetSerializer
from AssetRelation.serializers import OrderDetailAssetRelationSerializer
# from AssetRelation.serializers import AssetRelation
class ArrOrderPickSerializer(serializers.ModelSerializer):
    sender = OrderCreateComSenderReceiver(required=False)
    class Meta:
        model = Pick
        exclude = ('created_DT','updated_DT','is_active')


class AssignPickDriverSerializer(serializers.ModelSerializer):
    DriverID = serializers.IntegerField()
    sender = OrderCreateComSenderReceiver(required=False)
    asset  = AssetSerializer(many =True, required=False)
    ass_truck_id = serializers.IntegerField()
    asset_end = serializers.DateTimeField(required=False)
    class Meta:
        model = Pick
        exclude = ('created_DT','updated_DT','is_active')
        # depth = 1

class AssignPickDriverOrderDetailsSerializer(serializers.ModelSerializer):  
    OrderDriverID = CarrGetOrderDetailsOrderDriver(required=False)
    pick_asset = OrderDetailAssetRelationSerializer(required=False, many =True)
    class Meta:
        model = Pick
        fields = ('id','OrderDriverID','pick_asset','sender','pickup_date_time','pickup_date_time_to', 'actual_pickup_date_time', 'address', 'longitude', 'latitude', 'assigned_quantity', 'created_DT', 'updated_DT')
        depth = 1

class PickDriverOrderDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pick
        exclude = ('OrderID','OrderDriverID','is_active')
        depth = 1

class AddMorePickSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pick
        exclude = ('id','OrderID','OrderDriverID','created_DT','updated_DT','is_active')

    def create(self, validated_data):
        order_driver_id = self.context.get('order_driver_id')
        pick_obj = Pick.objects.create(OrderDriverID_id = order_driver_id, **validated_data)
        return 'success'


class AddMorePickSerializer2(serializers.ModelSerializer):
    OrderDriverID = AddMoreOrderDriverSerializer(required=False)
    tem_id = serializers.IntegerField()
    class Meta:
        model = Pick
        fields = ('tem_id','OrderDriverID','longitude','latitude','pickup_date_time','pickup_date_time_to','address','assigned_quantity')
        # exclude = ('OrderID','created_DT','updated_DT','is_active')


    def create(self, validated_data):
        print('validated_pick ', validated_data)    
        order_driver = validated_data.pop('OrderDriverID')
        st = Status.objects.filter(status_name = "pending").first()
        print('st from pickup',st)
        order_driver_obj = OrderDriver.objects.get_or_create(OrderID = order_driver['OrderID'],DriverID  = order_driver['DriverID'],order_status = st)
        if validated_data['tem_id'] != 0:
            print('in update')
            tem_id_pop = validated_data.pop('tem_id')
            pick_obj = Pick.objects.filter(id = tem_id_pop, OrderDriverID = order_driver_obj[0]).update(**validated_data)
        
        else:
            print('in create')
            tem_id_pop = validated_data.pop('tem_id')
            pick_obj = Pick.objects.create(OrderDriverID = order_driver_obj[0], **validated_data)
        
        status_obj = Status.objects.filter(status_name = "pending").values_list("id")
        order_id = order_driver['OrderID'].id
        ord_obj = Order.objects.filter(id = order_id).update(StatusID_id = status_obj[0][0])
        return 'success'