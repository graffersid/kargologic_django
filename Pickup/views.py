from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from Pickup.models import Pick
from OrderHistory.models import OrderHistory
from datetime import datetime
from Notifications.models import Notification
# Create your views here.
class DriverUpdatePick(APIView):
    permission_classes = (IsAuthenticated,)
    def post(sel, request):
        print('reqqqqqqqqqqqqqqqqqqqqq',request.data)
        try:
            order_id = request.data['order_id']
            carr_id  = request.data['carr_id']
            driver_name = request.data['driver_name']
            order_job   = request.data['order_job']            
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)    
            
        data = request.data['update_data']
        print('timeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee',data['actual_pickup_date_time'])
        pick_obj = Pick.objects.filter(id = data['pick_id']).update(actual_pickup_date_time = data['actual_pickup_date_time'])
        time_pick_obj = Pick.objects.filter(id = data['pick_id']).values_list('actual_pickup_date_time')

        print('DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD',str(time_pick_obj[0][0]))
        date = str(time_pick_obj[0][0])
        date = date.split('+')
        datetime_object = datetime.strptime(date[0], '%Y-%m-%d %H:%M:%S')

        old_format = '%Y-%m-%d %H:%M:%S'
        new_format = '%d/%m/%Y %I:%M %p'

        new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)
        print(new_datetime_str)


        comment = '%s has been picked up by %s on %s'%(order_job,driver_name, str(new_datetime_str))
        print('$$$$$$$$$$$$$$$$$$$$$$$$',comment)
        ord_his_obj = OrderHistory.objects.create(OrderID_id = order_id, Created_by_CarrierID_id = carr_id, comments = comment)
        # ord_his_obj = OrderHistory.objects.create(OrderID_id = order_id, Created_by_CustomerID_id = carr_id, comments = comment)
        notify_obj = Notification.objects.create(OrderID_id = order_id, created_for_carrier_id = carr_id, comments = comment)
        print('####################',ord_his_obj)
        context = {
            "data":"",
            "message":"success"
        }        
        return Response(context, status=status.HTTP_200_OK)