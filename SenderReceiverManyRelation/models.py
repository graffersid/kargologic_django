from django.db import models
from CompanySenderReceiver.models import CompanySenderReceiver

class SendReceiveManyRelation(models.Model):
    ComSendReceiverID = models.ForeignKey(CompanySenderReceiver,related_name='com_send_rec', on_delete = models.CASCADE, null=True, blank=True)
    attachment_name   = models.CharField(max_length = 100, null=True,blank=True)
    doc_upload        = models.URLField(null=True,blank=True)
    created_DT        = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT        = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_delete         = models.BooleanField(default=False)

    def __str__(self):
        return '%s (%s)' % (self.ComSendReceiverID,self.id)