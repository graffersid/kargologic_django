from rest_framework import serializers
from SenderReceiverManyRelation.models import SendReceiveManyRelation
from drf_extra_fields.fields import Base64FileField
from KargoLogics.settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME, EMAIL_HOST_USER,AWS_S3_CUSTOM_DOMAIN,BUCKET_URL
import boto3
from boto3.session import Session
class PDFBase64File(Base64FileField):
    ALLOWED_TYPES = ['pdf','img']

    def get_file_extension(self, filename, decoded_file):
            return 'img'


class CreatSenderCreateSerialz(serializers.ModelSerializer):
    image_read = PDFBase64File()
    class Meta:
        model = SendReceiveManyRelation
        exclude = ('created_DT','updated_DT')

    def create(self, validated_data):
        print('validated_data',validated_data)
        csr_id = self.context['csr_id']
        primary_contact = self.context['primary_contact']
        name = self.context['name']

        session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                            aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
    
        s3 = session.resource('s3')
        
        cloudFilename = "CompanySenderReceiver/"+(str(name)).replace(" ","_")+(str(primary_contact)).replace(" ","_")+(str(validated_data["attachment_name"])).replace(" ","_")
        s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=validated_data["image_read"], ACL='public-read')
        dp = str(BUCKET_URL+cloudFilename)
        SRM_obj = SendReceiveManyRelation.objects.create(ComSendReceiverID_id = csr_id ,attachment_name = validated_data["attachment_name"], doc_upload = dp)    
            
        
        return "success"


class Manager_docserializer(serializers.ModelSerializer):
    class Meta:
        model = SendReceiveManyRelation
        fields = ('id','attachment_name','doc_upload')
