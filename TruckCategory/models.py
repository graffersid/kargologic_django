from django.db import models

class TruckCategory(models.Model):
    categ_name          = models.CharField(max_length = 100, null=True,blank=True)
    capacity            = models.CharField(max_length = 100, null=True,blank=True)
    parent_id           = models.ForeignKey('self', on_delete = models.CASCADE, null=True, blank=True)    
    created_DT          = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT          = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active           = models.BooleanField(default=True)
    is_delete           = models.BooleanField(default=False)
    is_last             = models.BooleanField(default=False)
    def __str__(self):
        return '%s(%s)'%(self.categ_name,self.id)
        