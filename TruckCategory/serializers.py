from rest_framework import serializers
from TruckCategory.models import TruckCategory

class SubTruckCateSerializer(serializers.ModelSerializer):
    parent_id = serializers.SlugRelatedField(
        read_only=True,
        slug_field='categ_name'
     )
    class Meta:
        model = TruckCategory
        fields = ('id','categ_name','capacity','parent_id','is_last')

class ListTruckCateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TruckCategory
        fields = ('id','categ_name','capacity','is_last')


class InvitDriverTruckCateSerializer(serializers.ModelSerializer):
    parent_id = serializers.SlugRelatedField(
        read_only=True,
        slug_field='categ_name'
     )
    class Meta:
        model = TruckCategory
        fields = ('id','categ_name','capacity','parent_id','is_last')
