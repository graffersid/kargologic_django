from django.conf.urls import url

from . import views

urlpatterns = [
    url('truck_categ_list/', views.TruckCategoryList.as_view(), name='list-Truck'),
    url('truck_cat_csv_pload/', views.TruckCateCSVupload.as_view(), name='csv'),
]
