from django.shortcuts import render
from rest_framework.parsers import MultiPartParser,FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from TruckCategory.models import TruckCategory
from TruckCategory.serializers import ListTruckCateSerializer, InvitDriverTruckCateSerializer
import csv

class TruckCategoryList(APIView):

    def get(self, request):
        truc_Category_obj = TruckCategory.objects.all()
        serializ = InvitDriverTruckCateSerializer(truc_Category_obj, many = True)
        context = { 
            "data":serializ.data,
            "message":"success"
        }
        return Response(context,status=status.HTTP_200_OK)


class TruckCateCSVupload(APIView):
    parser_classes = [MultiPartParser,FileUploadParser]

    def post(self, request):
        data = request.data
        truck_cat = TruckCategory.objects.all()
        file_in = request.FILES['data_file']
        file_read = file_in.read()
        print('file_in type', type(file_in))
        # print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX ',file_in.read())
        with open('truck_cat.csv','wb') as f:
            f.write(file_read)
        with open('truck_cat.csv','r') as file:
            data_csv = csv.DictReader(file, delimiter=',')
            parent1 = ''
            parent2 = ''
            # print('######################')
            for data in data_csv:
                if data['parent1']:
                    if data['CAPACITY']:
                        truck_cat_obj = TruckCategory.objects.create(categ_name = data['parent1'],capacity = data['CAPACITY'], is_last = data['is_last'].capitalize())
                        parent1 = truck_cat_obj.id
                        # print('parent1',parent1)
                        # print('@@@@@@@@@@@@@@@@@@')
                        continue
                    else:
                        truck_cat_obj = TruckCategory.objects.create(categ_name = data['parent1'], is_last = data['is_last'].capitalize())
                        parent1 = truck_cat_obj.id
                if data['parent2']:
                    if data['CAPACITY']:
                        truck_cat_obj = TruckCategory.objects.create(categ_name = data['parent2'], parent_id_id = parent1, capacity = data['CAPACITY'], is_last = data['is_last'].capitalize())
                        parent2 = truck_cat_obj.id
                        # print('parent2',parent2)
                        # print('$$$$$$$$$$$$$$$$$$$$$$$$$$')
                        continue
                    else:
                        truck_cat_obj = TruckCategory.objects.create(categ_name = data['parent2'], parent_id_id = parent1, is_last = data['is_last'].capitalize())
                        parent2 = truck_cat_obj.id
                if data['parent3']:
                    if data['CAPACITY']:
                        truck_cat_obj = TruckCategory.objects.create(categ_name = data['parent3'], parent_id_id = parent2 , capacity = data['CAPACITY'], is_last = data['is_last'].capitalize())
                        # print('parent3',truck_cat_obj)
                        continue
                    else:
                        truck_cat_obj = TruckCategory.objects.create(categ_name = data['parent3'] , parent_id_id = parent2, is_last = data['is_last'].capitalize())
                        print('@@@@@@@@@@@@#####################$$$$$$$$$$$$$$$$$$$$$$$$')
                        continue

        return Response('at last')







