from django.db import models
from TruckCategory.models import TruckCategory
 
class TruckType(models.Model):
    truck_type          = models.CharField(max_length = 100, null=True,blank=True)
    truck_reg_num       = models.CharField(max_length = 100, null=True,blank=True)
    truck_category      = models.ForeignKey(TruckCategory, on_delete = models.CASCADE, null=True, blank=True)    
    capacity            = models.CharField(max_length = 100, null=True,blank=True)
    created_DT          = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT          = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active           = models.BooleanField(default=True)
    is_delete           = models.BooleanField(default=False)
    def __str__(self):
        return '%s(%s)'%(self.truck_type,self.id)