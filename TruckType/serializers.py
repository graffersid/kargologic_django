from rest_framework import serializers
from TruckCategory.serializers import SubTruckCateSerializer
from TruckType.models import TruckType

#from drivers.serializers import DriverExtraInfoSerializer, CarrDriverSearchSerializer, DrivCarrAddSerializer, CarriertoDriverListSerializer

class TruckTypeSerializer(serializers.ModelSerializer):
    #driver_extra_infoID = DriverExtraInfoSerializer(required=False)
    class Meta:
        model  = TruckType 
        exclude = ('created_DT','updated_DT','is_active')

class AddMoreDrivSerializer(serializers.ModelSerializer):
    class Meta:
        model  = TruckType
        fields = ('truck_type','capacity')

class CarrDrivSearchSerializer(serializers.ModelSerializer):
    #driver_extra_infoID = CarrDriverSearchSerializer(required=False)
    class Meta:
        model = TruckType
        exclude = ('id','created_DT','updated_DT','is_active')

class CarrDrivAddSerializer(serializers.ModelSerializer):
    #driver_extra_infoID = DrivCarrAddSerializer(required=False)
    class Meta:
        model   = TruckType
        fields = ('truck_type','driver_extra_infoID','capacity')

class CarrDrivtoListSerializer(serializers.ModelSerializer): 
    truck_category = SubTruckCateSerializer(required=False)
    class Meta:
        model  = TruckType
        exclude = ('created_DT','updated_DT','is_active','is_delete')


class CarrDrivOrderDetailsTruckTypeSer(serializers.ModelSerializer):
    truck_category = SubTruckCateSerializer(required=False)
    class Meta:
        model  = TruckType
        fields = ('truck_type','truck_category','truck_reg_num')

class CarrGetOrderDetailsTruckSerializer(serializers.ModelSerializer): 
    truck_category = SubTruckCateSerializer(required=False)
    class Meta:
        model  = TruckType
        fields = ('truck_type','truck_category','truck_reg_num')

class VehiclelistTruckTypeSerializer(serializers.ModelSerializer):
    truck_category = SubTruckCateSerializer(required=False) 
    class Meta:
        model  = TruckType
        fields = ('id','truck_category','capacity')

        