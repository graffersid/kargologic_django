from django.shortcuts import render
from rest_framework.parsers import MultiPartParser,FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
import csv
import io
import xlrd
import openpyxl
# Create your views here.


class FileVehicleData(APIView):
    parser_classes = [MultiPartParser,FileUploadParser]
    def post(self, request):
        data = request.data
        
        con = request.FILES['data_file']        
        wb = openpyxl.load_workbook(con)
        worksheet = wb["Vehicle Types"]
        print(worksheet)
        excel_data = list()
        # iterating over the rows and
        # getting value from each cell in row
        for row in worksheet.iter_rows():
            row_data = list()
            for cell in row:
                row_data.append(str(cell.value))
            excel_data.append(row_data)
        for inner_excel in excel_data:
            if inner_excel[0] is not None:
                serial_no = inner_excel[0]
                category_name = inner_excel[1]
                if inner_excel[2] is not None:
                    sub_category = inner_excel[2]
                if inner_excel[3] is not None:
                    gsm = inner_excel[3]
        return Response('final')