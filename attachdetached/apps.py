from django.apps import AppConfig


class AttachdetachedConfig(AppConfig):
    name = 'attachdetached'
