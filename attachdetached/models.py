from django.db import models
from Asset.models import Asset
# Create your models here.
class AttachDetached(models.Model):
    AssetID = models.ForeignKey(Asset,related_name='att_detta',on_delete = models.CASCADE, null =True, blank =True)
    schedule_from = models.DateTimeField(null=True,blank=True)
    schedule_to   = models.DateTimeField(null=True,blank=True)

    created_DT  = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT  = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active   = models.BooleanField(default=True)
    is_delete   = models.BooleanField(default=False)

    def __str__(self):
        return '%s-(%s)'%(self.AssetID,self.id)