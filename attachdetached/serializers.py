from rest_framework import serializers
from attachdetached.models import AttachDetached


class AttachDetachSerializer(serializers.ModelSerializer):
    class Meta:
        model = AttachDetached
        fields = ('id','AssetID','schedule_from','schedule_to')
