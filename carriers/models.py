from django.db import models
from custom_users.models import User
from user_role.models import UserRole
from domain.models import Domain

class CarrierExtraInfo(models.Model):
    CustomUserID  = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    somevalue     = models.CharField(max_length=100,null=True,blank=True)
    domainID      = models.ForeignKey(Domain, on_delete = models.CASCADE, null=True,blank=True)
    roleID        = models.ForeignKey(UserRole, on_delete = models.CASCADE, null=True,blank=True)
    is_active     = models.BooleanField(default=True)
    name          = models.CharField(max_length=100,null=True,blank=True)
    mobile_num    = models.CharField(max_length=100,null=True,blank=True)
    email         = models.EmailField(max_length=100,null=True,blank=True)    
    carr_image    = models.URLField(null=True,blank=True)
    created_DT    = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT    = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_delete                = models.BooleanField(default=False)
    def __str__(self):
        return '%s (%s)' % (self.CustomUserID,self.id)