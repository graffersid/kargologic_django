from rest_framework import serializers
from carriers.models import CarrierExtraInfo

from custom_users.serializers import UserSerializer

class CarrierExtraInfoSerializer(serializers.ModelSerializer):
    #CustomUserID = UserSerializer(required=False)
    class Meta:
        model = CarrierExtraInfo
        fields = ('id','name')
        

class CarrierSettingCarrierExtraInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarrierExtraInfo
        fields = ('id','name','mobile_num','email')

class CarrOrderDetailCarrierExtraInfoSerializer(serializers.ModelSerializer):
    #CustomUserID = UserSerializer(required=False)
    class Meta:
        model  = CarrierExtraInfo
        fields = ('name',)   