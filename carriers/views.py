import os
from django.shortcuts import render
from django.http import JsonResponse
from django.db import transaction
from rest_framework.parsers import FileUploadParser
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from django.core.mail import send_mail, EmailMultiAlternatives
from carriers.models import CarrierExtraInfo
from orders.models import Order
from status.models import Status
from custom_users.models import User
from company.models import Company
from drivers.models import DriverExtraInfo
from OrderComment.models import OrderComment
from OrderAttachment.models import OrderAttachment
from OrderDriver.models import OrderDriver
from OrderTemplate.models import OrderTemplate
from OrderExtraField.models import OrderExtraField
from Pickup.models import Pick
from Delivery.models import Drop

from company.serializer import CompanySerializer
from CarrierDriver.serializers import CarrierDriverListSerializer
from orders.serializers import OrderStatusCarrSerializer,OrderSerializer
from CompanyUser.serializers import CarrierSettingCompanyUserSerializer, CompanyListUserSerializer
from Pickup.serializers import AddMorePickSerializer2
from Delivery.serializers import AddMoreDropSerializer2
from TruckType.models import TruckType
from OrderDriver.models import OrderDriver
from CarrierDriver.models import CarrierDriver
from CustomerCarrier.models import CustomerCarrier
from CustomerCarrier.serializers import ListCustomerCarrierSerializer
from CompanyUser.models import CompanyUser
from domain.models import Domain
from user_role.models import UserRole
from attachdetached.models import AttachDetached
from django.db.models import Q
from OrderHistory.models import OrderHistory
from OrderHistory.serializers import EnrouteSerializer
from customers.models import CustomerExtraInfo
#from carriers.serializers import CarrierCreateSerializer
from datetime import datetime
from datetime import timedelta
from rest_framework.parsers import MultiPartParser,FileUploadParser
import pandas as pd

from django.contrib.auth import authenticate

from custom_users.models import User


# from KargoLogics.settings import AWS_ACCESS_KEY, AWS_SECRET_KEY, S3_BUCKET, REGION_NAME


from KargoLogics.settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME, EMAIL_HOST_USER,AWS_S3_CUSTOM_DOMAIN,BUCKET_URL,HERE_MAP_USER,HERE_MAP_KEY,HERE_MAP_APPID
from KargoLogics import settings
import boto3
from boto3.session import Session
import binascii
from KargoLogics.settings import SERVER_REACT_URL, DEFAULT_CUSTOMER
import requests
import json
from user_role import views as user_email
from AssetRelation.models import AssetRelation
from Asset.models import Asset
from configure.models import Configure
from companyconfigure.models import CompanyConfigure
from Notifications.models import Notification
from Notifications.serializers import NotificationSerializer
import io

from django.http.response import HttpResponse

from xlsxwriter.workbook import Workbook
import xlsxwriter
from django.http import StreamingHttpResponse
from drivers.views import ViewPaginatorMixin
from django.http import JsonResponse

class Carrier(APIView):
    @transaction.atomic
    def post(self, request, format='json'):      #create
        sid = transaction.savepoint()
        try:
            data = request.data    
            chech_em = user_email.email_check(data['email'])
            if chech_em['message'] == "":
                user_obj = User(
                                                                email       = (data['email']).lower(),
                                                                user_name   = data['user_name'],
                                                                phone       = data['phone']
                                                        )
                user_obj.set_password(data['password'])                                              
                user_obj.save()  
                send_email = user_obj.email 
                domain_obj = Domain.objects.filter(name = 'KargoLogic')
                
                user_role_obj = UserRole.objects.filter(role_name = "carrier")
                carr_obj = CarrierExtraInfo.objects.create(CustomUserID=user_obj,somevalue=data['ExtraInfo']['somevalue'],name = data['ExtraInfo']['user_name'], mobile_num = data['ExtraInfo']['phone'], email = data['ExtraInfo']['email'], domainID = domain_obj[0], roleID = user_role_obj[0])       
                carr_obj.save()
                
                com_obj = Company.objects.create(company_type = "carrier", name = data['company_name'], email = data['ExtraInfo']['email'])
                
                
                CompanyUser.objects.create(Created_by_CarrierID = carr_obj, CompanyID = com_obj)
                
                not_req_cus = Company.objects.filter(name = DEFAULT_CUSTOMER).first()
                cus_carr_not_req = CustomerCarrier.objects.create(CustomercompanyID = not_req_cus, CarriercompanyID = com_obj)
                order_table_ids = Configure.objects.filter(label = 'orders_table').first()
                
                
                time_slot_ids = Configure.objects.filter(label = 'time_slot').first()
                
                CompanyConfigure.objects.create(CompanyID_id = com_obj.id, ConfigureID_id = order_table_ids.id, configure_val = "Job Number, Created Date, Customer Name, Pick Up Time From, Pick Up Time To, Delivery Time From, Delivery Time To, Status" )
                CompanyConfigure.objects.create(CompanyID_id = com_obj.id, ConfigureID_id = time_slot_ids.id, configure_val ='30' )
                subject, from_email, to = 'Invitation to kargologic platform', settings.EMAIL_HOST_USER, send_email
                text_content = 'Welcome to KargoLogic'
                url = SERVER_REACT_URL+"/carrier/login"
                
                
                user_name= data['ExtraInfo']['user_name']
                html_content = """Hi {0}<br>,
                Thank you for signing up to use KargoLogic platform for your logistics operation.
                We're super excited in partnering with you to grow your business,
                improve delivery times and delight your customers.<br><br>
                Our platform is built with simplicity in mind.
                In three easy steps you are on your way to start creating delivery orders,
                allocating orders to the delivery drivers and tracking delivery orders in real-time.<br><br>
                Delight your customers by sharing real-time ETA's, 
                keep all permanently timestamped delivery related history and paperwork in one place 
                and access digital proof of delivery safely stored in the cloud. We are here to help you,
                so please do not hesitate to get in touch at enquiries@kargologic.com if you have any questions.<br><br> 
                You can access our friendly terms of use and privacy policy on www.kargologic.com.<br><br>
                In case you need to report an issue please email us on support@kargologic.com with detailed description of the 
                error or issue.<br><br>
                Thanks again for signing up today and welcome to KargoLogic!<br><br>
                To get started, <p><strong><a href='{1}'>Please Click here Login.</a></strong></p>  """.format(user_name, url)
                
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                
                
                context = {
                    "data": data['user_name'],
                    "message": 'Check Inbox and Spam folder for verification e-mail'
                }
                return Response(context, status=status.HTTP_200_OK)
            else:
                msg = chech_em['message']
                context = {
                    "data":"",
                    "message":"Already have an account as %s"%(msg)
                }
                return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            transaction.savepoint_rollback(sid)
            print("Carrier Create Exception: ",e)
            context = {
                "data": "",
                "message": 'invalid details or data not provided'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

class CarrInvSignUp(APIView):
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        # try:
        data = request.data
        user_obj, created = User.objects.get_or_create(email = (data['email']).lower(),  
                                                defaults = {"user_name":data['user_name'],
                                                            "phone":data['phone']}) 
        if created == False:
            context = {
                "data":"",
                "message":"This user already completed SignUp"
            }
            return Response(context,status=status.HTTP_200_OK)          
        
        user_obj.set_password(data['password'])
        user_obj.save()
        domain_obj = Domain.objects.filter(name = 'KargoLogic')
        user_role_obj = UserRole.objects.filter(role_name = "carrier")
        carr_ = CarrierExtraInfo.objects.filter(email = (data['email']).lower())
        carr_.update(CustomUserID=user_obj, name = data['user_name'], mobile_num = data['phone'], email = (data['email']).lower(), domainID = domain_obj[0], roleID = user_role_obj[0])        
        
        
        
        company_user_obj = CompanyUser.objects.filter(Created_by_CarrierID = carr_[0])
        if company_user_obj or (data['cust_com_id'] == ""):
            com_id = company_user_obj[0].CompanyID.id            
            carr_com_obj = Company.objects.filter(id = com_id).update(company_type = 'carrier', name = data['carr_com_name'], mobile_num = data['phone'], location = data['address1'],location2 = data['address2'],pin = data['pin'],country = data['country'])
            not_req_cus = Company.objects.filter(name = DEFAULT_CUSTOMER).first()
            cus_carr_not_req = CustomerCarrier.objects.get_or_create(CustomercompanyID = not_req_cus, CarriercompanyID_id = com_id)
            
            order_table_ids = Configure.objects.filter(label = 'orders_table').first()
            time_slot_ids = Configure.objects.filter(label = 'time_slot').first()
            CompanyConfigure.objects.create(CompanyID_id = com_id, ConfigureID_id = order_table_ids.id, configure_val = "Job Number, Created Date, Customer Name, Pick Up Time From, Pick Up Time To, Delivery Time From, Delivery Time To, Status" )
            CompanyConfigure.objects.create(CompanyID_id = com_id, ConfigureID_id = time_slot_ids.id, configure_val ='30' )
            context = {
                "data": data['user_name'],
                "message": 'account confirmed successfully'
            }
            return Response(context, status=status.HTTP_200_OK)
        
        elif (data['cust_com_id'] != ""):
            company_obj = Company.objects.create(company_type = 'carrier', name = data['carr_com_name'], mobile_num = data['phone'], location = data['address1'],location2 = data['address2'],pin = data['pin'],country = data['country'])
            carr_com_obj = CustomerCarrier.objects.create(CustomercompanyID_id = data['cust_com_id'], CarriercompanyID = company_obj)
            not_req_cus = Company.objects.filter(name = DEFAULT_CUSTOMER).first()
            cus_carr_not_req = CustomerCarrier.objects.get_or_create(CustomercompanyID = not_req_cus, CarriercompanyID_id = com_id)
            
            order_table_ids = Configure.objects.filter(label = 'orders_table').first()
            time_slot_ids = Configure.objects.filter(label = 'time_slot').first()
            CompanyConfigure.objects.create(CompanyID_id = company_obj.id, ConfigureID_id = order_table_ids.id, configure_val = "Job Number, Created Date, Customer Name, Pick Up Time From, Pick Up Time To, Delivery Time From, Delivery Time To, Status" )
            CompanyConfigure.objects.create(CompanyID_id = company_obj.id, ConfigureID_id = time_slot_ids.id, configure_val ='30' )

            context = {
                "data": data['user_name'],
                "message": 'account confirmed successfully'
            }
            return Response(context, status=status.HTTP_200_OK)
        # except Exception as e:
        #     transaction.savepoint_rollback(sid)
        #     context = {
        #         "data": str(e),
        #         "message": 'Exception'
        #     }
        #     return Response(context,status=status.HTTP_400_BAD_REQUEST)
class LogIn(APIView):
    @transaction.atomic
    def post(self, request, format='json'):
        sid = transaction.savepoint()
        try:
            user = authenticate(email = (request.data['email']).lower(), password = request.data['password'])
        
            if user is not None:
                try:
                    
                    # user_obj = Token.objects.filter(user=user).first()
                    # if user_obj:
                    #     user_obj.delete()
                    carrExtra_obj = CarrierExtraInfo.objects.filter(CustomUserID = user.id)
        
                    ob = CompanyUser.objects.filter(Created_by_CarrierID = carrExtra_obj)
        
                    carr_com_id = ((ob.values('CompanyID'))[0])['CompanyID']
        
                    com_id   = Company.objects.filter(id = carr_com_id).values('id')[0]['id']
        
                    com_name   = Company.objects.filter(id = carr_com_id).values('name')[0]['name']
        
                    com_address = Company.objects.select_related().filter(id = carr_com_id).values('location',"location2",'pin','country')
                    
        
                    token = Token.objects.get_or_create(user=user, domain_id = carrExtra_obj.values('domainID')[0]['domainID'], role_id = carrExtra_obj.values('roleID')[0]['roleID'], content_id = carr_com_id, param = carrExtra_obj.values('id')[0]['id'])
                    image_name = carrExtra_obj.values('carr_image')[0]['carr_image']
        
                    li =[]
                    obj_orders =  Order.objects.filter(CarrierCompanyID = carr_com_id, is_delete = False)
                except Exception as e:
                    transaction.savepoint_rollback(sid)
                    print('Carrier login object error',e)
                    context = {
                                "message":'Carrier Login Error',
                                "data": ''
                            }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)
                try:
                    context = {
                        "token": str(token[0]),
                        "email": (request.data['email']).lower(),
                        "carr_id":carrExtra_obj.values('id')[0]['id'],
                        "carrier_name":carrExtra_obj.values('name')[0]['name'],
                        "carr_phone":carrExtra_obj.values('mobile_num')[0]['mobile_num'],
                        "carr_image":image_name,
                        "carr_com_id":com_id,
                        "carrier_company_name":com_name,
                        "address1":com_address[0]['location'],
                        "address2":com_address[0]['location2'],
                        "pin":com_address[0]['pin'],
                        "country":com_address[0]['country'],
                        "message" : 'LoggedIn Successfully'
                    }
                    return JsonResponse(context,status=status.HTTP_200_OK)
                except Exception as e:
                    transaction.savepoint_rollback(sid)
                    print('obj_orders loop error',e)
                    context = {
                            "data":'',
                            "message":'Error'
                    }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)
            else:
                transaction.savepoint_rollback(sid)
                context = {
                    "data": '',
                    "message": 'Invalid Credentials'
                    }
                return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('user authentication obj error',e)
            context = {
                            "data": '',
                            "message":'Error email or password not provided properly'
                    }
            return Response(context,status=status.HTTP_200_OK)




def generate_key():
    return binascii.hexlify(os.urandom(20)).decode()

class CarrForgetPassword(APIView):
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            request_email = (request.data['email']).lower()
            carr_extr_obj = CarrierExtraInfo.objects.filter(email = request_email).first()
            
            user_chk_email = User.objects.filter(email = request_email).first()
            
            if carr_extr_obj and user_chk_email:
                token_key = generate_key()
                user_obj = User.objects.filter(email = request_email).update(custom_key = token_key)
            
                subject, from_email, to = "Request to reset the password", settings.EMAIL_HOST_USER, request_email
                text_content = 'Welcome to KargoLogic'
                url = str(SERVER_REACT_URL)+"/carrier/recover-password/?email=%s&token=%s"%(request_email,token_key)
                
                user_name = user_chk_email.user_name
                html_content = "<p> Hi {0} , it seems like you have requested to reset your password to access KargoLogic portal. If you didn't initiate this request simply ignore this message.</p><p>If you need to reset your password,<strong><a href='{1}'>click here to select your new password.</a></strong></p>".format(user_name, url)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                context = {
                "data":"",
                "message":"success"
                }
                return Response(context,status=status.HTTP_200_OK)

            elif (carr_extr_obj) and (user_chk_email == None):
                subject, from_email, to = 'Password reset request', settings.EMAIL_HOST_USER, request_email
                text_content = 'Welcome to KargoLogic'
                url = SERVER_REACT_URL+"/carriersignup/?email="+str(request_email)+"&cust_com_id="+str("None")+"&cust_com_name="+str("None")
                html_content = "You are an already invited user and you just need to complete signup then you will be able to reset your password. <br><br> Following url will help you to fill signup form: <br> %s "%(url)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                se = msg.send()
                if se:
                    context = {
                    "data":"",
                    "message":"success"
                    }
                    return Response(context,status=status.HTTP_200_OK)

                else:
                    transaction.savepoint_rollback(sid)
                    context = {
                        "data":"",
                        "message":"fail"
                    }
                    return Response(context,status=status.HTTP_200_OK)
            else:
                transaction.savepoint_rollback(sid)
                context = {
                    "data":"",
                    "message":"fail"
                }
                return Response(context,status=status.HTTP_200_OK) 
        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('something went wroung',e)
            context = {
                    "data":'',
                    "message":'Error'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class CarrResetPassword(APIView):
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            
            token      = request.data['token']
            new_pass   = request.data['password']
            user_email = (request.data['email']).lower()
            user_obj = User.objects.filter(email = user_email).first()
            internal_key = user_obj.custom_key
            if internal_key == token:
                user_pass_obj = User.objects.get(id = user_obj.id)
                user_pass_obj.set_password(new_pass)
                user_pass_obj.save()
                context = {
                    "data":"",
                    "message":"success"
                }
                return Response(context, status=status.HTTP_200_OK)
            else:
                transaction.savepoint_rollback(sid)
                context = {
                    "data":"",
                    "message":"fail"
                }
                return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('something went wroung',e)
            context = {
                    "data":'',
                    "message":'Error'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class CustomerCompanyList(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            
            customer_com_ = CustomerCarrier.objects.filter(CarriercompanyID_id = int(content[0][0])).values_list("CustomercompanyID")
            
            
            company_obj = Company.objects.filter(id__in = customer_com_ )
            
            company_user_obj = CompanyUser.objects.filter(CompanyID_id__in = customer_com_)
            
            serializ = CompanyListUserSerializer(company_user_obj, many = True)                                #CompanySerializer(company_obj, many = True)
            
            return Response(serializ.data)

        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)

"""carrier can get all order with passing all status param and can get
     order by status giving status name"""
class StatusResult(APIView ,ViewPaginatorMixin ):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        order_ins = ''
        try:
            if request.GET['status'] and request.GET['status'] != 'all' and request.GET['status'] != 'draft':
                try:
                    status_id = Status.objects.filter(status_name = request.GET['status']).values('id')[0]['id']
                    order_ins = Order.objects.filter(CarrierCompanyID = int(content[0][0]) , StatusID_id = status_id, is_delete = False, is_active = True).order_by('-id')
                except Exception as e:
                    print('error in status_id object or order_ins object',e)
                    context = {
                            "data": '',
                            "message":'error may be in "status" param key or in its value or make sure user has Carrier company'
                    }
                    return Response(context, status=status.HTTP_400_BAD_REQUEST)
            elif request.GET['status'] and request.GET['status'] == 'all':
                order_ins = Order.objects.filter(CarrierCompanyID = int(content[0][0]), is_delete = False).order_by('-id')

            elif request.GET['status'] and request.GET['status'] == 'draft':    
                order_ins = Order.objects.filter(CarrierCompanyID = int(content[0][0]), is_delete = False, is_active = False).order_by('-id')
            try:
                serialz = OrderStatusCarrSerializer(order_ins, many =True)

            except Exception as e:
                print('Serializer object initialization error')
                context = {
                                "data": '',
                                "message":'some internal error'
                        }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)    
                
            # try:
            #     # print('int(content[0][1])',(content))
            #     ord_obj_id_lst = Order.objects.filter(CarrierCompanyID_id = int(content[0][0]), is_delete = False).values_list('id')
            #     # print('ord_obj_id_lst',ord_obj_id_lst)
            #     order_driver_id_list = OrderDriver.objects.filter(OrderID_id__in = ord_obj_id_lst, is_delete = False).values_list('id')
            #     # print('order_driver_id_list',order_driver_id_list)
            #     delivery_obj = Drop.objects.filter(OrderDriverID_id__in = order_driver_id_list, is_delete = False).values_list('id','OrderDriverID','delivery_date_time_to','actual_delivery_date_time')
            #     # print('delivery_obj',delivery_obj)
                
            #     li = []
                
            #     for i in delivery_obj:
            #         order_dot = {}
            #         if i[3] and i[2]:
            #             order_driver_obj = OrderDriver.objects.filter(id = i[1]).first()
            #             od_id = order_driver_obj.OrderID.id
            #             # print('order id',order_driver_obj.OrderID)
            #             dot =  i[3]-i[2]
            #             # print('dot',dot)

            #             order_dot['order_id'] = od_id
            #             order_dot['dot'] = dot
            #             li.append(order_dot)

            #     # print('---------',li)

            #     oid = []
            #     final_dot = []
            #     for j in li:
            #         dics = {
            #             "dot":[]
            #         }
            #         if j['order_id']:
            #             if j['order_id'] not in oid:
            #                 oid.append(j['order_id'])
            #                 dics['ord_id'] = j['order_id']
            #                 for jt in li:
            #                     if jt['order_id'] == j['order_id']:
            #                         dics['dot'].append(jt['dot'])
                    
            #         final_dot.append(dics)
                            
                
                
                
            #     sort_final = []
            #     for aq in final_dot:
            #         if aq['dot'] == []:
            #             continue
            #         else:
            #             sort_final.append(aq)
            #     # print('sort_final',sort_final)
            #     # print('------------------------------------')
                
            #     ord_per_list = []
            #     for qw in sort_final:
            #         tcount = 0
            #         delaycount = 0
                    
            #         per_ord_dic = {}
            #         for tm in qw['dot']:
            #             tcount = tcount+1

            #             if tm >= timedelta(0):
            #                 # print('tm',tm)
            #                 delaycount = delaycount+1
            #         ratio_by_total = 100/tcount
            #         perc = 100 - ratio_by_total*delaycount
            #         # print('perc',perc)
            #         ordr_id = qw['ord_id']
            #         per_ord_dic['order_id'] = ordr_id
            #         per_ord_dic['perc'] = perc
            #         ord_per_list.append(per_ord_dic)
            #     # print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            #     # print('ord_per_list',ord_per_list)

            context = {
                "order_list":serialz.data,
                "ord_per_list":[], #ord_per_list,
                "message":"success"
            }
            return Response(context,status=status.HTTP_200_OK)
            # pg = 1
            # if request.GET['pg']:
            #     pg = request.GET['pg']
            # return JsonResponse({"resources": self.paginate(serialz.data, pg, 8)})
            # except Exception as e:
            #     print('Excepion',e)
            #     context = {
            #                     "data": '',
            #                     "message":'some internal error'
            #             }
            #     return Response(context,status=status.HTTP_400_BAD_REQUEST)   
            
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)


class CarrOrderDelete(APIView):
    @transaction.atomic
    def put(self, request):
        sid = transaction.savepoint()
        try:
            order_id_list = request.data['orderlist']
        except Exception as e:
            print('key error "orderlist"',e)
            context = {
                        "data": '',
                        "message":'key error orderlist'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            order_obj = Order.objects.filter(id__in = order_id_list).update(is_delete = True)
            attach_obj = OrderAttachment.objects.filter(OrderID_id__in = order_id_list).update(is_delete = True)
            comment_obj = OrderComment.objects.filter(OrderID_id__in = order_id_list).update(is_delete = True)
            order_extra_field_obj = OrderExtraField.objects.filter(OrderID_id__in = order_id_list).update(is_delete = True)
            pick_obj = Pick.objects.filter(OrderID_id__in = order_id_list).update(is_delete = True)
            drop_obj = Drop.objects.filter(OrderID_id__in = order_id_list).update(is_delete = True)
            order_hist_obj = OrderHistory.objects.filter(OrderID_id__in = order_id_list).update(is_delete = True)
            order_driver_obj = OrderDriver.objects.filter(OrderID_id__in = order_id_list).update(is_delete = True)
            context = {
            "message":"success",    
            "data":""
            }
            return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            print('object error',e)
            transaction.savepoint_rollback(sid)
            context = {
            "message":"Error: make sure orderlist values exist in database ",    
            "data":""
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

class CarrDriverDelete(APIView):
    @transaction.atomic
    def put(self, request):
        sid = transaction.savepoint()
        try:
            driver_id_list = request.data['driver_list']
        except Exception as e:
            print('key error "driver_list"')
        try:
            carr_driv_obj = CarrierDriver.objects.filter(DriverID_id__in = driver_id_list).update(is_delete = True)
            order_driver_obj = OrderDriver.objects.filter(DriverID_id__in = driver_id_list).update(is_delete = True)
            driverExtraInfo_obj = DriverExtraInfo.objects.filter(id__in = driver_id_list).update(is_delete = True)
            context = {
                "message":"driver deleted",    
                "data":""
            }
            return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('object error',e)
            context = {
            "message":"Error: make sure driver_list values exist in database ",    
            "data":""
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class OrderCount(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:    
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
            
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            
            st_pending = Status.objects.filter(status_name = "pending")
        
            st_cancelled = Status.objects.filter(status_name = "cancelled")
            st_process = Status.objects.filter(status_name = "in process")
            st_unassigned = Status.objects.filter(status_name = "unassigned")
            st_delivered = Status.objects.filter(status_name = "delivered")
        
        except Exception as e:
            print('status object error',e)
            context = {
                    "data": '',
                    "message":'internal error'
                }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
        
            all_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), is_delete = False).count()
            
            
            pending_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), StatusID = st_pending, is_delete = False, is_active = True).count()
            
            cancell_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), StatusID = st_cancelled, is_delete = False , is_active = True).count()
            
            process_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), StatusID = st_process, is_delete = False , is_active = True).count()
            
            unassigned_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), StatusID = st_unassigned, is_delete = False , is_active = True).count()
            
            delivered_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), StatusID = st_delivered, is_delete = False , is_active = True).count()
            
            draft = Order.objects.filter(CarrierCompanyID = int(content[0][0]),is_delete = False, is_active =False ).count()
        
        

       
            dic = {
                "all":all_order_count,
                "pending":pending_order_count,
                "cancelled":cancell_order_count,
                "unassigned":unassigned_order_count,
                "process":process_order_count,
                "delivered":delivered_order_count,
                "draft":draft
                }   
            return Response(dic,status=status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)



class OrderNoByType(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:    
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
            
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            
            st_pending = Status.objects.filter(status_name = "pending")
        
            st_cancelled = Status.objects.filter(status_name = "cancelled")
            st_process = Status.objects.filter(status_name = "in process")
            st_unassigned = Status.objects.filter(status_name = "unassigned")
            st_delivered = Status.objects.filter(status_name = "delivered")
        
        except Exception as e:
            print('status object error',e)
            context = {
                    "data": '',
                    "message":'internal error'
                }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
        
            all_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), is_delete = False).values_list('id','job_id')
            
            
            pending_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), StatusID = st_pending, is_delete = False, is_active = True).values_list('id','job_id')
            
            cancell_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), StatusID = st_cancelled, is_delete = False , is_active = True).values_list('id','job_id')
        
            process_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), StatusID = st_process, is_delete = False , is_active = True).values_list('id','job_id')
            
            unassigned_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), StatusID = st_unassigned, is_delete = False , is_active = True).values_list('id','job_id')
            
            delivered_order_count = Order.objects.filter(CarrierCompanyID = int(content[0][0]), StatusID = st_delivered, is_delete = False , is_active = True).values_list('id','job_id')
            
            draft = Order.objects.filter(CarrierCompanyID = int(content[0][0]),is_delete = False, is_active =False ).values_list('id','job_id')
        
        

       
            dic = {
                "all":all_order_count,
                "pending":pending_order_count,
                "cancelled":cancell_order_count,
                "unassigned":unassigned_order_count,
                "process":process_order_count,
                "delivered":delivered_order_count,
                "draft":draft
                }   
            return Response(dic,status=status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)


            
class DriverStatusCount(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()

        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            st_pending_driver = Status.objects.filter(status_name__icontains = "driver pending")
            st_added_driver = Status.objects.filter(status_name__icontains ="driver added" )
        except Exception as e:
            print('status object error',e)
            context = {
                        "data": '',
                        "message":'internal error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            all_carr_driv_obj_count  = CarrierDriver.objects.filter(CarrierCompanyID_id = int(content[0][0]), is_delete = False).count()
            pending_carr_driv_obj_count  = CarrierDriver.objects.filter(CarrierCompanyID_id = int(content[0][0]), is_delete = False, status = st_pending_driver).count()
            added_carr_driv_obj_count  = CarrierDriver.objects.filter(CarrierCompanyID_id = int(content[0][0]), is_delete = False, status = st_added_driver).count()
        except Exception as e:
            print('CarrierDriver object error',e)
            context = {
                        "data": '',
                        "message":'internal error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        dic = {
            "all_driver":all_carr_driv_obj_count,
            "added_driver":added_carr_driv_obj_count,
            "pending_driver":pending_carr_driv_obj_count
        }
        return Response(dic)


class CarrFilters(APIView):                                                     #carrier listing
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:
            if request.GET['filter']=='customer':
                obj_create = Order.objects.filter(CustomerCompanyID = request.GET["cust_id"], CarrierCompanyID = request.GET["carr_id"], is_delete = False)
                serialz    =  OrderStatusCarrSerializer(obj_create, many = True)
                return Response(serialz.data)
            
            elif request.GET["filter"]=='date':
                obj_create = Order.objects.filter(created_DT = request.GET["cre_dt"], CarrierCompanyID = request.GET["carr_id"], is_delete = False)
                serialz    =  OrderStatusCarrSerializer(obj_create, many = True)
                return Response(serialz.data)

            elif request.GET["filter"]=='pickup':
                obj_create = Order.objects.filter(expected_pickup_DT = request.GET["pic_dt"], CarrierCompanyID = request.GET["carr_id"])
                serialz    =  OrderStatusCarrSerializer(obj_create, many = True)
                return Response(serialz.data)

            elif request.GET["filter"]=='delivery':
                obj_create = Order.objects.filter(expected_delivery_DT = request.GET["del_dt"], CarrierCompanyID = request.GET["carr_id"])
                serialz    =  OrderStatusCarrSerializer(obj_create, many = True)
                return Response(serialz.data)

            elif request.GET["filter"]=='status':
                obj_create = Order.objects.filter(StatusID = request.GET["sta_id"], CarrierCompanyID = request.GET["carr_id"])
                serialz    =  OrderStatusCarrSerializer(obj_create, many = True)
                return Response(serialz.data) 

        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)


def search_name(q_,carr_id):
    obj_cus_carr = CustomerCarrier.objects.filter(CarriercompanyID = carr_id).values_list("CustomercompanyID_id")
    company_id_list = Company.objects.filter(id__in = obj_cus_carr, name__icontains = q_).values_list("id")
    
    order_obj  = Order.objects.filter(CustomerCompanyID__in = company_id_list, CarrierCompanyID = carr_id, is_delete = False)
    
    result_set = order_obj
   
    return result_set

def search_status(q_,carr_id):
    st_obj = Status.objects.filter(status_name__contains = q_).values_list("id")
    order_obj = Order.objects.filter(CarrierCompanyID = carr_id ,StatusID__in = st_obj, is_delete = False)
    
    result_set = order_obj
    return result_set
    

def search_dt(q_,carr_id):

    order_obj = Order.objects.filter(CarrierCompanyID = carr_id ,created_DT__contains=q_, is_delete = False)

    result_set = order_obj
    return result_set
    


def search_job(q_,carr_id):

    order_obj = Order.objects.filter(CarrierCompanyID = carr_id ,job_id__icontains=q_, is_delete = False)
    result_set = order_obj        
    return result_set
    

def search_pick(q_,carr_id):
    ord_obj = Order.objects.filter(CarrierCompanyID = carr_id, is_delete = False).values_list('id')
    pick_obj = Pick.objects.filter(OrderID__in = ord_obj, pickup_date_time__contains=q_, is_delete = False).values_list('OrderID')
    order_obj = Order.objects.filter(id__in = pick_obj, CarrierCompanyID = carr_id, is_delete = False)
    
    result_set = order_obj        
    return result_set
             

def search_deli(q_,carr_id):
    ord_obj = Order.objects.filter(CarrierCompanyID = carr_id, is_delete = False).values_list('id')
    drop_obj = Drop.objects.filter(OrderID__in = ord_obj, delivery_date_time__contains=q_, is_delete = False).values_list('OrderID')
    order_obj = Order.objects.filter(id__in = drop_obj, CarrierCompanyID = carr_id, is_delete = False)

        
    result_set = order_obj        
    return result_set
    

class CarrSearch(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
            carr_id = content[0][0]
            # try:
            q_search = request.GET["search"]
                # try:
            if q_search:
                # s=Order.objects.none()
                final_order = []

                #r_status = search_status(q_search,carr_id)
                #final_order.append(r_status)
                #r_dt     = search_dt(q_search,carr_id)
                #final_order.append(r_dt)
                r_job    = search_job(q_search,carr_id)
                final_order.append(r_job)
                r_name   = search_name(q_search,carr_id)
                final_order.append(r_name)
                #r_pick  = search_pick(q_search,carr_id)
                #final_order.append(r_pick)
                #r_deliv  = search_deli(q_search,carr_id)
                #final_order.append(r_deliv)
                Se = set()

                for data in final_order:
                    
                    if data:
                        for data_in in data:
                        
                            Se.add(data_in)
                    else:
                        continue
        

                or_list = list(Se)
                or_list.sort(key=lambda x: x.created_DT, reverse=True)
                try:
                    final_result = OrderStatusCarrSerializer(or_list, many = True)
                    
                    context = {
                        "order_list":final_result.data,
                        "ord_per_list":"",
                        "message":"success"
                    }
                    return Response(context,status=status.HTTP_200_OK)
                    
                except Exception as e:
                    print('error while serializer object creation',str(e))
                    context = {
                    "message":"internal error",
                    "data":''
                    }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
            else:
                
                context = {
                "message":"search is null",
                "data":''
                }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)

def search_name_frm_driv(q_,carr_id,driver_id):
    obj_cus_carr = CustomerCarrier.objects.filter(CarriercompanyID = carr_id).values_list("CustomercompanyID_id")
    company_id_list = Company.objects.filter(id__in = obj_cus_carr, name__contains = q_).values_list("id")
    order_driv = OrderDriver.objects.filter(DriverID_id = driver_id).values_list('OrderID')
    order_obj  = Order.objects.filter(id__in = order_driv,CustomerCompanyID__in = company_id_list, CarrierCompanyID = carr_id, is_delete = False)
    result_set = order_obj
    return result_set


def search_status_frm_driv(q_,carr_id,driver_id):
    st_obj = Status.objects.filter(status_name__contains = q_).values_list("id")
    order_driv = OrderDriver.objects.filter(DriverID_id = driver_id).values_list('OrderID')
    order_obj = Order.objects.filter(id__in = order_driv,CarrierCompanyID = carr_id ,StatusID__in = st_obj, is_delete = False)
    result_set = order_obj
    return result_set


def search_dt_frm_driv(q_,carr_id,driver_id):
    order_driv = OrderDriver.objects.filter(DriverID_id = driver_id).values_list('OrderID')
    order_obj = Order.objects.filter(id__in = order_driv,CarrierCompanyID = carr_id ,created_DT__contains=q_, is_delete = False)
    result_set = order_obj
    return result_set


def search_job_frm_driv(q_,carr_id,driver_id):
    order_driv = OrderDriver.objects.filter(DriverID_id = driver_id).values_list('OrderID')
    order_obj = Order.objects.filter(id__in = order_driv,CarrierCompanyID = carr_id ,job_id__contains=q_, is_delete = False)

    result_set = order_obj        
    return result_set
    

def search_pick_frm_driv(q_,carr_id,driver_id):
    ord_obj = Order.objects.filter(CarrierCompanyID = carr_id, is_delete = False).values_list('id')
    pick_obj = Pick.objects.filter(OrderID__in = ord_obj, pickup_date_time__contains=q_, is_delete = False).values_list('OrderID')
    order_driv = OrderDriver.objects.filter(DriverID_id = driver_id).values_list('OrderID')
    or_id = pick_obj.intersection(order_driv)
    order_obj = Order.objects.filter(id__in = or_id, CarrierCompanyID = carr_id, is_delete = False)
    result_set = order_obj        
    return result_set
           

def search_deli_frm_driv(q_,carr_id,driver_id):
    ord_obj = Order.objects.filter(CarrierCompanyID = carr_id, is_delete = False).values_list('id')
    drop_obj = Drop.objects.filter(OrderID__in = ord_obj, delivery_date_time__contains=q_, is_delete = False).values_list('OrderID')
    order_driv = OrderDriver.objects.filter(DriverID_id = driver_id).values_list('OrderID')
    or_id =  drop_obj.intersection(order_driv)
    order_obj = Order.objects.filter(id__in = or_id, CarrierCompanyID = carr_id, is_delete = False)
    result_set = order_obj        
    return result_set

class CarrSearchInDriver(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
            carr_id = content[0][0]
            # try:
            q_search = request.GET["search"]
            driver_id = request.GET["driver_id"]
                # try:
            if q_search:
                or_list = []
                
                r_status = search_status_frm_driv(q_search,carr_id,driver_id)
                or_list.append(r_status)
                r_dt     = search_dt_frm_driv(q_search,carr_id,driver_id)
                or_list.append(r_dt)
                r_job    = search_job_frm_driv(q_search,carr_id,driver_id)
                or_list.append(r_job)
                r_name   = search_name_frm_driv(q_search,carr_id,driver_id)
                or_list.append(r_name)
                r_pick  = search_pick_frm_driv(q_search,carr_id,driver_id)
                or_list.append(r_pick)
                r_deliv  = search_deli_frm_driv(q_search,carr_id,driver_id)
                or_list.append(r_deliv)
                Se = set()
                for data in or_list:
                    if data:
                        for data_in in data:
                            Se.add(data_in)
                    else:
                        continue
                or_list = list(Se)
                
                try:
                    final_result = OrderStatusCarrSerializer(or_list, many = True)
                    return Response(final_result.data,status=status.HTTP_200_OK)
                except Exception as e:
                    print('error while serializer object creation',str(e))
                    context = {
                    "message":"internal error",
                    "data":''
                    }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
            else:
                
                context = {
                "message":"search is null",
                "data":''
                }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)       
import json
class CarrierSetting(APIView): 
    permission_classes = (IsAuthenticated,) 
    parser_class = (FileUploadParser,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            data = request.data['payload']
            data = json.loads(data)
            
            key = request.auth
            user_id = request.user.id
            
            content = Token.objects.filter(key = key).values_list("content_id","param")


            if request.data['file']:    
                f = request.data['file']
                fileToUpload = f
                cloudFilename ="media/carrier/"+fileToUpload.name
                obj = CarrierExtraInfo.objects.filter(id = content[0][1])
                
                obj.update(carr_image = BUCKET_URL+cloudFilename)
                obj_url = CarrierExtraInfo.objects.filter(id = content[0][1]).values_list("carr_image")
                local_url = obj_url
                

                session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                            aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
                s3 = session.resource('s3')
                s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=fileToUpload, ACL='public-read')
                

                carr_company_user = CompanyUser(CompanyID_id = int(content[0][0]), Created_by_CarrierID_id = int(content[0][1]))
                
                
                serializ = CarrierSettingCompanyUserSerializer(carr_company_user, data = data, context = {"com_id":int(content[0][0]),"carr_id":int(content[0][1]),"user_id":user_id})
                
                if serializ.is_valid():
                    serializ.save()
                
                    context = {
                        "image_url":BUCKET_URL+str(cloudFilename),
                        "message":"update success",
                        "data":serializ.data
                    }
                    return Response(context,status=status.HTTP_200_OK)
                else:

                
                    context = {
                        "message":"update failed",
                        "data":serializ.errors
                    }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)    
            else:
                carr_company_user = CompanyUser(CompanyID_id = int(content[0][0]), Created_by_CarrierID_id = int(content[0][1]))
                
                serializ = CarrierSettingCompanyUserSerializer(carr_company_user, data = data, context = {"com_id":int(content[0][0]),"carr_id":int(content[0][1]),"user_id":user_id})
                
                if serializ.is_valid():
                    serializ.save()
                    obj_url = CarrierExtraInfo.objects.filter(id = content[0][1]).values_list("carr_image")
                    imange_name = obj_url[0][0]
                    context = {
                        "image_url":str(imange_name),
                        "message":"update success",
                        "data":serializ.data
                    }
                    return Response(context,status=status.HTTP_200_OK)
                else:

                
                    context = {
                        "message":"update failed",
                        "data":serializ.errors
                    }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)    
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)
                    
class UpdateProfileImageCarr(APIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (FileUploadParser,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            key = request.auth
            param = Token.objects.filter(key = key).values_list("param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            if 'file' not in request.data:
                raise ParseError("Empty content")

            f = request.data['file']
            
            obj = CarrierExtraInfo.objects.filter(id = param)
            
            obj.update(carr_image= f)
            obj_url = CarrierExtraInfo.objects.filter(id = param).values_list("carr_image")
            
            local_url = obj_url
            fileToUpload = f
            cloudFilename ="media/carrier/"+fileToUpload.name

            session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
            s3 = session.resource('s3')
            s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=fileToUpload)

            context = {
                "data":BUCKET_URL+str(cloudFilename),
                "message":"success"
            }

            return Response(context,status=status.HTTP_201_CREATED)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)        


'''check the user can change email credential detail, 
    note: token holder always shows does not exist because
    sam email he can update in data current email of account in response '''
class UserEmailCheck(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            data = request.data
            email = (data['email']).lower()
            key = request.auth
            user_id = request.user.id
            user_obj  = User.objects.filter(email=email).exclude(id = user_id)
            user_email= User.objects.get(id=user_id)
            if user_obj:
                context = {
                    "message":"Already Exist",
                    "data":user_email.email
                }
                return Response(context, status=status.HTTP_400_BAD_REQUEST)
            context = {
                    "message":"Does Not Exist",
                    "data":user_email.email
                }
            return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)            

class PasswordReset(APIView):
    permission_classes = (IsAuthenticated,)
    def put(self, request):
        try:
            key = request.auth
            user = request.user
            data = request.data
            current_pass = data['current_password']
            user = authenticate(email = (request.data['email']).lower(), password = data['current_password'])
            if user:
                try:
                    user.set_password(data['new_password'])
                except Exception as e:
                    print("key error 'new_password'",e)
                    context = {
                        "message":"key error 'new_password'",
                        "data":''
                    }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)
                user.save()
                context = {
                    "message":"password update success",
                    "data":""
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                context = {
                    "message":"current password entered is incorrect, try again",
                    "data":""
                }    
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print('error in key.auth or token object',e)
            context = {
                "message":"internal error",
                "data":''
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class DriverIndividualRemoveOrderDetail(APIView):
    @transaction.atomic
    def delete(self, request):
        sid = transaction.savepoint()
        try:
            driver_id = request.data['driver_id']
            order_id = request.data['order_id']

            ord_dri_obj = OrderDriver.objects.filter(DriverID_id = driver_id, OrderID_id = order_id)
            pick_obj = Pick.objects.filter(OrderDriverID = ord_dri_obj).delete()
            drop_obj = Drop.objects.filter(OrderDriverID = ord_dri_obj).delete()
            ord_dri_obj.update(is_delete = True)
            final_dic = {
                'data':"",
                'message':"success"
            }
            return Response(final_dic,status=status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)

class AddMorePickDrop(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            try:
                key = request.auth
                content = Token.objects.filter(key = key).values_list("content_id") 
            except Exception as e:
                print('key or token object error',e)
                context = {
                            "data": '',
                            "message":'token Error'
                        }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
            data = request.data['arr_order'][0]
            
            order_status = data['order_detail']['is_active']
            carr_com_id = int(data['order_detail']['CarrierCompanyID'])
            
            # try:
            new_driver = set()
            if data['assign_pick_driver']:
                for assign_pick_driver_data in data['assign_pick_driver']:
                
                    in_assi_pck_dr_data = assign_pick_driver_data

                    if in_assi_pck_dr_data['pick_id']:
                        try:
                            
                            if (in_assi_pck_dr_data['sender'] != {}) and ("kid" in (in_assi_pck_dr_data['sender'])):
                                assi_pck_dr_obj     = Pick.objects.filter(id = int(in_assi_pck_dr_data['pick_id'])).update(address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                                latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], 
                                pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status, sender_id = in_assi_pck_dr_data['sender']['kid'])        
                            else:
                                assi_pck_dr_obj     = Pick.objects.filter(id = int(in_assi_pck_dr_data['pick_id'])).update(address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                                latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], 
                                pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status)
                        except TypeError as e:
                            assi_pck_dr_obj  = Pick.objects.filter(id = int(in_assi_pck_dr_data['pick_id'])).update(address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                                latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], 
                                pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status)
                        if in_assi_pck_dr_data['asset'] != []:
                            for data_in in in_assi_pck_dr_data['asset']:
                                if 'is_for' in data_in:
                                    if data_in['is_for'] == 'add':
                                        asr_obj , created = AssetRelation.objects.get_or_create(CompanyID_id = carr_com_id, AssetID_id = data_in['aid'], PickID_id = int(in_assi_pck_dr_data['pick_id']))
                                        if asr_obj:
                                            AttachDetached.objects.create(AssetID_id = data_in['aid'], schedule_from = in_assi_pck_dr_data['pickup_date_time'], schedule_to = in_assi_pck_dr_data['pickup_date_time_to'])
                                    if data_in['is_for'] == 'remove':
                                        asr_obj= AssetRelation.objects.get(CompanyID_id = carr_com_id,AssetID_id = data_in['aid'], PickID_id = int(in_assi_pck_dr_data['pick_id']))
                                        if asr_obj:
                                            if data_in['adid']:
                                                ad_obj = AttachDetached.objects.filter(id = data_in['adid']).first()
                                                ad_obj.delete()
                                            asr_obj.delete()
                    else:
                        created_by_carr_id = int(in_assi_pck_dr_data['Created_by_CarrierID']['id'])
                        st = Status.objects.filter(status_name = "pending").first()
                        
                        ord_driv_obj, created = OrderDriver.objects.get_or_create(OrderID_id = int(data['order_detail']['order_id']), is_active = order_status, DriverID_id = int(in_assi_pck_dr_data['DriverID']), AssetID_id = int(in_assi_pck_dr_data["ass_truck_id"]),
                        Created_by_CarrierID_id = int(created_by_carr_id), order_status = st)
                        if created:
                            new_driver.add(ord_driv_obj)
                    
                        assi_pck_dr_obj = ''
                        try:
                            if (in_assi_pck_dr_data['sender'] != {}) and ("kid" in (in_assi_pck_dr_data['sender'])):
                                assi_pck_dr_obj     = Pick.objects.create(OrderDriverID = ord_driv_obj, address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                                latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status, sender_id = in_assi_pck_dr_data['sender']['kid'])
                            else:
                                assi_pck_dr_obj     = Pick.objects.create(OrderDriverID = ord_driv_obj, address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                                latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status)
                        except TypeError as e:
                            assi_pck_dr_obj     = Pick.objects.create(OrderDriverID = ord_driv_obj, address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                                latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status)                          
                        if in_assi_pck_dr_data['asset'] != []:
                            for data_in in in_assi_pck_dr_data['asset']:
                                if 'is_for' in data_in:
                                    if data_in['is_for'] == 'add':
                                        asr_obj , created = AssetRelation.objects.get_or_create(CompanyID_id = carr_com_id, AssetID_id = data_in['aid'], PickID_id = assi_pck_dr_obj.id)       
                                        if asr_obj:
                                            AttachDetached.objects.create(AssetID_id = data_in['aid'], schedule_from = in_assi_pck_dr_data['pickup_date_time'], schedule_to = in_assi_pck_dr_data['pickup_date_time_to'])
                                    if data_in['is_for'] == 'remove':
                                        asr_obj= AssetRelation.objects.get(CompanyID_id = carr_com_id, AssetID_id = data_in['aid'], PickID_id = int(in_assi_pck_dr_data['pick_id']))
                                        if asr_obj:
                                            if data_in['adid']:
                                                ad_obj = AttachDetached.objects.filter(id = data_in['adid']).first()
                                                ad_obj.delete()
                                            asr_obj.delete()

                        status_obj = Status.objects.filter(status_name = "pending").values_list("id")
                        ord_obj = Order.objects.filter(id = int(data['order_detail']['order_id'])).update(StatusID_id = int(status_obj[0][0]))
            
            if data['assign_drop_driver']:
                for assign_drop_driver in data['assign_drop_driver']:

                    in_assi_del_data = assign_drop_driver

                    if in_assi_del_data['drop_id']:
                        try:
                            if (in_assi_del_data['receiver'] != {}) and ("kid" in (in_assi_del_data['receiver'])):
                                internal_assi_del_obj  = Drop.objects.filter(id = int(in_assi_del_data['drop_id'])).update(address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                                longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                                delivery_date_time = in_assi_del_data['delivery_date_time'], 
                                delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status, receiver_id = in_assi_del_data['receiver']['kid']) 
                            else:
                                internal_assi_del_obj  = Drop.objects.filter(id = int(in_assi_del_data['drop_id'])).update(address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                                longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                                delivery_date_time = in_assi_del_data['delivery_date_time'], 
                                delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status) 
                        except TypeError as e:
                            internal_assi_del_obj  = Drop.objects.filter(id = int(in_assi_del_data['drop_id'])).update(address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                                longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                                delivery_date_time = in_assi_del_data['delivery_date_time'], 
                                delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status) 
                        if in_assi_del_data['asset'] != []:
                            for data_in in in_assi_del_data['asset']:
                                if 'is_for' in data_in:
                                    if data_in['is_for'] == 'add':
                                        asr_obj ,created = AssetRelation.objects.get_or_create(CompanyID_id = carr_com_id, AssetID_id = data_in['aid'], DropID_id = int(in_assi_del_data['drop_id']))            
                                        if asr_obj:
                                            AttachDetached.objects.create(AssetID_id = data_in['aid'], schedule_from = in_assi_del_data['delivery_date_time'], schedule_to = in_assi_del_data['delivery_date_time_to'])
                                    if data_in['is_for'] == 'remove':
                                        asr_obj = AssetRelation.objects.get(CompanyID_id = carr_com_id, AssetID_id = data_in['aid'], DropID_id = int(in_assi_del_data['drop_id']))            
                                        if asr_obj:
                                            if data_in['adid']:
                                                ad_obj = AttachDetached.objects.filter(id = data_in['adid']).first()
                                                ad_obj.delete()
                                            asr_obj.delete()
                    else:
                        created_by_carr_id = int(in_assi_del_data['Created_by_CarrierID']['id'])
                        std = Status.objects.filter(status_name = "pending").first()

                        ord_driv_obj, created = OrderDriver.objects.get_or_create(OrderID_id = int(data['order_detail']['order_id']), AssetID_id = int(in_assi_del_data["ass_truck_id"]), 
                        DriverID_id = int(in_assi_del_data['DriverID']), Created_by_CarrierID_id = int(created_by_carr_id),
                        is_active =order_status, order_status = std)
                        if created:
                            new_driver.add(ord_driv_obj)
                        internal_assi_del_obj = ''
                        try:
                            if (in_assi_del_data['receiver'] != {}) and ("kid" in (in_assi_del_data['receiver'])):
                                internal_assi_del_obj  = Drop.objects.create( OrderDriverID = ord_driv_obj, address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                                longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                                delivery_date_time = in_assi_del_data['delivery_date_time'], 
                                delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status, receiver_id = in_assi_del_data['receiver']['kid']) 
                            else:
                                internal_assi_del_obj  = Drop.objects.create( OrderDriverID = ord_driv_obj, address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                                longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                                delivery_date_time = in_assi_del_data['delivery_date_time'], 
                                delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status) 
                        except TypeError as e:
                            internal_assi_del_obj  = Drop.objects.create( OrderDriverID = ord_driv_obj, address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                                longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                                delivery_date_time = in_assi_del_data['delivery_date_time'], 
                                delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status) 

                        if in_assi_del_data['asset'] != []:
                            for data_in in in_assi_del_data['asset']:
                                if 'is_for' in data_in:
                                    if data_in['is_for'] == 'add':
                                        asr_obj ,created = AssetRelation.objects.get_or_create(CompanyID_id = carr_com_id, AssetID_id = data_in['aid'], DropID_id = internal_assi_del_obj.id)            
                                        if asr_obj:
                                            AttachDetached.objects.create(AssetID_id = data_in['aid'], schedule_from = in_assi_del_data['delivery_date_time'], schedule_to = in_assi_del_data['delivery_date_time_to'])
                                    if data_in['is_for'] == 'remove':
                                        asr_obj = AssetRelation.objects.get(CompanyID_id = carr_com_id, AssetID_id = data_in['aid'], DropID_id = internal_assi_del_obj.id)            
                                        if asr_obj:
                                            if data_in['adid']:
                                                ad_obj = AttachDetached.objects.filter(id = data_in['adid']).first()
                                                ad_obj.delete()
                                            asr_obj.delete()

                        status_obj = Status.objects.filter(status_name = "pending").values_list("id")
                        ord_obj = Order.objects.filter(id = int(data['order_detail']['order_id'])).update(StatusID_id = int(status_obj[0][0]))

            dt = str(data['date_time_of_create'])
            dt = dt.split('.')
            
            #2019-09-30T07:51:12.771Z
            datetime_object = datetime.strptime(dt[0], '%Y-%m-%dT%H:%M:%S')

            old_format = '%Y-%m-%d %H:%M:%S'
            new_format = '%d/%m/%Y %I:%M %p'
            
            new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format) 
            for od in new_driver:
                
                driv_comment = str('%s assigned you a new job(%s) for %s'%(od.OrderID.CarrierCompanyID.name, od.OrderID.job_id, od.OrderID.CustomerCompanyID.name))
                notif_obj = Notification.objects.create(OrderID_id = od.OrderID.id, created_by_carrier_id = od.Created_by_CarrierID.id, created_for_driver_id = od.DriverID.id, comments = driv_comment,date_time_of_create = data['date_time_of_create'])
            
            
            context = {
                "data":"",
                "message":"order updated"
            }
            return Response(context, status=status.HTTP_200_OK)
            # except Exception as e:
            #     print('exception', e)
            #     context = {
            #         "data":"",
            #         "message":"Error"
            #     }
            #     return Response(context, status=status.HTTP_400_BAD_REQUEST)
    
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)        
    
    

class CarrDriverDelete(APIView):
    @transaction.atomic
    def put(self, request):
        sid = transaction.savepoint()
        try:
            driv_id_list = request.data['driverlist']
            if driv_id_list:
                driv_obj       = DriverExtraInfo.objects.filter(id__in = driv_id_list).update(is_delete = True)
                order_driv_obj = OrderDriver.objects.filter(DriverID__in = driv_id_list).update(is_delete = True)
                carr_driv      = CarrierDriver.objects.filter(DriverID__in = driv_id_list).update(is_delete = True)
            
                context = {
                    "data":"",
                    "message":"success"
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                context = {
                    "data":"",
                    "message":"No driver selected"
                }
                return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)       


class order_en_route(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:
            order_id = request.GET['order_id']
            try:
                key = request.auth
                content = Token.objects.filter(key = key).values_list("content_id")
            except Exception as e:
                print('key or token object error',e)
                context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                        }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
            ord_his = OrderHistory.objects.filter(OrderID_id = order_id, is_en_route =True)
            serializ = EnrouteSerializer(ord_his, many =True)
            context = {
                "data":serializ.data,
                "message":"success"
            }
            return Response(context,status=status.HTTP_200_OK)

        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)
class CarrOrderComment(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:

            try:
                key = request.auth
                content = Token.objects.filter(key = key).values_list("content_id","param")
            except Exception as e:
                print('key or token object error',e)
                context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                        }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)

            data = request.data
            user_type = request.GET['user_type']
            order_obj = Order.objects.get(id = int(data['order_id']) )
            company_obj  = Company.objects.get(id = order_obj.CarrierCompanyID.id)
            com_user_obj = CompanyUser.objects.filter(id = company_obj.id).first()
            carr_id = com_user_obj.Created_by_CarrierID.id
            carr_name = com_user_obj.Created_by_CarrierID.name
            job_id = order_obj.job_id

            company_obj2  = Company.objects.get(id = order_obj.CustomerCompanyID.id)
            com_user_obj2 = CompanyUser.objects.filter(id = company_obj2.id).first()
            cust_id = com_user_obj2.Created_by_CustomerID.id
            cust_name = com_user_obj2.Created_by_CustomerID.name
            if user_type == "carrier":
                ordercomment_obj = OrderComment.objects.create(OrderID_id = int(data['order_id']), Created_by_CarrierID_id = content[0][1] , comment = data['comment'] , date_time_of_create = data['date_time_of_create'])
                order_hist       = OrderHistory.objects.create( Created_by_CarrierID_id = carr_id, comments = str(data['comment']))       
                
                
                dt = str(data['date_time_of_create'])
                dt = dt.split('.')
                
                #2019-09-30T07:51:12.771Z
                datetime_object = datetime.strptime(dt[0], '%Y-%m-%dT%H:%M:%S')

                old_format = '%Y-%m-%d %H:%M:%S'
                new_format = '%d/%m/%Y %I:%M %p'
                
                
                
                
                
                
                # date = str(data['date_time_of_create'])
                # print('date',date)
                
                # print('after split',date)
                # datetime_object = datetime.strptime(date, '%Y-%m-%dT%H:%M:%SZ')

                # old_format = '%Y-%m-%d %H:%M:%S'
                # new_format = '%d/%m/%Y %I:%M %p'

                new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)
                notif_obj        = Notification.objects.create(OrderID_id = int(data['order_id']) , created_by_carrier_id = carr_id, created_for_customer_id = cust_id, comments = carr_name+str(" has made comment on ")+str(job_id)+str(" on ")+str(new_datetime_str), date_time_of_create = data['date_time_of_create'])
                order_driver_obj = OrderDriver.objects.filter(OrderID_id = int(data['order_id'])).values_list('DriverID')
                driv_li = []
                if order_driver_obj != []:
                    for driv in order_driver_obj:
                        driv_li.append(driv[0])
                if driv_li != []:
                    for idd in driv_li:
                        driv_notif_obj = Notification.objects.create(OrderID_id = int(data['order_id']) , created_by_carrier_id = carr_id, created_for_driver_id = idd, comments = carr_name+str(" has made comment on ")+str(job_id), date_time_of_create = data['date_time_of_create'])    
            if user_type == "customer":
                ordercomment_obj = OrderComment.objects.create(OrderID_id = int(data['order_id']) ,Created_by_CustomerID_id = content[0][1] , comment = data['comment'] , date_time_of_create = data['date_time_of_create'])
                order_hist       = OrderHistory.objects.create(Created_by_CarrierID_id = carr_id, comments = str(data['comment']))
                dt = str(data['date_time_of_create'])
                dt = dt.split('.')
                
                #2019-09-30T07:51:12.771Z
                datetime_object = datetime.strptime(dt[0], '%Y-%m-%dT%H:%M:%S')

                old_format = '%Y-%m-%d %H:%M:%S'
                new_format = '%d/%m/%Y %I:%M %p'
                
                new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)     
                notif_obj        = Notification.objects.create(OrderID_id = int(data['order_id']) , created_for_carrier_id = carr_id, created_by_customer_id = cust_id, comments =str(cust_name)+str(" has made comment on ")+str(job_id)+str(" on ")+str(new_datetime_str), date_time_of_create = data['date_time_of_create'])
            context = {
                "data":"",
                "message":"commented successfully"
            }
            return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('something went wroung',e)
            context = {
                    "data":'',
                    "message":'Error'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

class UniqueEmail(APIView):
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            email = (request.data['email']).lower()
            
            user_obj = User.objects.filter(email = email).first()
            if user_obj:
                context = {
                    "data":"",
                    "message":False
                }
                return Response(context, status=status.HTTP_200_OK)
            else:
                context = {
                    "data":"",
                    "message":True
                }
                return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)        

class HereAdminLogin(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            try:
                key = request.auth
                content = Token.objects.filter(key = key).values_list("content_id","param")
            except Exception as e:
                print('key or token object error',e)
                context = {
                            "data": '',
                            "message":'Token Error'
                        }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)

            payload = {
            "email": HERE_MAP_USER,
            "password": HERE_MAP_KEY
                }
            headers = {
            "Content-Type" : "application/json"
                }
            here_url = "https://tracking.api.here.com/users/v2/login"
            
            r = requests.post(here_url, data=json.dumps(payload), headers=headers)    
            resp = r.json()

            acc_token = resp['accessToken']
            context = {
                "data":str(acc_token),
                "message":"success"
            }
            return Response(context,status=status.HTTP_200_OK)
        
        except Exception as e:
            print('key or token object error',e)
            transaction.savepoint_rollback(sid)
            context = {
                        "data": '',
                        "message":'something went wroung'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class CustomerListView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'Token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        CustCarr = CustomerCarrier.objects.filter(CarriercompanyID_id = int(content[0][0]), is_active = True , is_delete = False)
        serialz = ListCustomerCarrierSerializer(CustCarr, many = True)
        context = {
                "data":serialz.data,
                "message":"success"
        }
        return Response(context,status=status.HTTP_200_OK)


class CarrNotification(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'Token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        carr_notigfy_obj = Notification.objects.filter(created_for_carrier_id = int(content[0][1])).order_by('-id')
        serialz = NotificationSerializer(carr_notigfy_obj, many = True)
        context = {
            "data":serialz.data,
            "message":"success"
        }
        return Response(context, status=status.HTTP_200_OK)


class DownloadCustList(APIView):
    def get(self, request):
        carr_com_id = int(request.GET['carr_com_id'])
        cust_carr_obj = ''
        if request.GET['custlist'] != '[]':
            order_id = request.GET['custlist']
            res = order_id.strip('][').split(',')
            res = [int(i) for i in res]
            
            
            cust_carr_obj = CustomerCarrier.objects.filter(CarriercompanyID_id = carr_com_id, CustomercompanyID_id__in = res, is_active = True , is_delete = False)
        else:
            cust_carr_obj = CustomerCarrier.objects.filter(CarriercompanyID_id = carr_com_id, is_active = True , is_delete = False)



        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet("Customer template")
        header_format = workbook.add_format({
                                    'border': 1,
                                    'bg_color': '#FFFF33',
                                    'bold': True,
                                    'text_wrap': True,
                                    'valign': 'vcenter',
                                    'indent': 1,
                                    })
        worksheet.write('A1', 'customer business name(*)', header_format) 
        worksheet.write('B1', 'primary contact(*)', header_format) 
        worksheet.write('C1', 'primary email id', header_format) 
        worksheet.write('D1', 'primary phone(*)', header_format) 
        worksheet.write('E1', 'secondary contact', header_format) 
        worksheet.write('F1', 'secondary email', header_format) 
        worksheet.write('G1', 'secondary phone', header_format) 
        worksheet.write('H1', 'business address(*)', header_format) 
        worksheet.write('I1', 'contract start date', header_format) 
        worksheet.write('J1', 'contract end date', header_format) 
        worksheet.write('K1', 'Customer code', header_format) 
        


        row = 1
        for cc_data in cust_carr_obj:
            
            if cc_data.CustomercompanyID.name == 'NA':
                continue

            worksheet.write(row,0, cc_data.business_name)
            worksheet.write(row,1, cc_data.name)
            worksheet.write(row,2, cc_data.email)
            worksheet.write(row,3, cc_data.mobile_num)
            worksheet.write(row,4, cc_data.secondary_contact)
            worksheet.write(row,5, cc_data.sec_email_id)
            worksheet.write(row,6, cc_data.sec_phone)
            worksheet.write(row,7, cc_data.business_address)
            worksheet.write(row,8, cc_data.contract_start_date)
            worksheet.write(row,9, cc_data.contract_end_date)
            worksheet.write(row,10, cc_data.Customer_code)
            row = row+1
            # cust_com_user_obj = CompanyUser.objects.filter(CompanyID_id = cc_data.CustomercompanyID.id).values_list('Created_by_CustomerID')
            # print('cust_com_user_obj',cust_com_user_obj)
            # cust_ex_detail = CustomerExtraInfo.objects.filter(id = cust_com_user_obj[0][0]).values_list('name','mobile_num','email')
            # print('cust_ex_detail',cust_ex_detail)
        
        workbook.close()
        output.seek(0)

        response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename=CustomerDownload.xlsx"

        output.close()

        return response



class CustomerDeleteAPI(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def put(self, request):
        sid = transaction.savepoint()
        try:
            cust_list = request.data['cust_list']
            if cust_list:
                CustomerCarrier.objects.filter(CustomercompanyID_id__in = cust_list).update(is_active = False , is_delete = True)
                context = {
                    "data":"",
                    "message":"Deleted"
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                context = {
                    "data":"",
                    "message":"No customer selected"
                }
                return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)       



class CustomerEditByCarrier(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
            
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        carrcust_id = request.GET['carrcust_id']
        update_data = request.data
        cusscarr_obj = CustomerCarrier.objects.filter(id =  carrcust_id).update(**update_data)   
        if cusscarr_obj:
            context = {
                "data":"",
                "message":"Updated"
            }   
            return Response(context,status=status.HTTP_200_OK)
        else:
            context = {
                "data":"",
                "message":"Failed to update"
            }   
            return Response(context,status=status.HTTP_200_OK)


class CustomerSampleExcel(APIView):
    def get(self, request):
        try:
            output = io.BytesIO()
            workbook = xlsxwriter.Workbook(output, {'in_memory': True})
            worksheet = workbook.add_worksheet("Customer Template")
            header_format = workbook.add_format({
                                        'border': 1,
                                        'bg_color': '#FFFF33',
                                        'bold': True,
                                        'text_wrap': True,
                                        'valign': 'vcenter',
                                        'indent': 1,
                                        })
            worksheet.write('A1', "Customer Bussiness Name(*)", header_format) 
            worksheet.write('B1', "Primary Contact(*)", header_format)
            worksheet.write('C1', "Primary Email(*)", header_format)
            worksheet.write('D1', "Primary Phone No.(*)", header_format)
            worksheet.write('E1', "Business Address(*)", header_format)
            worksheet.write('F1', "Secondary Contact", header_format)
            worksheet.write('G1', "Secondary Email", header_format)
            worksheet.write('H1', "Secondary Phone No.", header_format)
            worksheet.write('I1', "Contract Start Date", header_format)
            worksheet.write('J1', "Contract End Date", header_format)
            worksheet.write('K1', "Customer Code", header_format)
            
            worksheet.data_validation('I2:I1000', {'validate': 'any',
                                'input_title': 'Date Field',
                                'input_message':'format: DD/MM/YYYY'
                                })
            worksheet.data_validation('J2:J1000', {'validate': 'any',
                                'input_title': 'Date Field',
                                'input_message':'format: DD/MM/YYYY'
                                })
            workbook.close()
            output.seek(0)

            response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = "attachment; filename=CustomerSampleExcel.xlsx"

            output.close()

            return response
        except Exception as e:
            print('Exception',e)
            
            context = {
                "data":"",
                "message":str(e)
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)



class CustomerUpload(APIView):
    parser_classes = [MultiPartParser,FileUploadParser]
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        # sid = transaction.savepoint()
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        
        request_file = request.FILES['data_file']

        df = pd.read_excel(request_file, sheetname='Customer Template',dtype = dict)
        file_dic = df.to_dict('records')
        
        
        for data_in in file_dic:
            email = (data_in["Primary Email(*)"]).lower()
            chech_em = user_email.email_check(email)


            if (chech_em["message"] == "Carrier") or (chech_em["message"] == "Driver"):
                continue
            else:
                cust_obj, check_cust = CustomerExtraInfo.objects.get_or_create(email = email,  defaults = {"name": data_in["Primary Contact(*)"], "mobile_num": data_in["Primary Phone No.(*)"] , "email": data_in["Primary Email(*)"] })
                
                if check_cust:
                    cust_com = Company.objects.create(name = data_in["Customer Bussiness Name(*)"], mobile_num = data_in["Primary Phone No.(*)"], company_type = "customer", location = data_in["Business Address(*)"])

                    st_id = Status.objects.filter(status_name = "added carr_cust").first()
                    cust_carr_obj , created = CustomerCarrier.objects.get_or_create(CustomercompanyID = cust_com , CarriercompanyID_id = content[0][0], defaults = {"r_status_id":st_id.id, "email":email,"business_name":data_in["Customer Bussiness Name(*)"],"name":data_in["Primary Contact(*)"],"mobile_num":data_in["Primary Phone No.(*)"],"email":data_in["Primary Email(*)"],"secondary_contact":data_in["Secondary Contact"],"sec_email_id":data_in["Secondary Email"],"sec_phone":data_in["Secondary Phone No."],"business_address":data_in["Business Address(*)"],"contract_start_date":data_in["Contract Start Date"],"contract_end_date":data_in["Contract End Date"],"Customer_code":data_in["Customer Code"] })
                    
                    com_user_create = CompanyUser.objects.create(CompanyID = cust_com, Created_by_CustomerID = cust_obj)
                else:
                    com_user_obj = CompanyUser.objects.filter(Created_by_CustomerID = cust_obj).first()
                    st_id = Status.objects.filter(status_name = "added carr_cust").first()
                    cust_carr_obj , created = CustomerCarrier.objects.get_or_create(CustomercompanyID = com_user_obj.CompanyID , CarriercompanyID_id = content[0][0], defaults = {"r_status_id":st_id.id, "email":email,"business_name":data_in["Customer Bussiness Name(*)"],"name":data_in["Primary Contact(*)"],"mobile_num":data_in["Primary Phone No.(*)"],"email":data_in["Primary Email(*)"],"secondary_contact":data_in["Secondary Contact"],"sec_email_id":data_in["Secondary Email"],"sec_phone":data_in["Secondary Phone No."],"business_address":data_in["Business Address(*)"],"contract_start_date":data_in["Contract Start Date"],"contract_end_date":data_in["Contract End Date"],"Customer_code":data_in["Customer Code"] })

        context = {
            "data":"",
            "message":"success"
        }            
        return Response(context, status=status.HTTP_200_OK)



