from django.db import models

class Company(models.Model):
    CHOICES = (
    ('carrier', 'carrier'),
    ('customer', 'customer'),)
    company_type  = models.CharField(max_length = 100,choices=CHOICES,null=True,blank=True)
    is_individual = models.BooleanField(default=True)
    
    name          = models.CharField(max_length =100,null=True,blank=True)
    mobile_num    = models.CharField(max_length =100,null=True,blank=True)
    location      = models.CharField(max_length = 500,null=True,blank=True)
    location2     = models.CharField(max_length = 500,null=True,blank=True)
    pin           = models.CharField(max_length = 500,null=True,blank=True)
    country       = models.CharField(max_length = 500,null=True,blank=True)
    email         = models.EmailField(max_length=100,null=True,blank=True)    
    created_DT    = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT    = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active     = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)
    def __str__(self):
        return '%s %s (%s)' % (self.name, self.company_type,self.id)