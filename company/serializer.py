from rest_framework import serializers
from company.models import Company


class CompanySerializer(serializers.ModelSerializer):
    
    class Meta:
        model  = Company
        fields = ('id','name','mobile_num','location','email')

class CompanyCustSerializer(serializers.ModelSerializer):
    
    class Meta:
        model  = Company
        fields = ('name',)


class CompanyCarrSerializer(serializers.ModelSerializer):
    
    class Meta:
        model  = Company
        fields = ('name',)

class DetailCarrDrivSerializer(serializers.ModelSerializer):
    
    class Meta:
        model  = Company
        fields = ('name',)


class CarrierSettingCompanySerializer(serializers.ModelSerializer):
    
    class Meta:
        model  = Company
        fields = ('id','name','location','location2','pin','country','mobile_num')

class CarrDrivOrderDetailsCompany(serializers.ModelSerializer):

    class Meta:
        model  = Company
        fields = ('name',)

