from django.db import models
from custom_users.models import User
from configure.models import Configure
from company.models import Company
class CompanyConfigure(models.Model):
    CustomUserID  = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    CompanyID     = models.ForeignKey(Company,on_delete=models.CASCADE,null=True,blank=True)
    ConfigureID   = models.ForeignKey(Configure,on_delete=models.CASCADE,null=True,blank=True)
    configure_val = models.CharField(max_length = 500, null=True, blank=True)
    is_changeable  = models.BooleanField(default=True)
    
    created_DT    = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT    = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_delete     = models.BooleanField(default=False)
    def __str__(self):
        return '%s (%s)' % (self.CompanyID,self.id)