from rest_framework import serializers

from companyconfigure.models import CompanyConfigure
from configure.serializers import ConfigurationSerialz

class CompanyConfigurationSerialz(serializers.ModelSerializer):
    ConfigureID = ConfigurationSerialz()
    class Meta:
        model = CompanyConfigure
        fields = ('id','ConfigureID','configure_val','is_changeable','CustomUserID')