from django.conf.urls import url

from companyconfigure import views

urlpatterns = [
   url('createconfig/',views.CreateCompanyConfigure.as_view()),
   url('getconfig/',views.GetCompanyConfigure.as_view()),
]
