from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db import transaction
from django.http import HttpResponse
from configure.models import Configure
from companyconfigure.models import CompanyConfigure
from companyconfigure.serializers import CompanyConfigurationSerialz
class CreateCompanyConfigure(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)    
        try:
            data = request.data
            for data_list in data:
                label_name = data_list['label_name']
                label_val  = data_list['label_val']
                config_obj = Configure.objects.filter(label = label_name).first()
                cc_obj , created = CompanyConfigure.objects.get_or_create(CompanyID_id = content[0][0], ConfigureID_id = config_obj.id, defaults = {"configure_val":label_val})
                if created == False:
                    cc_obj.configure_val = label_val
                    cc_obj.save()    
            context = {
                        "data":"",
                        "message":"created"
                    }
            return Response(context,status=status.HTTP_200_OK)    
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)


class GetCompanyConfigure(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:          
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            cc_obj = CompanyConfigure.objects.filter(CompanyID_id = content[0][0])
            serialz = CompanyConfigurationSerialz(cc_obj, many = True)
            context =  {
                "data":serialz.data,
                "message":"success"
            }
            return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)
