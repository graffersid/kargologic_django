from django.db import models

# Create your models here.
class Configure(models.Model):
    label = models.CharField(max_length=100,null=True,blank=True)

    created_DT    = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT    = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_delete     = models.BooleanField(default=False)
    
    def __str__(self):
        return '%s (%s)' % (self.label,self.id)