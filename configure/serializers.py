from rest_framework import serializers

from configure.models import Configure


class ConfigurationSerialz(serializers.ModelSerializer):
    class Meta:
        model = Configure
        exclude = ('created_DT','updated_DT','is_delete')