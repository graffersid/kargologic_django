from django.apps import AppConfig


class CustomerAttachConfig(AppConfig):
    name = 'customer_attach'
