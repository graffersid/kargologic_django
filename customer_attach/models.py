from django.db import models

from customers.models import CustomerExtraInfo

class CustomerAttach(models.Model):
    CustomerID = models.ForeignKey(CustomerExtraInfo, on_delete=models.CASCADE,null = True, blank = True)
    attach_url = models.URLField(null = True, blank = True)


    created_DT          = models.DateTimeField(auto_now_add=True)
    updated_DT          = models.DateTimeField(auto_now=True)
    is_active           = models.BooleanField(default=True)
    is_delete           = models.BooleanField(default=False)

    def __str__(self):
        return '%s (%s)' % (self.CustomerID, self.id)

