from rest_framework import serializers
from customers.models import CustomerExtraInfo

from custom_users.serializers import UserSerializer

class CustomerExtraInfoSerializer(serializers.ModelSerializer):
    #CustomUserID = UserSerializer(required=False)
    class Meta:
        model  = CustomerExtraInfo
        fields = ('id','name','Customer_code')

class CarrOrderDetailCustomerExtraInfoSerializer(serializers.ModelSerializer):
    #CustomUserID = UserSerializer(required=False)
    class Meta:
        model  = CustomerExtraInfo
        fields = ('name',)
          