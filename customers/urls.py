from django.conf.urls import url
from . import views

urlpatterns = [
    url('customer_create', views.CustomerCreate.as_view(), name='account-create-email'),
    url('invite/', views.CustomerInvit.as_view(), name='cust-invite'),
    url('login/', views.CustomerLogIn.as_view(), name='cust-login'),
    url('signup/', views.CustomerSignUp.as_view(), name='cust-signup'),
    url('request_forgot_pass/', views.ReqForgPass.as_view(), name='cust-req-for-pass'),
    url('cust_reset_pass/', views.CustResetPassword.as_view(), name='cust-reset-pass'),
    url('cust_dash_order/', views.CustOrderResultByStatus.as_view(), name='cust-dash-order'),
    url('listcarrieroncust/', views.CarrierListByStatus.as_view(), name='cust-dash-carr-list'),
    url('order_count/', views.StatusCountCustomer.as_view(), name='cust-dash-or-count'),
    url('setting_customer/', views.CustomerSetting.as_view(), name='cust-setting'),
    url('reset_pass_cus/', views.CustomerResetPass.as_view(), name='cust-reset-pass'),
    url('cust_dash_carriercount/', views.CustomerDashCarrierCount.as_view(), name='cust-dash-carr-count'),
    url('ord_carrier_cusdash/', views.CarrierDetails.as_view(), name='cust-dash-carrier-order'),
    url('carr_list/', views.CarrierCompanyList.as_view(), name='cust-list-carr'),
    url('order_search/', views.CustOrderSearch.as_view(), name='cust-order-search'),
    url('sample_download/', views.CustExcelDownload.as_view(), name='customer-sample-excel'),
    url('order_download/', views.CustOrderCsvDownload.as_view(), name='customer-order-download'),
    url('carrier_list_download/', views.CarrierListDownlaod.as_view(), name='carrier-list-download'),
    url('super_carr_invit/', views.CarrierInvite.as_view(), name='carrier-invite'),
    url('edit_cust_order/', views.CustEditOrder.as_view(), name='cust-edit-order'),
    url('sign_prepo/', views.CustomerInvitePrepopulate.as_view(), name='cust-sign-pre'),
    url('notify_cust/', views.CustNotification.as_view(), name='notify-cust'),
    url('order_id_job/', views.CusOrderNoByType.as_view(), name='order_id_job'),
    url('count_carr_order/', views.CarrierListByStatusCount.as_view(), name='CarrierListByStatusCount'),
]
