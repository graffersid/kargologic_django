from django.shortcuts import render
from django.db import transaction
from django.http import HttpResponse
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from django.core.mail import send_mail, EmailMultiAlternatives
from django.conf import settings
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from custom_users.models import User
from .models import CustomerExtraInfo
from company.models import Company
from CustomerCarrier.models import CustomerCarrier
from CompanyUser.models import CompanyUser
from domain.models import Domain
from user_role.models import UserRole
from company.serializer import CompanySerializer
from OrderTemplate.models import OrderTemplate
from OrderExtraField.models import OrderExtraField
from Pickup.models import Pick
from Delivery.models import Drop
from OrderComment.models import OrderComment
from OrderAttachment.models import OrderAttachment
import binascii
import os
from KargoLogics.settings import SERVER_REACT_URL
from orders.serializers import ArrOrderSerializer,CarrOrderCsvDownloadSerializer
# Create your views here.
from orders.serializers import OrderStatusCarrSerializer
from orders.models import Order
from status.models import Status
from CustomerCarrier.serializers import CustomerCarrierListSerializer
from OrderDriver.models import OrderDriver
from Delivery.models import Drop
from datetime import timedelta
from carriers.models import CarrierExtraInfo
from Pickup.models import Pick
from customer_attach.models import CustomerAttach
from drivers.models import DriverExtraInfo
from KargoLogics.settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME, EMAIL_HOST_USER,AWS_S3_CUSTOM_DOMAIN,BUCKET_URL,HERE_MAP_USER,HERE_MAP_KEY,HERE_MAP_APPID
from KargoLogics import settings
import boto3
from boto3.session import Session
import binascii
from KargoLogics.settings import SERVER_REACT_URL, DEFAULT_CUSTOMER
import requests
import json
from user_role import views as user_email
from configure.models import Configure
from companyconfigure.models import CompanyConfigure
from Notifications.models import Notification
from Notifications.serializers import NotificationSerializer
from datetime import datetime

class CustomerInvit(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self,request):
        sid = transaction.savepoint()
        try:
            try:
                key = request.auth
                content = Token.objects.filter(key = key).values_list("content_id")
                print('content',content)
            except Exception as e:
                print('key or token object error',e)
                context = {
                            "data": '',
                            "message":'token Error'
                        }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
            com_name   = Company.objects.filter(id = content[0][0]).values('name')[0]['name']
            com_id     = Company.objects.filter(id = content[0][0]).values('id')[0]['id']
            print('com_name',com_name)
            
            email = (request.data['email']).lower()
            extra_details = request.data['extra_details']
            attach = request.data['attach']
            chech_em = user_email.email_check(email)
            cust_obj, check_cust = CustomerExtraInfo.objects.get_or_create(email = email,  defaults = {**extra_details})
            print('check_cust',check_cust)

            #true means creating the new company for customer
            if (check_cust ==True) and (chech_em["message"] == ""):
                # user_create = User.objects.create(email = email) 
                cust_com_obj = Company.objects.create(company_type ="customer",name = extra_details['business_name'], email = email)
                com_user_obj = CompanyUser.objects.create(CompanyID = cust_com_obj,Created_by_CustomerID = cust_obj)
                st_id = Status.objects.filter(status_name = "added carr_cust").first()
                cust_carr_obj = CustomerCarrier.objects.create(CustomercompanyID = cust_com_obj, CarriercompanyID_id = com_id , r_status_id = st_id.id, email = email,  **extra_details)
                if attach != []:
                    for file_dic in attach:
                        print('file_dic',file_dic)
                        fileToUpload = file_dic['file_data']
                        filename     = (file_dic['file_name']).replace(' ','_')
                        cloudFilename ="media/customer/attach"+filename
                        session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
                        s3 = session.resource('s3')
                        s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=fileToUpload, ACL='public-read')
                        
                        ur = BUCKET_URL+cloudFilename

                        CustomerAttach.objects.create(CustomerID_id = cust_obj.id , attach_url = ur)

                subject, from_email, to = 'Invitation to kargologic platform', settings.EMAIL_HOST_USER, email
                text_content = 'Welcome to KargoLogic, your are invited by carrier "%s"'%(com_name)
                url = SERVER_REACT_URL+"/customer/signup/?email="+str(email)+"&carr_com_id="+str(com_id)+"&carr_com_name="+str(com_name)+"&cust_extra_id="+str(cust_obj.id)+"&cust_com_id="+str(cust_com_obj.id)
                html_content = """Dear {0},<br>
                Welcome to KargoLogic!<br><br>
                You are receiving this email because your logistics partner {1} wants to 
                share an ongoing delivery status updates with you online. {1} is using 
                KargoLogic platform, a cloud based logistics platform that connect customers, transport 
                carriers and delivery drivers on a single platform to share real-time updates.<br><br>
                Your team will be now be able to view real time status of every delivery job being carried out 
                by {1} specifically for your business. Attach critical delivery documents, 
                give special instructions and nominate pickup and delivery times in minutes. Soon as {1} 
                receives the information and allocates jobs to its drivers, you will see the updates 
                flowing in to your portal instantly. Feel free to use KargoLogic to create jobs or bulk upload the entire 
                manifest and allocate to {1} instead of sending emails or text messages.<br><br> 
                <strong><a href='{2}'> Please click here to complete the sign up and login.</a></strong><br><br> 
                We hope you experience in using KargoLogic is delightful. In case you encounter technical difficulty 
                please send us an email on support@kargologic.com with detail description of the problem.  
                Please visit our website www.kargologic.com to view our friendly terms of use and privacy policy.""".format(cust_obj.name,com_name,url)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                # msg.send()
                context  = {
                    "data":"",
                    "message":"Customer invitation sent successfully"
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                if chech_em['message'] == "Customer":
                    company_obj = CompanyUser.objects.filter(Created_by_CustomerID=cust_obj)
                    
                    st_id = Status.objects.filter(status_name = "added carr_cust").first()
                    cust_carr_obj , re_created = CustomerCarrier.objects.get_or_create(CustomercompanyID = company_obj[0].CompanyID , CarriercompanyID_id = com_id,defaults = {"r_status_id":st_id.id, "email":email, **extra_details})
                    print("re_created",re_created)
                    if re_created:
                        subject, from_email, to = 'Invitation to kargologic platform', settings.EMAIL_HOST_USER, email
                        text_content = 'Welcome to KargoLogic, your are invited by carrier "%s"'%(com_name)
                        
                        html_content = '<p>Welcome to KargoLogic, a platform that connect customers, transport carriers and logistics companies together on one platform. You have been invited by carrier %s. By signing up, %s will be able to share delivery updates with you.</p><br/>'%(com_name,com_name)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        # msg.send()

                        context  = {
                            "data":"",
                            "message":"Customer has been added to your profile"
                        }
                        return Response(context,status=status.HTTP_200_OK)
                    else:
                        context = {
                            "data":"",
                            "message":"This user already added to your profile"
                        }
                        return Response(context, status=status.HTTP_200_OK)
                else:
                    transaction.savepoint_rollback(sid)
                    msg = chech_em['message']
                    context  = {
                        "data":"",
                        "message":"This is already on %s plateform"%(msg)
                    }
                    return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('Exception',e)
            context  = {
                    "data":"",
                    "message":"Exception"
                }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class CustomerInvitePrepopulate(APIView):
    def get(self, request):
        cust_extra_id = int(request.GET["cust_extra_id"])
        cust_com_id   = int(request.GET["cust_com_id"])
        cust_obj      = CustomerExtraInfo.objects.filter(id = cust_extra_id ).values()
        com_obj       = Company.objects.filter(id = cust_com_id).values()
        context = {
            "cust_obj":cust_obj,
            "com_obj":com_obj
        }
        return Response(context,status=status.HTTP_200_OK)


class CustomerCreate(APIView):
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            data = request.data
            
            user_obj, created = User.objects.get_or_create(email = (data['email']).lower(),  
                                                    defaults = {"user_name":data['user_name'],
                                                                "phone":data['phone']}) 
            
            
            if created == False:
                context = {
                    "data":"",
                    "message":"This user already completed SignUp"
                }
                return Response(context,status=status.HTTP_200_OK)          
            
            user_obj.set_password(data['password'])
            user_obj.save()
            domain_obj = Domain.objects.filter(name = 'KargoLogic')
            user_role_obj = UserRole.objects.filter(role_name = "customer")
            cust_ = CustomerExtraInfo.objects.filter(email = (data['email']).lower())
            cust_id_obj = CustomerExtraInfo.objects.filter(email = (data['email']).lower()).first()
            extra_details = request.data['extra_details']
            cust_.update(CustomUserID=user_obj, name = data['user_name'], mobile_num = data['phone'], email = (data['email']).lower(), domainID = domain_obj[0], roleID = user_role_obj[0], **extra_details)        
            
            company_user_obj = CompanyUser.objects.filter(Created_by_CustomerID = cust_[0])
            if company_user_obj or (data['carr_com_id'] == ""):
                com_id = company_user_obj[0].CompanyID.id
            
                cust_com_obj = Company.objects.filter(id = com_id).update(company_type = 'customer', name = extra_details['business_name'], mobile_num = data['phone'], location = extra_details['business_address'], location2 = extra_details['business_address'],pin = data['pin'],country = data['country'])
                
                order_table_ids = Configure.objects.filter(label = 'orders_table').first()
                time_slot_ids = Configure.objects.filter(label = 'time_slot').first()
                CompanyConfigure.objects.create(CompanyID_id = com_id, ConfigureID_id = order_table_ids.id, configure_val = "Job Number, Created Date, Customer Name, Pick Up Time From, Pick Up Time To, Delivery Time From, Delivery Time To, Status" )
                CompanyConfigure.objects.create(CompanyID_id = com_id, ConfigureID_id = time_slot_ids.id, configure_val ='30' )
                
                if data["attach"] != []:
                    for file_dic in data["attach"]:
                        print('file_dic',file_dic)
                        fileToUpload = file_dic['file_data']
                        filename     = (file_dic['file_name']).replace(' ','_')
                        cloudFilename ="media/customer/attach"+filename
                        session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
                        s3 = session.resource('s3')
                        s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=fileToUpload, ACL='public-read')
                        
                        ur = BUCKET_URL+cloudFilename

                        CustomerAttach.objects.create(CustomerID_id = cust_id_obj.id , attach_url = ur)

                context = {
                    "data": data['user_name'],
                    "message": 'account confirmed successfully'
                }
                return Response(context, status=status.HTTP_200_OK)
            elif (data['carr_com_id'] != ""):
                company_obj = Company.objects.create(company_type = 'customer', name = extra_details['business_name'], mobile_num = data['phone'], location = extra_details['business_address'], location2 = extra_details['business_address'], pin = data['pin'], country = data['country'])
                cust_carr_comp = CustomerCarrier.objects.create(CustomercompanyID = company_obj, CarriercompanyID_id = data['carr_com_id'])
                
                order_table_ids = Configure.objects.filter(label = 'orders_table').first()
                time_slot_ids = Configure.objects.filter(label = 'time_slot').first()
                CompanyConfigure.objects.create(CompanyID_id = company_obj.id, ConfigureID_id = order_table_ids.id, configure_val = "Job Number, Created Date, Customer Name, Pick Up Time From, Pick Up Time To, Delivery Time From, Delivery Time To, Status" )
                CompanyConfigure.objects.create(CompanyID_id = company_obj.id, ConfigureID_id = time_slot_ids.id, configure_val ='30' )
                
                if data["attach"] != []:
                    for file_dic in data["attach"]:
                        print('file_dic',file_dic)
                        fileToUpload = file_dic['file_data']
                        filename     = (file_dic['file_name']).replace(' ','_')
                        cloudFilename ="media/customer/attach"+filename
                        session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
                        s3 = session.resource('s3')
                        s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=fileToUpload, ACL='public-read')
                        
                        ur = BUCKET_URL+cloudFilename

                        CustomerAttach.objects.create(CustomerID_id = cust_id_obj.id , attach_url = ur)

                context = {
                    "data": data['user_name'],
                    "message": 'account confirmed successfully'
                }
                return Response(context, status=status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data": str(e),
                "message": 'Exception'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)



class CustomerLogIn(APIView):
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            data = request.data
            email = (data['email']).lower()
            password = data['password']
            print(data)
            user = authenticate(email = (request.data['email']).lower(), password = request.data['password'])
            print('user', user)
            cust_extr_check      = CustomerExtraInfo.objects.filter(CustomUserID = user).first()
            if user and cust_extr_check:
                cust_extr      = CustomerExtraInfo.objects.filter(CustomUserID = user).first()
                com_user_rel   = CompanyUser.objects.filter(Created_by_CustomerID = cust_extr).first()
                cust_com_id    = com_user_rel.CompanyID.id
                cust_com       = Company.objects.filter(id = int(cust_com_id)).first()
                token          = Token.objects.get_or_create(user = user, domain_id = str(cust_extr.domainID.id), role_id = str(cust_extr.roleID.id), content_id = str(cust_com.id), param = str(cust_extr.id))

                context = {
                    "token":str(token[0]),
                    "cust_id":cust_extr.id,
                    "cust_name":cust_extr.name,
                    "email":user.email,
                    "cust_phone":user.phone,
                    "cust_image":cust_extr.cust_image,
                    "cust_com_id":cust_com_id,
                    "cust_com_name":cust_com.name,
                    "cust_com_phone":cust_com.mobile_num,
                    "address":cust_com.location,
                    "address2":cust_com.location2,
                    "pin":cust_com.pin,
                    "country":cust_com.country,
                    "message" :'LoggedIn Successfully'
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                transaction.savepoint_rollback(sid)
                context = {
                    "data":"",
                    "message":"user does not exist"
                }
                return Response(context,status=status.HTTP_200_OK)

        except Exception as e:
            transaction.savepoint_rollback(sid)
            print("Exception in customer login",e)
            context = {
                "data":"",
                "Exception":str(e),
                "message":"something went wrong"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)

class CustomerSignUp(APIView):
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            data = request.data
            # cust_ex_check = CustomerExtraInfo.objects.filter(email = data['cust_email']).first()        
            ch_em = user_email.email_check(data['cust_email'])
            print('ch_em',type(ch_em))
            # user_check     = User.objects.filter(email = data['cust_email'] ).first()
            # carr_check     = CarrierExtraInfo.objects.filter(email = data['cust_email']).first()
            # customer_check = CustomerExtraInfo.objects.filter(email = data['cust_email']).first()
            # driver_check   = DriverExtraInfo.objects.filter(email = data['cust_email']).first()
            # msg = ""
            # if carr_check:
            #     msg = "This email is already registered with %s platform, please login to make changes"%("Carrier")
            # if customer_check:
            #     msg = "This email is already registered with %s platform, please login to make changes"%("Customer")
            # if driver_check:
            #     msg = "This email is already registered with %s platform, please login to make changes"%("Driver")
            # if carr_check or customer_check or driver_check or user_check:    
            #     context = {
            #         "data":"",
            #         "message":msg
            #     }
            #     return Response(context,status=status.HTTP_200_OK)
            
            extra_details = request.data['extra_details']
            if ch_em['message'] != "":
                context = {
                    "data":"",
                    "message":  "This user already exist as %s Kargologic Platform"%(ch_em['message'])
                }
                return Response(context, status=status.HTTP_200_OK)

            else:
                
                user, created = User.objects.get_or_create(email = (data['cust_email']).lower(), defaults = {'user_name':data['user_name'], 'phone':data['cust_mobile']})
                if created:
                    user.set_password(request.data['password'])
                    user.save()
                    domain_obj = Domain.objects.filter(name = 'KargoLogic').first()
                    
                    user_role_obj = UserRole.objects.filter(role_name = "customer").first() 
                    
                    cust_extra = CustomerExtraInfo.objects.create(CustomUserID = user, domainID_id = domain_obj.id, roleID_id = user_role_obj.id,
                                    name = data['user_name'] , mobile_num = data['cust_mobile'], email = (data['cust_email']).lower(), **extra_details )

                    cust_company = Company.objects.create(company_type = 'customer', name = data['company_name'], mobile_num = data['cust_mobile'] , location = data['address1'], 
                                    location2 = data['address1'], pin = data['pin'], country = data['country'] )


                    com_cust_user = CompanyUser.objects.create(CompanyID = cust_company, Created_by_CustomerID = cust_extra )

                    order_table_ids = Configure.objects.filter(label = 'orders_table').first()
                    time_slot_ids = Configure.objects.filter(label = 'time_slot').first()
                    CompanyConfigure.objects.create(CompanyID_id = cust_company.id, ConfigureID_id = order_table_ids.id, configure_val = "Job Number, Created Date, Customer Name, Pick Up Time From, Pick Up Time To, Delivery Time From, Delivery Time To, Status" )
                    CompanyConfigure.objects.create(CompanyID_id = cust_company.id, ConfigureID_id = time_slot_ids.id, configure_val ='30' )
                    context = {
                        "data": data['user_name'],
                        "message": 'Check Inbox and Spam folder for verification e-mail'
                    }
                    return Response(context, status=status.HTTP_200_OK)

                else:
                    carr_check     = CarrierExtraInfo.objects.filter(email = (data['cust_email']).lower()).first()
                    customer_check = CustomerExtraInfo.objects.filter(email = (data['cust_email']).lower()).first()
                    driver_check   = DriverExtraInfo.objects.filter(email = (data['cust_email']).lower()).first()
                    
                    msg = ""
                    if carr_check:
                        msg = "This email is already registered with %s platform, please login to make changes"%("Carrier")
                    if customer_check:
                        msg = "This email is already registered with %s platform, please login to make changes"%("Customer")
                    if driver_check:
                        msg = "This email is already registered with %s platform, please login to make changes"%("Driver")
                    if carr_check or customer_check or driver_check: 
                        transaction.savepoint_rollback(sid)
                        context = {
                            "data":"",
                            "message":msg
                        }
                        return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            transaction.savepoint_rollback(sid)
            print("Exception in customer signup",e)
            context = {
                "data":"",
                "Exception":str(e),
                "message":"something went wrong"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)


def generate_key():
    return binascii.hexlify(os.urandom(20)).decode()


class ReqForgPass(APIView):
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            req_email = (request.data['email']).lower()

            user = User.objects.filter(email = req_email).first()
            cust_extr = CustomerExtraInfo.objects.filter(email = req_email).first()

            if user and cust_extr:
                token_key = generate_key()
                user_obj = User.objects.filter(email = req_email).update(custom_key = token_key)
                
                subject, from_email, to = "Request to reset the password", settings.EMAIL_HOST_USER, req_email
                text_content = 'Wellcome to KargoLogic'
                url = str(SERVER_REACT_URL)+"/customer/recover-password/?email=%s&token=%s"%(req_email,token_key)
                
                user_name = user.user_name
                html_content = """<p>Hi {0},<br> It seems like you have requested to reset your password to access KargoLogic portal. If you didn't initiate this request simply ignore this message. If you need to reset your password,<p><strong><a href="{1}">click here.</a></strong></p>""".format(user_name, url)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                context = {
                    "data":"",
                    "message":"success"
                    }
                return Response(context,status=status.HTTP_200_OK) 
            elif cust_extr and (user == None):
                subject, from_email, to = 'Request for reset Forgot Password', settings.EMAIL_HOST_USER, req_email
                text_content = "Welcome to KargoLogic"
                url = SERVER_REACT_URL+"/customer/signup/?email="+str(req_email)+"&carr_com_id="+str("None")+"&carr_com_name="+str("None")
                html_content = """<p>Welcome to KargoLogic,<br>It seems you are invited Customer and you need to fill signup form then you will be able to reset the password.<br>Following link help you to complete the signup:<br>%s"""%(url)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                se = msg.send()
                if se:
                    context = {
                    "data":"",
                    "message":"success"
                    }
                    return Response(context,status=status.HTTP_200_OK)
            else:
                transaction.savepoint_rollback(sid)
                context = {
                    "data":"",
                    "message":"fail"
                }
                return Response(context,status=status.HTTP_200_OK) 

        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('something went wrong',e)
            context = {
                    "data":'',
                    "message":'Error'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class CustResetPassword(APIView):
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            token      = request.data['token']
            new_pass   = request.data['password']
            user_email = (request.data['email']).lower()
            user_obj = User.objects.filter(email = user_email).first()
            internal_key = user_obj.custom_key
            if internal_key == token:
                user_pass_obj = User.objects.get(id = user_obj.id)
                user_pass_obj.set_password(new_pass)
                user_pass_obj.save()
                context = {
                    "data":"",
                    "message":"success"
                }
                return Response(context, status=status.HTTP_200_OK)
            else:
                transaction.savepoint_rollback(sid)
                context = {
                    "data":"",
                    "message":"fail"
                }
                return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('something went wrong',e)
            context = {
                    "data":'',
                    "message":'something went wrong'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class CustOrderResultByStatus(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has customer company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        order_ins = ''
        if request.GET['status'] and request.GET['status'] != 'all' and request.GET['status'] != 'draft':
            status_id = Status.objects.filter(status_name = request.GET['status']).values('id')[0]['id']
            order_ins = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]) , StatusID_id = int(status_id), is_delete = False, is_active = True).order_by('-id')
        
        elif request.GET['status'] and request.GET['status'] == 'all':
            order_ins = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), is_delete = False).order_by('-id')

        elif request.GET['status'] and request.GET['status'] == 'draft':    
            order_ins = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), is_delete = False, is_active = False).order_by('-id')
        
        serialz = OrderStatusCarrSerializer(order_ins, many =True)

        # context = {
        #     "data":serialz.data,
        #     "message":"success"
        # }
        # return Response(context,status=status.HTTP_200_OK)


        try:
            # print('int(content[0][1])',(content))
            ord_obj_id_lst = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), is_delete = False).values_list('id')
            # print('ord_obj_id_lst',ord_obj_id_lst)
            order_driver_id_list = OrderDriver.objects.filter(OrderID_id__in = ord_obj_id_lst, is_delete = False).values_list('id')
            # print('order_driver_id_list',order_driver_id_list)
            delivery_obj = Drop.objects.filter(OrderDriverID_id__in = order_driver_id_list, is_delete = False).values_list('id','OrderDriverID','delivery_date_time_to','actual_delivery_date_time')
            # print('delivery_obj',delivery_obj)
            
            li = []
            
            for i in delivery_obj:
                order_dot = {}
                if i[3] and i[2]:
                    order_driver_obj = OrderDriver.objects.filter(id = i[1]).first()
                    od_id = order_driver_obj.OrderID.id
                    # print('order id',order_driver_obj.OrderID)
                    dot =  i[3]-i[2]
                    # print('dot',dot)

                    order_dot['order_id'] = od_id
                    order_dot['dot'] = dot
                    li.append(order_dot)

            # print('---------',li)

            oid = []
            final_dot = []
            for j in li:
                dics = {
                    "dot":[]
                }
                if j['order_id']:
                    if j['order_id'] not in oid:
                        oid.append(j['order_id'])
                        dics['ord_id'] = j['order_id']
                        for jt in li:
                            if jt['order_id'] == j['order_id']:
                                dics['dot'].append(jt['dot'])
                
                final_dot.append(dics)
                        
            
            
            
            sort_final = []
            for aq in final_dot:
                if aq['dot'] == []:
                    continue
                else:
                    sort_final.append(aq)
            # print('sort_final',sort_final)
            # print('------------------------------------')
            
            ord_per_list = []
            for qw in sort_final:
                tcount = 0
                delaycount = 0
                
                per_ord_dic = {}
                for tm in qw['dot']:
                    tcount = tcount+1

                    if tm >= timedelta(0):
                        # print('tm',tm)
                        delaycount = delaycount+1
                ratio_by_total = 100/tcount
                perc = 100 - ratio_by_total*delaycount
                # print('perc',perc)
                ordr_id = qw['ord_id']
                per_ord_dic['order_id'] = ordr_id
                per_ord_dic['perc'] = perc
                ord_per_list.append(per_ord_dic)
            # print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            # print('ord_per_list',ord_per_list)

            context = {
                "order_list":serialz.data,
                "ord_per_list":ord_per_list,
                "message":"success"
            }
            return Response(context,status=status.HTTP_200_OK)
        
        except Exception as e:
            print('Excepion',e)
            context = {
                            "data": '',
                            "message":'some internal error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST) 







class CarrierListByStatus(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has customer company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        status_name = request.GET['status']
        search = request.GET["search"]
        customercarrier = ''
        serializ = ''
        if (status_name != "all carr_cust"):
            status_ = Status.objects.filter(status_name = status_name).first()
            if search != "":
                customercarrier = CustomerCarrier.objects.filter(CustomercompanyID_id = int(content[0][0]), CarriercompanyID__name__icontains = search ,r_status = status_)    
            else:
                customercarrier = CustomerCarrier.objects.filter(CustomercompanyID_id = int(content[0][0]) , r_status = status_)
            print("customercarrier",customercarrier)
            serializ = CustomerCarrierListSerializer(customercarrier, many =True)
            
        else:
            
            if search != "":
                customercarrier = CustomerCarrier.objects.filter(CustomercompanyID_id = int(content[0][0]), CarriercompanyID__name__icontains = search)    
            else:
                customercarrier = CustomerCarrier.objects.filter(CustomercompanyID_id = int(content[0][0]))
            serializ = CustomerCarrierListSerializer(customercarrier, many =True) 
        try:
            carrier_list = customercarrier.values_list('CarriercompanyID')
            print('carrier_list',carrier_list)
            carr_dot = []
            for tup in carrier_list:
                dic_carr_dot = {}
                for carr_com_ in tup:  
                    ord_obj_id_lst = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), CarrierCompanyID_id = int(carr_com_) , is_delete = False).values_list('id')
                    super_list = []
                    order_driver_id_list = OrderDriver.objects.filter(OrderID_id__in = ord_obj_id_lst, is_delete = False).values_list('id') 
                    delivery_obj = Drop.objects.filter(OrderDriverID_id__in = order_driver_id_list, is_delete = False).values_list('id','OrderDriverID','delivery_date_time_to','actual_delivery_date_time')          
                    li = []            
                    for i in delivery_obj:
                        order_dot = {}
                        if i[3] and i[2]:
                            order_driver_obj = OrderDriver.objects.filter(id = i[1]).first()
                            od_id = order_driver_obj.OrderID.id   
                            dot =  i[3]-i[2]
                            order_dot['order_id'] = od_id
                            order_dot['dot'] = dot
                            li.append(order_dot)

                    oid = []
                    final_dot = []
                    for j in li:
                        dics = {
                            "dot":[]
                        }
                        if j['order_id']:
                            if j['order_id'] not in oid:
                                oid.append(j['order_id'])
                                dics['ord_id'] = j['order_id']
                                for jt in li:
                                    if jt['order_id'] == j['order_id']:
                                        dics['dot'].append(jt['dot'])
                        
                        final_dot.append(dics)
                                
                    sort_final = []
                    for aq in final_dot:
                        if aq['dot'] == []:
                            continue
                        else:
                            sort_final.append(aq)
                    
                    ord_per_list = []
                    for qw in sort_final:
                        tcount = 0
                        delaycount = 0
                        
                        per_ord_dic = {}
                        for tm in qw['dot']:
                            tcount = tcount+1

                            if tm >= timedelta(0):
                    
                                delaycount = delaycount+1
                        ratio_by_total = 100/tcount
                        perc = 100 - ratio_by_total*delaycount
                    
                        ordr_id = qw['ord_id']
                        per_ord_dic['order_id'] = ordr_id
                        per_ord_dic['perc'] = perc
                        super_list.append(perc)
                        ord_per_list.append(per_ord_dic)
                    final_per_val = 0
                    for j in super_list:
                        final_per_val = final_per_val+j
                    
                    dic_carr_dot['carr_id'] = int(carr_com_)
                    if len(super_list):
                        dic_carr_dot['dot'] = final_per_val/len(super_list)
                        carr_dot.append(dic_carr_dot)
            context = {
                "data":serializ.data,
                "carr_dot":carr_dot,
                "message":"success"
            }
            return Response(context , status=status.HTTP_200_OK)    
        except Exception as e:
            print('Exception',e)
            context = {
                            "data": '',
                            "message":'Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


import json
class CustomerSetting(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has customer company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        data = request.data['payload']
        data = json.loads(data)
         
        cus_com_obj = Company.objects.filter(id = int(content[0][0])).update(name = data['com_name'],
                                            location = data['address1'], location2 = data['address2'], pin = data['pin'], country = data['country']) 
        
        customer_obj = CustomerExtraInfo.objects.filter(id = int(content[0][1])).update(name = data['cust_name'], email = data['email'], mobile_num = data['phone'])
        customer_ = CustomerExtraInfo.objects.get(id = int(content[0][1]))
        
        user_obj = User.objects.filter(id = customer_.CustomUserID.id).update(user_name = data['cust_name'] ,email = data['email'], phone = data['phone'])

        if request.data['file']:
            f = request.data['file']
            fileToUpload = f
            cloudFilename ="media/customer/"+fileToUpload.name
            obj = CustomerExtraInfo.objects.filter(id = content[0][1])
            
            obj.update(cust_image = BUCKET_URL+cloudFilename)
            obj_url = CustomerExtraInfo.objects.filter(id = content[0][1]).values_list("cust_image")
            local_url = obj_url
            

            session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
            s3 = session.resource('s3')
            s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=fileToUpload, ACL='public-read')
            context = {
                    "image_url":BUCKET_URL+str(cloudFilename),
                    "message":"update success"
                }
            return Response(context,status=status.HTTP_200_OK)
        else:
            obj_url = CustomerExtraInfo.objects.filter(id = content[0][1]).values_list("cust_image")
            context = {
                        "image_url":obj_url,
                        "message":"update success"
                    }
            return Response(context,status=status.HTTP_200_OK)

class StatusCountCustomer(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:    
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
            
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            
            st_pending = Status.objects.filter(status_name = "pending")
        
            st_cancelled = Status.objects.filter(status_name = "cancelled")
            st_process = Status.objects.filter(status_name = "in process")
            st_unassigned = Status.objects.filter(status_name = "unassigned")
            st_delivered = Status.objects.filter(status_name = "delivered")
        
        except Exception as e:
            print('status object error',e)
            context = {
                    "data": '',
                    "message":'internal error'
                }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        # try:
        
        all_order_count = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), is_delete = False).count()
        
        
        pending_order_count = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), StatusID = st_pending, is_delete = False, is_active = True).count()
        
        cancell_order_count = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), StatusID = st_cancelled, is_delete = False , is_active = True).count()
        
        process_order_count = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), StatusID = st_process, is_delete = False , is_active = True).count()
        
        unassigned_order_count = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), StatusID = st_unassigned, is_delete = False , is_active = True).count()
        
        delivered_order_count = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), StatusID = st_delivered, is_delete = False , is_active = True).count()
        
        draft = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]),is_delete = False, is_active =False ).count()
        
        

        # except Exception as e:
        #     print('order object error',e)
        #     context = {
        #             "data": '',
        #             "message":'internal error'
        #         }
        #     return Response(context,status=status.HTTP_400_BAD_REQUEST)
        dic = {
            "all":all_order_count,
            "pending":pending_order_count,
            "cancelled":cancell_order_count,
            "unassigned":unassigned_order_count,
            "process":process_order_count,
            "delivered":delivered_order_count,
            "draft":draft
            }   
        return Response(dic,status=status.HTTP_200_OK)

class CusOrderNoByType(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def get(self, request):
        sid = transaction.savepoint()
        try:    
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
            
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            
            st_pending = Status.objects.filter(status_name = "pending")
        
            st_cancelled = Status.objects.filter(status_name = "cancelled")
            st_process = Status.objects.filter(status_name = "in process")
            st_unassigned = Status.objects.filter(status_name = "unassigned")
            st_delivered = Status.objects.filter(status_name = "delivered")
        
        except Exception as e:
            print('status object error',e)
            context = {
                    "data": '',
                    "message":'internal error'
                }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
        
            all_order_count = Order.objects.filter(CustomerCompanyID = int(content[0][0]), is_delete = False).values_list('id','job_id')
            
            
            pending_order_count = Order.objects.filter(CustomerCompanyID = int(content[0][0]), StatusID = st_pending, is_delete = False, is_active = True).values_list('id','job_id')
            
            cancell_order_count = Order.objects.filter(CustomerCompanyID = int(content[0][0]), StatusID = st_cancelled, is_delete = False , is_active = True).values_list('id','job_id')
        
            process_order_count = Order.objects.filter(CustomerCompanyID = int(content[0][0]), StatusID = st_process, is_delete = False , is_active = True).values_list('id','job_id')
            
            unassigned_order_count = Order.objects.filter(CustomerCompanyID = int(content[0][0]), StatusID = st_unassigned, is_delete = False , is_active = True).values_list('id','job_id')
            
            delivered_order_count = Order.objects.filter(CustomerCompanyID = int(content[0][0]), StatusID = st_delivered, is_delete = False , is_active = True).values_list('id','job_id')
            
            draft = Order.objects.filter(CustomerCompanyID = int(content[0][0]),is_delete = False, is_active =False ).values_list('id','job_id')
        
        

       
            dic = {
                "all":all_order_count,
                "pending":pending_order_count,
                "cancelled":cancell_order_count,
                "unassigned":unassigned_order_count,
                "process":process_order_count,
                "delivered":delivered_order_count,
                "draft":draft
                }   
            return Response(dic,status=status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status = status.HTTP_400_BAD_REQUEST)


class CarrierListByStatusCount(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has customer company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        carr_id = request.GET["carr_id"]        
        pending = Order.objects.filter(CustomerCompanyID = int(content[0][0]), CarrierCompanyID = int(carr_id), StatusID__status_name = "pending", is_delete = False, is_active = True).count()
        unassigned = Order.objects.filter(CustomerCompanyID = int(content[0][0]), CarrierCompanyID = int(carr_id), StatusID__status_name = "unassigned", is_delete = False, is_active = True).count()
        in_process = Order.objects.filter(CustomerCompanyID = int(content[0][0]), CarrierCompanyID = int(carr_id), StatusID__status_name = "in process", is_delete = False, is_active = True).count()
        delivered = Order.objects.filter(CustomerCompanyID = int(content[0][0]), CarrierCompanyID = int(carr_id), StatusID__status_name = "delivered", is_delete = False, is_active = True).count()

        context = {
            "pending":pending,
            "unassigned":unassigned,
            "in_process":in_process,
            "delivered":delivered
        }
        return Response(context , status=status.HTTP_200_OK)  



class CustomerResetPass(APIView):
    permission_classes = (IsAuthenticated,)
    def put(self, request):
        try:
            key = request.auth
            user = request.user
            data = request.data
            current_pass = data['current_password']
            user = authenticate(email = (request.data['email']).lower(), password = data['current_password'])
            if user:
                try:
                    user.set_password(data['new_password'])
                except Exception as e:
                    print("key error 'new_password'",e)
                    context = {
                        "message":"key error 'new_password'",
                        "data":''
                    }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)
                user.save()
                context = {
                    "message":"password update success",
                    "data":""
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                context = {
                    "message":"current password entered is incorrect, try again",
                    "data":""
                }    
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print('error in key.auth or token object',e)
            context = {
                "message":"internal error",
                "data":''
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        
class CustomerDashCarrierCount(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:    
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
            
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        all_carr_count = CustomerCarrier.objects.filter(CustomercompanyID_id = int(content[0][0])).count()
        st_added = Status.objects.filter(status_name = "added carr_cust").first()
        added_carr_count = CustomerCarrier.objects.filter(CustomercompanyID_id = int(content[0][0]), r_status = st_added).count()
        st_pending = Status.objects.filter(status_name = "pending carr_cust").first()
        pending_carr_count = CustomerCarrier.objects.filter(CustomercompanyID_id = int(content[0][0]), r_status = st_pending ).count()

        context = {
            "all_carr_com_count":all_carr_count,
            "added_carr_com_count":added_carr_count,
            "pending_carr_com_count":pending_carr_count,
            "message":"success"
        }
        return Response(context,status=status.HTTP_200_OK)


class CarrierDetails(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:    
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
            
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        carr_com_id = int(request.data['carr_com_id'])
        carr_comp_user = CompanyUser.objects.filter(CompanyID_id = carr_com_id).first()
        carrier_extra = CarrierExtraInfo.objects.filter(id = carr_comp_user.Created_by_CarrierID.id).values_list('carr_image')
        company_data = Company.objects.filter(id = int(carr_com_id)).values_list('name','mobile_num','location','email')
        
        order_ins = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), CarrierCompanyID_id = int(carr_com_id), is_delete = False, is_active = True).order_by('-id')
        
        serialz = OrderStatusCarrSerializer(order_ins, many =True)
        
        context = {
            "company_data":{
                "company_name":company_data[0][0],
                "com_mob_no":company_data[0][1],
                "com_address":company_data[0][2],
                "com_email":company_data[0][3],
                "carrier_img":carrier_extra[0][0]
            },
            "order_data":serialz.data
        }
        return Response(context,status=status.HTTP_200_OK)



class CarrierCompanyList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        carrier_com_ = CustomerCarrier.objects.filter(CustomercompanyID_id = int(content[0][0])).values_list("CarriercompanyID")
        
        
        company_obj = Company.objects.filter(id__in = carrier_com_ )
        

        serializ = CompanySerializer(company_obj, many = True)
        
        return Response(serializ.data)

###################################################################

def search_name(q_,cust_id):
    obj_cus_carr = CustomerCarrier.objects.filter(CustomercompanyID_id = cust_id).values_list("CarriercompanyID")
    company_id_list = Company.objects.filter(id__in = obj_cus_carr, name__icontains = q_).values_list("id")
    
    order_obj  = Order.objects.filter(CarrierCompanyID__in = company_id_list, CustomerCompanyID_id = int(cust_id), is_delete = False)
    
    result_set = order_obj
   
    return result_set

def search_status(q_,cust_id):
    st_obj = Status.objects.filter(status_name__contains = q_).values_list("id")
    order_obj = Order.objects.filter(CustomerCompanyID_id = cust_id ,StatusID__in = st_obj, is_delete = False)
    
    result_set = order_obj
    return result_set
    

def search_dt(q_,cust_id):

    order_obj = Order.objects.filter(CustomerCompanyID_id = cust_id ,order_create_time__contains=q_, is_delete = False)

    result_set = order_obj
    return result_set
    


def search_job(q_,cust_id):

    order_obj = Order.objects.filter(CustomerCompanyID_id = cust_id ,job_id__icontains=q_, is_delete = False)
    result_set = order_obj        
    return result_set
    

def search_pick(q_,cust_id):
    ord_obj = Order.objects.filter(CustomerCompanyID_id = cust_id, is_delete = False).values_list('id')
    pick_obj = Pick.objects.filter(OrderID__in = ord_obj, pickup_date_time__contains=q_, is_delete = False).values_list('OrderID')
    order_obj = Order.objects.filter(id__in = pick_obj, CustomerCompanyID_id = cust_id, is_delete = False)
    
    result_set = order_obj        
    return result_set
             

def search_deli(q_,cust_id):
    ord_obj = Order.objects.filter(CustomerCompanyID_id = cust_id, is_delete = False).values_list('id')
    drop_obj = Drop.objects.filter(OrderID__in = ord_obj, delivery_date_time__contains=q_, is_delete = False).values_list('OrderID')
    order_obj = Order.objects.filter(id__in = drop_obj, CustomerCompanyID_id = cust_id, is_delete = False)

        
    result_set = order_obj        
    return result_set
    

class CustOrderSearch(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        # try:
        key = request.auth
        content = Token.objects.filter(key = key).values_list("content_id")
        cust_id = int(content[0][0])
        # try:
        q_search = request.GET["search"]
            # try:
        if q_search:
            # s=Order.objects.none()
            final_order = []

            # r_status = search_status(q_search,cust_id)
            # final_order.append(r_status)
            # r_dt     = search_dt(q_search,cust_id)
            # final_order.append(r_dt)
            r_job    = search_job(q_search,cust_id)
            final_order.append(r_job)
            r_name   = search_name(q_search,cust_id)
            final_order.append(r_name)
            # r_pick  = search_pick(q_search,cust_id)
            # final_order.append(r_pick)
            # r_deliv  = search_deli(q_search,cust_id)
            # final_order.append(r_deliv)
            Se = set()

            for data in final_order:
                
                if data:
                    for data_in in data:
                    
                        Se.add(data_in)
                else:
                    continue
    

            or_list = list(Se)
            or_list.sort(key=lambda x: x.created_DT, reverse=True)
            try:
                final_result = OrderStatusCarrSerializer(or_list, many = True)
                ord_obj_id_lst = Order.objects.filter(CustomerCompanyID_id = int(content[0][0]), is_delete = False).values_list('id')
                order_driver_id_list = OrderDriver.objects.filter(OrderID_id__in = ord_obj_id_lst, is_delete = False).values_list('id')
                delivery_obj = Drop.objects.filter(OrderDriverID_id__in = order_driver_id_list, is_delete = False).values_list('id','OrderDriverID','delivery_date_time_to','actual_delivery_date_time')
                li = []
                
                for i in delivery_obj:
                    order_dot = {}
                    if i[3] and i[2]:
                        order_driver_obj = OrderDriver.objects.filter(id = i[1]).first()
                        od_id = order_driver_obj.OrderID.id
                        dot =  i[3]-i[2]
                        order_dot['order_id'] = od_id
                        order_dot['dot'] = dot
                        li.append(order_dot)
                oid = []
                final_dot = []
                for j in li:
                    dics = {
                        "dot":[]
                    }
                    if j['order_id']:
                        if j['order_id'] not in oid:
                            oid.append(j['order_id'])
                            dics['ord_id'] = j['order_id']
                            for jt in li:
                                if jt['order_id'] == j['order_id']:
                                    dics['dot'].append(jt['dot'])
                    
                    final_dot.append(dics)
                sort_final = []
                for aq in final_dot:
                    if aq['dot'] == []:
                        continue
                    else:
                        sort_final.append(aq)
                
                ord_per_list = []
                for qw in sort_final:
                    tcount = 0
                    delaycount = 0
                    
                    per_ord_dic = {}
                    for tm in qw['dot']:
                        tcount = tcount+1

                        if tm >= timedelta(0):
                            delaycount = delaycount+1
                    ratio_by_total = 100/tcount
                    perc = 100 - ratio_by_total*delaycount
                    ordr_id = qw['ord_id']
                    per_ord_dic['order_id'] = ordr_id
                    per_ord_dic['perc'] = perc
                    ord_per_list.append(per_ord_dic)
                
                context = {
                    "order_list":final_result.data,
                    "ord_per_list":ord_per_list,
                    "message":"success"
                    }
                return Response(context,status=status.HTTP_200_OK)
            except Exception as e:
                print('error while serializer object creation',str(e))
                context = {
                "message":"internal error",
                "data":''
                }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        else:
            print('q_search is null')
            context = {
            "message":"search is null",
            "data":''
            }
        return Response(context,status=status.HTTP_400_BAD_REQUEST)
    
import io
import xlsxwriter
from django.http import StreamingHttpResponse
from datetime import date, time
class CustExcelDownload(APIView):
    def get(self, request):
        customer_drop_down = []
        cust_com_id = request.GET['cust_com_id']
        try:
            carrier_arr  = CustomerCarrier.objects.filter(CustomercompanyID_id = int(cust_com_id)).values_list("CarriercompanyID")
            print('carrier_arr',carrier_arr)
            carrier_name = Company.objects.filter(id__in = carrier_arr).values_list('id','name')
            print('carrier_name',carrier_name)
        except Exception as e:
            print('query error',e)
        carrier_arr = [str(i[0])+':'+str(i[1]) for i in carrier_name]
        
        # myFile = open('countries.csv', 'w')
        # with myFile:
        #     myFields = ['job_id','customer_code','CustomerCompanyID','price','pickup_location','pickup_date','pickup_time','delivery_location','delivery_date','delivery_time','load_carried','load_type','load_weight','length_load_dimen','width_load_dimen','height_load_dimen','measure_unit','units','hazardous','additional_note','field_name','field_value']
        #     writer = csv.DictWriter(myFile, fieldnames=myFields)
        #     writer.writeheader()
        #     writer.writerow({'CustomerCompanyID' : customer_arr})
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet()
        header_format = workbook.add_format({
                                    'border': 1,
                                    'bg_color': '#C6EFCE',
                                    'bold': True,
                                    'text_wrap': True,
                                    'valign': 'vcenter',
                                    'indent': 1,
                                    })
        worksheet.write('A1', 'Job No(*)', header_format) 
        worksheet.write('B1', 'Customer Code', header_format)
        worksheet.write('C1', 'Carrier Name(*)', header_format)
        worksheet.write('D1', 'Price', header_format)
        worksheet.write('E1', 'Pickup Location(*)', header_format)
        worksheet.write('F1', 'Pickup Date From(*)', header_format)
        worksheet.write('G1', 'Pickup Time From(*)', header_format)
        worksheet.write('H1', 'Pickup Date To(*)', header_format)
        worksheet.write('I1', 'Pickup Time To(*)', header_format)
        worksheet.write('J1', 'Delivery Location(*)', header_format)
        worksheet.write('K1', 'Delivery Date From(*)', header_format)
        worksheet.write('L1', 'Delivery Time From(*)', header_format)
        worksheet.write('M1', 'Delivery Date To(*)', header_format)
        worksheet.write('N1', 'Delivery Time To(*)', header_format)
        worksheet.write('O1', 'Load Carried', header_format)
        worksheet.write('P1', 'Load Type', header_format)
        worksheet.write('Q1', 'Load Weight', header_format)
        worksheet.write('R1', 'Length(Load Dimentions)', header_format)
        worksheet.write('S1', 'Width(Load Dimentions)', header_format)
        worksheet.write('T1', 'Height(Load Dimentions)', header_format)
        worksheet.write('U1', 'Measure Unit', header_format)
        worksheet.write('V1', 'Number of Units', header_format)
        worksheet.write('W1', 'Contains Hazardous', header_format)
        worksheet.write('X1', 'Additional Note', header_format)
        worksheet.write('Y1', 'Sender', header_format)
        worksheet.write('Z1', 'Receiver', header_format)
        worksheet.write('AA1', 'Extra Field Name', header_format)
        worksheet.write('AB1', 'Extra Field Value', header_format)
        
        worksheet.data_validation('C2:C1000', {'validate': 'list',
                                  'source':carrier_arr },
                                  
                                  )
        
        worksheet.data_validation('W2:W1000', {'validate': 'list',
                                'source':['YES','NO'] },)
        

        worksheet.data_validation('F2:F1000', {'validate': 'date',
                                'criteria': 'between',
                                'minimum': date(2013, 1, 1),
                                'maximum': date(3000, 12, 12),
                                'input_title': 'Date Field',
                                'input_message':'formate: YYYY-MM-DD'
                                })

        worksheet.data_validation('K2:K1000', {'validate': 'date',
                                'criteria': 'between',
                                'minimum': date(2013, 1, 1),
                                'maximum': date(3000, 12, 12),
                                'input_title': 'Date Field',
                                'input_message':'formate: YYYY-MM-DD'
                                })                          
        
        worksheet.data_validation('H2:H1000', {'validate': 'date',
                                'criteria': 'between',
                                'minimum': date(2013, 1, 1),
                                'maximum': date(3000, 12, 12),
                                'input_title': 'Date Field',
                                'input_message':'formate: YYYY-MM-DD'
                                })
        
        worksheet.data_validation('M2:M1000', {'validate': 'date',
                                'criteria': 'between',
                                'minimum': date(2013, 1, 1),
                                'maximum': date(3000, 12, 12),
                                'input_title': 'Date Field',
                                'input_message':'formate: YYYY-MM-DD'
                                })
        
        worksheet.data_validation('G2:G1000', {'validate': 'time',
                                'minimum': time(0, 0),
                                'maximum': time(23, 59,59),
                                'input_title': 'Time Field',
                                'input_message': 'formate: 24 hour, example: 23:59:00',
                                'criteria': 'between'}) 

        
        worksheet.data_validation('L2:L1000', {'validate': 'time',
                                'minimum': time(0, 0),
                                'maximum': time(23, 59, 59),
                                'input_title': 'Time Field',
                                'input_message': 'formate: 24 hour,example: 23:59:00',
                                'criteria': 'between'})

        worksheet.data_validation('I2:I1000', {'validate': 'time',
                                'minimum': time(0, 0),
                                'maximum': time(23, 59, 59),
                                'input_title': 'Time Field',
                                'input_message': 'formate: 24 hour,example: 23:59:00',
                                'criteria': 'between'})

        worksheet.data_validation('N2:N1000', {'validate': 'time',
                                'minimum': time(0, 0),
                                'maximum': time(23, 59, 59),
                                'input_title': 'Time Field',
                                'input_message': 'formate: 24 hour,example: 23:59:00',
                                'criteria': 'between'})                                                                                                                                                       

        workbook.close()
        output.seek(0)

        response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename=ordercreate.xlsx"

        output.close()

        return response



class CustOrderCsvDownload(APIView):
    def get(self, request):    
        order_obj = ''
        cust_com_id = int(request.GET['cust_com_id'])

        if request.GET['orderlist']:
            order_id = request.GET['orderlist']
            res = order_id.strip('][').split(',')
            print('res',res)
            res = [int(i) for i in res]
            order_obj = Order.objects.filter(id__in = res,CustomerCompanyID_id = cust_com_id, is_delete = False)
        else:
            order_obj = Order.objects.filter(CustomerCompanyID_id = cust_com_id, is_delete = False)
        
        order_serializer = CarrOrderCsvDownloadSerializer(order_obj, many = True)
        data = order_serializer.data

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet()
        header_format = workbook.add_format({
                                    'border': 1,
                                    'bg_color': '#C6EFCE',
                                    'bold': True,
                                    'text_wrap': True,
                                    'valign': 'vcenter',
                                    'indent': 1,
                                    })
        worksheet.write('A1', 'Job No', header_format) 
        worksheet.write('B1', 'Customer Code', header_format)
        worksheet.write('C1', 'Customer Name', header_format)
        worksheet.write('D1', 'Carrier Name', header_format)
        worksheet.write('E1', 'Price', header_format)
        worksheet.write('F1', 'Status', header_format)
        worksheet.write('G1', 'Load Carried', header_format)
        worksheet.write('H1', 'Load Type', header_format)
        worksheet.write('I1', 'Load Weight', header_format)
        worksheet.write('J1', 'Length(Load Dimentions)', header_format)
        worksheet.write('K1', 'Width(Load Dimentions)', header_format)
        worksheet.write('L1', 'Height(Load Dimentions)', header_format)
        worksheet.write('M1', 'Measure Unit', header_format)
        worksheet.write('N1', 'Number of Units', header_format)
        worksheet.write('O1', 'Contains Hazardous', header_format)
        worksheet.write('P1', 'Additional Note', header_format)
        worksheet.write('Q1', 'Pickup Date Time', header_format)
        worksheet.write('R1', 'PickUp Address', header_format)
        worksheet.write('S1', 'Delivery Date Time', header_format)
        worksheet.write('T1', 'Delivery Address', header_format)
        worksheet.write('U1', 'Comments', header_format)
        worksheet.write('V1', 'Extra Field Name', header_format)
        worksheet.write('W1', 'Extra Field Value', header_format)
        worksheet.write('X1', 'Attachment Name', header_format)
        row = 1
        for data_in in data:    
            com_count = 0
            if data_in['comments']:
                com_count = len(data_in['comments'])
            att_count = 0
            if data_in['order_attach']:
                att_count = len(data_in['order_attach'])
            ex_field = 0
            if data_in["extra_fields"]:
                ex_field = len(data_in["extra_fields"])
            od_pick = 0
            if data_in["order_pick"]:
                od_pick = len(data_in["order_pick"])
            od_dr = 0
            if data_in['order_drop']:
                od_dr = len(data_in['order_drop'])
            ord_his = 0 
            if data_in["order_history"]:
                ord_his = len(data_in["order_history"])
            x = max(com_count,att_count,ex_field,od_pick,od_dr,ord_his)
        
            worksheet.write(row,0,data_in['job_id'])
            
            worksheet.write(row,1,data_in['customer_code'])
            
            worksheet.write(row,2,data_in['CustomerCompany'])
            
            worksheet.write(row,3,data_in['CarrierCompanyID'])

            worksheet.write(row,4,data_in['price'])
            
            worksheet.write(row,5,data_in['Status'])
            
            worksheet.write(row,6,data_in['load_carried'])
            
            worksheet.write(row,7,data_in['load_type'])
            
            worksheet.write(row,8,data_in['load_weight'])
            
            worksheet.write(row,9,data_in['length_load_dimen'])
            
            worksheet.write(row,10,data_in['width_load_dimen'])
            
            worksheet.write(row,11,data_in['height_load_dimen'])
            
            worksheet.write(row,12,data_in['measure_unit'])
            
            worksheet.write(row,13,data_in['units'])
            
            HZ =  data_in['hazardous']
            HVAL = ''
            if HZ == False:
                HVAL = 'NO'
            if HZ == True:
                HVAL = 'YES'
            worksheet.write(row,14,HVAL)
            
            worksheet.write(row,15,data_in['additional_note'])

            for indexes in range(0,x):
                if data_in['order_pick'] != []:
                    if indexes < od_pick:
    
                        for key, val in (data_in['order_pick'][indexes]).items():
                            if key == 'pickup_date_time':
                                if val:
                                    datetime_object = datetime.strptime(val, '%Y-%m-%dT%H:%M:%SZ')
                                    old_format = '%Y-%m-%d %H:%M:%S'
                                    new_format = '%d/%m/%Y %I:%M %p'
                                    new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)
                                    worksheet.write(row,16,new_datetime_str)
                            if key == 'address':
                                worksheet.write(row,17,val)


                if data_in['order_drop'] != []:
                    if indexes < od_dr:
                        for key,val in (data_in['order_drop'][indexes]).items(): 
                            if key == 'delivery_date_time':
                                print('val',val)
                                if val:
                                    datetime_object = datetime.strptime(val, '%Y-%m-%dT%H:%M:%SZ')
                                    old_format = '%Y-%m-%d %H:%M:%S'
                                    new_format = '%d/%m/%Y %I:%M %p'
                                    new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)
                                    worksheet.write(row,18,new_datetime_str)
                            if key == 'address':    
                                worksheet.write(row,19,val)

                if data_in['comments'] != []:
                    if indexes < com_count:
                        for key,val in (data_in['comments'][indexes]).items(): 
                            if key == 'comment':
                                worksheet.write(row,20,val)

                if data_in['extra_fields'] != []:
                    if indexes < ex_field:
                        for key,val in (data_in['extra_fields'][indexes]).items(): 
                            if key == "field_name":
                                worksheet.write(row,21,val)
                            if key == "field_value":
                                worksheet.write(row,22,val)

                if data_in['order_attach'] != []:
                    if indexes < att_count:
                        for key,val in (data_in['order_attach'][indexes]).items(): 
                            if key == "attachment_name":
                                worksheet.write(row,23,val)
                
                row = row +1

        workbook.close()
        output.seek(0)

        response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename=orders.xlsx"

        output.close()

        return response


class CarrierListDownlaod(APIView):
    def get(self, request):
        if request.GET['carrier_com_id_list']:
            carr_com_id_list = request.GET['carrier_com_id_list']
            cust_com_id      = int(request.GET['cust_com_id'])
            res = carr_com_id_list.strip('][').split(',')
            res = [int(i) for i in res]
            print('res',res)
            cuscarr = CustomerCarrier.objects.filter(CarriercompanyID_id__in = res, CustomercompanyID_id = cust_com_id )
            serializ = CustomerCarrierListSerializer(cuscarr, many =True)
            data = serializ.data
            
            output = io.BytesIO()
            workbook = xlsxwriter.Workbook(output, {'in_memory': True})
            worksheet = workbook.add_worksheet()
            header_format = workbook.add_format({
                                        'border': 1,
                                        'bg_color': '#C6EFCE',
                                        'bold': True,
                                        'text_wrap': True,
                                        'valign': 'vcenter',
                                        'indent': 1,
                                        })
            worksheet.write('A1', 'Carrier Name', header_format) 
            worksheet.write('B1', 'Phone Number', header_format)
            worksheet.write('C1', 'Email', header_format)
            worksheet.write('D1', 'Address', header_format)
            worksheet.write('E1', 'Status', header_format)
            
            row = 1
            print('data',len(data))
            for data_in in data:
                print('data_in',data_in)    
                for key, val in data_in.items():
                    
                    if key == "CarriercompanyID":
                        for key1, val1 in val.items():
                            if key1 == "name":
                                worksheet.write(row,0,val1)
                            
                            if key1 == "mobile_num":
                                worksheet.write(row,1,val1)

                            if key1 == "email":
                                worksheet.write(row,2,val1)
                            
                            if key1 == "location":
                                worksheet.write(row,3,val1)

                    if key == "r_status":
                        if val != None:
                            for key1, val1 in val.items():
                                if (key1 == "status_name") and (val1 != None):
                                    if val1 == "added carr_cust":
                                        worksheet.write(row,4,"added")
                                    if val1 == "pending carr_cust":
                                        worksheet.write(row,4,"pending")

                row = row + 1 

            workbook.close()
            output.seek(0)

            response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = "attachment; filename=carriers.xlsx"

            output.close()

            return response


class CarrierInvite(APIView): 
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            req_email = (request.data["email"]).lower()
            check_em = user_email.email_check(req_email)
            extra_details = request.data['extra_details']
            email = (request.data['email']).lower()
            if (check_em['message'] == "") or (check_em['message'] == "Carrier"):
                if (check_em['message'] == "Carrier"):
                    carr_obj = CarrierExtraInfo.objects.filter(email = req_email).first()
                    if carr_obj:
                        carr_id = carr_obj.id
                        get_com = CompanyUser.objects.filter(Created_by_CarrierID_id = carr_id).first()
                        carr_com_id = Company.objects.filter(id = get_com.id).first()
                        st = Status.objects.filter(status_name = "pending carr_cust").first()
                        cuscarr, created = CustomerCarrier.objects.get_or_create(CustomercompanyID_id = int(content[0][0]), CarriercompanyID_id = int(carr_com_id.id) ,defaults = {"r_status_id":st.id, "email":email, **extra_details})
                        if created:
                            cuscarr.r_status = st
                            cuscarr.save()
                            cus_com_name = cuscarr.CustomercompanyID.name
                            cus_com_id   = cuscarr.CustomercompanyID.id
                            subject, from_email, to = 'Invitation to kargologic platform', settings.EMAIL_HOST_USER, req_email
                            text_content = 'Welcome to KargoLogic, your are invited by Customer "%s"'%(cus_com_name)
                            url = SERVER_REACT_URL+"/carrier/signup/?email="+str(req_email)+"&cust_com_id="+str(cus_com_id)+"&cust_com_name="+str(cus_com_name)
                            html_content = ''
                            if (carr_obj.CustomUserID):
                                html_content = """<h3>Welcome to KargoLogic!</h3><br>
                                <p>You have been invited by {0} to join their KargoLogic network. {0} is using KargoLogic platform to simplify their Supply chain through real time information. KargoLogic is the most user friendly and friction free multi-carrier platform that allows customers to onboard their contracted logistics partners to share, update and retrieve critical supply chain information in realtime.</p>
                                <p>By signing your business will be linked to {0} KargoLogic platform. In doing so, {0} can send you delivery tasks, delivery related documents and special instructions in the platform itself. No need for frequent emails, phone calls, text messages and misplaced paperwork.</p>
                                <p>Once you sign up online, you will be able to set up profiles of your drivers, delivery vehicles and other assets such as trailers easily. Simply review the jobs customers send you and allocate to delivery drivers with a single click or split the jobs between multiple drivers and vehicles for complex requirements. Drivers will be entitled to use the driver app (available on Google Play and Apple store) and complete their delivery milestones while being able to capture digital PODs.</p>
                                <p>In three easy steps you are on your way to start creating delivery orders, allocating orders to the delivery drivers and tracking delivery orders in real-time. Get real-time ETA's, keep all permanently timestamped delivery related history and paperwork in one place and access digital proof of delivery safely stored in the cloud. We are here to help you, so please do not hesitate to get in touch at enquiries@kargologic.com and review our terms of use and privacy policy at www.kargologic.com.</p><br>""".format(cus_com_name)
                            else:
                                html_content = """<h3>Welcome to KargoLogic!</h3><br>
                                <p>You have been invited by {0} to join their KargoLogic network. {0} is using KargoLogic platform to simplify their Supply chain through real time information. KargoLogic is the most user friendly and friction free multi-carrier platform that allows customers to onboard their contracted logistics partners to share, update and retrieve critical supply chain information in realtime.</p>
                                <p>By signing your business will be linked to {0} KargoLogic platform. In doing so, {0} can send you delivery tasks, delivery related documents and special instructions in the platform itself. No need for frequent emails, phone calls, text messages and misplaced paperwork.</p>
                                <p>Once you sign up online, you will be able to set up profiles of your drivers, delivery vehicles and other assets such as trailers easily. Simply review the jobs customers send you and allocate to delivery drivers with a single click or split the jobs between multiple drivers and vehicles for complex requirements. Drivers will be entitled to use the driver app (available on Google Play and Apple store) and complete their delivery milestones while being able to capture digital PODs.</p>
                                <p>In three easy steps you are on your way to start creating delivery orders, allocating orders to the delivery drivers and tracking delivery orders in real-time. Get real-time ETA's, keep all permanently timestamped delivery related history and paperwork in one place and access digital proof of delivery safely stored in the cloud. We are here to help you, so please do not hesitate to get in touch at enquiries@kargologic.com and review our terms of use and privacy policy at www.kargologic.com.</p><br>
                                <strong><a href="{1}">Click here to signup</a></strong>""".format(cus_com_name,url)
                            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                            msg.attach_alternative(html_content, "text/html")
                            se = msg.send()
                            if se:
                                context = {
                                "data":"",
                                "message":"This carrier already exist on Kargologic plateform, now you are linked with this carrier"
                                }
                                return Response(context,status=status.HTTP_200_OK)
                        else:
                            context = {
                                "data":"",
                                "message":"You are already linked with this carrier"
                            }
                            return Response(context,status=status.HTTP_200_OK)
                else:
                    carr_com_create = Company.objects.create(name = extra_details['business_name'], email = email, company_type ="carrier")
                    carr_obj = CarrierExtraInfo.objects.create(email = req_email, name = extra_details['name'], mobile_num = extra_details['mobile_num'])
                    get_com = CompanyUser.objects.create(Created_by_CarrierID = carr_obj, CompanyID = carr_com_create)
                    st = Status.objects.filter(status_name = "added carr_cust").first()
                    cuscarrier = CustomerCarrier.objects.create(CustomercompanyID_id = int(content[0][0]), CarriercompanyID = carr_com_create, r_status = st, email = email, **extra_details)
                    cus_com_name = cuscarrier.CustomercompanyID.name
                    cus_com_id   = cuscarrier.CustomercompanyID.id
                    subject, from_email, to = 'Invitation to kargologic platform', settings.EMAIL_HOST_USER, req_email
                    text_content = 'Welcome to KargoLogic, your are invited by Customer "%s"'%(cus_com_name)
                    url = SERVER_REACT_URL+"/carriersignup/?email="+str(req_email)+"&cust_com_id="+str(cus_com_id)+"&cust_com_name="+str(cus_com_name)
                    
                    html_content = """<h3>Welcome to KargoLogic!</h3><br>
                                <p>You have been invited by {0} to join their KargoLogic network. {0} is using KargoLogic platform to simplify their Supply chain through real time information. KargoLogic is the most user friendly and friction free multi-carrier platform that allows customers to onboard their contracted logistics partners to share, update and retrieve critical supply chain information in realtime.</p>
                                <p>By signing your business will be linked to {0} KargoLogic platform. In doing so, {0} can send you delivery tasks, delivery related documents and special instructions in the platform itself. No need for frequent emails, phone calls, text messages and misplaced paperwork.</p>
                                <p>Once you sign up online, you will be able to set up profiles of your drivers, delivery vehicles and other assets such as trailers easily. Simply review the jobs customers send you and allocate to delivery drivers with a single click or split the jobs between multiple drivers and vehicles for complex requirements. Drivers will be entitled to use the driver app (available on Google Play and Apple store) and complete their delivery milestones while being able to capture digital PODs.</p>
                                <p>In three easy steps you are on your way to start creating delivery orders, allocating orders to the delivery drivers and tracking delivery orders in real-time. Get real-time ETA's, keep all permanently timestamped delivery related history and paperwork in one place and access digital proof of delivery safely stored in the cloud. We are here to help you, so please do not hesitate to get in touch at enquiries@kargologic.com and review our terms of use and privacy policy at www.kargologic.com.</p><br>
                                <strong><a href="{1}">Click here to signup</a></strong>""".format(cus_com_name,url)
                    
                    
                    
                    
                    
                    
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    se = msg.send()
                    if se:
                        context = {
                            "data":"",
                            "message":"Invite to {0} sent successfully".format(extra_details['business_name'])
                        }
                        return Response(context,status=status.HTTP_200_OK)
                    else:
                        transaction.savepoint_rollback(sid)
                        print("email error")
                        context = {
                            "data":"",
                            "message":"Invitation failed, try again"
                        }
                        return Response(context,status=status.HTTP_200_OK)
            else:
                context = {
                    "data":"",
                    "message":"This user already exists as %s on Kargologic platform"%(check_em['message'])
                }
                return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            transaction.savepoint_rollback(sid)
            print("Exception",e)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

 


class CustEditOrder(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request): 
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id") 
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        data = request.data['arr_order'][0]
        order_status = data['order_detail']['is_active']
        if data['order_detail']:
            internal_ord_data = data['order_detail']
            order_detail_update = Order.objects.filter(id = internal_ord_data['order_id']).update(job_id = internal_ord_data['job_id'], customer_code = internal_ord_data['customer_code'],
            CarrierCompanyID = internal_ord_data['CarrierCompanyID'] , price = internal_ord_data['price'], load_carried = internal_ord_data['load_carried'], load_type = internal_ord_data['load_type'], 
            load_weight = internal_ord_data['load_weight'], length_load_dimen = internal_ord_data['length_load_dimen'], width_load_dimen = internal_ord_data['width_load_dimen'], height_load_dimen = internal_ord_data['height_load_dimen'], 
            measure_unit = internal_ord_data['measure_unit'], units = internal_ord_data['units'], hazardous = internal_ord_data['hazardous'], additional_note = internal_ord_data['additional_note'], is_active = internal_ord_data['is_active']) 


        if data['template']:
            internal_templ_data = data['template']
            internal_tem_obj = OrderTemplate.objects.filter(id = int(internal_templ_data['template_id'])).update(template_name = internal_templ_data['template_name'], is_active = order_status)


        if data['pickup']:
            for pick_data in data['pickup']:
                internal_pickup_data = pick_data

                if internal_pickup_data['pickup_id']:
                    try:
                        if (internal_pickup_data['sender'] != {}) and ("kid" in (internal_pickup_data['sender'])):
                            internal_pickup_obj = Pick.objects.filter(id = int(internal_pickup_data['pickup_id'])).update(address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                            latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status, sender_id = internal_pickup_data['sender']['kid'])
                        else:    
                            internal_pickup_obj = Pick.objects.filter(id = int(internal_pickup_data['pickup_id'])).update(address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                            latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status)
                    except TypeError as e:
                        internal_pickup_obj = Pick.objects.filter(id = int(internal_pickup_data['pickup_id'])).update(address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                        latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status)
                else:
                    try:
                        if (internal_pickup_data['sender'] != {}) and ("kid" in (internal_pickup_data['sender'])):
                            internal_pickup_obj = Pick.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                            latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status, sender_id = internal_pickup_data['sender']['kid'])
                        else:
                            internal_pickup_obj = Pick.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                            latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status)
                    except TypeError as e:
                        internal_pickup_obj = Pick.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                        latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status)
        
        if data['delivery']:
            for delivery_data in data['delivery']:

                internal_delivery_data = delivery_data

                if internal_delivery_data['delivery_id']:
                    try:
                        if (internal_delivery_data['receiver'] != {}) and ("kid" in (internal_delivery_data['receiver'])):
                            internal_delivery_obj  = Drop.objects.filter(id = int(internal_delivery_data['delivery_id'])).update(address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status, receiver_id = internal_delivery_data['receiver']['kid']) 
                        else:
                            internal_delivery_obj  = Drop.objects.filter(id = int(internal_delivery_data['delivery_id'])).update(address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status) 
                    except TypeError as e:
                        internal_delivery_obj  = Drop.objects.filter(id = int(internal_delivery_data['delivery_id'])).update(address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status) 
                else:
                    try:
                        if (internal_delivery_data['receiver'] != {}) and ("kid" in (internal_delivery_data['receiver'])):
                            internal_delivery_obj  = Drop.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status, receiver_id = internal_delivery_data['receiver']['kid'])
                        else:
                            internal_delivery_obj  = Drop.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status) 
                    except TypeError as e:
                        internal_delivery_obj  = Drop.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status)

                
        if data['extra_detail']:
            for extra_detail_data in data['extra_detail']:

                in_extra_data = extra_detail_data
                if in_extra_data['id']:
                    in_extra_obj = OrderExtraField.objects.filter(id = int(in_extra_data['id'])).update(field_name = in_extra_data['field_name'], field_value = in_extra_data['field_value'], is_active = order_status)    
                else:
                    if data['template']:
                        in_extra_obj = OrderExtraField.objects.create(OrderID_id = int(data['order_detail']['order_id']), OrderTemplateID_id = int(internal_templ_data['template_id']), field_name = in_extra_data['field_name'], field_value = in_extra_data['field_value'], is_active = order_status)    
                    else:
                        in_extra_obj = OrderExtraField.objects.create(OrderID_id = int(data['order_detail']['order_id']), field_name = in_extra_data['field_name'], field_value = in_extra_data['field_value'], is_active = order_status)        
        
        if data['attachment']:
            
            for attac_data in data['attachment']:
                created_by_cust_id = int(attac_data['Created_by_CustomerID']['id'])
            
                ordAttach = OrderAttachment.objects.create(OrderID_id = int(data['order_detail']['order_id']), Created_by_CustomerID_id = int(created_by_cust_id) ,
            
                attachment_name = attac_data['attachment_name'], physical_path = BUCKET_URL+"orderattachment/"+str(attac_data['attachment_name']))
                
                
                image_str = attac_data['image_read']


                session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
                    

                s3 = session.resource('s3')
                cloudFilename = "orderattachment/"+str(ordAttach.attachment_name)
                s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=image_str, ACL='public-read')
        
        context = {
            "data":"",
            "message":"order updated"
        }
        return Response(context, status=status.HTTP_200_OK)



class CustNotification(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'Token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        cust_notigfy_obj = Notification.objects.filter(created_for_customer_id = int(content[0][1])).order_by('-id')
        serialz = NotificationSerializer(cust_notigfy_obj, many = True)
        context = {
            "data":serialz.data,
            "message":"success"
        }
        return Response(context, status=status.HTTP_200_OK)