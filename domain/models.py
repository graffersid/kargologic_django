from django.db import models

# Create your models here.
class Domain(models.Model):
    name        = models.CharField(max_length = 100, null=True,blank=True)
    description = models.CharField(max_length = 500, null=True,blank=True)
    created_DT  = models.DateTimeField(auto_now_add=True)
    updated_DT  = models.DateTimeField(auto_now=True)
    is_active   = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)
    
    def __str__(self):
        return '%s(%s)'%(self.name,self.id)