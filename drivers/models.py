from django.db import models
from custom_users.models import User
from TruckType.models import TruckType
from user_role.models import UserRole
from domain.models import Domain
# Create your models here.
class DriverExtraInfo(models.Model):
    CustomuserID = models.ForeignKey(User, on_delete = models.CASCADE,null=True,blank=True)
    TruckType    = models.ForeignKey(TruckType, on_delete = models.CASCADE,null=True,blank=True)
    domainID      = models.ForeignKey(Domain, on_delete = models.CASCADE, null=True,blank=True)
    roleID        = models.ForeignKey(UserRole, on_delete = models.CASCADE, null=True,blank=True)
    name         = models.CharField(max_length=100,null=True,blank=True)
    mobile_num   = models.CharField(max_length=100,null=True,blank=True)
    license_num  = models.CharField(max_length = 100,null=True,blank=True)
    email        = models.EmailField(max_length=100,null=True,blank=True)
    address      = models.CharField(max_length = 100,null=True,blank=True)
    image        = models.URLField(null=True,blank=True)
    lat          = models.CharField(max_length=1000,null=True,blank=True)
    lng          = models.CharField(max_length=1000,null=True,blank=True)
    trackingId   = models.CharField(max_length=1000,null=True,blank=True)
    deviceId     = models.CharField(max_length=1000,null=True,blank=True)
    deviceSecret = models.CharField(max_length=1000,null=True,blank=True)
    
    created_DT   = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT   = models.DateTimeField(auto_now=True,null=True,blank=True)
    
    is_active    = models.BooleanField(default=True)
    is_delete    = models.BooleanField(default=False)
    def __str__(self):
        return '%s (%s)' %(self.CustomuserID,self.id)