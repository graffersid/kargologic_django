import random
import string
domains = [ "fake.kargologic.driver.com"]
letters = string.ascii_lowercase[:12]

def get_random_domain(domains):
    return random.choice(domains)

def get_random_name(letters, length):
    return ''.join(random.choice(letters) for i in range(length))

def generate_random_emails(nb, length):
    return [get_random_name(letters, length) + '@' + get_random_domain(domains) for i in range(nb)]

def main():
    print(generate_random_emails(1, 7))

if __name__ == "__main__":
    main()
