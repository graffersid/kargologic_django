from rest_framework import serializers
from drivers.models import DriverExtraInfo
from CarrierDriver.models import CarrierDriver
from custom_users.serializers import UserSerializer
from status.serializers import StatusSerializer
from TruckType.serializers import CarrDrivtoListSerializer, AddMoreDrivSerializer, CarrDrivOrderDetailsTruckTypeSer, CarrGetOrderDetailsTruckSerializer


class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverExtraInfo
        fields = ('id','name')

class DriverExtraInfoSerializer(serializers.ModelSerializer):
    #CustomuserID = UserSerializer(required=False)
    class Meta:
        model  = DriverExtraInfo
        exclude = ('created_DT','updated_DT','is_active')


class AddMoreDriverListSerializer(serializers.ModelSerializer):    
    TruckType = AddMoreDrivSerializer(required=False)
    class Meta:
        model = DriverExtraInfo
        fields = ('id', 'name', 'TruckType')


class CarriertoDriverListSerializer(serializers.ModelSerializer):    
    TruckType = CarrDrivtoListSerializer(required=False)
    class Meta:
        model = DriverExtraInfo
        fields = ('id', 'name', 'address', 'mobile_num', 'license_num', 'image', 'TruckType','trackingId')


class CarrDriverSearchSerializer(serializers.ModelSerializer):
    TruckType = CarrDrivtoListSerializer(required=False)
    class Meta:
        model = DriverExtraInfo
        fields = ('id', 'name', 'address', 'mobile_num', 'license_num', 'image', 'TruckType')


class DrivCarrAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverExtraInfo
        fields = ('id','name')


class CarrDrivOrderDetailsDriverExtraSer(serializers.ModelSerializer):
    TruckType = CarrDrivOrderDetailsTruckTypeSer(required=False)
    class Meta:
        model  = DriverExtraInfo
        fields = ('name','address','mobile_num','license_num','TruckType')

        
class CarrierDriverPersonalSerializer(serializers.ModelSerializer): 
    TruckType = CarrDrivOrderDetailsTruckTypeSer(required=False)
    class Meta:
        model  = DriverExtraInfo
        fields = ('name','email','address','image','mobile_num','license_num','TruckType')


class CarrGetOrderDetailsDriverInfoSerializer(serializers.ModelSerializer):  
    TruckType = CarrGetOrderDetailsTruckSerializer(required=False)
    class Meta:
        model  = DriverExtraInfo
        fields = ('id','name','mobile_num','license_num','TruckType')


class TrackingDriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverExtraInfo
        fields = ('id','name','trackingId')

class DriverCordiSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverExtraInfo
        fields = ('id','name','lat','lng')