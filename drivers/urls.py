from django.conf.urls import url

from . import views

urlpatterns = [
    url('driv_invite_user/', views.DriverCreate.as_view(), name='account-create'),
    url('driv_login/', views.DriverLogIn.as_view(), name='account-login'),
    url('search/',views.CarrDriverSearch.as_view()),
    url('add-more-driver/',views.AddDriverList.as_view()),
    url('drivlist/', views.CarrDriverList.as_view(), name = 'carr-driver'),
    url('driver-list-status/',views.CarrDrivStatus.as_view()),
    url('driv-order-detail/',views.CarrDrivDetail.as_view()),
    url('driv-personal-detail/',views.CarrierDriverpersonalDetail.as_view()),
    url('drivlist_download_carr/',views.CarrDrivListDownload.as_view()),
    url('driver_dashboard/',views.DriverDashboard.as_view()),
    url('driver_job_detail/',views.DriverJobDetail.as_view()),
    url('driver_vehicle_list/',views.DriverVehicleList.as_view()),
    url('unique_user_num/',views.DriverUniqueNumber.as_view()),
    url('driver_update_profile/',views.DriverProfileUpdate.as_view()),
    url('driver_reset_pass/',views.DriverResetPass.as_view()),
    url('driver_pr_upd_email_chk/',views.DriverProfileUpdateEmailCheck.as_view()),
    url('en_route_order/',views.En_RouteOrder.as_view()),
    url('driv_forgot_pass/',views.DriverForgotPass.as_view()),
    url('driver_order_comment/',views.DriverCommentOrder.as_view()),
    url('carr_driver_profile_update/',views.CarrierUpdateDriverProfile.as_view()),
    url('driver_tracker/',views.DriverTracker.as_view()),
    url('cor_driver_update/',views.DriverCordinate.as_view()),
    url('get_driver_cor/',views.GetDriverCordi.as_view()),
    url('notify-driv/',views.DriverNotification.as_view()),
    url('driver_order_count/',views.CarrDrivOrderCount.as_view()),
    url('search_driv/',views.SearchCarrDrivStatus.as_view()),
]
