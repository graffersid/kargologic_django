from django.shortcuts import render
from django.http import HttpRequest
from django.db import transaction
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth import authenticate
from django.contrib.auth.backends import ModelBackend
from django.db import transaction
from datetime import timedelta
from datetime import datetime
import requests
import json
from Delivery.models import Drop
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from OrderAttachment.serializers import DriverJobDetailAttachSerializer
from drivers.serializers import CarrDriverSearchSerializer, DriverCordiSerializer
from Delivery.serializers import AddMoreDropSerializer
from Pickup.serializers import AddMorePickSerializer
from drivers.serializers import CarrierDriverPersonalSerializer
from CarrierDriver.serializers import CarrierDriverListSerializer, AddMoreDriverSerializer, CarrDriverListDownload
from TruckType.serializers import CarrDrivtoListSerializer, VehiclelistTruckTypeSerializer
from orders.serializers import CarrDrivDetailSerializer
from OrderDriver.serializers import CarrierDriverOrderDetailsSer, DriverDashboardSerializer, DriverJobDetailSerializer
from rest_framework import generics, pagination
from django.core.paginator import Paginator
from rest_framework.authtoken.models import Token
from orders.models import Order
from OrderComment.models import OrderComment
from OrderHistory.models import OrderHistory
from OrderAttachment.models import OrderAttachment
from OrderDriver.models import OrderDriver
from CarrierDriver.models import CarrierDriver
from custom_users.models import User
from OrderComment.serializers import OrderCommentSerializer
from .models import DriverExtraInfo
from TruckType.models import TruckType
from status.models import Status
from TruckType.serializers import CarrDrivAddSerializer
import xlsxwriter
from django.http import StreamingHttpResponse
import io
import csv
from TruckCategory.models import TruckCategory
from drivers.random_email import generate_random_emails
from CompanyUser.models import CompanyUser
from company.models import Company
from domain.models import Domain
from user_role.models import UserRole
from django.core.mail import send_mail
from django.conf import settings
import random
import string
from KargoLogics.settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME, EMAIL_HOST_USER,AWS_S3_CUSTOM_DOMAIN,BUCKET_URL,HERE_MAP_USER,HERE_MAP_KEY,HERE_MAP_APPID
from django.core.mail import send_mail, EmailMultiAlternatives
import json
import boto3
from boto3.session import Session
from KargoLogics import settings
from base64 import decodestring
import base64
from django.core.files.base import ContentFile
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http import JsonResponse
from Notifications.models import Notification
from Notifications.serializers import NotificationSerializer, DriverNotificationSerializer
from django.db.models import Q
class ViewPaginatorMixin(object):
    min_limit = 1
    max_limit = 10

    def paginate(self, object_list, page=1, limit=10, **kwargs):
        try:
            page = int(page)
            if page < 1:
                page = 1
        except (TypeError, ValueError):
            page = 1

        try:
            limit = int(limit)
            if limit < self.min_limit:
                limit = self.min_limit
            if limit > self.max_limit:
                limit = self.max_limit
        except (ValueError, TypeError):
            limit = self.max_limit

        paginator = Paginator(object_list, limit)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)
        data = {
            'previous_page': objects.has_previous() and objects.previous_page_number() or None,
            'next_page': objects.has_next() and objects.next_page_number() or None,
            'data': list(objects)
        }
        return data

def randomStringDigits(stringLength=6):
    """Generate a random string of letters and digits """
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choice(lettersAndDigits) for i in range(stringLength))


class DriverCreate(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request, format='json'):
        sid = transaction.savepoint()
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        

        try:
            data = request.data
            driv_carr_status = data['status']
            truck_id = data['truck_id']
            rand_email = generate_random_emails(1, 7)[0]
            email_post = (request.data['email']).lower()
            passw = randomStringDigits(8)
            user_obj = User(
                                                            email       = email_post,
                                                            user_name   = data['user_name'],
                                                            phone       = data['phone']
                                                    )   
            
            user_obj.set_password(passw)                                              
            user_obj.save()
            truck_cate = TruckCategory.objects.filter(id = truck_id).values_list('id')[0][0]
            
            truck_create_obj = TruckType.objects.create(truck_reg_num = data['truck_reg_num'], truck_category_id = truck_cate)
           




            domain_obj = Domain.objects.filter(name = 'KargoLogic')
            user_role_obj = UserRole.objects.filter(role_name = "driver")

            driv_obj = DriverExtraInfo.objects.create(name = data['user_name'],mobile_num = data['phone'],email = email_post, CustomuserID = user_obj, TruckType = truck_create_obj, license_num=data['license_num'], domainID = domain_obj[0], roleID = user_role_obj[0], is_active = False)

            carr_dr_status = Status.objects.filter(status_name = driv_carr_status)
            
            carr_driver = CarrierDriver.objects.create(CarrierCompanyID_id = content[0][0], DriverID = driv_obj, status = carr_dr_status[0], is_active = False)
            com_name   = Company.objects.filter(id = content[0][0]).values('name')[0]['name']
            
            subject, from_email, to = 'Invitation to kargologic platform', settings.EMAIL_HOST_USER, email_post
            text_content ="""Hi {0}<br>
            Welcome to KargoLogic.<br><br>
            You have been invited to join KargoLogic platform by {1}. 
            KargoLogic is a cloud platform that connects customers, transport companies 
            and delivery drivers on a single platform.<br><br>
            By downloading and signing in to a driver app, you will be able to access your 
            delivery runs, access important paperwork for each delivery order, complete your 
            delivery milestones in real time and capture proof of delivery at the end of each 
            delivery.<br><br>
            When you download the app, please follow the prompts to give KargoLogic app the 
            permissions it requires to function properly. We request you to keep your smart 
            device plugged into a charging station while delivering orders.<br><br>

            If you use Android device, please download <strong><a href='https://play.google.com/store/apps/details?id=com.kargologic.app'>KargoLogic Driver app here</a></strong><br><br>
            
            If you use iOS device, please download <strong><a href='https://apps.apple.com/md/app/kargologic/id1467568223'>KargoLogic Driver app here</a></strong><br><br>

            Your login credentials are:<br>
            user phone : {2}<br> 
            user password: {3}<br><br>
            You can access our friendly terms of use and privacy policy 
            on www.kargologic.com. In case you need to report an issue with the driver app, 
            please email us on support@kargologic.com with detailed description of the error 
            or issue.<br><br>
            Thanks again for signing up today and welcome to KargoLogic! """.format(driv_obj.name,com_name,data['phone'],passw)
            
            html_content = str(text_content)
            msg = EmailMultiAlternatives(subject, '', from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            

            driv_id = driv_obj.id

            # payload = {
            # "email": HERE_MAP_USER,
            # "password": HERE_MAP_KEY
            #     }
            # headers = {
            # "Content-Type" : "application/json"
            #     }
            # here_url = "https://tracking.api.here.com/users/v2/login"
            
            # r = requests.post(here_url, data=json.dumps(payload), headers=headers)    
            # resp = r.json()

            # acc_token = resp['accessToken']

            # app_id = HERE_MAP_APPID

            # clam_url = "https://tracking.api.here.com/registry/v2/"+app_id+"/one-device?autoclaim=true"

            # clam_headers = {
            # "Content-Type" : "application/json",
            # "authorization" : "Bearer " + acc_token
            # }
    
            # clam_r = requests.post(clam_url, headers=clam_headers)

            # clam_device_data = clam_r.json()

            # print('clam_device_data',clam_device_data)

            # driv_update_tracking =  DriverExtraInfo.objects.filter(id = int(driv_id)).update(trackingId = clam_device_data['trackingId'], deviceId = clam_device_data['deviceId'], deviceSecret = clam_device_data['deviceSecret'])
            # print('driv_update_tracking',driv_update_tracking)
            # se = ''
            # if driv_update_tracking:
            #     se = msg.send()
            se = msg.send()  #####################
            
            if se:
                context = {
                    "data": data['user_name'],
                    "message": 'Check Inbox and Spam folder for verification e-mail'
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                context = {
                    "data": "",
                    "message": 'problem with sending mail, try again later'
                }
                return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            print('transaction#########')
            transaction.savepoint_rollback(sid)
            context = {
                "data": str(e),
                "message": 'something wents wrong'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)




class DriverLogIn(APIView):
    @transaction.atomic 
    def post(self, request):
        sid = transaction.savepoint()
        data = request.data
        try:
            user = User.objects.get(phone=data['phone'])
            try:
                user_name = user.user_name
                user_email = user.email
                user_phone = user.phone
                pwd_valid = user.check_password(data['password'])
                if pwd_valid:
                    
                    driv_id = DriverExtraInfo.objects.filter(CustomuserID = user).values_list('id','image','TruckType','license_num','trackingId','deviceId','deviceSecret')
                    
                    
                    
                    TOKEN_user_obj = Token.objects.filter(user=user).first()
                    if TOKEN_user_obj:
                        TOKEN_user_obj.delete()                   
                    
                    
                    token = Token.objects.get_or_create(user=user, content_id = driv_id[0][0])           
                    
                    
                    
                    
                    
                    carr_driv = CarrierDriver.objects.filter(DriverID_id = driv_id[0][0]).values_list('CarrierCompanyID')
                    company_user = CompanyUser.objects.filter(CompanyID = carr_driv[0][0]).values_list('Created_by_CarrierID')
                    
                    truck_obj = TruckType.objects.filter(id = int(driv_id[0][2])).values_list('truck_reg_num','capacity','truck_category')
                    truc_cat_obj = TruckCategory.objects.filter(id = truck_obj[0][2]).values_list('id','categ_name')
                    
                    context = {
                        "token":str(token[0]),
                        "name":user_name,
                        "phone":user_phone,
                        "email":user_email,
                        "trackingId":driv_id[0][4],
                        "deviceId":driv_id[0][5],
                        "deviceSecret":driv_id[0][6],
                        "image":driv_id[0][1],
                        "license_num":driv_id[0][3],
                        "categ_name":truc_cat_obj[0][1],
                        "cate_id":truc_cat_obj[0][0],
                        "truck_reg_num":truck_obj[0][0],
                        "carr_id":company_user[0][0],
                        "carr_com_id":carr_driv[0][0],
                        "message":"LoggedIn Successfully"
                    }
                    status_obj = Status.objects.filter(status_name = "driver added")
                    drv_obj = CarrierDriver.objects.filter(DriverID_id = driv_id[0][0]).update(status = status_obj[0])
                    return Response(context,status=status.HTTP_200_OK)
                else:
                    transaction.savepoint_rollback(sid)
                    context = {
                        "data":"",
                        "message":"passsword invalid"
                    }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)
            except Exception as e:
                transaction.savepoint_rollback(sid)
                print('exception',e)
                context = {
                        "data":"",
                        "message":"internal error"
                    }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)    
        except User.DoesNotExist:
            transaction.savepoint_rollback(sid)
            context = {
                    "data":"",
                    "message":"Invalid phone number"
                }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        context = {
            "data":"",
            "message":"request error"
        }
        return Response(context,status=status.HTTP_400_BAD_REQUEST)




class DriverProfileUpdateEmailCheck(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            user = request.user
            content = Token.objects.filter(key = key).values_list("content_id")
            carr_id = content[0][0]
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        check_email = (request.data['email']).lower()
        user_obj = User.objects.filter(email = check_email).exclude(id = user.id)
        if user_obj:
            context = {
                "data":"",
                "message":"email already exist"
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                "data":"",
                "message":"email does not exist"
            }
            return Response(context, status=status.HTTP_200_OK)

class DriverProfileUpdate(APIView): 
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        phone         = request.data['new_phone']
        email         = (request.data['new_email']).lower()
        license_num   = request.data['license_num']
        categ_id      = request.data['categ_id'] 
        truck_reg_num = request.data['truck_reg_num']     
        profile_image = request.data['profile_image']
        img_name      = request.data['img_name']
        try:
            key     = request.auth
            user    = request.user
            content = Token.objects.filter(key = key).values_list("content_id")
            carr_id = content[0][0]
        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


        try:
            user_update_obj = User.objects.filter(id = user.id).update(phone = phone, email = email)
            user_pass_obj = User.objects.get(id = user.id)
            
            
            user_pass_obj.save()

            driv_extr_obj = DriverExtraInfo.objects.filter(CustomuserID = user).first()

            truck_id = driv_extr_obj.TruckType.id

            tuk_obj = TruckType.objects.filter(id = int(truck_id) ).update(truck_reg_num = truck_reg_num,truck_category_id = int(categ_id))


            
            
            img_url = ''
            
            if profile_image:
                print('in image',profile_image)

                file_input = base64.b64decode(profile_image)

                
                

                session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                            aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

                s3 = session.resource('s3')
                cloudFilename = "media/driver/"+(str(randomStringDigits(8))+img_name).replace(' ','_')
                s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=file_input, ACL='public-read')
                driv_extr = DriverExtraInfo.objects.filter(CustomuserID = user).update(mobile_num = phone, email = email, 
                license_num = license_num, image = str(str(BUCKET_URL)+str(cloudFilename))) 
                img_url = str(str(BUCKET_URL)+str(cloudFilename))
            else:
                driv_extr = DriverExtraInfo.objects.filter(CustomuserID = user).update(mobile_num = phone, email = email, license_num = license_num)

            main_user_obj = User.objects.get(id = user.id)
            main_driv_obj = DriverExtraInfo.objects.filter(CustomuserID = main_user_obj).first()
            tuk = TruckType.objects.filter(id = int(truck_id) ).first()
            context = {
                    'data':{
                        "name":main_user_obj.user_name,
                        "phone":main_user_obj.phone,
                        "email":main_user_obj.email,
                        "image":main_driv_obj.image,
                        "license_num":main_driv_obj.license_num,
                        "cate_id":tuk.truck_category.id,
                        "truck_reg_num":tuk.truck_reg_num
                             },
                        'message':"success"
                    }
            return Response(context, status=status.HTTP_200_OK)


        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('something went wrong',e)
            context = {
                    "data":'',
                    "message":'Error'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class DriverResetPass(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            key     = request.auth
            user    = request.user
            content = Token.objects.filter(key = key).values_list("content_id")
            carr_id = content[0][0]
        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            old_pass = request.data['old_pass']
            new_pass = request.data['new_pass']


            user_pass_obj = User.objects.get(id = user.id)
            check_pass = user_pass_obj.check_password(old_pass)
            
            if check_pass:
                user_pass_obj.set_password(new_pass)
                user_pass_obj.save()
                context = {
                    "status": "success",
                    "message":"password reset successfully",
                    "data":""
                }
                return Response(context,status=status.HTTP_200_OK)
            else:
                transaction.savepoint_rollback(sid)
                context = {
                    "status": "failure",
                    "message":"wrong old password submitted",
                    "data":""
                }    
                return Response(context,status=status.HTTP_200_OK)

        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('something went wrong',e)
            context = {
                    "data":'',
                    "message":'Error'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


###################################################################

def search_name(q_,carr_com_id):
    dri_obj  =  DriverExtraInfo.objects.filter(name__contains = q_).values_list('id')
    carr_driv = CarrierDriver.objects.filter(DriverID__in = dri_obj, CarrierCompanyID_id = carr_com_id, is_delete = False)
    
    set_ = carr_driv
    
    return set_

def search_lice(q_,carr_com_id):   
    dri_obj = DriverExtraInfo.objects.filter(license_num__contains = q_).values_list('id')
    carr_driv = CarrierDriver.objects.filter(DriverID__in = dri_obj, CarrierCompanyID_id = carr_com_id, is_delete = False)
    
    set_ = carr_driv
    return set_

def search_phone(q_,carr_com_id):
    dri_obj = DriverExtraInfo.objects.filter(mobile_num__contains = q_).values_list('id')
    carr_driv = CarrierDriver.objects.filter(DriverID__in = dri_obj, CarrierCompanyID_id = carr_com_id, is_delete = False)
    
    set_ = carr_driv
    return set_

def search_trac_type(q_,carr_com_id):
    truc_obj = TruckType.objects.filter(truck_type__contains = q_).values_list('id')
    dri_obj = DriverExtraInfo.objects.filter(TruckType__in = truc_obj).values_list('id')
    carr_driv = CarrierDriver.objects.filter(DriverID__in = dri_obj, CarrierCompanyID_id = carr_com_id, is_delete = False)    
    
    set_ = carr_driv
    return set_

def search_reg(q_,carr_com_id):
    truc_obj = TruckType.objects.filter(truck_reg_num__contains = q_).values_list('id')
    dri_obj = DriverExtraInfo.objects.filter(TruckType__in = truc_obj).values_list('id')
    carr_driv = CarrierDriver.objects.filter(DriverID__in = dri_obj, CarrierCompanyID_id = carr_com_id, is_delete = False)
    
    set_ = carr_driv
    return set_


class CarrDriverSearch(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
            carr_id = content[0][0]
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:    
            q_search = request.GET["search"]
        except Exception as e:
            print('key error "search"',e)
            context = {
                "data":"",
                "message":"key error 'search'"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        dr_list = []
        try:
            re_name     = search_name(q_search,carr_id)
            dr_list.append(re_name)
            
            re_lice     = search_lice(q_search,carr_id)
            dr_list.append(re_lice)
            
            re_phone    = search_phone(q_search,carr_id)
            dr_list.append(re_phone)
            
            re_trc_type = search_trac_type(q_search,carr_id)
            dr_list.append(re_trc_type)
            
            re_reg      = search_reg(q_search,carr_id)
            dr_list.append(re_reg)
            
            SE = set()

            for data in dr_list:
                
                if data:
                    for data_in in data:
                    
                        SE.add(data_in)
                else:
                    continue

            or_list = list(SE)   
                     
        except Exception as e:
            print('function invoke error',e)
            context = {
                "data":"",
                "message":"Error: make sure all details provided are correct"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)
        serialz = CarrierDriverListSerializer(or_list, many =True)
        


        carr_dr_obj = CarrierDriver.objects.filter(CarrierCompanyID = int(content[0][0]), is_delete = False).order_by('-id')
        driver_  =  carr_dr_obj.values_list('DriverID')
        
        driver_list = []
        for driver_tup in driver_:
        
            driver_list.append(driver_tup[0])

        final_li = []
        for ids in driver_list:
            fine_dic = {
                "driver_id":ids,
                "dot":0
            }
            dot_list = []
            avg_total = 0
            EARL = []
            TOTALCOUNT = []
            orderdriver_list = OrderDriver.objects.filter(DriverID_id = int(ids), is_delete = False).values_list('id','OrderID')
        
            for odids in orderdriver_list:
                delivery_time_list = Drop.objects.filter(OrderDriverID_id = int(odids[0])).values_list('id','delivery_date_time_to','actual_delivery_date_time','OrderDriverID')
                
                total_count = delivery_time_list.count()
                earlycount = 0
                for timetup in delivery_time_list:
                    if timetup[2] and timetup[1]:
                        I = timetup[1]-timetup[2]
                        if I >= timedelta(0):
                            earlycount = earlycount+1
                

                if total_count:
                    
                    perc = (earlycount/total_count)*100
                    EARL.append(earlycount)
                    TOTALCOUNT.append(total_count)
                    dot_list.append(perc)
                    avg_total = avg_total + 1

                

            e = 0
            for i in EARL:
                e = e+i
            t = 0
            for j in TOTALCOUNT:
                t = t+j
            if t:
                RES = (e/t)*100
                # print('RES',RES)
                fine_dic['dot'] = RES
                final_li.append(fine_dic)

        try:
            context = {
               "data":{
                    "driver_list":serialz.data,
                    "dot_data":final_li
                },
                "message":"success"
            }
            return Response(context,status=status.HTTP_200_OK) 
            
        except Exception as e:
            print('context')
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)

class AddDriverList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        carr_id = request.GET["carr_id"]
        carr_driv = CarrierDriver.objects.filter(CarrierCompanyID = carr_id)
        serialz   = AddMoreDriverSerializer(carr_driv, many = True)
        return Response(serialz.data)

    @transaction.atomic
    def post(self,request):
        pick_data = request.data['assign_pick']
        drop_data = request.data['assign_drop']
        order_id  = request.GET['order_id']
        driver_id = request.GET['driver_id']
        sid = transaction.savepoint()
        order_driver_obj = OrderDriver.objects.get_or_create(OrderID_id = int(order_id),DriverID_id = int(driver_id))
        
        pick_serializer = AddMorePickSerializer(data = pick_data, context = {"order_driver_id":order_driver_obj[0].id}, many = True) 
        if pick_serializer.is_valid(): 
            pick_serializer.save()
        else:
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Pick Error",
                "Exception":pick_serializer.errors
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)    

        drop_serializer = AddMoreDropSerializer(data = drop_data, context = {"order_driver_id":order_driver_obj[0].id}, many = True) 
        if drop_serializer.is_valid():
            drop_serializer.save()
        else:
            transaction.savepoint_rollback(sid)
            context = {
                "data":"",
                "message":"Drop Error",
                "Exception":drop_serializer.errors
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)
        status_obj = Status.objects.filter(status_name = "pending").values_list("id")
        ord_obj = Order.objects.filter(id = order_id).update(StatusID_id = status_obj[0][0])
        context = {
                "data":"",
                "message":"success",
                "Exception":""
        }
        return Response(context,status=status.HTTP_200_OK)


from rest_framework.pagination import PageNumberPagination

class CarrDrivDetail(APIView):
    permission_classes = (IsAuthenticated,)
    #pagination_class = StandardResultsSetPagination
    def get(self, request):
        try:
            driv_id = request.GET['dri_id']
        except Exception as e:
            print('key error "dri_id"',e)
            context = {
                "data":"",
                "message":"key error 'dri_id'"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            order_driver_obj = OrderDriver.objects.filter(DriverID_id = driv_id, is_delete = False).order_by('-id')
        except Exception as e:
            print('order_driver_obj query error',e)
            context = {
                "data":"",
                "message":"Error: make sure this dri_id is related to order"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)
        order_list_serializ = CarrierDriverOrderDetailsSer(order_driver_obj, many =True)
        try:
            dri_obj = DriverExtraInfo.objects.filter(id = driv_id, is_delete = False)
        except Exception as e:
            print("query error at dri_obj",e)
            context = {
                "data":"",
                "message":"Error: make sure this driv_id exist in database"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)       
        personal_drivdetail_serializ = CarrierDriverPersonalSerializer(dri_obj, many = True)
        final_dic = {
            "driver_detail":personal_drivdetail_serializ.data,
            "order_list":order_list_serializ.data
        }
        return Response(final_dic,status=status.HTTP_200_OK)


class CarrDrivOrderCount(APIView):
    permission_classes = (IsAuthenticated,)
    #pagination_class = StandardResultsSetPagination
    def get(self, request):
        try:
            driv_id = request.GET['dri_id']
        except Exception as e:
            print('key error "dri_id"',e)
            context = {
                "data":"",
                "message":"key error 'dri_id'"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        order_pending    = OrderDriver.objects.filter(DriverID_id = driv_id, OrderID__StatusID__status_name = 'pending' , is_delete = False).count()
        order_in_process = OrderDriver.objects.filter(DriverID_id = driv_id, OrderID__StatusID__status_name = 'in process' , is_delete = False).count()
        order_delivered  = OrderDriver.objects.filter(DriverID_id = driv_id, OrderID__StatusID__status_name = 'delivered' , is_delete = False).count()
        final_dic = {
            "pending":order_pending,
            "in_process":order_in_process,
            "delivered":order_delivered,
        }
        return Response(final_dic,status=status.HTTP_200_OK)


class CarrierDriverpersonalDetail(APIView):   
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            driv_id = request.GET['dri_id']
        except Exception as e:
            print("key error 'dri_id'")
            context = {
                "data":"",
                "message":"key error 'dri_id'"
            }
        try:
            dri_obj = DriverExtraInfo.objects.filter(id = driv_id, is_delete = False)
        except Exception as e:
            print("query error at dri_obj",e)
            context = {
                "data":"",
                "message":"Error: make sure this driv_id exist in database"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)       
        serializ = CarrierDriverPersonalSerializer(dri_obj, many = True)
        return Response(serializ.data)


class CarrDrivStatus(APIView): 
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")[0][0]     
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        carr_dr_obj = ''
        try:
            if request.GET["status"] and request.GET["status"] != 'all':
                status_name = request.GET["status"]
                try:
                    status_id   = Status.objects.filter(status_name = status_name)
                    carr_dr_obj = CarrierDriver.objects.filter(CarrierCompanyID = int(content), status = status_id, is_delete = False).order_by('-id') 

                except Exception as e:
                    print("query error",e)
                    context = {
                                    "data": '',
                                    "message":"Error: make sure 'status' priveded is correct "
                            }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)        
            else:
                try:
                    carr_dr_obj = CarrierDriver.objects.filter(CarrierCompanyID = int(content), is_delete = False).order_by('-id')
                except Exception as e:
                    print("query error",e)
                    context = {
                                    "data": '',
                                    "message":"Error"
                            }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("key error 'status'",e)
            context = {
                            "data": '',
                            "message":"key error 'status'"
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        driver_  =  carr_dr_obj.values_list('DriverID')
        
        driver_list = []
        for driver_tup in driver_:
        
            driver_list.append(driver_tup[0])

        final_li = []
        for ids in driver_list:
            fine_dic = {
                "driver_id":ids,
                "dot":0
            }
            dot_list = []
            avg_total = 0
            EARL = []
            TOTALCOUNT = []
            orderdriver_list = OrderDriver.objects.filter(DriverID_id = int(ids), is_delete = False).values_list('id','OrderID')
        
            for odids in orderdriver_list:
                delivery_time_list = Drop.objects.filter(OrderDriverID_id = int(odids[0])).values_list('id','delivery_date_time_to','actual_delivery_date_time','OrderDriverID')
                
                total_count = 0              #delivery_time_list.count()
                earlycount = 0
                for timetup in delivery_time_list:
                    if timetup[2] and timetup[1]:
                        total_count = total_count + 1
                        I = timetup[1]-timetup[2]
                        if I >= timedelta(0):
                            earlycount = earlycount+1
                

                if total_count:
                    
                    perc = (earlycount/total_count)*100
                    EARL.append(earlycount)
                    TOTALCOUNT.append(total_count)
                    dot_list.append(perc)
                    avg_total = avg_total + 1

                

            e = 0
            for i in EARL:
                e = e+i
            t = 0
            for j in TOTALCOUNT:
                t = t+j
            if t:
                RES = (e/t)*100
                # print('RES',RES)
                fine_dic['dot'] = RES
                final_li.append(fine_dic)

            # if avg_total:
            #     m = 0
            #     for u in dot_list:
            #         m = m+u
        
        
            #     result = (m/(avg_total*100))*100
            #     fine_dic['dot'] = result
            #     final_li.append(fine_dic)
                
                

                
        try:
            serialz = CarrierDriverListSerializer(carr_dr_obj, many =True)
            context = {
                "data":{
                    "driver_list":serialz.data,
                    "dot_data":final_li
                },
                "message":"success"
            }        
            return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            print('serializer initialization error',serialz.errors)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)

class SearchCarrDrivStatus(APIView): 
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")[0][0]     

        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        carr_dr_obj = ''
        q_search = request.GET['q_search']
        try:
            if request.GET["status"] and request.GET["status"] != 'all':
                status_name = request.GET["status"]
                try:
                    status_id   = Status.objects.filter(status_name = status_name)
                    carr_dr_obj = CarrierDriver.objects.filter(Q(DriverID__name__icontains = q_search, CarrierCompanyID = int(content), status = status_id, is_delete = False) | Q(DriverID__mobile_num__icontains = q_search, CarrierCompanyID = int(content), status = status_id, is_delete = False)).order_by('-id') 

                except Exception as e:
                    print("query error",e)
                    context = {
                                    "data": '',
                                    "message":"Error: make sure 'status' priveded is correct "
                            }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)        
            else:
                try:
                    carr_dr_obj = CarrierDriver.objects.filter(Q(DriverID__name__icontains = q_search , CarrierCompanyID = int(content), is_delete = False) | Q(DriverID__mobile_num__icontains = q_search, CarrierCompanyID = int(content), is_delete = False)).order_by('-id')
                except Exception as e:
                    print("query error",e)
                    context = {
                                    "data": '',
                                    "message":"Error"
                            }
                    return Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("key error 'status'",e)
            context = {
                            "data": '',
                            "message":"key error 'status'"
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        driver_  =  carr_dr_obj.values_list('DriverID')
        
        driver_list = []
        for driver_tup in driver_:
        
            driver_list.append(driver_tup[0])

        final_li = []
        for ids in driver_list:
            fine_dic = {
                "driver_id":ids,
                "dot":0
            }
            dot_list = []
            avg_total = 0
            EARL = []
            TOTALCOUNT = []
            orderdriver_list = OrderDriver.objects.filter(DriverID_id = int(ids), is_delete = False).values_list('id','OrderID')
        
            for odids in orderdriver_list:
                delivery_time_list = Drop.objects.filter(OrderDriverID_id = int(odids[0])).values_list('id','delivery_date_time_to','actual_delivery_date_time','OrderDriverID')
                
                total_count = 0              #delivery_time_list.count()
                earlycount = 0
                for timetup in delivery_time_list:
                    if timetup[2] and timetup[1]:
                        total_count = total_count + 1
                        I = timetup[1]-timetup[2]
                        if I >= timedelta(0):
                            earlycount = earlycount+1
                

                if total_count:
                    
                    perc = (earlycount/total_count)*100
                    EARL.append(earlycount)
                    TOTALCOUNT.append(total_count)
                    dot_list.append(perc)
                    avg_total = avg_total + 1

                

            e = 0
            for i in EARL:
                e = e+i
            t = 0
            for j in TOTALCOUNT:
                t = t+j
            if t:
                RES = (e/t)*100
                # print('RES',RES)
                fine_dic['dot'] = RES
                final_li.append(fine_dic)

            # if avg_total:
            #     m = 0
            #     for u in dot_list:
            #         m = m+u
        
        
            #     result = (m/(avg_total*100))*100
            #     fine_dic['dot'] = result
            #     final_li.append(fine_dic)
                
                

                
        try:
            serialz = CarrierDriverListSerializer(carr_dr_obj, many =True)
            context = {
                "data":{
                    "driver_list":serialz.data,
                    "dot_data":final_li
                },
                "message":"success"
            }        
            return Response(context,status=status.HTTP_200_OK)
        except Exception as e:
            print('serializer initialization error',serialz.errors)
            context = {
                "data":"",
                "message":"Error"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)

class CarrDrivListDownload(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")[0][0]
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        carr_dr_obj = ''
        try:
            if request.data['driverlist'] != []:
                driver_id = request.data['driverlist']
                carr_dr_obj = CarrierDriver.objects.filter(DriverID__in = driver_id, CarrierCompanyID = int(content), is_delete = False)     
            else:
                carr_dr_obj = CarrierDriver.objects.filter(CarrierCompanyID_id = int(content), is_delete = False) 
        except Exception as e:
            print("key error 'driverlist'",e)
            context = {
                            "data": '',
                            "message":"key error 'driverlist'"
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        serializ = CarrDriverListDownload(carr_dr_obj, many = True)
        data = serializ.data
        my_fields = ['driver_id', 'name','address', 'mobile_number', 'license_number', 'status']
        buffer = io.StringIO()     
        writer = csv.DictWriter(buffer, fieldnames=my_fields)
        writer.writeheader()
        for data in data:
            writer.writerow(data)
        buffer.seek(0)
        response = StreamingHttpResponse(buffer, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=driverlist.csv'
        return response

class DriverDashboard(ViewPaginatorMixin, APIView): 
    permission_classes = (IsAuthenticated,)
    
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        status_data = request.GET['status']
        status_obj = Status.objects.filter(status_name = status_data).first()
        order_driver_obj = OrderDriver.objects.filter(DriverID_id = content[0][0], order_status = status_obj,is_delete = False, is_active =True).order_by('-id')
        serializ = DriverDashboardSerializer(order_driver_obj, many = True)
        final_dic = {
            "data":serializ.data,
            "message":"success"
        }
        return Response(final_dic, status=status.HTTP_200_OK)
        # return JsonResponse({"resources": self.paginate(serializ.data, 1, 10)})


class DriverJobDetail(APIView): 
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        # driver_id = request.GET['driver_id']
        order_id  = request.GET['order_id']
        order_driver_obj = OrderDriver.objects.filter(OrderID_id = order_id ,DriverID_id = content[0][0])
        serializ = DriverJobDetailSerializer(order_driver_obj, many = True) 
        order_attch_obj = OrderAttachment.objects.filter(OrderID_id = int(order_id))
        order_attach = DriverJobDetailAttachSerializer(order_attch_obj, many = True)
        
        
        
        oder_com_obj =  OrderComment.objects.filter(OrderID_id = int(order_id))

        
        order_comt_ser =  OrderCommentSerializer(oder_com_obj, many = True)

        
        final_dic = {
            "main":serializ.data,
            "order_comment":order_comt_ser.data, 
            "attachment":order_attach.data
        }
        return Response(final_dic, status=status.HTTP_200_OK)


class DriverVehicleList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        driver_id = request.GET['driver_id']
        key = request.auth
        content           = Token.objects.filter(key = key).values_list("content_id")[0][0]
        driver_obj        = DriverExtraInfo.objects.filter(id = driver_id).values_list('TruckType')
        
        truck_type_obj   = TruckType.objects.filter(id = driver_obj)
        truck_serializer = VehiclelistTruckTypeSerializer(truck_type_obj, many =True)
        
        final_dic = {
            "truck_detail":truck_serializer.data,
            "message":"success"
        }
        return Response(final_dic,status=status.HTTP_200_OK)


class CarrDriverList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, format=None):
        key = request.auth
        content = Token.objects.filter(key = key).values_list("content_id")[0][0]    
        carr_dri_obj = CarrierDriver.objects.filter(CarrierCompanyID = int(content))
        seri = CarrierDriverListSerializer(carr_dri_obj, many =True)
        return Response(seri.data)


class DriverUniqueNumber(APIView):
    
    def post(self, request):
        num = request.data['number']
        user_obj = User.objects.filter(phone = num)

        if user_obj:
            context = {
                "data":"",
                "message":"exist"
            }
            return Response(context,status=status.HTTP_200_OK)
        else:
            context = {
                "data":"",
                "message":"does not exist"
            }
            return Response(context,status=status.HTTP_200_OK)


class En_RouteOrder(APIView): 
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        status_obj = Status.objects.filter(status_name = 'in process').values_list('id')   
        
        order_obj = Order.objects.filter(id = order_id).update(StatusID_id = status_obj[0][0])

        context = {
            "data":"",
            "message":"success"
        }
        return Response(context,status=status.HTTP_200_OK)

class CarrierUpdateDriverProfile(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        
        try:
            data = request.data['payload']
            data = json.loads(data)

            Driver_obj = DriverExtraInfo.objects.filter(id = data['driver_id']).first()
            DriverExtraInfo.objects.filter(id = data['driver_id']).update(name = data['name'], mobile_num = data['phone'], license_num = data['license_num'], email = (data['email']).lower(), address =  data['address'])      
            
            
            TruckType.objects.filter(id = Driver_obj.TruckType.id).update(truck_reg_num = data['truck_reg_num'], truck_category_id = data['truck_category'])

            User.objects.filter(id = Driver_obj.CustomuserID.id).update(user_name = data['name'], phone = data['phone'], email = (data['email']).lower())

            if request.data['file']:
                f = request.data['file']
                
                fileToUpload = f
                cloudFilename ="media/driver/"+fileToUpload.name
                obj = DriverExtraInfo.objects.filter(id = data['driver_id'])
                
                obj.update(image = BUCKET_URL+cloudFilename)
                
                session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                            aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
                s3 = session.resource('s3')
                s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=fileToUpload, ACL='public-read')

                context = {
                    "data":"",
                    "image_url":BUCKET_URL+str(cloudFilename),
                    "message":"Update Successfully"
                }
                return Response(context,status=status.HTTP_200_OK)

            else:
                context = {
                    "data":"",
                    "image_url":'',
                    "message":"Update Successfully"
                }
                return Response(context,status=status.HTTP_200_OK)

        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('something went wrong',e)
            context = {
                    "data":'',
                    "message":'Error'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class DriverCommentOrder(APIView):
    permission_classes = (IsAuthenticated,)
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            try:
                key = request.auth
                content = Token.objects.filter(key = key).values_list("content_id","param")
            except Exception as e:
                print('key or token object error',e)
                context = {
                                "data": '',
                                "message":'token Error'
                        }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
            driver_obj = DriverExtraInfo.objects.get(id = int(content[0][0]))
            comment_by_driver = request.data['comment']
            order_id          = int(request.data['order_id'])
            order_obj = Order.objects.get(id = order_id)
            carr_com_id = order_obj.CarrierCompanyID.id
            com_user = CompanyUser.objects.filter(CompanyID_id = carr_com_id)
            created_by_carr = (com_user[0]).Created_by_CarrierID.id
            order_come_obj    = OrderComment.objects.create(OrderID_id = order_id, Created_by_DriverID_id = int(content[0][0]), comment = comment_by_driver, date_time_of_create = request.data['date_time_of_create'])
            driver_name = driver_obj.name
            job_id = order_obj.job_id
            
            date = str(request.data['date_time_of_create'])
            
            datetime_object = datetime.strptime(date, '%Y-%m-%dT%H:%M:%SZ')

            old_format = '%Y-%m-%d %H:%M:%S'
            new_format = '%d/%m/%Y %I:%M %p'

            new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)

            
            
            comm = "{0} has made a comment for job number {1} on {2}".format(driver_name,job_id,new_datetime_str)
            order_hist  = OrderHistory.objects.create(OrderID_id = order_id, Created_by_CarrierID_id = created_by_carr, comments = str(comm))
            noti_obj    = Notification.objects.create(OrderID_id = order_id, created_by_driver_id = int(content[0][0]), created_for_carrier_id = created_by_carr ,comments = comm, date_time_of_create = request.data['date_time_of_create'])
            context = {
                "data":"",
                "message":"success"
            }
            return Response(context,status=status.HTTP_200_OK)

        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('Exception',e)
            context = {
                            "data": '',
                            "message":'Exception'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class DriverForgotPass(APIView):
    @transaction.atomic
    def post(self, request):
        sid = transaction.savepoint()
        try:
            email = (request.data['email']).lower()
            usr_obj = User.objects.filter(email = email).first()
            driv_obj = DriverExtraInfo.objects.filter(email = email).first()

            if usr_obj and driv_obj:
                passw = randomStringDigits(8)
                

                usr_obj.set_password(passw)
                usr_obj.save()


                subject, from_email, to = 'Request for reset Forgot Password', settings.EMAIL_HOST_USER, email
                text_content = 'Welcome to KargoLogic'
                
                html_content = '<p> Hi {0},<br>It seems like you have requested to reset your password to access KargoLogic portal.</p><p>Your Mobile Number: <strong>{1}</strong></p><p>New Password: <strong>{2}</strong></p>'.format(driv_obj.name,usr_obj.phone,passw)
                # html_content = "<p>Wellcome to KargoLogic</p><br/><p><strong><a href='%s'>Click here Login</a></strong></p>"%(url)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                context = {
                    "data":"To see updated password, kindly check your email",
                    "message":"success"
                }
                return Response(context,status=status.HTTP_200_OK)

            else:
                transaction.savepoint_rollback(sid)
                context = {
                    "data":"user with this email does not exist",
                    "message":"fail"
                }
                return Response(context,status=status.HTTP_200_OK)
        
        except Exception as e:
            transaction.savepoint_rollback(sid)
            print('something went wrong',e)
            context = {
                    "data":'',
                    "message":'Error'
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

class DriverTracker(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            try:
                key = request.auth
                content = Token.objects.filter(key = key).values_list("content_id","param")
            except Exception as e:
                print('key or token object error',e)
                context = {
                                "data": '',
                                "message":'token Error'
                        }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
            driver_list = request.data['driver_list']
            track_id_tup = ''
            if driver_list != []:
                track_id_tup = DriverExtraInfo.objects.filter(id__in = driver_list).values_list('trackingId')
            else:
                carr_dr_obj = CarrierDriver.objects.filter(CarrierCompanyID_id = int(content[0][0]), is_delete = False).values_list('DriverID')
                
                driver_id_list = []
                for i in carr_dr_obj:
                    for m in i:
                
                        if m:
                
                            driver_id_list.append(m)
                
                track_id_tup = DriverExtraInfo.objects.filter(id__in = driver_id_list).values_list('trackingId')
                
            track_list = []
            for j in track_id_tup:
                
                for n in j:
                    if n:
                
                        track_list.append(n)

            payload = {
            "email": HERE_MAP_USER,
            "password": HERE_MAP_KEY
                }
            headers = {
            "Content-Type" : "application/json"
                }
            here_url = "https://tracking.api.here.com/users/v2/login"
            
            r = requests.post(here_url, data=json.dumps(payload), headers=headers)    
            resp = r.json()

            acc_token = resp['accessToken']

            bat_headers = {
                "Content-Type" : "application/json",
                "authorization" : "Bearer " + acc_token
                }

            bat_url = "https://tracking.api.here.com/shadows/v2/batch"

            
            get_mark = requests.post(bat_url, data=json.dumps(track_list), headers=bat_headers)
            
            res_dt = get_mark.json()

            context = {
                "data":res_dt,
                "message":"success"
            }
            return Response(context,status = status.HTTP_200_OK)
        except Exception as e:
            print('Exception',e)
            context = {
                "data":'',
                "message":"something went wrong"
            }
            return Response(context,status = status.HTTP_400_BAD_REQUEST)


class DriverCordinate(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        data = request.data
        
        driv_old = DriverExtraInfo.objects.filter(id = int(content[0][0])).first()
        
        
        driv_obj = DriverExtraInfo.objects.filter(id = int(content[0][0])).update(lat = data['lat'], lng = data['lng'])
        
        driv_new = DriverExtraInfo.objects.filter(id = int(content[0][0])).first()
        
        
        if driv_obj:
            context = {
                "data":"",
                "message":"success"
            }
            return Response(context,status=status.HTTP_200_OK)
        else:
            context = {
                            "data": '',
                            "message":'Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)


class GetDriverCordi(APIView):
    
    def post(self, request):
        
        data = request.data

        driv_obj = DriverExtraInfo.objects.filter(id__in = data["driver_ids"])
        serialz = DriverCordiSerializer(driv_obj, many = True)
        context = {
            "data":serialz.data,
            "message":"success"
        }
        return Response(context,status=status.HTTP_200_OK)

        
class DriverNotification(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                        "data": '',
                        "message":'Token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        driv_notigfy_obj = Notification.objects.filter(created_for_driver_id = int(content[0][0]), is_read = False ).order_by('-id')
        all_notifications = Notification.objects.filter(created_for_driver_id = int(content[0][0])).order_by('-id')
        if len(driv_notigfy_obj) != 0:
            serialz = DriverNotificationSerializer(all_notifications, many = True)
            context = {
                "data":serialz.data,
                "message":"success",
                "new_message":True
            }
            driv_notigfy_obj.update(is_read = True)
            
            return Response(context, status=status.HTTP_200_OK)
        else:
            serialz = DriverNotificationSerializer(all_notifications, many = True)
            context = {
                "data":serialz.data,
                "message":"success",
                "new_message":False
            }
            return Response(context, status=status.HTTP_200_OK)
        