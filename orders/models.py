from django.db import models
from custom_users.models import User
from company.models import Company
from status.models import Status
from domain.models import Domain
from user_role.models import UserRole
from customers.models import CustomerExtraInfo
from carriers.models import CarrierExtraInfo

from django.core.validators import RegexValidator

# Create your models here.
class Order(models.Model):
    alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')

    domainID              = models.ForeignKey(Domain, on_delete = models.CASCADE, null=True,blank=True)
    roleID                = models.ForeignKey(UserRole, on_delete = models.CASCADE, null=True,blank=True)
    Created_by_CustomerID = models.ForeignKey(CustomerExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    Created_by_CarrierID  = models.ForeignKey(CarrierExtraInfo,on_delete = models.CASCADE,null=True,blank=True)
    CustomerCompanyID     = models.ForeignKey(Company,related_name='customer_order',on_delete = models.CASCADE,null=True,blank=True)
    CarrierCompanyID      = models.ForeignKey(Company, on_delete = models.CASCADE,null=True,blank=True)
    StatusID              = models.ForeignKey(Status, on_delete = models.CASCADE,null=True,blank=True)
    customer_code         = models.CharField(max_length = 500,null=True,blank=True)
    price                 = models.CharField(max_length = 500,null=True,blank=True)
    load_carried          = models.CharField(max_length = 500,null=True,blank=True) 
    load_type             = models.CharField(max_length = 500,null=True,blank=True)
    load_weight           = models.CharField(max_length = 500,null=True,blank=True)
    length_load_dimen     = models.CharField(max_length = 500,null=True,blank=True)
    width_load_dimen      = models.CharField(max_length = 500,null=True,blank=True)
    height_load_dimen     = models.CharField(max_length = 500,null=True,blank=True)
    measure_unit          = models.CharField(max_length = 500,null=True,blank=True)
    units                 = models.CharField(max_length = 500,null=True,blank=True)
    hazardous             = models.BooleanField(default=True)
    additional_note       = models.CharField(max_length = 500,null=True,blank=True)
    quantity              = models.CharField(max_length = 500,null=True,blank=True)
    job_id                = models.CharField(max_length=50, blank=False, null=True, validators=[alphanumeric])
    pickup_location       = models.CharField(max_length = 500,null=True,blank=True)
    delivery_location     = models.CharField(max_length = 500,null=True,blank=True)

    expected_pickup_DT    = models.DateTimeField(null=True,blank=True)
    expected_delivery_DT  = models.DateTimeField(null=True,blank=True)
    actual_pickup_TD      = models.DateTimeField(null=True,blank=True)
    actual_delivery_TD    = models.DateTimeField(null=True,blank=True)
    order_create_time     = models.CharField(max_length = 500,null=True,blank=True)
    created_DT            = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_DT            = models.DateTimeField(auto_now=True,null=True,blank=True)
    is_active             = models.BooleanField(default=True)
    is_delete             = models.BooleanField(default=False)
    def __str__(self):
        return '%s (%s)'%(self.job_id, self.id)