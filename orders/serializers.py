from rest_framework import serializers
from orders.models import Order

from domain.serializers import DomainSerializer
from user_role.serializers import UserRoleSerializer
from custom_users.serializers import UserSerializer
from company.serializer import CompanySerializer, DetailCarrDrivSerializer,CompanyCarrSerializer
from company.serializer import CompanyCustSerializer, CarrDrivOrderDetailsCompany
from status.serializers import StatusSerializer, CarrDrivOrderDetailsStatus
from status.serializers import StatusOrderCarrSerializer
from customers.serializers import CustomerExtraInfoSerializer, CarrOrderDetailCustomerExtraInfoSerializer
from carriers.serializers import CarrierExtraInfoSerializer, CarrOrderDetailCarrierExtraInfoSerializer

from CompanySenderReceiver.models import CompanySenderReceiver
from company.models import Company
from domain.models import Domain
from user_role.models import UserRole
from orders.models import Order
from status.models import Status

class OrderSerializer(serializers.ModelSerializer):
    # domainID = DomainSerializer(required=False)
    # roleID= UserRoleSerializer(required=False)
    # Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    # Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    # CustomerCompanyID = CompanySerializer(required=False)
    # CarrierCompanyID   = CompanySerializer(required=False)
    # StatusID  = StatusSerializer(required=False)

    class Meta:
        model  = Order
        exclude = ('created_DT','updated_DT','is_active')


class OrderStatusCarrSerializer(serializers.ModelSerializer):
    # Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    # Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)

    # domainID = DomainSerializer(required=False)
    # roleID= UserRoleSerializer(required=False)
    pickup_date_time = serializers.SlugRelatedField(
                                            many=True,
                                            read_only=True,
                                            source='order_pick_related',
                                            slug_field='pickup_date_time')

    pickup_date_time_to = serializers.SlugRelatedField(
                                            many=True,
                                            read_only=True,
                                            source='order_pick_related',
                                            slug_field='pickup_date_time_to')                                        

    delivery_date_time = serializers.SlugRelatedField(
                                            many=True,
                                            read_only=True,
                                            source='order_drop_related',
                                            slug_field='delivery_date_time')
    
    

    delivery_date_time_to = serializers.SlugRelatedField(
                                            many=True,
                                            read_only=True,
                                            source='order_drop_related',
                                            slug_field='delivery_date_time_to')                                            
    CustomerCompanyID = CompanyCustSerializer(required=False)
    CarrierCompanyID   = CompanyCarrSerializer(required=False)
    StatusID  = StatusOrderCarrSerializer(required=False)

    Created_by_CustomerID = CarrOrderDetailCustomerExtraInfoSerializer(required=False)
    Created_by_CarrierID = CarrOrderDetailCarrierExtraInfoSerializer(required=False)
    class Meta:
        model  = Order
        fields = ('id',"job_id",'customer_code','price','load_carried','load_type','load_weight','measure_unit','units','quantity','CustomerCompanyID','CarrierCompanyID',"pickup_date_time","pickup_date_time_to","delivery_date_time","delivery_date_time_to","order_create_time","created_DT",'StatusID',"Created_by_CustomerID","Created_by_CarrierID","is_active")


class OrderDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model  = Order
        exclude = ('created_DT','updated_DT','is_active','domainID','roleID')



class CarrDrivDetailSerializer(serializers.ModelSerializer):
    CustomerCompanyID = DetailCarrDrivSerializer(required=False)
    StatusID  = StatusSerializer(required=False)
    class Meta:
        model = Order
        fields = ('id','CustomerCompanyID','StatusID','quantity','job_id','expected_pickup_DT','expected_delivery_DT','created_DT',"Created_by_CustomerID","Created_by_CarrierID","is_active")


class ArrOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model  = Order
        exclude = ('created_DT','updated_DT')
        


class CarrDrivOrderDetailsOrderSer(serializers.ModelSerializer): 
    CustomerCompanyID = CarrDrivOrderDetailsCompany(required=False)
    StatusID  = CarrDrivOrderDetailsStatus(required=False)

    class Meta:
        model  = Order
        fields = ('id','job_id',"order_create_time",'created_DT','price','CustomerCompanyID','StatusID',"Created_by_CustomerID","Created_by_CarrierID", 'is_active')

class CarrierOrderDetailsSer(serializers.ModelSerializer):
    # pickup_date_time = serializers.SlugRelatedField(
    #                                         many=True,
    #                                         read_only=True,
    #                                         source='order_pick_related',
    #                                         slug_field='pickup_date_time')

    # delivery_date_time = serializers.SlugRelatedField(
    #                                         many=True,
    #                                         read_only=True,
    #                                         source='order_drop_related',
    #                                         slug_field='delivery_date_time')
    # attachment_name = serializers.SlugRelatedField(
    #                                         many=True,
    #                                         read_only=True,
    #                                         source='order_attach_related',
    #                                         slug_field='attachment_name')
    
    # physical_path = serializers.SlugRelatedField(
    #                                         many=True,
    #                                         read_only=True,
    #                                         source='order_attach_related',
    #                                         slug_field='physical_path')
    # field_name = serializers.SlugRelatedField(
    #                                         many=True,
    #                                         read_only=True,
    #                                         source='extra_fields',
    #                                         slug_field='field_name')
    # field_value = serializers.SlugRelatedField(
    #                                         many=True,
    #                                         read_only=True,
    #                                         source='extra_fields',
    #                                         slug_field='field_value')

    # actual_pickup_date_time = serializers.SlugRelatedField(
    #                                         many=True,
    #                                         read_only=True,
    #                                         source='order_pick_related',
    #                                         slug_field='actual_pickup_date_time')
    # actual_pickup_date_time = serializers.SlugRelatedField(
    #                                         many=True,
    #                                         read_only=True,
    #                                         source='order_drop_related',
    #                                         slug_field='actual_delivery_date_time')
    
    
    # # comments = serializers.SlugRelatedField(many=True,
    # #                                         read_only=True,
    # #                                         source='comment',
    # #                                         slug_field='comment')   



    comments = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    # #comment = serializers.StringRelatedField(many = True)
    CustomerCompanyID = CompanyCustSerializer(required=False)
    # CarrierCompanyID   = CompanySerializer(required=False)
    Created_by_CustomerID = CarrOrderDetailCustomerExtraInfoSerializer(required=False)
    Created_by_CarrierID = CarrOrderDetailCarrierExtraInfoSerializer(required=False)
    StatusID  = StatusOrderCarrSerializer(required=False)
    # def get_my_fields(self, data):
    #     return {
    #         "id" : data.id
    #}
    # def to_representation(self, value):
    #     data = {
    #         "job_id":value.job_id,
    #         "Created_by_CustomerID":value.Created_by_CustomerID,
    #         "Created_by_CarrierID":value.Created_by_CarrierID.name
    #     }
    #     return '%s'%(data)

    class Meta:
        model = Order
    
        exclude = ('domainID','roleID','actual_pickup_TD','actual_delivery_TD','expected_delivery_DT','expected_pickup_DT','pickup_location','delivery_location')
    
class CarrGetOrderDetail(serializers.ModelSerializer):
    # domainID = DomainSerializer(required=False)
    # roleID= UserRoleSerializer(required=False)
    Created_by_CustomerID = CustomerExtraInfoSerializer(required=False)
    Created_by_CarrierID = CarrierExtraInfoSerializer(required=False)
    CustomerCompanyID = CompanySerializer(required=False)
    CarrierCompanyID  = CompanySerializer(required=False)
    StatusID  = StatusSerializer(required=False)

    class Meta:
        model  = Order
        exclude = ('domainID','roleID','actual_pickup_TD','actual_delivery_TD','expected_delivery_DT','expected_pickup_DT','pickup_location','delivery_location')
        

class CarrOrderCsvDownloadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('id','Created_by_CustomerID','Created_by_CarrierID','CustomerCompanyID','CarrierCompanyID',
        'StatusID','customer_code','price','load_carried','load_type','load_weight','length_load_dimen',
        'width_load_dimen','height_load_dimen','measure_unit','units','hazardous','additional_note',
        'quantity','job_id','created_DT','is_active','comments','order_attach_related','extra_fields',
        'order_pick_related','order_drop_related','order_history')
        depth = 1

    def to_representation(self, instance):
        data = super(CarrOrderCsvDownloadSerializer, self).to_representation(instance)
        
        sort_dic = {}
        sort_dic['comments'] = []
        sort_dic['order_attach'] = []
        sort_dic['extra_fields'] = []
        sort_dic['order_pick'] = []
        sort_dic['order_drop'] = []
        sort_dic['order_history'] = []
        
        for key,val in data.items():
            if key == 'id':
                sort_dic['id'] = val
            if key == 'customer_code':
                sort_dic['customer_code'] = val
            if key == 'price':
                sort_dic['price'] = val
            if key == 'load_carried':
                sort_dic['load_carried'] = val
            if key == 'load_weight':
                sort_dic['load_weight'] = val
            if key == 'load_type':
                sort_dic['load_type'] = val
            if key == 'length_load_dimen':
                sort_dic['length_load_dimen'] = val
            if key == 'width_load_dimen':
                sort_dic['width_load_dimen'] = val
            if key == 'height_load_dimen':
                sort_dic['height_load_dimen'] = val
            if key == 'measure_unit':
                sort_dic['measure_unit'] = val
            if key == 'units':
                sort_dic['units'] = val
            if key == 'hazardous':
                sort_dic['hazardous'] = val
            if key == 'additional_note':
                sort_dic['additional_note'] = val
            if key == 'job_id':
                sort_dic['job_id'] = val
            if key == 'created_date_time':
                sort_dic['created_date_time'] = val                    
            if key == 'Created_by_CustomerID' and val:
                for key1,val1 in val.items():
                    if key1 == 'name':
                        sort_dic['Created_by_Customer'] = val1

            if key == 'Created_by_CarrierID' and val:
                for key1,val1 in val.items():
                    if key1 == 'name':
                        sort_dic['Created_by_Carrier'] = val1

            if key == 'CustomerCompanyID':
                for key1,val1 in val.items():
                    if key1 == 'name':
                        sort_dic['CustomerCompany'] = val1
            
            if key == 'CarrierCompanyID':
                for key1,val1 in val.items():
                    if key1 == 'name':
                        sort_dic['CarrierCompanyID'] = val1
            
            if key == 'StatusID':
                for key1,val1 in val.items():
                    if key1 == 'status_name':
                        sort_dic['Status'] = val1

            if key == 'comments' and key !=[]:
                li = []
                for val1 in val:
                    sub_dic = {}
                    for key2,val2 in val1.items():
                        if key2 == 'comment':
                            sub_dic['comment'] = val2
                    if sub_dic:
                        li.append(sub_dic.copy())                   
                sort_dic['comments'].extend(li.copy())

            if key == 'order_attach_related' and key !=[]:
                li = []
                for val1 in val:
                    sub_dic = {}
                    for key2,val2 in val1.items():
                        if key2 == 'attachment_name':
                            sub_dic['attachment_name'] = val2
                    if sub_dic:
                        li.append(sub_dic.copy())    
                sort_dic['order_attach'].extend(li.copy())

            if key == 'extra_fields' and key !=[]:
                li = []
                for val1 in val:
                    sub_dic = {}
                    for key2,val2 in val1.items():    
                        if key2 == 'field_name':
                            sub_dic['field_name'] = val2
                        if key2 == 'field_value':
                            sub_dic['field_value'] = val2
                    if sub_dic:
                        li.append(sub_dic.copy())
                sort_dic['extra_fields'].extend(li.copy())
        
            if key == 'order_pick_related' and key !=[]:
                li = []
                for val1 in val:
                    sub_dic = {}
                    for key2,val2 in val1.items():
                        if key2 == 'pickup_date_time':
                            sub_dic['pickup_date_time'] = val2
                        
                        if key2 == 'actual_pickup_date_time':
                            sub_dic['actual_pickup_date_time'] = val2
                        
                        if key2 == 'address':
                            sub_dic['address'] = val2
                        
                        if key2 == 'sender':
                            
                            ass = CompanySenderReceiver.objects.filter(id = val2).first()
                            s_name = ''
                            if ass:
                                s_name = ass.name
                            sub_dic['sender'] = s_name
                    if sub_dic:
                        li.append(sub_dic.copy())

                sort_dic['order_pick'].extend(li.copy())

            if key == 'order_drop_related' and key !=[]:
                li = []
                for val1 in val:
                    sub_dic = {}
                    for key2,val2 in val1.items():
                        if key2 == 'delivery_date_time':
                            sub_dic['delivery_date_time'] = val2
                        
                        if key2 == 'actual_delivery_date_time':
                            sub_dic['actual_delivery_date_time'] = val2
                        
                        if key2 == 'address':
                            sub_dic['address'] = val2
                        
                        if key2 == 'receiver':
                            print('receiver val2',val2)
                            ass = CompanySenderReceiver.objects.filter(id = val2).first()
                            print('ass',ass)
                            r_name = ''
                            if ass:
                                r_name = ass.name
                            sub_dic['receiver'] = r_name

                    if sub_dic:
                        li.append(sub_dic.copy())      
                sort_dic['order_drop'].extend(li.copy())
            
            if key == 'order_history' and key !=[]:
                li = []
                for val1 in val:
                    sub_dic = {}
                    for key2,val2 in val1.items():
                        sub_dic = {}
                        if key2 == 'comments':
                            sub_dic['comments'] = val2
                    if sub_dic:
                        li.append(sub_dic.copy())      
                sort_dic['order_history'].extend(li.copy())

        return sort_dic