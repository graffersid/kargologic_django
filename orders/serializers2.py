from django.db import transaction
from rest_framework.response import Response
from rest_framework import serializers
from KargoLogics.settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME, EMAIL_HOST_USER,AWS_S3_CUSTOM_DOMAIN,BUCKET_URL

import boto3
from boto3.session import Session

from orders.serializers import ArrOrderSerializer, CarrGetOrderDetail
from OrderExtraField.serializers import ArrOrderExtraFieldSerializer, CarrGetOrderDetailExtraFields
from OrderTemplate.serializers import ArrOrderTemplate, CarrGetOrderDetailTemplate
from CarrierDriver.serializers import ArrOrderDriver
from Pickup.serializers import ArrOrderPickSerializer, AssignPickDriverSerializer, AssignPickDriverOrderDetailsSerializer, PickDriverOrderDetailsSerializer
from Delivery.serializers import ArrOrderDropSerializer, AssignDropDriverSerializer, AssignDropDriverOrderDetailSerializer, DropDriverOrderDetailSerializer
from OrderComment.serializers import OrderCommentSerializer
from OrderAttachment.serializers import OrderAttachmentSerializer, OrderCreateAttachSerializer
from OrderHistory.serializers import GetOrderCommentSerializer
from OrderDriver.serializers import ProgressbarOrderDriverSerializer, TrackingOrderDriverSerializer
from rest_framework.response import Response
from orders import views
from OrderDriver import views as v2
from rest_framework import status
from orders.models import Order
from OrderTemplate.models import OrderTemplate
from OrderDriver.models import OrderDriver
from OrderExtraField.models import OrderExtraField
from Delivery.models import Drop
from Pickup.models import Pick
from status.models import Status
from OrderHistory.models import OrderHistory
from OrderAttachment.models import OrderAttachment
from base64 import decodestring
from CompanySenderReceiver.models import CompanySenderReceiver
from AssetRelation.serializers import AssetRelationSerializer, OrderDetailAssetRelationSerializer
from Asset.models import Asset
from attachdetached.models import AttachDetached 
from AssetRelation.models import AssetRelation
from Notifications.models import Notification
from CompanyUser.models import CompanyUser
class ArrayOrderSerializer(serializers.Serializer):   
    order_detail          = ArrOrderSerializer(required=False)
    extra_detail          = ArrOrderExtraFieldSerializer(many = True, required=False)
    template              = ArrOrderTemplate(required=False)
    pickup                = ArrOrderPickSerializer(many = True, required=False)
    delivery              = ArrOrderDropSerializer(many = True, required=False)
    assign_pick_driver    = AssignPickDriverSerializer(many = True, required=False) 
    assign_drop_driver    = AssignDropDriverSerializer(many = True, required=False)
    attachment            = OrderCreateAttachSerializer(many = True, required = False)
    assetrel              = AssetRelationSerializer(many = True, required = False)
    def create(self, validated_data):
        res_data = validated_data
    
        try:
            val = validated_data['order_detail']
        except Exception as e:
            print('key error "order_detail"',e)
            context = {
                "data":"",
                "message":"key error 'order_detail'"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)    
        try:
            domain_id = self.context.get("domain_id")
            role_id   = self.context.get("role_id")
            com_id = self.context.get("com_id")
            carr_id = self.context.get("carr_id")
            status_id = self.context.get("status_id")
        except Exception as e:
            print('key errors in "context"',e)
            context = {
                "data":"",
                "message":"internal error"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)    
        try:
            created_order= Order.objects.create(domainID_id = domain_id,roleID_id = role_id, Created_by_CarrierID_id = carr_id, CarrierCompanyID_id = com_id , StatusID_id = status_id,**validated_data['order_detail'])
            order_id=created_order.id
            job_id = created_order.job_id
            carrier_com_name = created_order.CarrierCompanyID.name
            com_user_obj = CompanyUser.objects.filter(CompanyID_id = created_order.CarrierCompanyID.id).first()
            master_carr_id = com_user_obj.Created_by_CarrierID.id
            carrier_com_id = created_order.CarrierCompanyID.id
            customer_com_name = created_order.CustomerCompanyID.name
            
            cust_com_user_obj = CompanyUser.objects.filter(CompanyID_id = created_order.CustomerCompanyID.id).first()
            master_cust_id = cust_com_user_obj.Created_by_CustomerID.id
            created_by_carr_id = created_order.Created_by_CarrierID.id
        except Exception as e:
            print('order query error',e)
            context = {
                "data":"",
                "message":" order creation failed"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)    
        check_order_save = created_order.is_active
        carr_com_id = created_order.CarrierCompanyID.id
        created_by_carr_name = created_order.Created_by_CarrierID.name
        try:
            if validated_data['template'] != {}:
                try:
                    templ_obj = OrderTemplate.objects.create(Created_by_CarrierID_id = carr_id ,is_active = check_order_save,**validated_data["template"])
                    templ_id = templ_obj.id
                except Exception as e:
                    print('template query error',e)
                    context = {
                        "data":"",
                        "message":" template creation failed"
                            }
                    return Response(context, status=status.HTTP_400_BAD_REQUEST)    
        except Exception as e:
            print('key errors in "template"',e)
            context = {
                "data":"",
                "message":"key errors in 'template'"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)
            
        try:
            if validated_data["extra_detail"] != []:
                ex_data   = validated_data.pop("extra_detail")
                for ex in ex_data:
                    try:        
                        if validated_data['template']:
                            extr_obj  = OrderExtraField.objects.create(OrderID_id = order_id,OrderTemplateID_id =templ_id,is_active = check_order_save,**ex)
                    except Exception as e:
                        extr_obj  = OrderExtraField.objects.create(OrderID_id = order_id,is_active = check_order_save,**ex)

        except Exception as e:
            print('extra_detail',e)
            pass
        if validated_data["pickup"] != []:
            pickup_data = validated_data.pop("pickup")
            
            for data in pickup_data:
                send_id = ''
                if data['sender'] != {}:
                    if data['sender']['kid']:
                        pick_obj = Pick.objects.create(OrderID_id = order_id,is_active = check_order_save, sender_id = data['sender']['kid'] , address = data['address'], longitude = data['longitude'],
                                                            latitude = data['latitude'], pickup_date_time = data['pickup_date_time'], pickup_date_time_to = data['pickup_date_time_to'])
                        send_id = data['sender']['kid']
                    else:
                        comsd = CompanySenderReceiver.objects.create(CompanyID_id = carr_com_id, is_sender = "True", **data['sender'])
                        pick_obj = Pick.objects.create(OrderID_id = order_id,is_active = check_order_save, sender_id = comsd.id, address = data['address'], 
                                                        longitude = data['longitude'], latitude = data['latitude'], pickup_date_time = data['pickup_date_time'], pickup_date_time_to = data['pickup_date_time_to'])
    

                else:
                    pick_obj = Pick.objects.create(OrderID_id = order_id,is_active = check_order_save, address = data['address'], longitude = data['longitude'],
                                                            latitude = data['latitude'], pickup_date_time = data['pickup_date_time'], pickup_date_time_to = data['pickup_date_time_to'])
        
    
        if validated_data["delivery"] != []:
            delivery_data = validated_data.pop("delivery")
            
            for data in delivery_data:
    
                if data['receiver'] != {}:
                    if data['receiver']['kid']:
                        drop_obj = Drop.objects.create(OrderID_id = order_id,is_active = check_order_save, receiver_id = data['receiver']['kid'], address = data['address'], longitude = data['longitude'], latitude = data['latitude'],
                                                        delivery_date_time = data['delivery_date_time'], delivery_date_time_to = data['delivery_date_time_to'])
    
                    else:
                        comsd = CompanySenderReceiver.objects.create(CompanyID_id = carr_com_id, is_sender = "False" , **data['receiver'])
                        drop_obj = Drop.objects.create(OrderID_id = order_id, is_active = check_order_save, receiver_id = comsd.id,  address = data['address'], longitude = data['longitude'], latitude = data['latitude'],
                                                        delivery_date_time = data['delivery_date_time'], delivery_date_time_to = data['delivery_date_time_to'])
    
                else:
                    drop_obj = Drop.objects.create(OrderID_id = order_id,is_active = check_order_save, address = data['address'], longitude = data['longitude'], latitude = data['latitude'],
                                                    delivery_date_time = data['delivery_date_time'], delivery_date_time_to = data['delivery_date_time_to'])
    
        try:
            if validated_data["assign_pick_driver"] != []:
                for data in validated_data["assign_pick_driver"]:
                    st = Status.objects.filter(status_name = "pending").first()
                    print('pick data', data)

                    ord_driv_obj = OrderDriver.objects.get_or_create(OrderID_id = order_id, is_active = check_order_save, DriverID_id = int(data["DriverID"]), Created_by_CarrierID_id = created_by_carr_id, order_status = st, AssetID_id = int(data["ass_truck_id"]))
                     
                    driver_id = data.pop('DriverID')
                    pick_id = ''
                    if data['sender'] != {}:
                        
                        if data['sender']['kid']:
                            pick_obj = Pick.objects.create(OrderDriverID = ord_driv_obj[0], is_active = check_order_save,sender_id = data['sender']['kid'] , address = data['address'], longitude = data['longitude'],
                                                            latitude = data['latitude'], pickup_date_time = data['pickup_date_time'], pickup_date_time_to = data['pickup_date_time_to'], assigned_quantity = data['assigned_quantity'])
                            pick_id = pick_obj.id

                        else:
                            comsd = CompanySenderReceiver.objects.create(CompanyID_id = carr_com_id, is_sender = "True", **data['sender'])
                            pick_obj = Pick.objects.create(OrderDriverID = ord_driv_obj[0], is_active = check_order_save, sender_id = comsd.id , address = data['address'], longitude = data['longitude'],
                                                            latitude = data['latitude'], pickup_date_time = data['pickup_date_time'], pickup_date_time_to = data['pickup_date_time_to'], assigned_quantity = data['assigned_quantity'])
                            pick_id = pick_obj.id
                    else:
                        pick_obj = Pick.objects.create(OrderDriverID = ord_driv_obj[0], is_active = check_order_save, address = data['address'], longitude = data['longitude'],
                                                    latitude = data['latitude'], pickup_date_time = data['pickup_date_time'], pickup_date_time_to = data['pickup_date_time_to'], assigned_quantity = data['assigned_quantity'])
                        pick_id = pick_obj.id
                    
                    
                    if data['asset'] != []:
                        for data_in in data['asset']:
                            if data_in['aid']:
                                AttachDetached.objects.create(AssetID_id = data_in['aid'], schedule_from = data['pickup_date_time'], schedule_to = data['pickup_date_time_to'])
                                AssetRelation.objects.create(CompanyID_id = carrier_com_id, AssetID_id = data_in['aid'], PickID_id = pick_id, is_attach = "True")
                    status_obj = Status.objects.filter(status_name = "pending").values_list("id")
                    ord_obj = Order.objects.filter(id = order_id).update(StatusID_id = status_obj[0][0])

                    driv_comment = str('%s (%s) assigned you a new job(%s) for %s'%(created_by_carr_name, carrier_com_name, job_id, customer_com_name))
                    notif_for_driv = OrderHistory.objects.create(OrderID_id = order_id, Created_by_DriverID_id = int(driver_id), comments = driv_comment, is_active = check_order_save)
                    notif_obj = Notification.objects.create(OrderID_id = order_id, created_by_carrier_id = master_carr_id, created_for_driver_id = int(driver_id), comments = driv_comment)
        except Exception as e:
            print('assign_pick_driver',e)
            
        try:
            if validated_data["assign_drop_driver"] != []:
                for data in validated_data["assign_drop_driver"]:
                    std = Status.objects.filter(status_name = "pending").first()
                    print('drop data', data)
                    ord_driv_obj = OrderDriver.objects.get_or_create(OrderID_id = order_id, DriverID_id = int(data["DriverID"]), Created_by_CarrierID_id = created_by_carr_id,is_active =check_order_save, order_status = std , AssetID_id = int(data["ass_truck_id"]))
                    data.pop('DriverID')
                    drop_id = ''
                    if data['receiver'] != {}:
                        if data['receiver']['kid']:
                            drop_obj = Drop.objects.create(OrderDriverID = ord_driv_obj[0],is_active = check_order_save,receiver_id = data['receiver']['kid'], address = data['address'], longitude = data['longitude'], latitude = data['latitude'],
                                                        delivery_date_time = data['delivery_date_time'], delivery_date_time_to = data['delivery_date_time_to'], assigned_quantity = data['assigned_quantity'])
                            drop_id = drop_obj.id
                        else:
                            comsd = CompanySenderReceiver.objects.create(CompanyID_id = carr_com_id, is_sender = "False", **data['receiver'])
                            drop_obj = Drop.objects.create(OrderDriverID = ord_driv_obj[0],is_active = check_order_save,receiver_id = comsd.id, address = data['address'], longitude = data['longitude'], latitude = data['latitude'],
                                                        delivery_date_time = data['delivery_date_time'], delivery_date_time_to = data['delivery_date_time_to'], assigned_quantity = data['assigned_quantity'])
                            drop_id = drop_obj.id
                    else:
                        drop_obj = Drop.objects.create(OrderDriverID = ord_driv_obj[0],is_active = check_order_save, address = data['address'], longitude = data['longitude'], latitude = data['latitude'],
                                                    delivery_date_time = data['delivery_date_time'], delivery_date_time_to = data['delivery_date_time_to'], assigned_quantity = data['assigned_quantity'])                        
                        drop_id = drop_obj.id
                    if data['asset'] != []:
                        for data_in in data['asset']:
                            if data_in['aid']:
                                AttachDetached.objects.create(AssetID_id = data_in['aid'], schedule_from = data['delivery_date_time'], schedule_to = data['delivery_date_time_to'])
                                AssetRelation.objects.create(CompanyID_id = carrier_com_id, AssetID_id = data_in['aid'], DropID_id = drop_id, is_attach = "True")            

                    status_obj = Status.objects.filter(status_name = "pending").values_list("id")
                    ord_obj = Order.objects.filter(id = order_id).update(StatusID_id = status_obj[0][0])
        except Exception as e:
            print('drop_pick_driver',e)

        try:
            if validated_data["attachment"] != []: 
    
                for data in validated_data["attachment"]:

                    ordAttach = OrderAttachment.objects.create(OrderID_id = order_id, Created_by_CarrierID_id = created_by_carr_id , attachment_name = data['attachment_name'], physical_path = BUCKET_URL+"orderattachment/"+str(data['attachment_name']))
                    session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
                    
                    image_str = data['image_read']
    
                    # file_input = decodestring(image_str)


                    s3 = session.resource('s3')
                    cloudFilename = "orderattachment/"+str(ordAttach.attachment_name)
                    s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=image_str, ACL='public-read')
    
        except Exception as e:
        
            print('error in attachment',e)
        
        # if validated_data["assetrel"] != []:
        #     for data in validated_data["assetrel"]:
        #         if validated_data["assetrel"]["AssetID"]:

        dic = {
            "order_id":order_id,
            "job_id":job_id,
            "created_time":created_order.created_DT,
            "order_created_time":created_order.order_create_time,
            "master_cust_id":master_cust_id,
            "order_status":created_order.is_active
        }
        return dic

    def to_representation(self, data):
    
        return data

class CarrGetOrderDetails(serializers.Serializer): 
    order = CarrGetOrderDetail(required = False)
    extra_fields = CarrGetOrderDetailExtraFields(required = False, many = True)


    def to_representation(self, instance):    
        
        order = CarrGetOrderDetail(instance['order'],required = False, many =True)
        extra_fields = CarrGetOrderDetailExtraFields(instance['extra_fields'], required = False, many =True)
        order_comment = OrderCommentSerializer(instance['order_comment'], required = False, many =True)
        order_attachment = OrderAttachmentSerializer(instance['order_attachment'], required = False, many =True)
        order_pick = PickDriverOrderDetailsSerializer(instance['order_pick'], required = False, many =True)
        order_drop = DropDriverOrderDetailSerializer(instance['order_drop'], required = False, many =True)
        ass_pick  = AssignPickDriverOrderDetailsSerializer(instance['ass_pick_detail'], required = False, many =True)
        ass_drop  = AssignDropDriverOrderDetailSerializer(instance['ass_drop_detail'], required = False, many =True)
        order_hist = GetOrderCommentSerializer(instance['order_his'], required = False, many =True)
        progressbar = ProgressbarOrderDriverSerializer(instance['progressbar'], required = False, many =True)
        tracking_driv = TrackingOrderDriverSerializer(instance['progressbar'], required = False, many =True) 
        pick_asset = OrderDetailAssetRelationSerializer(instance['pick_asset'], required = False, many =True)
        drop_asset = OrderDetailAssetRelationSerializer(instance['drop_asset'], required = False, many =True)
        final_dic = { 
            "order_details":order.data,
            "extra_fields":extra_fields.data,
            "order_comment":order_comment.data,
            "order_attachment":order_attachment.data,
            "order_history":order_hist.data,
            "order_pick":order_pick.data,
            "order_drop":order_drop.data,
            "assign_pick":ass_pick.data,
            "pick_asset":pick_asset.data,
            "assign_drop":ass_drop.data,
            "drop_asset":drop_asset.data,
            "progressbar":progressbar.data,
            "tracking_driv":tracking_driv.data
        }
        return final_dic


# class EditOrderSerializer(serializers.Serializer):   
#     order_detail = ArrOrderSerializer(required=False)
#     extra_detail = ArrOrderExtraFieldSerializer(many = True, required=False)
#     template     = ArrOrderTemplate(required=False)
#     pickup       = ArrOrderPickSerializer(many = True, required=False)
#     delivery     = ArrOrderDropSerializer(many = True, required=False)
#     assign_pick_driver = AssignPickDriverSerializer(many = True, required=False) 
#     assign_drop_driver = AssignDropDriverSerializer(many = True, required=False)
#     attachment   = OrderCreateAttachSerializer(many = True, required = False)
#     def create(self, validated_data):
#         res_data = validated_data
#         val = validated_data['order_detail']
 
#         domain_id = self.context.get("domain_id")
#         role_id   = self.context.get("role_id")
#         com_id = self.context.get("com_id")
#         carr_id = self.context.get("carr_id")
#         status_id = self.context.get("status_id")

        
#         order_obj = Order.objects.filter(id = ex_order_id).update(domainID_id = domain_id,roleID_id = role_id, Created_by_CarrierID_id = carr_id, CarrierCompanyID_id = com_id , StatusID_id = status_id,**validated_data['order_detail'])
#         order_id=ex_order_id
#         main_order_obj = Order.objects.filter(id = ex_order_id).first()
#         job_id = main_order_obj.job_id

#         check_order_save = main_order_obj.is_active
#         created_by_carr_id = main_order_obj.Created_by_CarrierID.id
        
#         if validated_data['template']:              
#             templ_obj = OrderTemplate.objects.filter(id = ).update(Created_by_CarrierID_id = carr_id ,is_active = check_order_save,**validated_data["template"])
#             templ_id = templ_obj.id


            

class CustomerArrayOrderSerializer(serializers.Serializer):   
    order_detail = ArrOrderSerializer(required=False)
    extra_detail = ArrOrderExtraFieldSerializer(many = True, required=False)
    template     = ArrOrderTemplate(required=False)
    pickup       = ArrOrderPickSerializer(many = True, required=False)
    delivery     = ArrOrderDropSerializer(many = True, required=False)
    attachment   = OrderCreateAttachSerializer(many = True, required = False)
    def create(self, validated_data):
        res_data = validated_data
        
        try:
            val = validated_data['order_detail']
        except Exception as e:
            print('key error "order_detail"',e)
            context = {
                "data":"",
                "message":"key error 'order_detail'"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)    
        try:
            domain_id = self.context.get("domain_id")
            role_id   = self.context.get("role_id")
            cus_com_id = self.context.get("cus_com_id")
            cus_id = self.context.get("cus_id")
            status_id = self.context.get("status_id")
        except Exception as e:
            print('key errors in "context"',e)
            context = {
                "data":"",
                "message":"internal error"
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)    
        
        created_order= Order.objects.create(domainID_id = domain_id,roleID_id = role_id, Created_by_CustomerID_id = int(cus_id), CustomerCompanyID_id = int(cus_com_id) , StatusID_id = status_id,**validated_data['order_detail'])
        order_id=created_order.id
        job_id = created_order.job_id
        customer_com_name = created_order.CustomerCompanyID.name    
        check_order_save = created_order.is_active
        cust_com_id = created_order.CustomerCompanyID.id
        com_user_obj = CompanyUser.objects.filter(CompanyID_id = created_order.CarrierCompanyID.id).first()
        master_carr_id = com_user_obj.Created_by_CarrierID.id
      
        if validated_data['template'] != {}:
            try:
                templ_obj = OrderTemplate.objects.create(Created_by_CustomerID_id = int(cus_id), is_active = check_order_save,**validated_data["template"])
                templ_id = templ_obj.id
            except Exception as e:
        
                context = {
                    "data":"",
                    "message":" template creation failed"
                        }
                return Response(context, status=status.HTTP_400_BAD_REQUEST)    
        
        
        if validated_data["extra_detail"] != []:
            ex_data   = validated_data.pop("extra_detail")
            for ex in ex_data:
                try:        
                    if validated_data['template'] !={}:
                        extr_obj  = OrderExtraField.objects.create(OrderID_id = order_id,OrderTemplateID_id =templ_id,is_active = check_order_save,**ex)
                except Exception as e:
                    extr_obj  = OrderExtraField.objects.create(OrderID_id = order_id,is_active = check_order_save,**ex)

        

        
        if validated_data["pickup"] != []:
            pickup_data = validated_data.pop("pickup")
            
            for data in pickup_data:
                if data['sender'] != {}:
                    if data['sender']['kid']:
                        pick_obj = Pick.objects.create(OrderID_id = order_id,is_active = check_order_save, sender_id = data['sender']['kid'] , address = data['address'], longitude = data['longitude'],
                                                            latitude = data['latitude'], pickup_date_time = data['pickup_date_time'], pickup_date_time_to = data['pickup_date_time_to'])
        
                    else:
                        comsd = CompanySenderReceiver.objects.create(CompanyID_id = cust_com_id, name = data['sender']['name'], is_sender = data['sender']['is_sender'])
                        pick_obj = Pick.objects.create(OrderID_id = order_id,is_active = check_order_save, sender_id = comsd.id, address = data['address'], 
                                                        longitude = data['longitude'], latitude = data['latitude'], pickup_date_time = data['pickup_date_time'], pickup_date_time_to = data['pickup_date_time_to'])
        
                else:
                    pick_obj = Pick.objects.create(OrderID_id = order_id,is_active = check_order_save, address = data['address'], longitude = data['longitude'],
                                                        latitude = data['latitude'], pickup_date_time = data['pickup_date_time'], pickup_date_time_to = data['pickup_date_time_to'])       
        
        
        if validated_data["delivery"] != []:
            delivery_data = validated_data.pop("delivery")
            
            for data in delivery_data:
                if data['receiver'] != {}:
                    if data['receiver']['kid']:
                        drop_obj = Drop.objects.create(OrderID_id = order_id,is_active = check_order_save, receiver_id = data['receiver']['kid'], address = data['address'], longitude = data['longitude'], latitude = data['latitude'],
                                                        delivery_date_time = data['delivery_date_time'], delivery_date_time_to = data['delivery_date_time_to'])
        
                    else:
                        comsd = CompanySenderReceiver.objects.create(CompanyID_id = cust_com_id, name = data['receiver']['name'], is_sender = data['receiver']['is_sender'])
                        drop_obj = Drop.objects.create(OrderID_id = order_id, is_active = check_order_save, receiver_id = comsd.id,  address = data['address'], longitude = data['longitude'], latitude = data['latitude'],
                                                        delivery_date_time = data['delivery_date_time'], delivery_date_time_to = data['delivery_date_time_to'])
        
                else:
                    drop_obj = Drop.objects.create(OrderID_id = order_id,is_active = check_order_save, address = data['address'], longitude = data['longitude'], latitude = data['latitude'],
                                                    delivery_date_time = data['delivery_date_time'], delivery_date_time_to = data['delivery_date_time_to'])      
                                                                    
        if validated_data["attachment"] !=[]: 
        
            for data in validated_data["attachment"]:
                if data['image_read']:
                    ordAttach = OrderAttachment.objects.create(OrderID_id = order_id, Created_by_CustomerID_id = int(cus_id) , attachment_name = data['attachment_name'], physical_path = BUCKET_URL+"orderattachment/"+str(data['attachment_name']))
                    session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
                    
                    image_str = data['image_read']
                    print('---------------------------------------------------',type(image_str))
        


                    s3 = session.resource('s3')
                    cloudFilename = "orderattachment/"+str(ordAttach.attachment_name)
                    s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=image_str, ACL='public-read')

        
        dic = {
            "order_id":order_id,
            "job_id":job_id,
            "created_time":created_order.created_DT,
            "order_created_time":created_order.order_create_time,
            "master_carr_id":master_carr_id,
            "order_status":created_order.is_active
        }
        return dic

    def to_representation(self, data):
    
        return data