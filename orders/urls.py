from django.conf.urls import url

from orders import views

urlpatterns = [
   url('carr-order-create/',views.CarrOrderCreate.as_view()),
   url('arr/',views.ArrOrderCreate.as_view()),
   url('orcustcreate/',views.CustomerCreateOrder.as_view()),
   url('carr_edit_order/',views.CarrEditOrder.as_view()),
   url('csvupload/',views.CsvOrderCreate.as_view()),
   url('carrcsv/',views.CsvFile.as_view()),
   url('ord_details_carrier/',views.CarrierOrderDetails.as_view()),
   url('ord_tracking/',views.OrderTracking.as_view()),
   url('carr_order_csv_download/',views.CarrOrderCsvDownload.as_view()),
   url('driver_chng_ord_stat_/',views.DriverChangeOrderStatus.as_view()),
   url('order_track_sendemail/',views.TrackingOrderEmailSend.as_view()),
   url('comsendreceive/',views.ComSenderReceiverList.as_view()),
   url('jobgenerate/',views.OrderJobID.as_view()),
]
