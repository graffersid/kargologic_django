import csv
from django.shortcuts import render
from django.db import transaction
import pandas as pd
from django.http import FileResponse
from django.http import HttpResponse
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
import pdb
from carriers.models import CarrierExtraInfo
from orders.models import Order
from status.models import Status
from custom_users.models import User
from company.models import Company
from drivers.models import DriverExtraInfo
from CustomerCarrier.models import CustomerCarrier
from OrderHistory.models import OrderHistory

from CarrierDriver.serializers import CarrierDriverListSerializer
from orders.serializers import OrderStatusCarrSerializer,OrderSerializer, CarrierOrderDetailsSer
from OrderExtraField.serializers import CarrtoOrderCreateSerializer, OrderExtraFieldSerializer, CarrCreateOrderExtraFieldSerializer
from OrderTemplate.serializers import OrderTemplateSerializer
from orders.serializers2 import ArrayOrderSerializer, CarrGetOrderDetails, CustomerArrayOrderSerializer
from orders.serializers import ArrOrderSerializer,CarrOrderCsvDownloadSerializer
from OrderExtraField.serializers import ArrOrderExtraFieldSerializer
from OrderTemplate.serializers import ArrOrderTemplate
from CarrierDriver.serializers import ArrOrderDriver

from OrderTemplate.models import OrderTemplate
from TruckType.models import TruckType
from OrderDriver.models import OrderDriver
from CarrierDriver.models import CarrierDriver
from CustomerCarrier.models import CustomerCarrier
from CompanyUser.models import CompanyUser
from domain.models import Domain
from user_role.models import UserRole
from orders.models import Order
from OrderExtraField.models import OrderExtraField
from OrderComment.models import OrderComment
from OrderAttachment.models import OrderAttachment
from Delivery.models import Drop
from Pickup.models import Pick
from django.http import FileResponse
from attachdetached.models import AttachDetached
import io
from KargoLogics import settings
from django.core.mail import send_mail, EmailMultiAlternatives
from rest_framework.parsers import MultiPartParser,FileUploadParser
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.parsers import FileUploadParser
from OrderDriver.views import assign_driver
from datetime import date, time
import xlsxwriter
from django.http import StreamingHttpResponse
# Create your views here.
import boto3
import requests
import json
from boto3.session import Session
from datetime import datetime
from KargoLogics.settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME, EMAIL_HOST_USER,AWS_S3_CUSTOM_DOMAIN,BUCKET_URL,HERE_MAP_USER,HERE_MAP_KEY,HERE_MAP_APPID
from AssetRelation.models import AssetRelation
from Asset.models import Asset
from Notifications.models import Notification
from customers.models import CustomerExtraInfo
from AssetRelation.models import AssetRelation
"""carrier can see order detail on click on particular order """
class CarrOrderDetails(APIView):
    def get(self, request):
        or_ex_obj = OrderExtraField.objects.filter(id = request.GET["order_id"])
        serialz = OrderExtraFieldSerializer(or_ex_obj, many = True)
        return Response(serialz.data)



def create_order(resp):
    try:
        order_info = resp['order_detail']
    except Exception as e:
        print("key error 'order_detail'")
        context = { 
            "message":"key error 'order_detail'",
            "exception":str(e),
            "data":""
            }
        return context

    order_ser = ArrOrderSerializer(data = resp['order_detail'])
    if order_ser.is_valid():
        new_order = order_ser.save()
        new_order_id = new_order.id
        tem_count = 0
        template_id = ''
        if resp['template'] is not "":
            templ_info = resp['template']
            templ_ser = OrderTemplateSerializer(data = resp['template'])
            if templ_ser.is_valid():
                new_template = templ_ser.save()
                template_id = new_template.id
                tem_count = tem_count + 1
            else:
                context = {
                    "message":"'template' invalid entry",
                    "exception":"",
                    "data":""
                }
                print('Error template-----' )
                return context
        
        if resp['extra_detail'] is not "":
            ext_info = resp['extra_detail']
            order_ext_ser=''
            if template_id:
                order_ext_ser = CarrCreateOrderExtraFieldSerializer(data = resp['extra_detail'], many=True, context = {'template_id':template_id, 'order_id' : new_order_id })  
            
            else:
                order_ext_ser = CarrCreateOrderExtraFieldSerializer(data = resp['extra_detail'], many=True, context = {'template_id':'', 'order_id' : new_order_id })
            

            if order_ext_ser.is_valid():
                order_ext_ser.save()
                
                if tem_count==1:
                    context = {
                        "message":"'Order','Order Template' and 'Order Extra Fields' are created successfully",
                        "exception":"",
                        "data":new_order_id
                    }
                    return context
                else:
                    context = {
                        "message":"'Order' and 'Order Extra Fields' are created successfully",
                        "exception":"",
                        "data":new_order_id
                    }
                    return context 
            else:
                context = {
                    "message": " 'Order Extra Fields', provided information is not valid",
                    "exception":"",
                    "data":""
                    }
                print('Error Extra-----')
                
                return context
        
        context = {
            "message": "'Order', created successfully",
            "exception":"",
            "data":new_order_id
            }
        
        return context
    else:
        context = {
            "message": "'Order', provided information is not valid",
            "exception":order_ser.errors,
            "data":""
            }
        print('Error----- order_ser-------', order_ser.errors )
        return context


class CarrOrderCreate(APIView): 
    @transaction.atomic
    def post(self, request):
        try:
            res_data = request.data
            sid = transaction.savepoint()
            try:
                fun = create_order(res_data)
            except Exception as e:
                print('error in invoke of create_order function')
                context = {
                    "data":"",
                    "message":"internal error"
                }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
            if fun['data']:
                if res_data['driver']:
                    driv_fun = assign_driver(res_data, fun['data'])
                    if driv_fun["message"]=="success":
                        context = {
                            "message": "%s 'driver' added successfully"%(fun["message"]),
                            "exception":"",
                            "data":""
                            }
                        return Response(context,status=status.HTTP_200_OK)
                    else:
                        transaction.savepoint_rollback(sid)
                        context = {
                            "message":'%s %s'%(fun["message"],driv_fun["message"]),
                            "exception":"",
                            "data":""
                            }
                        return Response(context,status=status.HTTP_400_BAD_REQUEST)

                else:
                    return Response(fun["message"],status=status.HTTP_400_BAD_REQUEST)
            else:
                transaction.savepoint_rollback(sid)
                context = {
                            "message":fun["message"],
                            "exception":"",
                            "data":""
                            }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print('$$$$$$$$$$$')
    def delete(self, request, pk, format=None):
        order_obj = Order.objects.filter(id = pk)
        order_obj.delete()
        return Response("deleted")

class ArrOrderCreate(APIView):  
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)    
        try:
            data = request.data["arr_order"]
            
        except Exception as e:
            print('key error',e)
            context = {
                "data":"",
                "message":"key error 'arr_order'"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            order_status = Status.objects.filter(status_name = "unassigned") 
            status_id = order_status.values('id')[0]['id']
        except Exception as e:
            print('order_status or status_id query error',e)
            context = {
                "data":"",
                "message":"internal error"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            context = {"com_id":int(content[0][0]),"carr_id":int(content[0][1]),"domain_id":int(content[0][2]),"role_id":int(content[0][3]),"status_id":status_id}
        except Exception as e:
            print('context declaration error',e)
            context = {
                "data":"",
                "message":"internal error"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        print('data',data)
        serialz = ArrayOrderSerializer(data = data, context = context, many = True)
        if serialz.is_valid():
            serialz.save()
            
            for order in serialz.data:
                order_id = order['order_id']
                job_id   = order['job_id']
                created_time = order['created_time']
                order_created_time = order['order_created_time']
                order_sts    = order['order_status']
                master_cust_id = order['master_cust_id']
                # date =  str(created_time)     #"2019-05-28 10:44:44.468601+00:00"
                # date = date.split('.')
                # datetime_object = datetime.strptime(date[0], '%Y-%m-%d %H:%M:%S')

                # old_format = '%Y-%m-%d %H:%M:%S'
                # new_format = '%d/%m/%Y %I:%M %p'

                # new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)
                

            
                if order_sts:
                    comments = '%s is successfully placed on %s'%(job_id,str(order_created_time))
                    
                    order_hist_obj = OrderHistory.objects.create(Created_by_CarrierID_id = int(content[0][1]), OrderID_id = int(order_id), comments = str(comments))
                    notif_obj = Notification.objects.create(OrderID_id = order_id, created_for_carrier_id = int(content[0][1]), created_for_customer_id = master_cust_id, comments = str(comments))
            
            context={
                "message":"order success",
                "exception":"",
                "data":""
            }
            return Response(context,status=status.HTTP_200_OK)
        else:
            print('serializer error ',str(serialz.errors))
            context={
                "message":" Order save failed!!",
                "exception":"json data provided is incorrect",
                "data":""
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)



class CustomerCreateOrder(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param","domain_id","role_id")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)    
        try:
            data = request.data["arr_order"]
            print('data',data)
        except Exception as e:
            print('key error',e)
            context = {
                "data":"",
                "message":"key error 'arr_order'"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            order_status = Status.objects.filter(status_name = "unassigned") 
            status_id = order_status.values('id')[0]['id']
        except Exception as e:
            print('order_status or status_id query error',e)
            context = {
                "data":"",
                "message":"internal error"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            context = {"cus_com_id":int(content[0][0]),"cus_id":int(content[0][1]),"domain_id":int(content[0][2]),"role_id":int(content[0][3]),"status_id":status_id}
        except Exception as e:
            print('context declaration error',e)
            context = {
                "data":"",
                "message":"internal error"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST) 
        serialz = CustomerArrayOrderSerializer(data = data, context = context, many = True) 
        if serialz.is_valid():
            serialz.save()
            
            for order in serialz.data:
                order_id = order['order_id']
                job_id   = order['job_id']
                created_time = order['created_time']
                order_created_time = order['order_created_time']
                order_sts    = order['order_status']
                master_carr_id = order['master_carr_id']
                if order_sts:
                    comments = '%s is successfully placed on %s'%(job_id,str(order_created_time))
                    
                    order_hist_obj = OrderHistory.objects.create(Created_by_CustomerID_id = int(content[0][1]), OrderID_id = int(order_id), comments = str(comments))
                    notif_obj = Notification.objects.create(OrderID_id = int(order_id), created_for_carrier_id = master_carr_id, created_for_customer_id = int(content[0][1]),comments = str(comments))
            
            context={
                "message":"order success",
                "exception":"",
                "data":""
            }
            return Response(context,status=status.HTTP_200_OK)
        else:
            print('serializer error ',str(serialz.errors))
            context={
                "message":" Order save failed!!",
                "exception":"json data provided is incorrect",
                "data":""
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
    


import io

from django.http.response import HttpResponse

from xlsxwriter.workbook import Workbook

class CsvFile(APIView):
    def get(self, request):
        customer_drop_down = []
        carr_id = request.GET['carr_com_id']
        try:
            customer_arr  = CustomerCarrier.objects.filter(CarriercompanyID_id = int(carr_id)).values_list("CustomercompanyID")
            print('customer_arr########################',customer_arr)
            customer_name = Company.objects.filter(id__in = customer_arr).values_list('id','name')
        except Exception as e:
            print('query error',e)
        customer_arr = [str(i[0])+':'+str(i[1]) for i in customer_name]
        
        # myFile = open('countries.csv', 'w')
        # with myFile:
        #     myFields = ['job_id','customer_code','CustomerCompanyID','price','pickup_location','pickup_date','pickup_time','delivery_location','delivery_date','delivery_time','load_carried','load_type','load_weight','length_load_dimen','width_load_dimen','height_load_dimen','measure_unit','units','hazardous','additional_note','field_name','field_value']
        #     writer = csv.DictWriter(myFile, fieldnames=myFields)
        #     writer.writeheader()
        #     writer.writerow({'CustomerCompanyID' : customer_arr})
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet()
        header_format = workbook.add_format({
                                    'border': 1,
                                    'bg_color': '#C6EFCE',
                                    'bold': True,
                                    'text_wrap': True,
                                    'valign': 'vcenter',
                                    'indent': 1,
                                    })
        worksheet.write('A1', 'Job No(*)', header_format) 
        worksheet.write('B1', 'Customer Code', header_format)
        worksheet.write('C1', 'Customer Name(*)', header_format)
        worksheet.write('D1', 'Price', header_format)
        worksheet.write('E1', 'Pickup Location(*)', header_format)
        worksheet.write('F1', 'Pickup Date From(*)', header_format)
        worksheet.write('G1', 'Pickup Time From(*)', header_format)
        worksheet.write('H1', 'Pickup Date To(*)', header_format)
        worksheet.write('I1', 'Pickup Time To(*)', header_format)
        worksheet.write('J1', 'Delivery Location(*)', header_format)
        worksheet.write('K1', 'Delivery Date From(*)', header_format)
        worksheet.write('L1', 'Delivery Time From(*)', header_format)
        worksheet.write('M1', 'Delivery Date To(*)', header_format)
        worksheet.write('N1', 'Delivery Time To(*)', header_format)
        worksheet.write('O1', 'Load Carried', header_format)
        worksheet.write('P1', 'Load Type', header_format)
        worksheet.write('Q1', 'Load Weight', header_format)
        worksheet.write('R1', 'Length(Load Dimentions)', header_format)
        worksheet.write('S1', 'Width(Load Dimentions)', header_format)
        worksheet.write('T1', 'Height(Load Dimentions)', header_format)
        worksheet.write('U1', 'Measure Unit', header_format)
        worksheet.write('V1', 'Number of Units', header_format)
        worksheet.write('W1', 'Contains Hazardous', header_format)
        worksheet.write('X1', 'Additional Note', header_format)
        worksheet.write('Y1', 'Sender', header_format)
        worksheet.write('Z1', 'Receiver', header_format)
        worksheet.write('AA1', 'Extra Field Name', header_format)
        worksheet.write('AB1', 'Extra Field Value', header_format)
        
        worksheet.data_validation('C2:C1000', {'validate': 'list',
                                  'source':customer_arr },
                                  )
        
        worksheet.data_validation('W2:W1000', {'validate': 'list',
                                'source':['YES','NO'] },)
        

        worksheet.data_validation('F2:F1000', {'validate': 'date',
                                'criteria': 'between',
                                'minimum': date(2013, 1, 1),
                                'maximum': date(3000, 12, 12),
                                'input_title': 'Date Field',
                                'input_message':'formate: YYYY-MM-DD'
                                })
        
        worksheet.data_validation('K2:K1000', {'validate': 'date',
                                'criteria': 'between',
                                'minimum': date(2013, 1, 1),
                                'maximum': date(3000, 12, 12),
                                'input_title': 'Date Field',
                                'input_message':'formate: YYYY-MM-DD'
                                })                          
        
        worksheet.data_validation('H2:H1000', {'validate': 'date',
                                'criteria': 'between',
                                'minimum': date(2013, 1, 1),
                                'maximum': date(3000, 12, 12),
                                'input_title': 'Date Field',
                                'input_message':'formate: YYYY-MM-DD'
                                })
        
        worksheet.data_validation('M2:M1000', {'validate': 'date',
                                'criteria': 'between',
                                'minimum': date(2013, 1, 1),
                                'maximum': date(3000, 12, 12),
                                'input_title': 'Date Field',
                                'input_message':'formate: YYYY-MM-DD'
                                })
        
        worksheet.data_validation('G2:G1000', {'validate': 'time',
                                'minimum': time(0, 0),
                                'maximum': time(23, 59,59),
                                'input_title': 'Time Field',
                                'input_message': 'formate: 24 hour, example: 23:59:00',
                                'criteria': 'between'}) 

        
        worksheet.data_validation('L2:L1000', {'validate': 'time',
                                'minimum': time(0, 0),
                                'maximum': time(23, 59, 59),
                                'input_title': 'Time Field',
                                'input_message': 'formate: 24 hour,example: 23:59:00',
                                'criteria': 'between'})

        worksheet.data_validation('I2:I1000', {'validate': 'time',
                                'minimum': time(0, 0),
                                'maximum': time(23, 59, 59),
                                'input_title': 'Time Field',
                                'input_message': 'formate: 24 hour,example: 23:59:00',
                                'criteria': 'between'})

        worksheet.data_validation('N2:N1000', {'validate': 'time',
                                'minimum': time(0, 0),
                                'maximum': time(23, 59, 59),
                                'input_title': 'Time Field',
                                'input_message': 'formate: 24 hour,example: 23:59:00',
                                'criteria': 'between'})                                                                                                                                                       

        workbook.close()
        output.seek(0)

        response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename=ordercreate.xlsx"

        output.close()

        return response
        


class CsvOrderCreate(APIView):
    parser_classes = [MultiPartParser,FileUploadParser]

    def post(self, request):
        data = request.data
        try:
            con = request.FILES['data_file']

            content = con.read()

            st = content.decode('UTF-8')
        except Exception as e:
            print('file request error',e)
            context = {
                "data":"",
                "message":"file does not provide properly "
            }
        # extra_detail = []
        # arr_order = []
        is_active_arr = []
        order_obj_arr = []
        extra_obj_arr = []
        order_driv_obj_arr = []
        job_arr = []
        order_templ_dic = {}
        arrange_data = {"arr_order":''}
        with open('testcs.csv','wb') as f:
            f.write(content)
        with open('testcs.csv','r') as file:
            data_csv = csv.DictReader(file, delimiter=',')
            # order_detail_dc = {}
            # temp_dc  = {}
            # order_extra_list = []
            # driver_list = []
            # arr_dic = {}
            # job_list = []

            for data in data_csv:
                if data['job_id']:
                    job_arr.append(data['job_id'])
                    stat_obj = Status.objects.filter(status_name = data['Status'])
                    stat_ = stat_obj.values('id')[0]['id']
                    is_active_arr.append((data['active']).capitalize())
                    
                    id = ''
                    for i in data['CustomerCompanyID']:
                        if i == ':':
                            break
                        else:
                            
                            id = id+str(i)
                    order_obj = Order.objects.create(CustomerCompanyID_id = int(id),CarrierCompanyID_id = int(data['CarrierCompanyID']),customer_code = data['customer_code'], StatusID_id = stat_,quantity= data['quantity'],job_id= data['job_id'],pickup_location= data['pickup_location'],delivery_location= data['delivery_location'],is_active = is_active_arr[-1])
                    order_id = order_obj.id
                    order_obj_arr.append(order_id)
                    template_obj = OrderTemplate.objects.create(template_name = data['template_name'],CustomerCompanyID_id = int(id), is_active = is_active_arr[-1])
                    templ_id = template_obj.id
                    if templ_id:
                        order_templ_dic.update(orderid = order_id, template_id = templ_id, is_active = is_active_arr[-1])
                        extra_field_obj = OrderExtraField(OrderID_id = order_id, OrderTemplateID_id = templ_id, field_name = data['field_name'],field_value = data['field_value'],field_type = data['field_type'], is_active = is_active_arr[-1])    
                        extra_obj_arr.append(extra_field_obj)
                    else:
                        extra_field_obj = OrderExtraField(OrderID_id = order_id,field_name = data['field_name'],field_value = data['field_value'],field_type = data['field_type'], is_active = is_active_arr[-1])
                        extra_obj_arr.append(extra_field_obj)
                
                    # if data['Driver_name']:
                    #     order_driver_obj = OrderDriver(OrderID_id = order_id, DriverID_id = data['Driver_name'], capacity = data['capacity'], is_active = is_active_arr[-1])
                    #     order_driv_obj_arr.append(order_driver_obj)

                else:
                    order_obj_ = order_obj_arr[-1]
                    if order_templ_dic['orderid'] == order_obj_:
                        extra_field_obj = OrderExtraField(OrderID_id = order_id, OrderTemplateID_id = order_templ_dic['template_id'],field_name = data['field_name'],field_value = data['field_value'],field_type = data['field_type'], is_active = is_active_arr[-1])
                        extra_obj_arr.append(extra_field_obj)
                    else:
                        extra_field_obj = OrderExtraField(OrderID_id = order_obj_,field_name = data['field_name'],field_value = data['field_value'],field_type = data['field_type'], is_active = is_active_arr[-1])
                        extra_obj_arr.append(extra_field_obj)
                    # if data['Driver_name']:
                    #     order_driver_obj = OrderDriver(OrderID_id = order_obj_, DriverID_id = data['Driver_name'], capacity = data['capacity'], is_active = is_active_arr[-1])
                    #     order_driv_obj_arr.append(order_driver_obj)
        
        OrderExtraField.objects.bulk_create(extra_obj_arr)
        # OrderDriver.objects.bulk_create(order_driv_obj_arr)
        return Response(order_obj_arr)

        
class CarrOrderCsvDownload(APIView): 
    # permission_classes = (IsAuthenticated,)
    def get(self, request):
        # try:
        #     key = request.auth
        #     content = Token.objects.filter(key = key).values_list("content_id")
        # except Exception as e:
        #     print('key or token object error',e)
        #     context = {
        #                     "data": '',
        #                     "message":'token Error or make sure user has carrier company'
        #             }
        #     return Response(context,status=status.HTTP_400_BAD_REQUEST) 
        order_obj = ''
        carr_com_id = int(request.GET['carr_com_id'])
        
        if request.GET['orderlist']:
            order_id = request.GET['orderlist']
            res = order_id.strip('][').split(',')
            res = [int(i) for i in res]
            
            order_obj = Order.objects.filter(id__in = res,CarrierCompanyID_id = carr_com_id, is_delete = False)
        else:
            order_obj = Order.objects.filter(CarrierCompanyID_id = carr_com_id, is_delete = False)
        # except Exception as e:
        #     print("key error 'orderlist'",e)
        #     context = {
        #         "data":"",
        #         "message":"key error 'orderlist'"
        #     }    
        
        order_serializer = CarrOrderCsvDownloadSerializer(order_obj, many = True)
        data = order_serializer.data
        
        
        
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet()
        header_format_green = workbook.add_format({
                                    'border': 1,
                                    'bg_color': '#BAECBD',
                                    'bold': True,
                                    'text_wrap': True,
                                    'valign': 'vcenter',
                                    'indent': 1,
                                    })
        header_format_yellow = workbook.add_format({
                                    'border': 1,
                                    'bg_color': '#EFF51B',
                                    'bold': True,
                                    'text_wrap': True,
                                    'valign': 'vcenter',
                                    'indent': 1,
                                    })
        header_format_blue = workbook.add_format({
                                    'border': 1,
                                    'bg_color': '#ABAAEE',
                                    'bold': True,
                                    'text_wrap': True,
                                    'valign': 'vcenter',
                                    'indent': 1,
                                    })
        worksheet.write('A1', 'Job No', header_format_green) 
        worksheet.write('B1', 'Customer Code', header_format_green)
        worksheet.write('C1', 'Customer Name', header_format_green)
        worksheet.write('D1', 'Carrier Name', header_format_green)
        worksheet.write('E1', 'Price', header_format_green)
        worksheet.write('F1', 'Status', header_format_green)
        worksheet.write('G1', 'Load Carried', header_format_green)
        worksheet.write('H1', 'Load Type', header_format_green)
        worksheet.write('I1', 'Load Weight', header_format_green)
        worksheet.write('J1', 'Length(Load Dimentions)', header_format_green)
        worksheet.write('K1', 'Width(Load Dimentions)', header_format_green)
        worksheet.write('L1', 'Height(Load Dimentions)', header_format_green)
        worksheet.write('M1', 'Measure Unit', header_format_green)
        worksheet.write('N1', 'Number of Units', header_format_green)
        worksheet.write('O1', 'Contains Hazardous', header_format_green)
        worksheet.write('P1', 'Additional Note', header_format_green)
        worksheet.write('Q1', 'Pickup Date Time', header_format_green)
        worksheet.write('R1', 'PickUp Address', header_format_green)
        worksheet.write('S1', 'Sender', header_format_green)
        worksheet.write('T1', 'Delivery Date Time', header_format_green)
        worksheet.write('U1', 'Delivery Address', header_format_green)
        worksheet.write('V1', 'Receiver', header_format_green)
        # worksheet.write('W1', 'Comments', header_format_green)
        worksheet.write('W1', 'Extra Field Name', header_format_green)
        worksheet.write('X1', 'Extra Field Value', header_format_green)
        # worksheet.write('Z1', 'Attachment Name', header_format_green)
        worksheet.write('Y1', 'Pickup Vehicle type', header_format_yellow)
        worksheet.write('Z1', 'Driver', header_format_yellow)
        worksheet.write('AA1', 'Driver Pickup Date', header_format_yellow)
        worksheet.write('AB1', 'Driver Pickup Time', header_format_yellow)
        worksheet.write('AC1', 'Driver Pickup Address', header_format_yellow)
        worksheet.write('AD1', 'Driver Pickup Quantity', header_format_yellow)
        worksheet.write('AE1', 'Driver Pickup Asset', header_format_yellow)
        worksheet.write('AF1', 'Pickup Asset Registration', header_format_yellow)
        worksheet.write('AG1', 'Delivery Vehicle type', header_format_blue)
        worksheet.write('AH1', 'Driver', header_format_blue)
        worksheet.write('AI1', 'Driver Delivery Date', header_format_blue)
        worksheet.write('AJ1', 'Driver Delivery Time', header_format_blue)
        worksheet.write('AK1', 'Driver Delivery Address', header_format_blue)
        worksheet.write('AL1', ' Driver Delivery Quantity', header_format_blue)
        worksheet.write('AM1', 'Driver Delivery Asset', header_format_blue)
        worksheet.write('AN1', 'Delivery Asset Registration', header_format_blue)
        row = 1
        for data_in in data:
            
            com_count = 0
            if data_in['comments']:
                com_count = len(data_in['comments'])
            att_count = 0
            if data_in['order_attach']:
                att_count = len(data_in['order_attach'])
            ex_field = 0
            if data_in["extra_fields"]:
                ex_field = len(data_in["extra_fields"])
            od_pick = 0
            if data_in["order_pick"]:
                od_pick = len(data_in["order_pick"])
            od_dr = 0
            if data_in['order_drop']:
                od_dr = len(data_in['order_drop'])
            ord_his = 0 
            if data_in["order_history"]:
                ord_his = len(data_in["order_history"])


            order_driv_obj = OrderDriver.objects.filter(OrderID = data_in['id'])
            assign_pick = []
            assign_drop = []
            if len(order_driv_obj) != 0:
                assign_pick = Pick.objects.filter(OrderDriverID__in = order_driv_obj, is_delete = False).exclude(OrderID = data_in['id'])
    
                assign_drop = Drop.objects.filter(OrderDriverID__in = order_driv_obj, is_delete = False).exclude(OrderID = data_in['id'])

            cp_count = 0
            cd_count = 0
            for cp in assign_pick:
                pic_ass_ = AssetRelation.objects.filter(PickID = cp)
                for l in pic_ass_:
                    if l.AssetID:
                        cp_count += 1
            
            for cd in assign_drop:
                drp_ass_ = AssetRelation.objects.filter(DropID = cd)        
                for m in drp_ass_:
                    if m.AssetID:
                        cd_count += 1
            
            x = max(ex_field,od_pick,od_dr,len(assign_pick),len(assign_drop),cp_count,cd_count)
            
            
            worksheet.write(row,0,data_in['job_id'])
            
            
            worksheet.write(row,1,data_in['customer_code'])
            
            worksheet.write(row,2,data_in['CustomerCompany'])
            
            worksheet.write(row,3,data_in['CarrierCompanyID'])

            worksheet.write(row,4,data_in['price'])
            
            worksheet.write(row,5,data_in['Status'])
            
            worksheet.write(row,6,data_in['load_carried'])
            
            worksheet.write(row,7,data_in['load_type'])
            
            worksheet.write(row,8,data_in['load_weight'])
            
            worksheet.write(row,9,data_in['length_load_dimen'])
            
            worksheet.write(row,10,data_in['width_load_dimen'])
            
            worksheet.write(row,11,data_in['height_load_dimen'])
            
            worksheet.write(row,12,data_in['measure_unit'])
            
            worksheet.write(row,13,data_in['units'])
            
            HZ =  data_in['hazardous']
            HVAL = ''
            if HZ == False:
                HVAL = 'NO'
            if HZ == True:
                HVAL = 'YES'
            worksheet.write(row,14,HVAL)
            
            worksheet.write(row,15,data_in['additional_note'])
            local_row = 0
            d_local_row = 0
            for indexes in range(0,x):
                if data_in['order_pick'] != []:
                    if indexes < od_pick:
            
                        for key, val in (data_in['order_pick'][indexes]).items():
                            if key == 'pickup_date_time':
                                if val:
                                    datetime_object = datetime.strptime(val, '%Y-%m-%dT%H:%M:%SZ')
                                    old_format = '%Y-%m-%d %H:%M:%S'
                                    new_format = '%d/%m/%Y %I:%M %p'
                                    new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)
                                    worksheet.write(row,16,new_datetime_str)
                            if key == 'address':
                                worksheet.write(row,17,val)
                            if key == 'sender':
                                worksheet.write(row,18,val)
                


                if data_in['order_drop'] != []:
                    if indexes < od_dr:
                        for key,val in (data_in['order_drop'][indexes]).items(): 
                            if key == 'delivery_date_time':
                                
                                if val:
                                    datetime_object = datetime.strptime(val, '%Y-%m-%dT%H:%M:%SZ')
                                    old_format = '%Y-%m-%d %H:%M:%S'
                                    new_format = '%d/%m/%Y %I:%M %p'
                                    new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)
                                    worksheet.write(row,19,new_datetime_str)
                            if key == 'address':    
                                worksheet.write(row,20,val)
                            if key == 'receiver':
                                worksheet.write(row,21,val)


                # if data_in['comments'] != []:
                #     if indexes < com_count:
                #         for key,val in (data_in['comments'][indexes]).items(): 
                #             if key == 'comment':
                #                 worksheet.write(row,22,val)

                if data_in['extra_fields'] != []:
                    if indexes < ex_field:
                        for key,val in (data_in['extra_fields'][indexes]).items(): 
                            if key == "field_name":
                                worksheet.write(row,22,val)
                            if key == "field_value":
                                worksheet.write(row,23,val)

                # if data_in['order_attach'] != []:
                #     if indexes < att_count:
                #         for key,val in (data_in['order_attach'][indexes]).items(): 
                #             if key == "attachment_name":
                #                 worksheet.write(row,25,val)
                
                
                
                if len(assign_pick) != 0:
                    if indexes < len(assign_pick):
                        pick_data = assign_pick[indexes]
                        
                        if pick_data.OrderDriverID.AssetID:
                            ass_reg   = pick_data.OrderDriverID.AssetID.registeration_no
                            
                            worksheet.write(row+local_row,24,ass_reg)
                        driver_name             = pick_data.OrderDriverID.DriverID.name
                        
                        worksheet.write(row+local_row,25,driver_name)
                        if pick_data.pickup_date_time_to:
                            pickup_date_time_to     = pick_data.pickup_date_time_to
                            
                            t = pickup_date_time_to.strftime("%Y-%m-%dT%H:%M:%SZ")
                            datetime_object = datetime.strptime(t, '%Y-%m-%dT%H:%M:%SZ')
                            old_format = '%Y-%m-%d %H:%M:%S'
                            new_format = '%d/%m/%Y'
                            
                            new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)

                            worksheet.write(row+local_row,26,new_datetime_str)
                            new_format_time = '%I:%M %p'
                            new_time_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format_time)
                            worksheet.write(row+local_row,27,new_time_str)
                            
                        actual_pickup_date_time = pick_data.actual_pickup_date_time
                        
                        if actual_pickup_date_time:
                            t = actual_pickup_date_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                            datetime_object = datetime.strptime(t, '%Y-%m-%dT%H:%M:%SZ')
                            old_format = '%Y-%m-%d %H:%M:%S'
                            new_format = '%d/%m/%Y %I:%M %p'
                            new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)
                            # worksheet.write(row,22,new_datetime_str)
                        
                        address                 = pick_data.address
                        worksheet.write(row+local_row,28,address)
                        assigned_quantity       = pick_data.assigned_quantity
                        worksheet.write(row+local_row,29,assigned_quantity)


                        pic_ass_ = AssetRelation.objects.filter(PickID = pick_data)
                        if len(pic_ass_) != 0:
                            l = 0
                            for in_index in range(0,len(pic_ass_)):
                                
                                asp = pic_ass_[in_index]
                                if asp:
                                    worksheet.write(row+local_row+l, 30, asp.AssetID.asset_id)
                                    worksheet.write(row+local_row+l, 31, asp.AssetID.registeration_no)
                                    l += 1
                            local_row = local_row + l - 1

                if len(assign_drop) != 0:
                    if indexes < len(assign_drop):
                        drop_data = assign_drop[indexes]
                        
                        if drop_data.OrderDriverID.AssetID:
                            ass_reg   = drop_data.OrderDriverID.AssetID.registeration_no 
                            worksheet.write(row+d_local_row,32,ass_reg)
                        driver_name  = drop_data.OrderDriverID.DriverID.name
                        worksheet.write(row+d_local_row,33,driver_name)
                        if drop_data.delivery_date_time_to:
                            delivery_date_time_to   = drop_data.delivery_date_time_to
                            
                            t = delivery_date_time_to.strftime("%Y-%m-%dT%H:%M:%SZ")
                            datetime_object = datetime.strptime(t, '%Y-%m-%dT%H:%M:%SZ')
                            old_format = '%Y-%m-%d %H:%M:%S'
                            new_format = '%d/%m/%Y'
                            new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)
                            worksheet.write(row+d_local_row,34,new_datetime_str)
                            
                            new_format_time = '%I:%M %p'
                            new_time_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format_time)
                            worksheet.write(row+d_local_row,35,new_time_str)
                        actual_delivery_date_time = drop_data.actual_delivery_date_time
                        if actual_delivery_date_time:
                            t = actual_delivery_date_time.strftime("%Y-%m-%dT%H:%M:%SZ")
                            datetime_object = datetime.strptime(t, '%Y-%m-%dT%H:%M:%SZ')
                            old_format = '%Y-%m-%d %H:%M:%S'
                            new_format = '%d/%m/%Y %I:%M %p'
                            new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)
                        
                            # worksheet.write(row,28,new_datetime_str)
                        address                 = drop_data.address
                        worksheet.write(row+d_local_row,36,address)
                        assigned_quantity       = drop_data.assigned_quantity
                        worksheet.write(row+d_local_row,37,assigned_quantity)

                        

                        drp_ass_ = AssetRelation.objects.filter(DropID = drop_data)
                        if len(drp_ass_) != 0:
                            l = 0
                            for in_index in range(0,len(drp_ass_)):
                                
                                asp = drp_ass_[in_index]
                                if asp:
                                    worksheet.write(row+d_local_row+l, 38, asp.AssetID.asset_id)
                                    worksheet.write(row+d_local_row+l, 39, asp.AssetID.registeration_no)
                                    l += 1
                            d_local_row = d_local_row + l - 1
                row = row +1
            





























        # myFields = ['id','Created_by_Customer','Created_by_Carrier','CustomerCompany','CarrierCompany',
        # 'Status','customer_code','price','load_carried','load_type','load_weight','length_load_dimen',
        # 'width_load_dimen','height_load_dimen','measure_unit','units','hazardous','additional_note',
        # 'job_id','created_date_time','comments','order_attach','extra_fields',
        # 'order_pick','order_drop','order_history']
        # buffer = io.StringIO()   
        # writer = csv.DictWriter(buffer, fieldnames=myFields)
        # writer.writeheader()
        # for data in data:
        #     writer.writerow(data)
        # buffer.seek(0)
        # response = StreamingHttpResponse(buffer, content_type='text/csv')
        # response['Content-Disposition'] = 'attachment; filename=stockitems_misuper.csv'
        # return response


        workbook.close()
        output.seek(0)

        response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename=ordercreate.xlsx"

        output.close()

        return response
class CarrierOrderDetails(APIView):  
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            order_id = request.GET['order_id']
        except Exception as e:
            print("key error 'order_id'")
            context = {
                "data":"",
                "message":"key error 'order_id'"
            }
        order_obj = Order.objects.filter(id = int(order_id))
        obj = self.get_objects(order_id,content)
        serializ = CarrGetOrderDetails([obj], many =True)
        # print('serializ.data',serializ.data)
         
        return Response(serializ.data)

    def get_objects(self, order_obj,param):
        try:
            model1 = Order.objects.filter(id = order_obj, is_delete = False)
            model2 = OrderTemplate.objects.filter()
            model3 = OrderExtraField.objects.filter(OrderID = order_obj, is_delete = False)
            model4 = OrderComment.objects.filter(OrderID = order_obj, is_delete = False)
            model5 = OrderAttachment.objects.filter(OrderID = order_obj, is_delete = False)
    
            model6 = OrderDriver.objects.filter(OrderID = order_obj)

        
            model7 = Pick.objects.filter(OrderID = order_obj, is_delete = False)
            model8 = Drop.objects.filter(OrderID = order_obj, is_delete = False)
            model9 = Pick.objects.filter(OrderDriverID__in = model6, is_delete = False)
        
            model10 = Drop.objects.filter(OrderDriverID__in = model6, is_delete = False)
            model11 = OrderHistory.objects.filter(OrderID_id = order_obj, is_en_route = False, is_delete = False)
            model12 = AssetRelation.objects.filter(PickID_id__in = model9 , is_delete = False)
            model13 = AssetRelation.objects.filter(DropID_id__in = model10 , is_delete = False)

        except Exception as e:
            print('get_objects query error',e)
            context = {
                "data":"",
                "message":"Error: make sure order_id exist in database"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        models_obj = {'order': model1,'template':model2,'extra_fields':model3,'order_comment':model4,'order_attachment':model5,'progressbar':model6,'order_pick':model7,'order_drop':model8,'ass_pick_detail':model9,'ass_drop_detail':model10, 'order_his':model11, 'pick_asset':model12, 'drop_asset':model13}
        return models_obj


class OrderTracking(APIView):
    
    def get(self, request):
        try:
            order_id = request.GET['order_id']
        except Exception as e:
            print("key error 'order_id'")
            context = {
                "data":"",
                "message":"key error 'order_id'"
            }
        order_obj = Order.objects.filter(id = int(order_id))
        obj = self.get_objects(order_id) 
        serializ = CarrGetOrderDetails([obj], many =True)

        od_obj = OrderDriver.objects.filter(OrderID_id = int(order_id)).values_list('DriverID')
        dr_id = []
        for s in od_obj:
            for q in s:
                if q:
                    dr_id.append(q)
        
        Dr_obj = DriverExtraInfo.objects.filter(id__in = dr_id).values_list('trackingId')
        
        here_list = []
        for i in Dr_obj:
            for a in i:
                if a:
                    here_list.append(a)
        
        

    
        payload = {
        "email": HERE_MAP_USER,
        "password": HERE_MAP_KEY
            }
        headers = {
        "Content-Type" : "application/json"
            }
        here_url = "https://tracking.api.here.com/users/v2/login"
        
        r = requests.post(here_url, data=json.dumps(payload), headers=headers)    
        resp = r.json()

        acc_token = resp['accessToken']

        bat_headers = {
            "Content-Type" : "application/json",
            "authorization" : "Bearer " + acc_token
            }

        bat_url = "https://tracking.api.here.com/shadows/v2/batch"

        get_mark = requests.post(bat_url, data=json.dumps(here_list), headers=bat_headers)
        
        res_dt = get_mark.json()
        context = {
            "data":serializ.data,
            "tracking_data":res_dt
        }
        return Response(context,status=status.HTTP_200_OK)

    def get_objects(self, order_obj):
        try:
            model1 = Order.objects.filter(id = order_obj, is_delete = False)
            model2 = OrderTemplate.objects.filter()
            model3 = OrderExtraField.objects.filter(OrderID = order_obj, is_delete = False)
            model4 = OrderComment.objects.filter(OrderID = order_obj, is_delete = False)
            model5 = OrderAttachment.objects.filter(OrderID = order_obj, is_delete = False)
    
            model6 = OrderDriver.objects.filter(OrderID = order_obj)

        
            model7 = Pick.objects.filter(OrderID = order_obj, is_delete = False)
            model8 = Drop.objects.filter(OrderID = order_obj, is_delete = False)
            model9 = Pick.objects.filter(OrderDriverID__in = model6, is_delete = False)
        
            model10 = Drop.objects.filter(OrderDriverID__in = model6, is_delete = False)
            model11 = OrderHistory.objects.filter(OrderID_id = order_obj, is_en_route = False, is_delete = False)
            model12 = AssetRelation.objects.filter(PickID_id__in = model9 , is_delete = False)
            model13 = AssetRelation.objects.filter(DropID_id__in = model10 , is_delete = False)

        except Exception as e:
            print('get_objects query error',e)
            context = {
                "data":"",
                "message":"Error: make sure order_id exist in database"
            }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        models_obj = {'order': model1,'template':model2,'extra_fields':model3,'order_comment':model4,'order_attachment':model5,'progressbar':model6,'order_pick':model7,'order_drop':model8,'ass_pick_detail':model9,'ass_drop_detail':model10, 'order_his':model11, 'pick_asset':model12, 'drop_asset':model13}
        return models_obj





class DriverChangeOrderStatus(APIView): 
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        en_route_time = request.data['en_route_time']
    
        order_id = int(request.data['order_id'])
        carr_id = int(request.data['carr_id'])
        final_delivery_date_time = request.data['final_delivery_date_time']
        ord_obj1 = Order.objects.filter(id = order_id).first()
        cust_com_id = ord_obj1.CustomerCompanyID.id
        cust_com_user = CompanyUser.objects.filter(CompanyID_id = cust_com_id).first()
        cust_ex_id = cust_com_user.Created_by_CustomerID.id
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id") 
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            order_id = int(request.data['order_id'])
            order_obj_job_id = Order.objects.filter(id = order_id).values_list('job_id')[0][0]
            stat_id = ''
            driver_obj = DriverExtraInfo.objects.filter(id = int(content[0][0])).values_list('name')
        

            orderdrivobj_enro = OrderDriver.objects.filter(OrderID_id = order_id, order_enroute__isnull = False)

            stat_in_proc_id = Status.objects.filter(status_name = 'in process').values_list('id')[0][0]

            if orderdrivobj_enro.count() == 0:
                    
                order_in_proc_obj = Order.objects.filter(id = order_id).update(StatusID = stat_in_proc_id)


            


            
            if request.GET['en_route_or_pod'] == "en_route":

                stat_id = Status.objects.filter(status_name = 'in process').values_list('id')[0][0]
                orddriv_st_chng = OrderDriver.objects.filter(DriverID_id = int(content[0][0]), OrderID_id = order_id).update(order_status_id = int(stat_id))
                
            if request.GET['en_route_or_pod'] == "pod":

                stat_id = Status.objects.filter(status_name = 'delivered').values_list('id')[0][0]

                orddriv_st_chng = OrderDriver.objects.filter(DriverID_id = int(content[0][0]), OrderID_id = int(order_id)).update(order_status_id = int(stat_id))

                
                comment_completed = 'Congratulation! you have completed job (%s)'%(order_obj_job_id)
                orderhist_driv = OrderHistory.objects.create(Created_by_DriverID_id = int(content[0][0]), comments = str(comment_completed))
                # orderhist_driv = OrderHistory.objects.create(Created_by_CustomerID_id = int(content[0][0]), comments = str(comment_completed))  
                notify_obj = Notification.objects.create(OrderID_id = int(order_id) , created_for_driver_id = int(content[0][0]), comments = str(comment_completed))


            if request.GET['en_route_or_pod'] == "en_route":

                order_driver_obj = OrderDriver.objects.filter(DriverID_id = int(content[0][0]), OrderID_id = int(order_id)).update(order_status_id = int(stat_id), order_enroute = en_route_time) 
            
            if request.GET['en_route_or_pod'] == "pod":
                
                order_driver_obj = OrderDriver.objects.filter(DriverID_id = int(content[0][0]), OrderID_id = int(order_id)).update(order_status_id = int(stat_id), order_final_deliver = final_delivery_date_time) 



            order_driver_ = OrderDriver.objects.filter(DriverID_id = int(content[0][0]), OrderID_id = int(order_id)).values_list('order_enroute','order_final_deliver')

            if request.GET['en_route_or_pod'] == "en_route":
                

                date = str(order_driver_[0][0])
                date = date.split('+')
                datetime_object = datetime.strptime(date[0], '%Y-%m-%d %H:%M:%S')

                old_format = '%Y-%m-%d %H:%M:%S'
                new_format = '%d/%m/%Y %I:%M %p'

                new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)

                
                
                
                
                ord_his3 = OrderHistory.objects.create(Created_by_CarrierID_id = carr_id, OrderID_id = order_id, comments ='%s in en-route by driver %s on %s'%(order_obj_job_id, driver_obj[0][0],str(new_datetime_str) ))
                # ord_his4 = OrderHistory.objects.create(Created_by_CustomerID_id = carr_id, OrderID_id = order_id, comments ='%s in en-route by driver %s on %s'%(order_obj_job_id, driver_obj[0][0],str(new_datetime_str) ))
                notify_obj = Notification.objects.create(OrderID_id = int(order_id) ,created_for_carrier_id = carr_id, comments = str('%s in en-route by driver %s on %s'%(order_obj_job_id, driver_obj[0][0],str(new_datetime_str) )))
            if request.GET['en_route_or_pod'] == "pod":

                date = str(order_driver_[0][1])
                date = date.split('+')
                datetime_object = datetime.strptime(date[0], '%Y-%m-%d %H:%M:%S')

                old_format = '%Y-%m-%d %H:%M:%S'
                new_format = '%d/%m/%Y %I:%M %p'

                new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format)

                
                
                
                ord_his = OrderHistory.objects.create(Created_by_CarrierID_id = carr_id, OrderID_id = order_id, comments ='%s delivered Successfully on %s'%(order_obj_job_id,str(new_datetime_str)))
                # ord_his = OrderHistory.objects.create(Created_by_CustomerID_id = carr_id, OrderID_id = order_id, comments ='%s delivered Successfully on %s'%(order_obj_job_id,str(new_datetime_str)))
                notify_obj2 = Notification.objects.create(OrderID_id = int(order_id) ,created_for_customer_id = cust_ex_id, created_for_carrier_id = carr_id, comments = str('%s delivered Successfully on %s'%(order_obj_job_id,str(new_datetime_str))))






            orderdrivobj_fin_deliv = OrderDriver.objects.filter(OrderID_id = int(order_id), order_final_deliver__isnull=True)

            stat_deliv_id = Status.objects.filter(status_name = 'delivered').values_list('id')[0][0]


            if orderdrivobj_fin_deliv.count() == 0:

                order_deliv_obj = Order.objects.filter(id = int(order_id)).update(StatusID_id = int(stat_deliv_id))



            context = {
                "data":"",
                "message":"success"
            }
            return Response(context, status=status.HTTP_200_OK)
        
        except Exception as e:
            print('Exception',e)
            context = {
                            "data": '',
                            "message":'Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)



class CarrEditOrder(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id") 
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'token Error or make sure user has carrier company'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        data = request.data['arr_order'][0]
        print('edit order data',data)
        

        order_status = data['order_detail']['is_active']
        order_deta = ''
        if data['order_detail']:
            internal_ord_data = data['order_detail']

            order_detail_update = Order.objects.filter(id = internal_ord_data['order_id']).update(job_id = internal_ord_data['job_id'], customer_code = internal_ord_data['customer_code'], 
            CustomerCompanyID = internal_ord_data['CustomerCompanyID'], price = internal_ord_data['price'], load_carried = internal_ord_data['load_carried'], load_type = internal_ord_data['load_type'], 
            load_weight = internal_ord_data['load_weight'], length_load_dimen = internal_ord_data['length_load_dimen'], width_load_dimen = internal_ord_data['width_load_dimen'], height_load_dimen = internal_ord_data['height_load_dimen'], 
            measure_unit = internal_ord_data['measure_unit'], units = internal_ord_data['units'], hazardous = (str(internal_ord_data['hazardous'])).title(), additional_note = internal_ord_data['additional_note'], is_active = internal_ord_data['is_active']) 

            order_deta = Order.objects.filter(id = internal_ord_data['order_id']).first()

        if data['template']:
            internal_templ_data = data['template']

            internal_tem_obj = OrderTemplate.objects.filter(id = int(internal_templ_data['template_id'])).update(template_name = internal_templ_data['template_name'], is_active = order_status)


        if data['pickup']:
            for pick_data in data['pickup']:
                internal_pickup_data = pick_data

                if internal_pickup_data['pickup_id']:
                    try:
                        if (internal_pickup_data['sender'] != {}) and ("kid" in (internal_pickup_data['sender'])):
                            internal_pickup_obj = Pick.objects.filter(id = int(internal_pickup_data['pickup_id'])).update(address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                            latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status, sender_id = internal_pickup_data['sender']['kid'])
                        else:    
                            internal_pickup_obj = Pick.objects.filter(id = int(internal_pickup_data['pickup_id'])).update(address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                            latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status)
                    except TypeError as e:
                        internal_pickup_obj = Pick.objects.filter(id = int(internal_pickup_data['pickup_id'])).update(address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                        latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status)
                else:
                    try:
                        if (internal_pickup_data['sender'] != {}) and ("kid" in (internal_pickup_data['sender'])):
                            internal_pickup_obj = Pick.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                            latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status, sender_id = internal_pickup_data['sender']['kid'])
                        else:
                            internal_pickup_obj = Pick.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                            latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status)
                    except TypeError as e:
                        internal_pickup_obj = Pick.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_pickup_data['address'], longitude = internal_pickup_data['longitude'], 
                        latitude = internal_pickup_data['latitude'], pickup_date_time = internal_pickup_data['pickup_date_time'], pickup_date_time_to = internal_pickup_data['pickup_date_time_to'], is_active = order_status)
        
        if data['delivery']:
            for delivery_data in data['delivery']:

                internal_delivery_data = delivery_data

                if internal_delivery_data['delivery_id']:
                    try:
                        if (internal_delivery_data['receiver'] != {}) and ("kid" in (internal_delivery_data['receiver'])):
                            internal_delivery_obj  = Drop.objects.filter(id = int(internal_delivery_data['delivery_id'])).update(address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status, receiver_id = internal_delivery_data['receiver']['kid']) 
                        else:
                            internal_delivery_obj  = Drop.objects.filter(id = int(internal_delivery_data['delivery_id'])).update(address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status) 
                    except TypeError as e:
                        internal_delivery_obj  = Drop.objects.filter(id = int(internal_delivery_data['delivery_id'])).update(address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status) 
                else:
                    try:
                        if (internal_delivery_data['receiver'] != {}) and ("kid" in (internal_delivery_data['receiver'])):
                            internal_delivery_obj  = Drop.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status, receiver_id = internal_delivery_data['receiver']['kid'])
                        else:
                            internal_delivery_obj  = Drop.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status) 
                    except TypeError as e:
                        internal_delivery_obj  = Drop.objects.create(OrderID_id = int(data['order_detail']['order_id']), address = internal_delivery_data['address'], 
                            longitude = internal_delivery_data['longitude'], latitude = internal_delivery_data['latitude'], 
                            delivery_date_time = internal_delivery_data['delivery_date_time'], 
                            delivery_date_time_to = internal_delivery_data['delivery_date_time_to'], is_active = order_status) 
        new_driver = set()
        if data['assign_pick_driver']:
            for assign_pick_driver_data in data['assign_pick_driver']:
            
                in_assi_pck_dr_data = assign_pick_driver_data

                if in_assi_pck_dr_data['pick_id']:
                    try:
                        if (in_assi_pck_dr_data['sender'] != {}) and ("kid" in (in_assi_pck_dr_data['sender'])):
                            assi_pck_dr_obj     = Pick.objects.filter(id = int(in_assi_pck_dr_data['pick_id'])).update(address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                            latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], 
                            pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status, sender_id = in_assi_pck_dr_data['sender']['kid'])        
                        else:
                            assi_pck_dr_obj     = Pick.objects.filter(id = int(in_assi_pck_dr_data['pick_id'])).update(address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                            latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], 
                            pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status)
                    except TypeError as e:
                        assi_pck_dr_obj  = Pick.objects.filter(id = int(in_assi_pck_dr_data['pick_id'])).update(address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                            latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], 
                            pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status)
                    if in_assi_pck_dr_data['asset'] != []:
                        for data_in in in_assi_pck_dr_data['asset']:
                            if 'is_for' in data_in:
                                if data_in['is_for'] == 'add':
                                    asr_obj , created = AssetRelation.objects.get_or_create(CompanyID_id = order_deta.CarrierCompanyID.id, AssetID_id = data_in['aid'], PickID_id = int(in_assi_pck_dr_data['pick_id']))
                                    if asr_obj:
                                        AttachDetached.objects.create(AssetID_id = data_in['aid'], schedule_from = in_assi_pck_dr_data['pickup_date_time'], schedule_to = in_assi_pck_dr_data['pickup_date_time_to'])
                                if data_in['is_for'] == 'remove':
                                    asr_obj= AssetRelation.objects.get(CompanyID_id = order_deta.CarrierCompanyID.id,AssetID_id = data_in['aid'], PickID_id = int(in_assi_pck_dr_data['pick_id']))
                                    if asr_obj:
                                        if data_in['adid']:
                                            ad_obj = AttachDetached.objects.filter(id = data_in['adid']).first()
                                            ad_obj.delete()
                                        asr_obj.delete()
                else:
                    created_by_carr_id = int(in_assi_pck_dr_data['Created_by_CarrierID']['id'])
                    st = Status.objects.filter(status_name = "pending").first()
                    
                    ord_driv_obj, created = OrderDriver.objects.get_or_create(OrderID_id = int(data['order_detail']['order_id']), is_active = order_status, DriverID_id = int(in_assi_pck_dr_data['DriverID']), Created_by_CarrierID_id = int(created_by_carr_id), order_status = st, AssetID_id = int(in_assi_pck_dr_data["ass_truck_id"]))
                    print('ord_driv_obj',ord_driv_obj,created )
                    if created:
                        new_driver.add(ord_driv_obj)
                    assi_pck_dr_obj = ''
                    try:
                        if (in_assi_pck_dr_data['sender'] != {}) and ("kid" in (in_assi_pck_dr_data['sender'])):
                            assi_pck_dr_obj     = Pick.objects.create(OrderDriverID = ord_driv_obj, address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                            latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status, sender_id = in_assi_pck_dr_data['sender']['kid'])
                        else:
                            assi_pck_dr_obj     = Pick.objects.create(OrderDriverID = ord_driv_obj, address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                            latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status)
                    except TypeError as e:
                        assi_pck_dr_obj     = Pick.objects.create(OrderDriverID = ord_driv_obj, address = in_assi_pck_dr_data['address'], longitude = in_assi_pck_dr_data['longitude'], assigned_quantity = str(in_assi_pck_dr_data['assigned_quantity']),
                            latitude = in_assi_pck_dr_data['latitude'], pickup_date_time = in_assi_pck_dr_data['pickup_date_time'], pickup_date_time_to = in_assi_pck_dr_data['pickup_date_time_to'] , is_active = order_status)                          
                    if in_assi_pck_dr_data['asset'] != []:
                        for data_in in in_assi_pck_dr_data['asset']:
                            if 'is_for' in data_in:
                                if data_in['is_for'] == 'add':
                                    asr_obj , created = AssetRelation.objects.get_or_create(CompanyID_id = order_deta.CarrierCompanyID.id, AssetID_id = data_in['aid'], PickID_id = assi_pck_dr_obj.id)       
                                    if asr_obj:
                                        AttachDetached.objects.create(AssetID_id = data_in['aid'], schedule_from = in_assi_pck_dr_data['pickup_date_time'], schedule_to = in_assi_pck_dr_data['pickup_date_time_to'])
                                if data_in['is_for'] == 'remove':
                                    asr_obj= AssetRelation.objects.get(CompanyID_id = order_deta.CarrierCompanyID.id, AssetID_id = data_in['aid'], PickID_id = int(in_assi_pck_dr_data['pick_id']))
                                    if asr_obj:
                                        if data_in['adid']:
                                            ad_obj = AttachDetached.objects.filter(id = data_in['adid']).first()
                                            ad_obj.delete()
                                        asr_obj.delete()

                    status_obj = Status.objects.filter(status_name = "pending").values_list("id")
                    ord_obj = Order.objects.filter(id = int(data['order_detail']['order_id'])).update(StatusID_id = int(status_obj[0][0]))
        
        if data['assign_drop_driver']:
            for assign_drop_driver in data['assign_drop_driver']:

                in_assi_del_data = assign_drop_driver

                if in_assi_del_data['drop_id']:
                    try:
                        if (in_assi_del_data['receiver'] != {}) and ("kid" in (in_assi_del_data['receiver'])):
                            internal_assi_del_obj  = Drop.objects.filter(id = int(in_assi_del_data['drop_id'])).update(address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                            longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                            delivery_date_time = in_assi_del_data['delivery_date_time'], 
                            delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status, receiver_id = in_assi_del_data['receiver']['kid']) 
                        else:
                            internal_assi_del_obj  = Drop.objects.filter(id = int(in_assi_del_data['drop_id'])).update(address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                            longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                            delivery_date_time = in_assi_del_data['delivery_date_time'], 
                            delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status) 
                    except TypeError as e:
                        internal_assi_del_obj  = Drop.objects.filter(id = int(in_assi_del_data['drop_id'])).update(address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                            longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                            delivery_date_time = in_assi_del_data['delivery_date_time'], 
                            delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status) 
                    if in_assi_del_data['asset'] != []:
                        for data_in in in_assi_del_data['asset']:
                            if 'is_for' in data_in:
                                if data_in['is_for'] == 'add':
                                    asr_obj ,created = AssetRelation.objects.get_or_create(CompanyID_id = order_deta.CarrierCompanyID.id, AssetID_id = data_in['aid'], DropID_id = int(in_assi_del_data['drop_id']))            
                                    if asr_obj:
                                        AttachDetached.objects.create(AssetID_id = data_in['aid'], schedule_from = in_assi_del_data['delivery_date_time'], schedule_to = in_assi_del_data['delivery_date_time_to'])
                                if data_in['is_for'] == 'remove':
                                    asr_obj = AssetRelation.objects.get(CompanyID_id = order_deta.CarrierCompanyID.id, AssetID_id = data_in['aid'], DropID_id = int(in_assi_del_data['drop_id']))            
                                    if asr_obj:
                                        if data_in['adid']:
                                            ad_obj = AttachDetached.objects.filter(id = data_in['adid']).first()
                                            ad_obj.delete()
                                        asr_obj.delete()
                else:
                    created_by_carr_id = int(in_assi_del_data['Created_by_CarrierID']['id'])
                    std = Status.objects.filter(status_name = "pending").first()

                    ord_driv_obj, created = OrderDriver.objects.get_or_create(OrderID_id = int(data['order_detail']['order_id']), 
                    DriverID_id = int(in_assi_del_data['DriverID']), Created_by_CarrierID_id = int(created_by_carr_id),
                    is_active =order_status, order_status = std, AssetID_id = int(in_assi_del_data["ass_truck_id"]))
                    if created:
                        new_driver.add(ord_driv_obj)
                    internal_assi_del_obj = ''
                
                    try:
                        if (in_assi_del_data['receiver'] != {}) and ("kid" in (in_assi_del_data['receiver'])):
                            internal_assi_del_obj  = Drop.objects.create( OrderDriverID = ord_driv_obj, address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                            longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                            delivery_date_time = in_assi_del_data['delivery_date_time'], 
                            delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status, receiver_id = in_assi_del_data['receiver']['kid']) 
                        else:
                            internal_assi_del_obj  = Drop.objects.create( OrderDriverID = ord_driv_obj, address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                            longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                            delivery_date_time = in_assi_del_data['delivery_date_time'], 
                            delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status) 
                    except TypeError as e:
                        internal_assi_del_obj  = Drop.objects.create( OrderDriverID = ord_driv_obj, address = in_assi_del_data['address'],  assigned_quantity = str(in_assi_del_data['assigned_quantity']),
                            longitude = in_assi_del_data['longitude'], latitude = in_assi_del_data['latitude'], 
                            delivery_date_time = in_assi_del_data['delivery_date_time'], 
                            delivery_date_time_to = in_assi_del_data['delivery_date_time_to'], is_active = order_status) 

                    if in_assi_del_data['asset'] != []:
                        for data_in in in_assi_del_data['asset']:
                            if 'is_for' in data_in:
                                if data_in['is_for'] == 'add':
                                    asr_obj ,created = AssetRelation.objects.get_or_create(CompanyID_id = order_deta.CarrierCompanyID.id, AssetID_id = data_in['aid'], DropID_id = internal_assi_del_obj.id)            
                                    if asr_obj:
                                        AttachDetached.objects.create(AssetID_id = data_in['aid'], schedule_from = in_assi_del_data['delivery_date_time'], schedule_to = in_assi_del_data['delivery_date_time_to'])
                                if data_in['is_for'] == 'remove':
                                    asr_obj = AssetRelation.objects.get(CompanyID_id = order_deta.CarrierCompanyID.id, AssetID_id = data_in['aid'], DropID_id = internal_assi_del_obj.id)            
                                    if asr_obj:
                                        if data_in['adid']:
                                            ad_obj = AttachDetached.objects.filter(id = data_in['adid']).first()
                                            ad_obj.delete()
                                        asr_obj.delete()

                    status_obj = Status.objects.filter(status_name = "pending").values_list("id")
                    ord_obj = Order.objects.filter(id = int(data['order_detail']['order_id'])).update(StatusID_id = int(status_obj[0][0]))
        dt = str(data['date_time_of_create'])
        dt = dt.split('.')
        print('after split',dt)
        #2019-09-30T07:51:12.771Z
        datetime_object = datetime.strptime(dt[0], '%Y-%m-%dT%H:%M:%S')

        old_format = '%Y-%m-%d %H:%M:%S'
        new_format = '%d/%m/%Y %I:%M %p'
        
        new_datetime_str = datetime.strptime(str(datetime_object), old_format).strftime(new_format) 
        for od in new_driver:
            print('odddddddddddddddddddddddddddddddddd',od)
            driv_comment = str('%s assigned you a new job(%s) for %s'%(od.OrderID.CarrierCompanyID.name, od.OrderID.job_id, od.OrderID.CustomerCompanyID.name))
            notif_obj = Notification.objects.create(OrderID_id = od.OrderID.id, created_by_carrier_id = od.Created_by_CarrierID.id, created_for_driver_id = od.DriverID.id, comments = driv_comment,date_time_of_create = data['date_time_of_create'])
        if data['extra_detail']:
            for extra_detail_data in data['extra_detail']:

                in_extra_data = extra_detail_data
                if in_extra_data['id']:
                    in_extra_obj = OrderExtraField.objects.filter(id = int(in_extra_data['id'])).update(field_name = in_extra_data['field_name'], field_value = in_extra_data['field_value'], is_active = order_status)    
                else:
                    if data['template']:
                        in_extra_obj = OrderExtraField.objects.create(OrderID_id = int(data['order_detail']['order_id']), OrderTemplateID_id = int(internal_templ_data['template_id']), field_name = in_extra_data['field_name'], field_value = in_extra_data['field_value'], is_active = order_status)    
                    else:
                        in_extra_obj = OrderExtraField.objects.create(OrderID_id = int(data['order_detail']['order_id']), field_name = in_extra_data['field_name'], field_value = in_extra_data['field_value'], is_active = order_status)        
        
        if data['attachment']:
            
            for attac_data in data['attachment']:
                created_by_carr_id = int(attac_data['Created_by_CarrierID']['id'])
            
                ordAttach = OrderAttachment.objects.create(OrderID_id = int(data['order_detail']['order_id']), Created_by_CarrierID_id = int(created_by_carr_id) ,
            
                attachment_name = attac_data['attachment_name'], physical_path = BUCKET_URL+"orderattachment/"+str(attac_data['attachment_name']))
                
                
                image_str = attac_data['image_read']


                session = boto3.session.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
                    

                s3 = session.resource('s3')
                cloudFilename = "orderattachment/"+str(ordAttach.attachment_name)
                s3.Bucket(AWS_STORAGE_BUCKET_NAME).put_object(Key=cloudFilename, Body=image_str, ACL='public-read')
        
        context = {
            "data":"",
            "message":"order updated"
        }
        return Response(context, status=status.HTTP_200_OK)


class TrackingOrderEmailSend(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            try:
                key = request.auth
                content = Token.objects.filter(key = key).values_list("content_id","param")
            except Exception as e:
                print('key or token object error',e)
                context = {
                                "data": '',
                                "message":'Token Error'
                        }
                return Response(context,status=status.HTTP_400_BAD_REQUEST)
            
            if request.GET['dashboard'] == "carrier":
                send_email_id = request.data['send_email_id']
                order_tracking_key = request.data['order_tracking_key']
                carr_data = CarrierExtraInfo.objects.filter(id = int(content[0][1])).values_list('name','mobile_num') 
                carr_com_name = Company.objects.filter(id = int(content[0][0])).first()
                subject, from_email, to = 'order track from kargologic', settings.EMAIL_HOST_USER, send_email_id
                text_content = 'Welcome to KargoLogic a Logistic Plateform'
                url = order_tracking_key
                
                html_content = """<p>Your shipment is on its way, track every movement by clicking this link:</p><br><strong><a href="{0}">Click Here To Track Order</a></strong><br>
                <p>If you have any question about your shipment please get in touch with your carrier <strong>{1}</strong> on {2}</p>""".format(url,carr_com_name.name,carr_data[0][1])
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                context = {
                    "data":"",
                    "message":"success"
                }
                return Response(context, status=status.HTTP_200_OK)
            elif request.GET['dashboard'] == "customer":
                send_email_id = request.data['send_email_id']
                order_tracking_key = request.data['order_tracking_key']
                cust_data = CustomerExtraInfo.objects.filter(id = int(content[0][1])).values_list('name','mobile_num') 
                cust_com_name = Company.objects.filter(id = int(content[0][0])).first()
                subject, from_email, to = 'order track from kargologic', settings.EMAIL_HOST_USER, send_email_id
                text_content = 'Welcome to KargoLogic a Logistic Plateform'
                url = order_tracking_key
                
                html_content = """<p>Your shipment is on its way, track every movement by clicking this link:</p><br><strong><a href="{0}">Click Here To Track Order</a></strong><br>
                <p>If you have any question about your shipment please get in touch with your carrier <strong>{1}</strong> on {2}</p>""".format(url,cust_com_name.name,cust_data[0][1])
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                context = {
                    "data":"",
                    "message":"success"
                }
                return Response(context, status=status.HTTP_200_OK)

        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'Something went wroung'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)    

from CompanySenderReceiver.serializers import listComSenderReceiver
from CompanySenderReceiver.models import CompanySenderReceiver
class ComSenderReceiverList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'Token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)

        comsendrc_obj = CompanySenderReceiver.objects.filter(CompanyID_id = int(content[0][0]))
        comsenreceiver_serializer = listComSenderReceiver(comsendrc_obj , many = True)
        context = {
            "data":comsenreceiver_serializer.data,
            "message":"success"
        }
        return Response(context,status=status.HTTP_200_OK)
        

class OrderJobID(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try:
            key = request.auth
            content = Token.objects.filter(key = key).values_list("content_id","param")
        except Exception as e:
            print('key or token object error',e)
            context = {
                            "data": '',
                            "message":'Token Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        try:
            if request.GET['company'] == "carrier":
                ord_obj = (Order.objects.filter(Created_by_CarrierID_id = int(content[0][1]) , CarrierCompanyID_id = int(content[0][0])).order_by('-id')).first()
                print('ord_obj',ord_obj)
                if ord_obj:
                    job_id = ord_obj.job_id
                    print('job_id',job_id)
                    
                    new_job_id = int(job_id)+1
                    context = {
                        "data":new_job_id,
                        "message":"success"
                    }
                    return Response(context,status=status.HTTP_200_OK)
                else:
                    context = {
                        "data":"",
                        "message":"This user has no order yet"
                    }
                    return Response(context,status=status.HTTP_200_OK)
            
            elif request.GET['company'] == "customer":
                ord_obj = (Order.objects.filter( Created_by_CustomerID_id = int(content[0][1]) ,CustomerCompanyID_id = int(content[0][0])).order_by('-id')).first()
                print('ord_obj',ord_obj)
                if ord_obj:
                    job_id = ord_obj.job_id
                    print('job_id',job_id)
                    
                    new_job_id = int(job_id)+1
                    context = {
                        "data":new_job_id,
                        "message":"success"
                    }
                    return Response(context,status=status.HTTP_200_OK)
                else:
                    context = {
                        "data":"",
                        "message":"This user has no order yet"
                    }
                    return Response(context,status=status.HTTP_200_OK)

        except ValueError as f:
            print('Your last Job ID contains alphabets')
            context = {
                            "data": '',
                            "message":'Your last Job ID contains alphabets. Contact admin'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print('error',e)
            context = {
                            "data": '',
                            "message":'Error'
                    }
            return Response(context,status=status.HTTP_400_BAD_REQUEST)