from django.apps import AppConfig


class RenderIndexConfig(AppConfig):
    name = 'render_index'
