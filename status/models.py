from django.db import models
from domain.models import Domain

# Create your models here.
class Status(models.Model):
    domainID    = models.ForeignKey(Domain, on_delete = models.CASCADE, null=True,blank=True)
    status_name = models.CharField(max_length = 50, null=True,blank=True)
    created_DT  = models.DateTimeField(auto_now_add=True)
    updated_DT  = models.DateTimeField(auto_now=True)
    is_active   = models.BooleanField(default=True)
    is_delete                = models.BooleanField(default=False)

    def __str__(self):
        return '%s(%s)'%(self.status_name,self.id)
