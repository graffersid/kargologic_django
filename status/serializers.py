from rest_framework import serializers
from status.models import Status

from domain.serializers import DomainSerializer

class StatusSerializer(serializers.ModelSerializer):
    #domainID = DomainSerializer(required=False)
    class Meta:
        model  = Status
        fields = ('status_name',)

class CarrDrivStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Status
        fields = ('status_name',) 


class StatusOrderCarrSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ('status_name',)

class CarrDrivOrderDetailsStatus(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ('status_name',)                     


