from django.conf.urls import url
from user_role.views import *

urlpatterns = [
    url('email_check', Email_Verification.as_view(), name='email-check'),
    url('user_check', CheckToken.as_view(), name='user-check'),
]