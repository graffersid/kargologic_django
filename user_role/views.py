from django.shortcuts import render

# Create your views here.
from customers.models import CustomerExtraInfo
from carriers.models import CarrierExtraInfo
from drivers.models import DriverExtraInfo
from custom_users.models import User
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
def email_check(email):
    email = email.lower()
    carr_check     = CarrierExtraInfo.objects.filter(email = email).first()
    customer_check = CustomerExtraInfo.objects.filter(email = email).first()
    driver_check   = DriverExtraInfo.objects.filter(email = email).first()
    user_check     = User.objects.filter(email = email).first()
    msg = ""
    if carr_check:
        msg = "Carrier"
    if customer_check:
        msg = "Customer"
    if driver_check:
        msg = "Driver"
    
    context = {
        "data":"",
        "message":msg
    }
    return context


class Email_Verification(APIView):
    def post(self, request):
        email = request.data['email']
        res = email_check(email)
        return Response(res, status=status.HTTP_200_OK)
    



class CheckToken(APIView):
    def post(self, request):
        token = request.data['token']
        user_type = request.data['user_type']
        user_email = Token.objects.get(key = token).user.email
        email = user_email.lower()
        carr_check     = CarrierExtraInfo.objects.filter(email = email).first()
        customer_check = CustomerExtraInfo.objects.filter(email = email).first()
        driver_check   = DriverExtraInfo.objects.filter(email = email).first()
        user_check     = User.objects.filter(email = email).first()
        msg = ""
        if carr_check:
            msg = "Carrier"
        elif customer_check:
            msg = "Customer"
        else:
            context = {
                "data":"",
                "message":"Invalid Token",
                "status":False
            }
            return Response(context)
        
        if user_type == msg:
            context = {
                "data":"",
                "message":msg,
                "status":True
            }
            return Response(context)
        else:
            context = {
                "data":"",
                "message":msg,
                "status":False
            }
            return Response(context)